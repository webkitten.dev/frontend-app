const resolve = require('path').resolve;

module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: [
    'plugin:vue/strongly-recommended',
    'airbnb-base',
  ],
  plugins: ['vue'],
  rules: {
    semi: [2, 'always'],
    'max-len': [2, 120, { 'ignoreUrls': true, ignorePattern: 'src=|background', ignoreRegExpLiterals: true }],
    'vue/this-in-template': 2,
    'vue/order-in-components': [2, {
      'order': [
        'el',
        'name',
        'parent',
        'functional',
        ['delimiters', 'comments'],
        ['components', 'directives', 'filters'],
        'extends',
        'mixins',
        'inheritAttrs',
        'model',
        ['layout', 'loading', 'scrollToTop'],
        'watchQuery',
        'key',
        'middleware',
        'validate',
        'fetch',
        ['props', 'propsData'],
        'asyncData',
        'data',
        ['asyncComputed', 'computed'],
        'validators',
        'head',
        'watch',
        'LIFECYCLE_HOOKS',
        'methods',
        ['template', 'render'],
        'renderError',
      ],
    }],
    'comma-dangle': [2, 'always-multiline'],
    'no-console': 0,
    'no-plusplus': [2, { 'allowForLoopAfterthoughts': true }],
    'prefer-destructuring': 0,
    'no-shadow': [2, { allow: ['state', 'getters'] }],
    'no-underscore-dangle': [2, {
      allow: [
        '__dangerouslyDisableSanitizersByTagID', // vue-meta
        '__typename', // graphql
      ],
    }],
    'object-shorthand': [2, 'always', { avoidQuotes: false }], // без avoidQuotes: true ругается на валидаторы и вотчеры типа 'type.id'()
    'no-unused-expressions': [2, { allowShortCircuit: true }],
    'no-param-reassign': 0,
    'import/extensions': [2, 'never'],
    'import/prefer-default-export': 0,
    // 'import/order': 0,
  },
  settings: {
    // устанавливаются вместе с nuxt
    'import/core-modules': [
      'vue',
      'vuex',
      'vue-router',
      'webpack',
    ],
    'import/resolver': {
      webpack: {
        config: {
          resolve: {
            extensions: [
              '.js',
              '.ts',
              '.vue',
            ],
            alias: {
              '~/assets': resolve(__dirname, 'assets'),
              '~/backend': resolve(__dirname, 'backend'),
              '~/components': resolve(__dirname, 'components'),
              '~/layouts': resolve(__dirname, 'layouts'),
              '~/node_modules': resolve(__dirname, 'node_modules'),
            },
          },
        },
      },
    },
  },
};
