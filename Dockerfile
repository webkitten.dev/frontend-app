FROM node:10.15-alpine AS deps
WORKDIR /app
ARG NPM_REGISTRY_URI
RUN apk add --no-cache python make g++
COPY package.json ./package.json
COPY package-lock.json ./package-lock.json
RUN npm ci --registry=$NPM_REGISTRY_URI

FROM deps AS builder
WORKDIR /app
ARG NUXT_ENV
ARG STATIC_PUBLIC_PATH
ARG NPM_REGISTRY_URI
ENV NUXT_ENV=$NUXT_ENV
ENV STATIC_PUBLIC_PATH=$STATIC_PUBLIC_PATH
ENV NUXT_MODE build
ENV NODE_CONFIG_DIR /app/backend/config
ENV LOG_LEVEL info
COPY . .
RUN npm run build
RUN npm ci --registry=$NPM_REGISTRY_URI --only=production

FROM node:10.15-alpine
WORKDIR /app
RUN apk add --no-cache curl ca-certificates
ENV NUXT_ENV=$NUXT_ENV
ENV NUXT_MODE run
ENV NODE_CONFIG_DIR /app/backend/config
COPY --from=builder /app/static ./static
COPY --from=builder /app/config ./config
COPY --from=builder /app/assets ./assets
COPY --from=builder /app/modules ./modules
COPY --from=builder /app/plugins ./plugins
COPY --from=builder /app/backend ./backend
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/.nuxt ./.nuxt
COPY --from=builder /app/.nuxt/main/dist/client /opt/cdn/main
COPY --from=builder /app/.nuxt/helper/dist/client /opt/cdn/helper
# HEALTHCHECK --start-period=5s --interval=5s --timeout=5s --retries=3 CMD \
# curl --fail --silent http://localhost:3000/_backend/ping || exit 1

CMD ["npm", "run", "start"]
