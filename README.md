# GisAuto Frontend App

## Основные команды

``` bash
# стартовая инициализация после клонирования репозитория
$ docker/script/install.sh

# поднимает все необходимые контейнеры и запускает сборку
# висит после этого в фоне, поддерживая HMR для фронта
# spoiler: это просто обёртка над docker-compose с назначением
# env переменных, можно использовать аналогично
$ docker/app up

# перезапускает nginx
$ docker/nginx_reload
```

Вся работа с npm (install, update, etc.) только через докер.

## Дополнительно

``` bash
# линтинг фронта
$ docker/npm run eslint

# линтинг бэка
$ docker/npm run tslint

# юнит-тесты
$ docker/npm run unit-test

# генерация типов из graphql (https://git.gisauto.ru/gisauto/graphql-api-service)
$ docker/npm run graphql-codegen
```

## Документация
[Nuxt.js](https://github.com/nuxt/nuxt.js)  
[Gitlab CI/CD](https://docs.gitlab.com/ee/ci/)  
[Dockerfile](https://docs.docker.com/engine/reference/builder/)  


## Конфиги контейнеров
[Production](https://git.gisauto.ru/gisauto/frontend-app/blob/develop/.helm/values.yaml)  
[Dev](https://git.gisauto.ru/gisauto/frontend-app/blob/develop/.helm/values.dev.yaml)  
[Local](https://git.gisauto.ru/gisauto/frontend-app/blob/develop/docker/docker-compose.yml.dist)  

## Git Flow
[шпаргалка](https://danielkummer.github.io/git-flow-cheatsheet/index.ru_RU.html)

### Hotfix
* смотрим последний tag https://git.gisauto.ru/gisauto/frontend-app/tags
* плюсуем версию патча (1.50.4 -> 1.50.5)
* пишем в канал frontend: `@here start 1.50.5`
``` bash
git checkout develop
git pull
git checkout master
git pull
git flow hotfix start 1.50.5
```
* меняем код, коммитим
* **либо** завершаем хотфикс
```bash
# в ветке hotfix/1.50.5
git flow finish
git push origin master develop --tags
# в канал frontend: `@here finish 1.50.5`
```
* **либо** отправляем хотфикс на проверку
```bash
# в ветке hotfix/1.50.5
git flow publish
# в канал frontend: `@here publish 1.50.5`
```

* если была задача: в комментарий к задаче пишем версию хотфикса: "исправлено/сделано в frontend-app:1.50.5"

### Release
* смотрим последний тэг https://git.gisauto.ru/gisauto/frontend-app/tags
* плюсуем минорную версию (1.50.4 -> 1.51.0)
* пишем в канал frontend: `@here start 1.51.0`
``` bash
git checkout develop
git pull
git checkout master
git pull
git flow release start 1.51.0
git flow finish
git push origin master develop --tags
```
* пишем в канал frontend: `@here finish 1.51.0`
* чистим старые образы в [registry](https://git.gisauto.ru/gisauto/frontend-app/container_registry) (оставлять для отката предпоследний релиз).

При коммите в хотфикс/релиз надо поднимать версию пакета
в package.json и package-lock.json (в соответствии с версией фикса/релиза)
это гарантирует, что образ пересоберётся полностью
