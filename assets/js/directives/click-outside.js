export default {
  bind(el, { expression }, vnode) {
    el.clickOutsideEvent = (event) => {
      if (el !== event.target && !el.contains(event.target)) {
        if (event.type === 'mousedown') {
          vnode.context[expression](event);
        }
      }
    };
    document.body.addEventListener('mousedown', el.clickOutsideEvent);
  },

  unbind(el) {
    document.body.removeEventListener('mousedown', el.clickOutsideEvent);
  },
};
