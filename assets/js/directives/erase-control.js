import {
  has,
  set,
  get,
  isEmpty,
} from 'lodash-es';

export default {
  bind(el, binding, vnode) {
    el.addEventListener('click', (e) => {
      e.preventDefault();
      if (has(vnode.context, binding.expression)) {
        set(vnode.context, binding.expression, '');
      }
    });
  },

  update(el, binding, vnode) {
    if (has(vnode.context, binding.expression)) {
      const v = get(vnode.context, binding.expression, '');
      if (isEmpty(v)) {
        el.style.display = 'none';
      } else {
        el.style.display = 'block';
      }
    }
  },
};
