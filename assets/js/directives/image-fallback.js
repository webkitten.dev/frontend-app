export default {
  bind(el, binding) {
    const { value } = binding;
    const defaultLoading = 'http://de.4-traders.com/images/loading_100.gif';
    const defaultError = '/frontapp/no-photo.svg';
    const img = new Image();

    let loading = defaultLoading;
    let error = defaultError;
    const original = el.src;

    if (value instanceof Object) {
      loading = value.loading || defaultLoading;
      error = value.error || defaultError;
    }

    img.src = original;
    el.src = loading;

    img.onload = () => {
      el.src = original;
    };

    img.onerror = () => {
      el.src = error;
    };
  },
};
