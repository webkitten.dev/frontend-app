import { disableBodyScroll, enableBodyScroll } from '~/assets/js/vendor/body-scroll-lock';

const options = {
  reserveScrollBarGap: true,
  allowTouchMove: (el) => {
    while (el && el !== document.body) {
      if (el.getAttribute('body-scroll-lock-ignore') !== null) {
        return true;
      }
      el = el.parentNode;
    }
    return false;
  },
};

export default {
  inserted(el, binding) {
    if (binding.value) {
      disableBodyScroll(el, options);
    }
  },

  componentUpdated(el, binding) {
    // Сделали дополнительную проверку со binding.oldValue,
    // чтобы избежать добавления дубликатов модалки в looks: [],
    // так как в итоге это и ломало поведения данной директивы.
    // https://github.com/willmcpo/body-scroll-lock/blob/master/src/bodyScrollLock.js
    // метод disableBodyScroll()
    if (binding.value !== binding.oldValue && binding.value) {
      disableBodyScroll(el, options);
    }

    if (!binding.value) {
      enableBodyScroll(el, options);
    }
  },

  unbind(el) {
    enableBodyScroll(el, options);
  },
};
