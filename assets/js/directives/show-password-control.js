export default {
  bind(el, { value }) {
    el.addEventListener('click', (e) => {
      e.preventDefault();
      const input = document.getElementById(value);
      if (input.type === 'password') {
        input.setAttribute('type', 'text');
      } else {
        input.setAttribute('type', 'password');
      }
      input.focus();
    });
  },
};
