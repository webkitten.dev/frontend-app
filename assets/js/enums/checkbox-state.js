export default Object.freeze({
  CHECKED: 1,
  UNCHECKED: 0,
});
