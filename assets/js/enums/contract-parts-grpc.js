// TODO: выкосить к чертям, когда перейдём на graphql
export const MerchantType = Object.freeze({
  MERCHANT_ANY: 0,
  MERCHANT_PERSON: 1,
  MERCHANT_SHOP: 2,
});

export const Placement = Object.freeze({
  PLACEMENT_ANY: 0,
  PLACEMENT_FRONT: 1,
  PLACEMENT_REAR: 2,
  PLACEMENT_LEFT: 3,
  PLACEMENT_RIGHT: 4,
  PLACEMENT_TOP: 5,
  PLACEMENT_BOTTOM: 6,
});

export const ImagesPresence = Object.freeze({
  IMAGES_ANY: 0,
  IMAGES_PRESENT: 1,
  IMAGES_ABSENT: 2,
});

export const PartCondition = Object.freeze({
  CONDITION_ANY: 0,
  CONDITION_NEW: 1,
  CONDITION_USED: 2,
});

export const ItemStatus = Object.freeze({
  STATUS_ANY: 0,
  STATUS_ACTIVE: 1,
  STATUS_ARCHIVED: 2,
  STATUS_DELETED: 3,
});
