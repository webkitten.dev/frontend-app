export default Object.freeze({
  SMALL: 'small',
  MEDIUM: 'medium',
  BIG: 'big',
  EXTRA: 'extra',
});
