export default Object.freeze({
  CASH: 'Наличные',
  INTERNET: 'Через интернет',
  BANK: 'Через банк',
  VISA: 'Visa',
  MAESTRO: 'Maestro',
  MASTERCARD: 'MasterCard',
  MIR: 'Мир',
});
