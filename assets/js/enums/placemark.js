export default Object.freeze({
  OFFER: 'offer',
  SELLER: 'seller',
  SHOP: 'shop',
});
