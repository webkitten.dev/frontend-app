export default Object.freeze({
  PC: 'pc',
  EMAIL: 'email',
  HREF: 'href',
  FTP: 'ftp',
  S1: 's1',
  PIECE: 'piece',
});
