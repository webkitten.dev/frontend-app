export default Object.freeze({
  ORIGIN: 'origin',
  CONTRACT: 'contract',
  WHEELS: 'wheels',
  TYRES: 'tyres',
});
