export default Object.freeze({
  CHARACTERISTICS: 'characteristics',
  APPLICABILITY: 'applicability',
  ORIGIN: 'origin',
  ANALOG: 'analog',
});
