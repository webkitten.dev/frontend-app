import GridType from './ppn-grid-types';

export default Object.freeze({
  ORIGINALS: {
    name: GridType.INTERNAL_ORIGINAL,
    title: 'Оригиналы',
    tabShow: 'originals',
  },
  COUNTERPART: {
    name: GridType.INTERNAL_COUNTERPART,
    title: 'Аналоги, Заменители',
    tabShow: 'analogs',
  },
  SPECIFICATIONS: {
    name: '',
    title: 'Характе-ристики',
    tabShow: 'descriptions',
  },
  APPLICABILITY: {
    name: '',
    title: 'Приме-няемость',
    tabShow: 'applicability',
  },
});
