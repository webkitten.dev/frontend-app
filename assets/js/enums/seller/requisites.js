export default Object.freeze({
  NAME: 'Наименование',
  INN: 'ИНН',
  KPP: 'КПП',
  DIRECTOR_NAME: 'ФИО Директора',
  OGRN: 'ОГРН',
  FULL_NAME: 'Полное название',
});
