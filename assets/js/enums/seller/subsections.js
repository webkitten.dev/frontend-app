export default Object.freeze({
  ORIGIN: 'origin',
  CONTRACT: 'contract',
  RATING: 'rating',
  REQUISITES: 'requisites',
  BRANCHES: 'branches',
  FEEDBACK: 'feedback',
});
