export default Object.freeze({
  SKYPE: 58,
  ICQ: 59,
  TELEGRAM: 60,
  WHATSAPP: 161,
  VIBER: 162,
});
