export default Object.freeze({
  LIST: 'list',
  FILTERS: 'filters',
  POPULAR: 'popular',
});
