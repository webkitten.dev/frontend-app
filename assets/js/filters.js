import {
  capitalize as _capitalize,
  toUpper as _toUpper,
  toLower as _toLower,
} from 'lodash-es';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';

dayjs.extend(customParseFormat);

export const sellerPartName = (value) => {
  // https://gisauto.atlassian.net/browse/GA-3403
  value = (value || '').toString();
  value = value.replace(/[a-z]+/gi, match => match.toUpperCase());
  value = value.replace(/[а-яё]+/gi, match => match.toLowerCase());
  value = value.replace(/[а-яё]/, match => match.toUpperCase());
  return value;
};

export const specifyingSearchRequest = value => (value || '').toString().replace(/[^a-zа-яё0-9 ]/gi, '');

export const spacifyNumber = value => (value || '0').toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');

export const brandSlug = value => (value || '').toString().toLowerCase().replace(/[^a-zа-яё0-9]/g, '-');

export const vendorCodeSlug = value => (value || '').toString().toLowerCase().replace(/[^a-zа-яё0-9]/g, '');

export const dateFormat = (value, options = { nouns: false, time: false }) => {
  const getDateTimeFormatByOptions = (date, format) => dayjs(date).format(format);

  const getStringDataFormat = (date) => {
    const dateToCompare = dayjs(new Date(date));
    const diff = dayjs(new Date()).diff(dateToCompare, 'day');

    switch (diff) {
      case 0:
        return 'сегодня';
      case 1:
        return 'вчера';
      case 2:
        return '2 дня назад';
      case 3:
        return '3 дня назад';
      case 4:
        return '4 дня назад';
      default:
        return getDateTimeFormatByOptions(date, 'DD.MM.YYYY');
    }
  };

  if (/^\d+$/.test(value)) {
    value = Number(value) * 1000;
  }

  try {
    switch (true) {
      case (options.nouns && !options.time):
        return getStringDataFormat(value);

      case (options.time && !options.nouns):
        return getDateTimeFormatByOptions(value, 'DD.MM.YYYY HH:mm');

      case (options.time && options.nouns):
        return `${getStringDataFormat(value)}, `
          + `${getDateTimeFormatByOptions(value, 'HH:mm')}`;

      default:
        return getDateTimeFormatByOptions(value, 'DD.MM.YYYY');
    }
  } catch (e) {
    console.error(`invalid date ${value}`);
  }

  return null;
};

export const round = (inputValue, precision) => {
  const value = Number(inputValue);
  return precision ? value.toFixed(precision) : Math.round(value);
};

/**
 * Строка с заглавной буквы
 * @param value
 * @return {string}
 */
export const capitalize = value => _capitalize(value.toString());

/**
 * Строка в верхний регистр
 * @param value
 * @return {string}
 */
export const uppercase = value => _toUpper(value.toString());

/**
 * Строка в нижний регистр
 * @param value
 * @return {string}
 */
export const lowercase = value => _toLower(value.toString());

/**
 * Соеденить массив в строку с помощью клея
 * @param value
 * @return {*}
 */
export const join = (value, glue = ', ') => value.join(glue);

/**
 * Функция предназначена для склонения слов используемых вместе с числительными.
 * @param number
 * @param words
 * @return string
 */
export const transChoose = (number, words) => {
  const cases = [2, 0, 1, 1, 1, 2];
  return words[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
};

export const trimPhoneNumber = value => (value ? value.replace(/[^+0-9.]/g, '') : null);
