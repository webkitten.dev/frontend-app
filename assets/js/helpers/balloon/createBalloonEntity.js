import Placemark from '~/assets/js/enums/placemark';
import PlacemarkOffer from '~/assets/js/model/placemark/placemarkOffer';
import PlacemarkShop from '~/assets/js/model/placemark/placemarkShop';
import getBalloonContractParts from '~/assets/js/helpers/balloon/getBalloonContractParts';

export default (balloonType, entity) => {
  switch (balloonType) {
    case Placemark.OFFER:
      return new PlacemarkOffer(entity);
    case Placemark.SELLER:
      return getBalloonContractParts(entity);
    case Placemark.SHOP:
      return new PlacemarkShop(entity);
    default:
      return {};
  }
};
