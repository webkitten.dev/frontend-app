export default entity => ({
  price: entity.cost || 0,
  shopName: entity.shopData.shop_name || '',
  shopId: entity.shopId || null,
  organizationId: entity.organizationId || null,
  cityName: entity.shopData.city_name || '',
  street: entity.shopData.street || '',
  building: entity.shopData.building || '',
  office: '',
  sellerURL: `${entity.shopId}/${entity.organization.id}`,
  sellerQuantity: false,
});
