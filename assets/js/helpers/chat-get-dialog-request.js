// @todo отрефакторить к чертям, уродливо
export default function (type, elementId, params) {
  // TODO: Argument type {progress: boolean} is not assignable to parameter type AxiosRequestConfig | undefined
  // TODO: нужно разобраться в чем проблема?
  if (typeof params !== 'object') {
    params = {};
  }
  const requestParams = {
    elementId,
    ...params,
  };
  let url = null;
  switch (type) {
    case 'request':
      url = '/_chat/dialog/fromRequest';
      break;
    case 'order':
      url = '/_chat/dialog/fromShopOrder';
      break;
    case 'shop':
      url = '/_chat/dialog/fromShop';
      break;
    case 'contractPart':
      url = '/_chat/dialog/fromContractPart';
      break;
    default:
      return null;
  }
  return this.$axios.$post(url, requestParams, { progress: false });
}
