import { Placement } from '~/assets/js/mocks/table/contract-parts';

export function formattedCarYears(carYears) {
  const yearStart = carYears.min;
  const yearEnd = carYears.max;

  let result = null;
  if (yearStart && yearEnd) {
    if (yearStart === yearEnd) {
      result = [yearStart, 'года'];
    } else {
      result = [`${yearStart}-${yearEnd}`, 'годов'];
    }
  }
  return result.join(' ');
}

export function formattedPlacement(placementList) {
  return placementList.map(item => Placement[item]).join(', ');
}
