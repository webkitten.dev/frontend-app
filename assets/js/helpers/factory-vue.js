// eslint-disable-next-line import/no-extraneous-dependencies
import { mount, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import localVue from '~/assets/js/helpers/get-local-vue';

/**
 * Список всех аргументов для создания instance vue
 * https://vue-test-utils.vuejs.org/api/options.html#mounting-options
 * @param options.type { string } тип монтирования. Может быть 'mount' или 'shallowMount'
 */

const mountedTypes = {
  mount,
  shallowMount,
};

function create(options) {
  if (!options) {
    return false;
  }

  const {
    state,
    getters,
    mutations,
    actions,
    modules,
    propsData,
    type,
    mocks,
    context,
    slots,
    scopedSlots,
    stubs,
    attachToDocument,
    attrs,
    listeners,
    parentComponent,
    provide,
    sync,
  } = options;

  const store = new Vuex.Store({
    state,
    actions,
    mutations,
    getters,
    modules,
  });

  return mountedTypes[type](options.component, {
    store,
    localVue,
    context,
    slots,
    stubs,
    scopedSlots,
    propsData,
    mocks,
    attachToDocument,
    attrs,
    listeners,
    parentComponent,
    provide,
    sync,
  });
}

function getVueInstance(options) {
  if (!options) {
    return false;
  }

  return create(options);
}

export default getVueInstance;
