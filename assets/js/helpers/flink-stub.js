export default {
  name: 'FlinkStub',
  props: {
    href: {
      type: String,
      required: true,
    },
    domain: {
      type: String,
      required: false,
      default: '',
    },
    event: {
      type: [String, Array],
      default: 'click',
    },
  },
  render: function render(h) {
    return h('a', undefined, this.$slots.default);
  },
};
