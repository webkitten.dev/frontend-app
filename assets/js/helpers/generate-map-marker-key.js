export default function generateMapMarkerKey(entity) {
  const key = [];
  entity.item_id && key.push(entity.item_id);
  entity.shopId && key.push(entity.shopId);
  entity.is_wholesale && key.push(entity.is_wholessale);
  entity.id && key.push(entity.id);

  return key.join('');
}
