// eslint-disable-next-line import/no-extraneous-dependencies
import { createLocalVue, RouterLinkStub, config } from '@vue/test-utils';
import stubModal from 'semantic-ui-vue/dist/commonjs/modules/Modal/Modal';
import Vuex from 'vuex';
import VueRouter from 'vue-router';

import FlinkStub from '~/assets/js/helpers/flink-stub';
import VScrollLockDirective from '~/assets/js/directives/scroll-lock';
import DomMoveDirective from '~/assets/js/directives/dom-move';
import FLink from '~/components/blocks/link';
import VTooltip from 'v-tooltip';

import { spacifyNumber } from '~/assets/js/filters';

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(VueRouter);
localVue.directive('scroll-lock', VScrollLockDirective);
localVue.directive('dom-move', DomMoveDirective);
localVue.component('flink', FLink);

// TODO: Не понятно почему установка опций отрабатывает некорректно.
// VTooltip.options.disposeTimeout = 0;
// VTooltip.options.defaultClass = 'kit tooltip dark';
// VTooltip.options.popover.defaultBaseClass = 'ui popup';

localVue.use(VTooltip);

localVue.filter('spacifyNumber', spacifyNumber);

// @todo перенести стабы в тесты
config.stubs['nuxt-link'] = RouterLinkStub;
config.stubs['sui-modal'] = stubModal;
config.stubs.flink = FlinkStub;

config.mocks = {
  $ym: () => {},
  $ga: () => {},
  $stat: () => {},
  $statDo: () => {},
};

export default localVue;
