import Placemark from '~/assets/js/enums/placemark';
import PlacemarkShop from '~/assets/js/model/placemark/placemarkShop';
import getHintSearchByNumber from '~/assets/js/helpers/hint/getHintSearchByNumber';
import getHintContractParts from '~/assets/js/helpers/hint/getHintContractParts';

export default (balloonType, entity) => {
  switch (balloonType) {
    case Placemark.OFFER:
      return getHintSearchByNumber(entity);
    case Placemark.SELLER:
      return getHintContractParts(entity);
    case Placemark.SHOP:
      return new PlacemarkShop(entity);
    default:
      return {};
  }
};
