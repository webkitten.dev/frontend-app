// TODO: ИМХО можем сделать потом класс HintSearchByNumber
export default entity => ({
  price: entity.price || 0,
  oldPrice: entity.old_price || 0,
  discountCheck: entity.discount_check || 0,
  inStock: entity.in_stock || '',
  quantity: entity.quantity || 0,
  deliveryString: entity.delivery_string || '',
  sellerQuantity: true,
});
