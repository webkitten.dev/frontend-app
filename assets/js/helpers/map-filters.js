export default function mapFilters(filters) {
  return filters.reduce((result, filter) => {
    result[filter] = function filterMapper(...args) {
      return this.$options.filters[filter](...args);
    };
    return result;
  }, {});
}
