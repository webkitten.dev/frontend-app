import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';

dayjs.extend(utc);
/*
За основу взято parseFromTimeZone из date-fns-timezone
Из пакета timezone-support было взято:
findTransitionIndex
getTransition
getUTCOffset
объект timeZone статичный. Так как пока нам не нужны другие time zone.
Объект типа timeZone возвращает метод findTimeZone из date-fns-timezone.
*/

// eslint-disable-next-line consistent-return
function findTransitionIndex(unixTime, timeZone) {
  const { untils } = timeZone;
  for (let i = 0, length = untils.length; i < length; ++i) {
    if (unixTime < untils[i]) {
      return i;
    }
  }
}

function getTransition(unixTime, timeZone) {
  const transitionIndex = findTransitionIndex(unixTime, timeZone);
  const abbreviation = timeZone.abbreviations[transitionIndex];
  const offset = timeZone.offsets[transitionIndex];
  return { abbreviation, offset };
}

function getUTCOffset(date, timeZone) {
  const unixTime = typeof date === 'number' ? date : date.getTime();
  const { abbreviation, offset } = getTransition(unixTime, timeZone);
  return { abbreviation, offset };
}

function parseFromUTC(dateString) {
  const timeZone = {
    abbreviations: ['UTC'],
    name: 'Etc/UTC',
    offsets: [0],
    population: 0,
    untils: [Infinity],
  };

  const date = dayjs.utc(dateString).toDate();

  let { offset } = getUTCOffset(date, timeZone);

  offset -= date.getTimezoneOffset();

  return new Date(date.getTime() + offset * 60 * 1000);
}

export default parseFromUTC;
