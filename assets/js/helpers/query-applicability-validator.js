export default (inputBrand, inputModel, applicability) => {
  if (
    !inputBrand
    || !inputModel
    || !applicability
    || !applicability.length
  ) {
    return false;
  }

  inputBrand = inputBrand.toLowerCase();
  inputModel = inputModel
    .toLowerCase()
    .replace('+', ' ')
    .split(' ')[0];

  const matchedApplicability = applicability.find((item) => {
    const brand = item.brand.toLowerCase();
    const model = item.model_name.toLowerCase().split(' ')[0];
    if (brand === inputBrand && model === inputModel) {
      return true;
    }
    return false;
  });

  if (matchedApplicability) {
    return true;
  }
  return false;
};
