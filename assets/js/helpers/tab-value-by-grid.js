import Tabs from '~/assets/js/enums/search-by-number-tabs';

/**
  * returns selected proppert from Tabs object
  * @param value - value of searchable prop
  * @param searchProp - name of searchable prop
  * @param returnProp - name of requested prop
*/

export default function findTabValue(value, searchProp, returnProp) {
  const tabOject = Object.values(Tabs).find(tabItem => tabItem[searchProp] === value);

  if (tabOject && returnProp) {
    return tabOject[returnProp];
  }

  return Tabs.ORIGINALS[returnProp];
}
