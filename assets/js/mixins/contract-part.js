import { createNamespacedHelpers } from 'vuex';

const STORE_USER = createNamespacedHelpers('user');

export default {
  computed: {
    ...STORE_USER.mapState(['auth']),
  },
  methods: {
    isSameUser(part) {
      if (!this.auth) {
        return false;
      }
      if (this.auth.isOrganization && part.shopId) {
        return this.auth.organization
          && this.auth.organization.legals
          && this.auth.organization.legals.some(
            legal => legal.shops.some(shop => Number(shop.id) === Number(part.shopId))
          );
      }
      if (!this.auth.isOrganization && part.userId) {
        return Number(this.auth.id) === Number(part.userId);
      }
      return false;
    },
    getCondition(part) {
      let res = '';
      if (part.condition) {
        res = part.condition === 1 ? 'Новая' : 'Б/у';
      }
      return res;
    },
    getLocation(part) {
      const result = [];
      part.geo && result.push(part.geo.name);
      part.metro && result.push(`м. ${part.metro.name}`);
      return result.join(', ');
    },
  },
};
