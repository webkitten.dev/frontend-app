export default {
  beforeMount() {
    this.isMobile();
  },
  methods: {
    isMobile() {
      if (!process.client) {
        return false;
      }
      return window.innerWidth < 1024;
    },
  },
};
