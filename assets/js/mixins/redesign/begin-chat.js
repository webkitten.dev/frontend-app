import Vue from 'vue';
import { createNamespacedHelpers } from 'vuex';
import SimpleVueValidation from 'simple-vue-validator';

import { REFETCH_USER } from '~/assets/js/store/user/action-types';
import {
  SHOW_AUTH_MODAL,
} from '~/assets/js/store/actionManager/mutation-types';

const STORE_USER = createNamespacedHelpers('user');
const STORE_CHAT = createNamespacedHelpers('chat');
const STORE_ACTION_MANAGER = createNamespacedHelpers('actionManager');

SimpleVueValidation.extendTemplates({
  required: 'Поле обязательно для заполнения',
  maxLength: 'Максимальное количество символов: {0}',
  email: 'Некорректный формат e-mail',
});
const SimpleVueValidator = SimpleVueValidation.Validator;

export default Vue.util.mergeOptions(SimpleVueValidation.mixin, {
  data() {
    return {
      name: null,
      email: null,
      phone: null,
      loading: false,
      issetErrors: {
        email: null,
        phone: null,
      },
      inputErrors: {
        phone: false,
      },
      apiError: false,
    };
  },
  computed: {
    ...STORE_CHAT.mapState({
      chatLoaded: 'opened',
    }),
    isVisible: {
      get() {
        return this.shown;
      },
      set(newVal) {
        if (newVal) {
          this.show();
        } else {
          this.hide();
        }
      },
    },
    submitDisabled() {
      return this.validation.errors.length > 0 || this.inputErrors.phone || !this.name;
    },
  },
  validators: {
    name(value) {
      return SimpleVueValidator.value(value).required().maxLength(40);
    },
    email(value) {
      return SimpleVueValidator.value(value).email();
    },
  },
  watch: {
    chatLoaded(newValue) {
      if (newValue) {
        this.isVisible = false;
      }
    },
    isVisible(newValue) {
      if (!newValue) {
        this.loading = false;
        this.name = null;
        this.email = null;
        this.phone = null;
        this.issetErrors = {
          email: null,
          phone: null,
        };
        this.inputErrors = {
          phone: false,
        };
        this.validation.reset();
      }
    },
    email() {
      this.issetErrors.email = null;
    },
    phone() {
      this.issetErrors.phone = null;
    },
  },
  methods: {
    ...STORE_USER.mapActions({
      fetchUser: REFETCH_USER,
    }),
    ...STORE_ACTION_MANAGER.mapMutations({
      showAuthModal: SHOW_AUTH_MODAL,
    }),
    async send() {
      this.apiError = false;
      const isValid = await this.$validate();
      if (!isValid || this.inputErrors.phone) {
        return false;
      }

      this.loading = true;
      try {
        if (this.email) {
          await this.sendUser();
        } else {
          await this.sendGuest();
        }
        await this.fetchUser();
      } catch (e) {
        switch (e.response.status) {
          case 401:
            if (e.response.data.error.errorType === 'phone exists') {
              this.issetErrors.phone = e.response.data.message;
            } else if (e.response.data.error.errorType === 'email exists') {
              this.issetErrors.email = e.response.data.message;
            } else {
              this.apiError = true;
              console.error(e);
            }
            break;
          default:
            this.apiError = true;
            console.error(e);
        }
        this.loading = false;
      }

      return true;
    },
    async sendGuest() {
      await this.$axios.$post('/_main/user/createGuest', {
        firstName: this.name,
        phone: this.phone,
      });
    },
    async sendUser() {
      await this.$axios.$post('/_main/user/createChatUser', {
        firstName: this.name,
        username: this.email,
        phone: this.phone,
      });
    },
  },
});
