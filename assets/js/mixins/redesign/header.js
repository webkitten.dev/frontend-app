import {
  createNamespacedHelpers,
  mapActions,
  mapState,
} from 'vuex';
import {
  SHOW_AUTH_MODAL,
  SHOW_LOCATION_MODAL,
  SHOW_HEADER_SUBMENU,
  HIDE_HEADER_SUBMENU,
  TOGGLE_SIDEBAR,
} from '~/assets/js/store/actionManager/mutation-types';
import { DISABLE_AUTO_DETECT_LOCATION } from '~/assets/js/store/root/action-types';

// TODO если logout в другом месте(сайдбар), убрать
import { LOGOUT } from '~/assets/js/store/user/action-types';

import isMobileMixin from '~/assets/js/mixins/is-mobile';
import Sections from '~/assets/js/mocks/sections/index';
import Currency from '~/assets/js/mocks/currency';
import AppSearch from '~/components/redesign/app-search/app-search';
import AppPopover from '~/components/redesign/app-tooltip/app-popover';
import AppButton from '~/components/redesign/app-buttons/app-button';
import AppSelectCurrency from '~/components/redesign/app-select/app-select-currency';
import AppCart from '~/components/redesign/app-cart/app-cart';
import PartsCatalogs from '~/components/redesign/layout/header/parts-catalogs';
import AddPriceButton from '~/components/redesign/app-buttons/app-button-add-price';
import AppUserActions from '~/components/redesign/app-user/app-user-actions';
import ContractFilters from '~/components/redesign/filter-groups/contract-filters';

const TYPES_SECTION = {
  checkAuto: 'check-auto',
  helper: 'helper',
};

const STORE_USER = createNamespacedHelpers('user');
const STORE_ACTION_MANAGER = createNamespacedHelpers('actionManager');

export default {
  mixins: [isMobileMixin],
  components: {
    AppSearch,
    AppButton,
    AppCart,
    PartsCatalogs,
    AppSelectCurrency,
    AppPopover,
    AddPriceButton,
    AppUserActions,
    ContractFilters,
  },
  props: {},
  data() {
    return {
      cart: {
        count: 32,
        amount: 32055,
      },
      tooltipLocationpOptions: {
        trigger: 'manual',
        open: true,
        offset: 17,
        additionalClasses: ['tooltipLocation', 'tooltipDark'],
      },
      hasOpenMenuSearch: false,
      hasOpenMenuFilters: false,
      hasModal: false,
      scrollable: false,
    };
  },
  computed: {
    ...mapState(['location', 'navigation', 'domain']),
    ...STORE_USER.mapState(['auth', 'counters']),
    ...STORE_ACTION_MANAGER.mapState({
      headerSubMenuShow: 'headerSubMenuShow',
      hasOpenMenu: 'sidebarShow',
    }),
    sections() {
      if (this.navigation === TYPES_SECTION.helper) {
        return Sections.helperSections();
      }

      if (this.navigation === TYPES_SECTION.checkAuto) {
        return Sections.checkAutoSections();
      }

      return Sections.partsSections();
    },
    currency() {
      return Currency();
    },
    locationAutoDetected() {
      return this.location.autoDetected;
    },
    locationSelected() {
      return this.location && this.location.id;
    },
    isHeaderSubmenuOpened: {
      get() {
        return this.headerSubMenuShow;
      },
      set(newVal) {
        if (newVal) {
          this.showHeaderSubmenu();
        } else {
          this.hideHeaderSubmenu();
        }
      },
    },
  },
  methods: {
    ...mapActions({
      disableAutoDetectLocation: DISABLE_AUTO_DETECT_LOCATION,
    }),
    ...STORE_ACTION_MANAGER.mapMutations({
      showAuthModal: SHOW_AUTH_MODAL,
      showLocationModal: SHOW_LOCATION_MODAL,
      showHeaderSubmenu: SHOW_HEADER_SUBMENU,
      hideHeaderSubmenu: HIDE_HEADER_SUBMENU,
      openMenu: TOGGLE_SIDEBAR,
    }),
    ...STORE_USER.mapActions({
      logout: LOGOUT,
    }),
    openMenuFixBody() {
      this.openMenu();
      if (this.isMobile() && this.hasOpenMenu) {
        this.scrollable = true;
      } else {
        this.scrollable = false;
      }
    },
    disableFixBody() {
      this.scrollable = false;
    },
    login() {
      this.showAuthModal();
    },
    openSearch() {
      this.hasOpenMenuSearch = !this.hasOpenMenuSearch;
    },
    openFilters() {
      this.hasOpenMenuFilters = !this.hasOpenMenuFilters;
    },
    isSparePartsCatalogs(section) {
      if (!section || !section.href) {
        return false;
      }

      return section.href === '/catalog';
    },
    isPartsCategories(section) {
      if (!section || !section.href) {
        return false;
      }

      return section.href === '/categories';
    },
    onHideCityNotFoundTooltip() {
      this.disableAutoDetectLocation();
      this.$ym('city_not_found');
    },
    isActive(section) {
      if (!section) {
        return false;
      }
      return (section.href.startsWith('/faq/') && this.$route.path.startsWith('/faq/'))
        || (section.href.startsWith('/tyres') && this.$route.path.startsWith('/wheels'))
        || (section.href.startsWith('/catalog') && this.$route.path.startsWith('/categories'));
    },
    isActiveClassSection(section) {
      return (section.href === '/' || section.href === '/check-auto') ? '' : '-active';
    },
    onClickOutsideHeaderSubmenu() {
      if (!this.isHeaderSubmenuOpened) {
        return;
      }
      this.isHeaderSubmenuOpened = false;
    },
    toggleHeaderSubMenuShow(section) {
      if (this.isSparePartsCatalogs(section)) {
        this.isHeaderSubmenuOpened = !this.isHeaderSubmenuOpened;
      }
    },
  },
};
