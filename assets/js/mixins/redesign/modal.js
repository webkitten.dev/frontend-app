import AppModal from '~/components/redesign/app-modals/app-modal';

export default {
  components: {
    AppModal,
  },
  model: {
    prop: 'show',
    event: 'toggle',
  },
  props: {
    show: {
      type: Boolean,
      default: false,
    },
  },
  methods: {
    toggle(value) {
      this.$emit('toggle', value);
    },
    close() {
      this.toggle(false);
    },
  },
};
