import {
  isEmpty,
  pickBy,
  mapValues,
  map,
} from 'lodash-es';

import OrderDirections from '~/assets/js/enums/order-directions';
import DisplayStates from '~/assets/js/enums/tyres-wheels-display-states';
import FiltersGroups from '~/assets/js/enums/tyres-wheels-filters-groups';

export default (routeName, apiUrls) => ({
  data() {
    const displayCookieName = `${routeName}Display`;
    let display = this.$cookies.get(displayCookieName);
    const filtersMobileScrollable = display === DisplayStates.FILTERS;
    const tagsMobileScrollable = display === DisplayStates.POPULAR;
    if (!display || !Object.values(DisplayStates).includes(display)) {
      display = DisplayStates.LIST;
    }

    const filtersGroupCookieName = `${routeName}FiltersGroup`;
    let filtersGroup = this.$cookies.get(filtersGroupCookieName);
    if (!filtersGroup || !Object.values(FiltersGroups).includes(filtersGroup)) {
      filtersGroup = FiltersGroups.PARAMS;
    }

    return {
      routeName,
      apiUrls,
      displayCookieName,
      display,
      filtersGroupCookieName,
      filtersGroup,
      loading: false,
      filtersMobileScrollable,
      tagsMobileScrollable,
      offset: 0,
    };
  },
  computed: {
    grid() {
      return {
        data: this.data,
        count: this.count,
        order: this.order,
        offset: this.offset,
        countShown: this.lazyLoadEnabled ? this.limit + this.offset : this.count,
      };
    },
    tags() {
      return this.tagsOrigins.map(tag => ({
        ...tag,
        href: this.$router.resolve({
          name: this.routeName,
          params: mapValues(
            pickBy(tag.filters, item => item !== null),
            (value, key) => `${this.$options.RouteParamsPrefixMapping[key]}_${value}`
          ),
        }).href,
      }));
    },
    apiCriteria() {
      return mapValues(
        pickBy(this.filtersModel, item => item !== null),
        item => item.id,
      );
    },
    apiRequestParams() {
      return {
        criteria: this.apiCriteria,
        limit: this.limit,
        offset: this.offset,
        order: this.order,
      };
    },
    lazyLoadEnabled() {
      return !isEmpty(this.apiCriteria)
        && this.count > this.limit && (this.limit + this.offset) < this.count;
    },
    unboundFiltersModel() {
      return Object.assign({}, this.filtersModel);
    },
    sorting() {
      return this.$options.headings
        .filter(entry => entry.sorting.has)
        .reduce((acc, entry) => acc.concat(
          {
            id: `${OrderDirections.ASC}${entry.sorting.criteria}`,
            criteria: entry.sorting.criteria,
            titleMobile: entry.title,
            direction: OrderDirections.ASC,
          },
          {
            id: `${OrderDirections.DESC}${entry.sorting.criteria}`,
            criteria: entry.sorting.criteria,
            titleMobile: entry.title,
            direction: OrderDirections.DESC,
          }
        ), []);
    },
    countSelectedFilters() {
      return map(this.apiCriteria).length;
    },
  },
  watch: {
    unboundFiltersModel: {
      async handler() {
        await this.loadNewData();
      },
      deep: true,
    },
    async order() {
      if (this.loading) {
        return false;
      }
      this.offset = 0;
      const { data } = { ...await this.fetchNewDataPart() };
      this.data = data;
      return true;
    },
  },
  created() {
    this.$options.displayStates = DisplayStates;
    this.$options.filtersGroups = FiltersGroups;
  },
  methods: {
    async fetchNewDataPart() {
      if (this.loading) {
        return false;
      }
      try {
        this.loading = true;
        const { data } = await this.$axios.$get(this.apiUrls.data, {
          params: this.apiRequestParams,
        });
        return { data };
      } catch (e) {
        console.error(e);
        this.$error({ statusCode: 500 });
      } finally {
        this.loading = false;
      }
      return {};
    },
    async showMore() {
      if (!this.lazyLoadEnabled) {
        return false;
      }
      this.offset += this.limit;
      const { data } = { ...await this.fetchNewDataPart() };
      this.data = this.data.concat(data);
      return true;
    },
    loadNewData() {
      this.loading = true;
      this.$router.push({
        name: this.routeName,
        params: mapValues(this.apiCriteria, (value, key) => `${this.$options.RouteParamsPrefixMapping[key]}_${value}`),
      });
    },
    resetFilters() {
      if (this.loading) {
        return false;
      }

      const route = {
        name: this.routeName,
      };
      const targetHref = this.$router.resolve(route).href;

      if (this.$route.fullPath === targetHref) {
        this.filtersModel = { ...this.$options.InitialFiltersModel };
        return true;
      }

      this.loading = true;
      this.$router.push(route);
      return true;
    },
    changeDisplay(target) {
      if (!Object.values(DisplayStates).includes(target)) {
        return false;
      }
      this.display = target;
      this.filtersMobileScrollable = target === DisplayStates.FILTERS;
      this.tagsMobileScrollable = target === DisplayStates.POPULAR;
      this.$cookies.set(this.displayCookieName, target, {
        domain: `.${this.$config.domains.base}`,
        path: '/',
        maxAge: 60 * 60 * 24 * 365 * 10,
      });
      return true;
    },
    changeFilterGroup(target) {
      if (!Object.values(FiltersGroups).includes(target)) {
        return false;
      }
      this.filtersGroup = target;
      this.$cookies.set(this.filtersGroupCookieName, target, {
        domain: `.${this.$config.domains.base}`,
        path: '/',
        maxAge: 60 * 60 * 24 * 365 * 10,
      });
      return true;
    },
    onOrderChange(payload) {
      this.order = payload;
    },
  },
});
