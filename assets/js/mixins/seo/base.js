import { has, get, isObject } from 'lodash-es';

export default {
  head() {
    const result = {
      meta: [],
      link: [],
    };

    if (this.seoRobots) {
      result.meta.push({ hid: 'robots', name: 'robots', content: this.seoRobots });
    }

    let seoOgTitle = '';
    if (this.seoTitle) {
      result.title = this.seoTitle;
      seoOgTitle = this.seoTitle;
    }

    if (this.seoOgTitle) {
      seoOgTitle = this.seoOgTitle;
    }

    if (seoOgTitle) {
      result.meta.push({ hid: 'og:title', name: 'og:title', content: seoOgTitle });
    }

    if (this.seoDescription) {
      result.meta.push({ hid: 'description', name: 'description', content: this.seoDescription });
      this.seoOgDescription = this.seoDescription;
    }

    if (this.seoOgDescription) {
      result.meta.push({ hid: 'og:description', name: 'og:description', content: this.seoOgDescription });
    }


    if (this.seoOgImage) {
      let url = isObject(this.seoOgImage) ? get(this.seoOgImage, 'url', '') : this.seoOgImage;
      if (url) {
        if (!url.match(/https?:\/\//g)) {
          url = `https://${this.$config.domains.main}${url}`;
        }
        result.meta.push({ hid: 'og:image', name: 'og:image', content: url });

        if (isObject(this.seoOgImage)) {
          const width = get(this.seoOgImage, 'width', '');
          const height = get(this.seoOgImage, 'height', '');

          width && result.meta.push({ hid: 'og:image:width', name: 'og:image:width', content: width });
          height && result.meta.push({ hid: 'og:image:width', name: 'og:image:width', content: width });
        }
      }
    }

    if (this.seoOgUrl) {
      result.meta.push({
        hid: 'og:url',
        name: 'og:url',
        content: `https://${this.$store.state.domain}${this.seoOgUrl}`,
      });
    } else {
      result.meta.push({
        hid: 'og:url',
        name: 'og:url',
        content: `https://${this.$store.state.domain}${this.$route.fullPath}`,
      });
    }

    if (this.seoCanonical) {
      result.link.push({
        hid: 'canonical',
        rel: 'canonical',
        href: `https://${this.$store.state.domain}${this.seoCanonical}`,
      });
    }

    if (this.seoScripts && this.seoScripts.length) {
      result.script = this.seoScripts;
      result.__dangerouslyDisableSanitizersByTagID = this.seoScripts.reduce((accumulator, value) => {
        if (
          has(value, 'hid')
          && has(value, 'innerHTML')
        ) {
          return {
            ...accumulator,
            [value.hid]: 'innerHTML',
          };
        }
        return accumulator;
      }, {});
    }

    return result;
  },
};
