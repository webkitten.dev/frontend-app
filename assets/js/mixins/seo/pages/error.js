export default {
  computed: {
    seoTitle() {
      if (this.error.statusCode === 404) {
        return '404 Not Found';
      }
      return '';
    },
    seoDescription() {
      if (this.error.statusCode === 404) {
        return 'Запрашиваемая Вами страница не найдена. Воспользуйтесь нашим быстрым '
        + 'поиском запчастей или ссылками на другие страницы - обязательно что-нибудь '
        + 'найдете, у нас много предложений';
      }
      return '';
    },
  },
};
