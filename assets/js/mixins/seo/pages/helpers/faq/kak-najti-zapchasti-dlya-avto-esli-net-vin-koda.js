export default {
  data() {
    return {
      seoTitle: 'Как найти запчасти для авто, если нет VIN кода',
      seoDescription: 'Если не известен VIN код, но нужно найти оригинальную или не '
        + 'оригинальную запчасть на автомобиль, то не все потеряно. Мы расскажем о 4 '
        + 'действующих способах, которые помогут Вам выйти из ситуации и найти подходящую запчасть. '
        + 'Автоконсьерж GisAuto поможет вам найти запчасти, '
        + 'записать в ближайшее СТО и купить хороший автомобиль.',
    };
  },
};
