export default {
  data() {
    return {
      seoTitle: 'Ставим авто на учет через МФЦ (что нужно сделать, какие документы нужны)',
      seoDescription: 'Можно ли поставить авто на учет в ГИБДД через МФЦ, '
        + 'что нужно сделать для этого и куда идти, какие документы нужны для этой процедуры,'
        + ' подробнее о «флагманском МФЦ» и о том, что нужно делать по прибытию в МФЦ.',
    };
  },
};
