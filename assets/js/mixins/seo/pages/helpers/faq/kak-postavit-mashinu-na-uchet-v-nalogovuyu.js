export default {
  data() {
    return {
      seoTitle: 'Ставим авто на учет в налоговую (документы и способы постановки)',
      seoDescription: 'Какие документы нужны для постановки авто на учет в налоговую, варианты постановки на учет: '
        + 'личный кабинет налогоплательщика, отправка по почте, личный визит в ФНС по месту регистрации.',
    };
  },
};
