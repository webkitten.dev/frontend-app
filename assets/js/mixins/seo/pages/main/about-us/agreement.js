export default {
  data() {
    return {
      seoTitle: 'Соглашение на обработку персональных данных',
      seoDescription: 'О проекте GisAuto. Об обработке персональных данных пользователей сайта ГисАвто: какие данные '
          + 'пользователя передаются на обработку, обязательства GisAuto в отношении персональных данных, о защите '
          + 'персональных данных и способах отзыва своего согласия пользователем.',
    };
  },
};
