export default {
  data() {
    return {
      seoTitle: 'Условия публичной оферты для сервиса Консьерж',
      seoDescription: 'О проекте GisAuto. Публичная оферта для сервиса Консьерж от ГисАвто: определения и термины, '
          + 'предмет Договора, порядок и условия оказания Консультаций, права и обязанности Сторон, стоимость '
          + 'Консультаций, срок действия Договора и гарантии.',
    };
  },
};
