export default {
  data() {
    return {
      seoTitle: 'Контактные данные',
      seoDescription: 'О проекте GisAuto. Способы обратной связи с ГисАвто: бесплатный номер телефона (по России), '
          + 'форма обратной связи и email.',
    };
  },
};
