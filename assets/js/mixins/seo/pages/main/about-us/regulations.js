export default {
  data() {
    return {
      seoTitle: 'Правила пользования порталом',
      seoDescription: 'О проекте GisAuto. Правила выставления оценок '
          + 'и написания отзывов для пользователей портала ГисАвто.',
    };
  },
};
