export default {
  data() {
    return {
      seoTitle: 'Реквизиты ООО «ГисАвто»',
      seoDescription: 'О проекте GisAuto. Реквизиты компании '
          + 'ООО «ГисАвто»: ИНН, КПП, ОГРН, юридический адрес, телефон и e-mail.',
    };
  },
};
