export default {
  computed: {
    seoTitle() {
      return this.title;
    },
    seoDescription() {
      return this.description;
    },
  },
};
