export default {
  data() {
    return {
      seoTitle: 'Заказать полную онлайн-проверку автомобиля по VIN-номеру',
      seoDescription: 'Сервис для онлайн-проверки автомобиля на ограничения, угон, залог, аварии, таможню, утилизацию, '
        + 'лизинг и других важных данных для автовладельца. Для проверки достаточно ввести VIN-номер и заказать отчет, '
        + 'он поможет узнать о владельцах и истории авто - особенно полезно при покупке поддержанного авто.',
    };
  },
};
