import { has } from 'lodash-es';

export default {
  computed: {
    seoTitle() {
      return `${this.metaTitle} | №${this.id}`;
    },
    seoDescription() {
      const seoDescription = [`Продам контрактную запчасть «${this.title}»`];
      if (
        has(this.carManufacturer, 'name')
        && has(this.carModel, 'name')
      ) {
        seoDescription.push(`для автомобиля ${this.carManufacturer.name} ${this.carModel.name}`);
      }
      seoDescription.push(`за ${this.cost} руб в ${this.geo.prepositionalName}.`);
      seoDescription.push(`Опубликовано ${this.dateFormat(this.actualAt.seconds)}, ${this.photoAvailability}.`);
      seoDescription.push(`Объявление №${this.id}`);
      return seoDescription.join(' ');
    },
    seoOgImage() {
      if (!this.images('extra').length) {
        return null;
      }

      return {
        url: this.images('extra')[0].url,
        width: 500,
        height: null,
      };
    },
    seoOgUrl() {
      return this.$route.path;
    },
    seoCanonical() {
      return this.$route.path;
    },
    seoScripts() {
      const name = () => {
        const result = [`${this.title}`];

        if (has(this.carManufacturer, 'name') && has(this.carModel, 'name')) {
          result.push(`для ${this.carManufacturer.name} ${this.carModel.name}`);
        }

        if (has(this.formattedCarYears, 'value')) {
          result.push(`${this.formattedCarYears.value} ${this.formattedCarYears.noun}`);
        }

        return result.join(' ');
      };

      const offerInfo = {
        '@context': 'https://schema.org',
        '@type': 'Product',
        name: name(),
        description: this.description ? this.description : this.seoDescription,
        offers: {
          '@type': 'Offer',
          availability: 'https://schema.org/InStock',
          price: this.cost ? this.round(this.cost) : '',
          priceCurrency: 'RUB',
          url: `https://${this.$store.state.domain}${this.seoCanonical}`,
          itemCondition: this.condition === 1 ? 'https://schema.org/NewCondition' : 'https://schema.org/UsedCondition',
          seller: {
            '@type': this.isPrivateSeller ? 'Person' : 'Organization',
            name: this.isPrivateSeller
              ? this.user.userProfile.firstName
              : this.shop.shopData.shop_name,
          },
        },
      };

      if (this.images().length) {
        offerInfo.image = this.images('extra').map(image => image.url);
      }

      if (this.characteristics && this.characteristics.vendorCode.trim()) {
        offerInfo.mpn = this.characteristics.vendorCode;
      }

      if (this.characteristics && this.characteristics.partManufacturer
          && this.characteristics.partManufacturer.brand) {
        offerInfo.brand = {
          '@type': 'Thing',
          name: this.characteristics.partManufacturer.brand,
        };
      }

      return [{ hid: 'product-info', type: 'application/ld+json', innerHTML: JSON.stringify(offerInfo) }];
    },
  },
};
