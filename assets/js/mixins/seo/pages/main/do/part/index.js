export default {
  computed: {
    seoTitle() {
      return this.title;
    },
    seoDescription() {
      const cityValue = this.getRouteParamValue('city');
      const carManufacturer = this.getRouteParamValue('carManufacturer');
      const carModel = this.getRouteParamValue('carModel');
      const city = cityValue ? `в ${cityValue.prepositionalName}` : 'во всех городах';

      if (!carManufacturer && !carModel) {
        return `Ищите где купить контрактные запчасти ${city}? Мы собрали все предложения контрактных запчастей от `
          + `продавцов ${city} и вы можете быстро и удобно выбрать нужную запчасть для своего автомобиля.`;
      }

      const seoDescription = ['Все предложения от продавцов контрактных запчастей для'];
      carManufacturer && seoDescription.push(carManufacturer.name);
      carModel && seoDescription.push(carModel.name);
      seoDescription.push(`${city}. Актуальные объявления, удобная сортировка и фильтрация объявлений в вашем городе.`);

      return seoDescription.join(' ');
    },
    seoCanonical() {
      return this.$route.path;
    },
  },
};
