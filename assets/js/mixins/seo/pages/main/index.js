export default {
  data() {
    return {
      seoTitle: 'GisAuto.ru - купить автозапчасти по выгодным ценам',
      seoDescription: 'Все продавцы на одном портале: купить или '
            + 'продать запчасти, каталоги запчастей, шины и диски, запрос по VIN, контрактные '
            + 'и новые запчасти от продавцов по всей России.',
      seoCanonical: this.$route.path,
      seoScripts: [{
        hid: 'main-schemaorg',
        type: 'application/ld+json',
        innerHTML: JSON.stringify({
          '@context': 'https://schema.org',
          '@type': 'WebSite',
          url: 'https://gisauto.ru/',
          potentialAction: {
            '@type': 'SearchAction',
            target: 'https://gisauto.ru/search/{search_term_string}',
            'query-input': 'required name=search_term_string',
          },
        }),
      }],
    };
  },
};
