import { capitalize } from '~/assets/js/filters';

export default {
  computed: {
    seoTitle() {
      switch (this.partInfo.type) {
        case 'tyre':
          return `${this.name}. Купить или заказать шины`;
        default: {
          return [
            `Купить ${this.partInfo.partVendorCode},`,
            this.partInfo.partManufacturer,
            this.partInfo.partManufacturer === 'MOBIS' ? '[Hyundai/KIA]' : null,
            `(${this.name})`,
          ].filter(Boolean).join(' ');
        }
      }
    },
    seoDescription() {
      switch (this.partInfo.type) {
        case 'tyre':
          return `${this.partInfo.partName} - купить или заказать шины по выгодным ценам у продавцов по всей России.`
            + 'Информация о продавцах: цены, отзывы, адреса магазинов и телефоны.';
        default: {
          const result = [];

          this.countOriginal && result.push(`предложений: ${this.countOriginal}`);
          // this.countCounterpart && result.push(`заменителей: ${this.countCounterpart}`);

          if (this.countOriginal === 1 && this.partInfo.minPrice) {
            result.push(`цена: ${this.partInfo.minPrice} руб.`);
          }

          if (this.countOriginal > 1 && this.partInfo.minPrice && this.partInfo.maxPrice) {
            result.push(`цена: от ${this.partInfo.minPrice} до ${this.partInfo.maxPrice} руб.`);
          }

          this.partInfo.counterparts.length && result.push('аналоги');
          this.partInfo.characteristics.length && result.push('характеристики');
          this.partInfo.images.length && result.push('фото');
          this.partInfo.applicability.length && result.push('применяемость');

          const offer = `${this.partInfo.partVendorCode} ${this.partInfo.partManufacturer}`;
          return `Цены и наличие ${offer} в автомагазинах. ${capitalize(result.join(', '))}`;
        }
      }
    },
    seoOgTitle() {
      return `${this.partInfo.partVendorCode}, ${this.partInfo.partManufacturer}`
       + ' - поиск выгодных цен и ближайшего продавца';
    },
    seoOgImage() {
      if (!this.partInfo.images.length) {
        return false;
      }
      return this.partInfo.images[0].src;
    },
    seoCanonical() {
      return this.$route.path;
    },
    seoScripts() {
      if (!this.partInfo.valid) {
        return false;
      }
      const productInfo = {
        '@context': 'https://schema.org/',
        '@type': 'Product',
        name: this.partInfo.partName,
        brand: {
          '@type': 'Thing',
          name: this.partInfo.partManufacturer,
        },
        mpn: this.partInfo.partVendorCode,
        offers: {
          '@type': 'AggregateOffer',
          priceCurrency: 'RUR',
          highPrice: Number(this.partInfo.maxPrice),
          lowPrice: Number(this.partInfo.minPrice),
          offerCount: Number(this.countOriginal),
        },
      };
      if (this.partInfo.images.length) {
        productInfo.image = `https://${this.$config.domains.main}${this.partInfo.images[0].src}`;
      }
      return [{ hid: 'product-info', type: 'application/ld+json', innerHTML: JSON.stringify(productInfo) }];
    },
  },
};
