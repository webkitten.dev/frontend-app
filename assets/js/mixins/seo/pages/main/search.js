export default {
  data() {
    const query = (this.$route.params.query || '').replace(/"/g, '&quot;');
    return {
      seoTitle: `${query}. Найдены запчасти для авто`,
      seoDescription: `Мы нашли несколько результатов по вашему запросу &laquo;${query}&raquo;. Выберите нужный `
        + 'товар или уточните свой запрос - мы обязательно найдем запчасти для вашего авто по лучшим ценам.',
    };
  },
};
