export default {
  computed: {
    seoTitle() {
      return `${this.shopData.shop_name}. Информация о продавце и отзывы`;
    },
    seoDescription() {
      return [
        `Персональная страница компании &laquo;${this.shopData.shop_name}&raquo;.`,
        'Отзывы, способы оплаты, контакты и местоположение на карте',
      ].join(' ');
    },
    seoCanonical() {
      return this.$route.path;
    },
    seoOgUrl() {
      return this.seoCanonical;
    },
    seoScripts() {
      const sellerInfo = {
        '@context': 'https://schema.org',
        '@type': 'AutomotiveBusiness',
        '@id': `https://${this.$store.state.domain}${this.seoCanonical}`,
        image: 'https://gisauto.ru/_nuxt/img/profile-sidebar-avatar-organization.svg',
        name: this.shopData.shop_name || '',
        address: {
          '@type': 'PostalAddress',
          streetAddress: `ул.${this.shopData.street || ''}, д. ${this.shopData.building || ''}`,
          addressLocality: this.shopData.city_name || '',
          addressCountry: 'RU',
        },
        // TODO: Как будет сделан график работы и будет выводиться в шаблоне. Необходимо будет добавить
        // openingHoursSpecification: [
        //   {
        //     '@type': 'OpeningHoursSpecification',
        //     dayOfWeek: [
        //       'Monday',
        //       'Tuesday',
        //       'Wednesday',
        //       'Thursday',
        //       'Friday',
        //     ],
        //     opens: '09:00',
        //     closes: '21:00',
        //   },
        // ],
      };

      if (this.site) {
        sellerInfo.url = this.site;
      }

      if (this.feedbacks.length) {
        sellerInfo.aggregateRating = {
          '@type': 'AggregateRating',
          ratingCount: this.feedbacks.length,
          ratingValue: parseInt(this.shopData.avg_rating, 10),
        };
      }

      return [{ hid: 'seller-info', type: 'application/ld+json', innerHTML: JSON.stringify(sellerInfo) }];
    },
  },
};
