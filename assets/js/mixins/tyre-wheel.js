import {
  find,
  values,
  isEmpty,
  pickBy,
  map,
  mapValues,
} from 'lodash-es';

export default {
  methods: {
    applyTag(tag) {
      this.filtersModel = mapValues(
        this.filtersModel,
        (model, key) => {
          const newValue = tag.filters[key];
          if (!newValue) {
            return model;
          }

          const newModel = find(this.filtersList[key], o => String(o.id) === String(newValue));
          if (!newModel) {
            return model;
          }

          return newModel;
        },
      );
    },
    toggleTableTile() {
      this.displayType = this.displayType === 'tile' ? 'table' : 'tile';
    },
    orderDirectionChange(direction) {
      if (direction === this.orderDirection) {
        return false;
      }
      this.orderDirection = direction;
      this.loadData();
      return true;
    },
    resetFilters() {
      if (isEmpty(this.apiCriteria)) {
        return false;
      }
      this.$refs.filters.reset();
      return true;
    },
    scrollUp() {
      window.scrollTo(0, 0);
    },
  },
  computed: {
    url() {
      return [
        this.baseUrl,
        ...map(this.apiCriteria, (item, key) => `${this.filterParamsPrefixMapping[key]}_${item}`),
      ].join('/');
    },
    tagsOriginsWithHrefs() {
      return this.tagsOrigins.map(tag => ({
        ...tag,
        href: [
          this.baseUrl,
          ...map(
            pickBy(tag.filters, item => Boolean(item)),
            (item, key) => `${this.filterParamsPrefixMapping[key]}_${item}`,
          ),
        ].join('/'),
      }));
    },
    apiRequestParams() {
      if (isEmpty(this.apiCriteria)) {
        // если нет фильтров - отдаём из кэша
        return {
          criteria: this.apiCriteria,
        };
      }
      return {
        criteria: this.apiCriteria,
        limit: this.limit,
        offset: this.offset,
        orderDirection: this.orderDirection,
      };
    },
    lazyLoadEnabled() {
      return values(this.filtersModel).some(x => x !== null) && !this.lazyLoadDisabledByNoResult && this.countLeft > 0;
    },
    unboundFiltersModel() {
      return Object.assign({}, this.filtersModel);
    },
  },
};
