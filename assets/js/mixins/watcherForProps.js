import { isObject, cloneDeep } from 'lodash-es';

export default (props, options) => {
  const result = {
    data() {
      return {
        isPropChanged: false,
      };
    },
    watch: {},
    computed: {},
  };

  props.forEach((prop) => {
    const propName = `unlinked_${prop}`;

    result.computed[propName] = function computed() {
      if (Array.isArray(this[prop]) || isObject(this[prop])) {
        return cloneDeep(this[prop]);
      }
      return this[prop];
    };

    result.watch[propName] = {
      handler() {
        this.isPropChanged = true;
      },
      deep: options.deep ? options.deep : false,
    };
  });

  return result;
};
