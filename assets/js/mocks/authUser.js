export default () => ({
  displayedName: 'gregoryk@xgs.ru',
  id: '20540',
  isCompleteProfile: true,
  isOrganization: true,
  isOrganizationWithShop: false,
  organization: {},
  organizationSended: false,
  userProfile: {
    city: {
      id: '569',
      name: 'Абаза',
      parent: {},
    },
    confirmedEmail: false,
    confirmedPhone: false,
    firstName: 'Gregory',
    lastName: '',
    middleName: '',
    phone: '+7 (913) 555-55-55',
  },
  username: 'gregoryk@xgs.ru',
});
