export default () => [{
  name: 'RUB (российские рубли)',
  code: 'RUB',
  id: 390,
},
{
  name: 'BYN (белорусские рубли)',
  code: 'BYN',
  id: 260,
},
{
  name: 'KZT (казахские теньге)',
  code: 'KZT',
  id: 1,
},
{
  name: 'UAH (украинская гривна)',
  code: 'UAH',
  id: 1964,
},
{
  name: 'CNY (китайский юань)',
  code: 'CNY',
  id: 1959,
},
{
  name: 'KRW (южно-корейская вона)',
  code: 'KRW',
  id: 1971,
},
{
  name: 'RUB (российские рубли)',
  code: 'RUB',
},
{
  name: 'BYN (белорусские рубли)',
  code: 'BYN',
},
{
  name: 'KZT (казахские теньге)',
  code: 'KZT',
},
{
  name: 'UAH (украинская гривна)',
  code: 'UAH',
},
{
  name: 'CNY (китайский юань)',
  code: 'CNY',
},
];
