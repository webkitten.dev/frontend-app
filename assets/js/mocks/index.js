import getAuthUser from './authUser';
import getLocation from './location';
import getFastOrder from './fastOrder';
import getParts from './parts';
import getListFooter from './listFooter';
import getListExample from './listExample';

export default {
  getAuthUser,
  getLocation,
  getFastOrder,
  getParts,
  getListFooter,
  getListExample,
};
