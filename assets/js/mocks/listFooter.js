export default () => ({
  listFooterNavigation: [
    {
      title: 'Покупателям',
      list: [
        {
          name: 'Как искать запчасти на сайте',
          href: '#',
        },
        {
          name: 'Что такое «контрактные запчасти»?',
          href: '#',
        },
        {
          name: 'Как сделать заказ',
          href: '#',
        },
        {
          name: 'Рейтинг продавцов',
          href: '#',
        },
        {
          name: 'Безопасная покупка',
          href: '#',
        },
        {
          name: 'Наши гарантии',
          href: '#',
        },
        {
          name: 'Частые вопросы',
          href: '#',
        },
      ],
    },
    {
      title: 'Продавцам',
      list: [
        {
          name: 'Как мы работаем',
          href: '#',
        },
        {
          name: 'Партнерская программа',
          href: '#',
        },
        {
          name: 'С чего начать работу',
          href: '#',
        },
        {
          name: 'Чем мы отличаемся от других',
          href: '#',
        },
        {
          name: 'Статисытика по регионам',
          href: '#',
        },
      ],
    },
    {
      title: 'Каталоги',
      list: [
        {
          name: 'Легковые',
          href: '#',
        },
        {
          name: 'Грузовые',
          href: '#',
        },
        {
          name: 'Мотоциклы',
          href: '#',
        },
        {
          name: 'Спецтехника',
          href: '#',
        },
      ],
    },
    {
      title: 'Услуги',
      list: [
        {
          name: 'Автоконсъерж',
          href: '#',
        },
        {
          name: 'Статистика для продавцов',
          href: '#',
        },
        {
          name: 'Аренда авто',
          href: '#',
        },
        {
          name: 'Поиск СТО',
          href: '#',
        },
        {
          name: 'Проверка авто',
          href: '#',
        },
      ],
    },
    {
      title: 'О нас',
      list: [
        {
          name: 'О проекте «ГисАвто»',
          href: '#',
        },
        {
          name: 'Наши возможности',
          href: '#',
        },
        {
          name: 'Контакты',
          href: '#',
        },
      ],
    },
  ],
  listFooterLinks: [
    {
      title: 'Пользовательское соглашение',
      href: '#',
    },
    {
      title: 'Политика конфиденциальности',
      href: '#',
    },
    {
      title: '© 2015–2019  GISAUTO',
    },
  ],
  listFooterSocial: [
    {
      name: 'vk',
      href: '#',
      svg: '~/assets/svg/social/footer-vk.svg',
    },
    {
      name: 'ok',
      href: '#',
      svg: '~/assets/svg/social/footer-ok.svg',
    },
    {
      name: 'instagram',
      href: '#',
      svg: '~/assets/svg/social/footer-inst.svg',
    },
    {
      name: 'telegram',
      href: '#',
      svg: '~/assets/svg/social/footer-telegram.svg',
    },
  ],
});
