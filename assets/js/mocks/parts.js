/*eslint-disable*/
export default () => {
  return {
    listCategories: [
      {
        "id": 1,
        "name": "Впускная система",
        "slug": "vpusknaya-sistema",
        "groupsList": [{
          "id": 10003,
          "name": "Запчасти" +
            " воздуховода",
          "slug": "zapchasti-vozduhovoda",
          "groupsList": [{
            "id": 25515464,
            "name": "Бачок резонатора фильтра" +
              " воздушного",
            "slug": "bachok-rezonatora-filtra-vozdushnogo",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 929,
              "total": 3,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 52,
              "total": 1,
              "slug": "afa",
              "brand": "AFA",
              "aliasesList": ["AFA"],
              "typesList": [2]
            }, {
              "id": 2353,
              "total": 1,
              "slug": "vaz",
              "brand": "VAZ",
              "aliasesList": ["АВТОВАЗ\" ОАО Г", "\"АВТОВАЗ\" ОАО Г. ТОЛЬЯТТИ", "LADA", "LADA (РОССИЯ)", "LADA IMAGE", "AVTOVAZ", "ВАЗ ЛАДА", "ВАЗ Г", "ВАЗ (САМАРА)", "ВАЗ (ОАО АВТОВАЗ Г", "ВАЗ", "VAZ", "ЛАДА", "АО ЛАДА-ИМИДЖ", "АО ВАЗ", "АО «ЛАДА ИМИДЖ»", "НИВА", "АВТОВАЗ", "ОАО АВТОВАЗ", "ОАО АВТОВАЗ (РЕМКОМПЛЕКТЫ)", "ОАО ВАЗ", "GM-AVTOVAZ", "АВТОВАЗ ОАО"],
              "typesList": [1]
            }]
          }, {
            "id": 26665398,
            "name": "Впускной патрубок воздушного фильтра",
            "slug": "vpusknoj-patrubok-vozdushnogo-filtra",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 1814,
              "total": 1,
              "slug": "porsche",
              "brand": "PORSCHE",
              "aliasesList": ["ПОРШЕ", "PORSCHE", "VW-PORSCHE"],
              "typesList": [1]
            }]
          }, {
            "id": 25093585,
            "name": "Датчик абсолютного давления",
            "slug": "datchik-absolyutnogo-davleniya_1",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 366,
              "total": 34,
              "slug": "bosch",
              "brand": "BOSCH",
              "aliasesList": ["BOSCH TRUCK", "BOSCH (ГЕРМАНИЯ)", "РОБЕРТБОШ", "ROBERT BOSCH", "BOSCH (OEM) GERMANY", "BOSH", "BOSCH DIAGNOSTICS", "BOSCH MOBA, ГЕРМАНИЯ", "BOSCH", "BOSCH BATTERY"],
              "typesList": [2]
            }, {
              "id": 4155,
              "total": 27,
              "slug": "startvolt",
              "brand": "СТАРТВОЛЬТ",
              "aliasesList": ["СТАРВОЛЬТ", "СТАРТВОЛЬТ"],
              "typesList": [2]
            }, {
              "id": 2514,
              "total": 24,
              "slug": "startvolt_1",
              "brand": "STARTVOLT",
              "aliasesList": ["STARTVOLT"],
              "typesList": [2]
            }, {
              "id": 2582,
              "total": 13,
              "slug": "zzvf",
              "brand": "ZZVF",
              "aliasesList": ["ZZVF (КИТАЙ)", "ZZVF"],
              "typesList": [2]
            }, {
              "id": 2134,
              "total": 11,
              "slug": "standard",
              "brand": "STANDARD",
              "aliasesList": ["STANDARD", "STANDARD MOTOR PRODUCTS", "STANDART"],
              "typesList": [2]
            }, {
              "id": 587,
              "total": 10,
              "slug": "delphi",
              "brand": "DELPHI",
              "aliasesList": ["DELPHI KOREA", "ﾃﾞﾙﾌｧｲ", "DELPHI", "DELPHI ELECTRICAL", "DELPHI DIESEL", "DELPHI [LOCKHEED AP]", "DELPHI (США)", "DELPHI (OEM) USA", "DELFI", "LOCKHEED (DELPHI)"],
              "typesList": [2]
            }, {
              "id": 498,
              "total": 8,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 16264,
              "total": 7,
              "slug": "doda",
              "brand": "DODA",
              "aliasesList": ["DODA"],
              "typesList": [2]
            }, {
              "id": 735,
              "total": 6,
              "slug": "era",
              "brand": "ERA",
              "aliasesList": ["ERA BENELUX", "ERA", "ERA ELETTRO"],
              "typesList": [2]
            }, {
              "id": 4086,
              "total": 6,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 2112,
              "total": 5,
              "slug": "spectra-premium",
              "brand": "SPECTRA PREMIUM",
              "aliasesList": ["SPECTRA", "SPECTRA PREMIUM"],
              "typesList": [2]
            }, {
              "id": 1487,
              "total": 4,
              "slug": "meat-doria",
              "brand": "MEAT & DORIA",
              "aliasesList": ["MEAT&amp;DORIA", "MEAT AND DORIA", "MEAT & DORIA"],
              "typesList": [2]
            }, {
              "id": 1550,
              "total": 4,
              "slug": "mopar-parts",
              "brand": "MOPAR PARTS",
              "aliasesList": ["MOPAR,КАНАДА", "MOPAR PARTS", "MOPAR"],
              "typesList": [2]
            }, {
              "id": 1976,
              "total": 4,
              "slug": "sat",
              "brand": "SAT",
              "aliasesList": ["SAT-TYG", "SATA", "SAT (КИТАЙ)", "SAT", "SAT/TYC", "SAT PREMIUM"],
              "typesList": [2]
            }, {
              "id": 863,
              "total": 3,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 1533,
              "total": 3,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1789,
              "total": 3,
              "slug": "pierburg",
              "brand": "PIERBURG",
              "aliasesList": ["PIERBURG", "Pierburg truck", "PIERBURG FRANCE"],
              "typesList": [2]
            }, {
              "id": 1916,
              "total": 3,
              "slug": "roers-parts",
              "brand": "ROERS PARTS",
              "aliasesList": ["ROERS PARTS"],
              "typesList": [2]
            }, {
              "id": 2168,
              "total": 3,
              "slug": "swag",
              "brand": "SWAG",
              "aliasesList": ["SWAG"],
              "typesList": [2]
            }, {
              "id": 490,
              "total": 2,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 543,
              "total": 2,
              "slug": "crown",
              "brand": "CROWN",
              "aliasesList": ["CROWN"],
              "typesList": [2]
            }, {
              "id": 559,
              "total": 2,
              "slug": "daewoo",
              "brand": "DAEWOO",
              "aliasesList": ["UZ-DAEWOO", "DAEWOO (КОРЕЯ)", "DAEWOO MOTOR", "DAEWOOPARTS, PMC", "DAEWOO"],
              "typesList": [1, 4]
            }, {
              "id": 783,
              "total": 2,
              "slug": "fae",
              "brand": "FAE",
              "aliasesList": ["FAE (ИСПАНИЯ)", "FAE"],
              "typesList": [2]
            }, {
              "id": 920,
              "total": 2,
              "slug": "gaz",
              "brand": "GAZ",
              "aliasesList": ["ГАЗ 3302", "ГАЗ 2705", "ГАЗ 52", "ГАЗ 2217", "ГАЗ", "ВОЛГА-ОЙЛ", "ВОЛГА", "ГАЗ 53", "ГАЗ 66", "ГАЗ ОАО", "ГАЗ ООО", "ГАЗЕЛЬ", "ВАЛДАЙ", "ГОРЬКОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД ОАО, Н.Н", "ОАО ГАЗ", "УМЗ Г.УЛЬЯНОВСК", "УМЗ Г", "VOLGA", "УМЗ", "GAZ", "ГАЗ 4301", "ГАЗ 3309", "ГАЗ 3308", "ГАЗ 3307", "OE GAZ"],
              "typesList": [1, 3]
            }, {
              "id": 1062,
              "total": 2,
              "slug": "herth-buss-elparts",
              "brand": "HERTH + BUSS ELPARTS",
              "aliasesList": ["H+B ELPARTS", "HERTH-BUSS", "HERTH + BUSS FAHRZEUGTEILE", "HERTH + BUSS ELPARTS"],
              "typesList": [2]
            }, {
              "id": 1083,
              "total": 2,
              "slug": "hoffer",
              "brand": "HOFFER",
              "aliasesList": ["HOFFER"],
              "typesList": [2]
            }, {
              "id": 1092,
              "total": 2,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 1325,
              "total": 2,
              "slug": "kroner",
              "brand": "KRONER",
              "aliasesList": ["KRONER (KRONERAUTO)", "KRONER"],
              "typesList": [2]
            }, {
              "id": 1382,
              "total": 2,
              "slug": "lifan",
              "brand": "LIFAN",
              "aliasesList": ["LIFAN", "ЛИФАН"],
              "typesList": [1]
            }, {
              "id": 1599,
              "total": 2,
              "slug": "mv-parts",
              "brand": "MV-PARTS",
              "aliasesList": ["MV-PARTS"],
              "typesList": [2]
            }, {
              "id": 1734,
              "total": 2,
              "slug": "ossca",
              "brand": "OSSCA",
              "aliasesList": ["OSSCA", "OSSKA"],
              "typesList": [2]
            }, {
              "id": 1940,
              "total": 2,
              "slug": "ruei",
              "brand": "RUEI",
              "aliasesList": ["RUEI"],
              "typesList": [2]
            }, {
              "id": 2354,
              "total": 2,
              "slug": "vdo",
              "brand": "VDO",
              "aliasesList": ["VDO", "SIEMENS VDO", "VDO SIEMENS", "SIEMENS"],
              "typesList": [2]
            }, {
              "id": 2410,
              "total": 2,
              "slug": "wells",
              "brand": "WELLS",
              "aliasesList": ["ADVANTECH", "WELLS"],
              "typesList": [2]
            }, {
              "id": 5286,
              "total": 2,
              "slug": "avtotrejd",
              "brand": "АВТОТРЕЙД",
              "aliasesList": ["АВТОТРЕЙД"],
              "typesList": [2]
            }, {
              "id": 24,
              "total": 1,
              "slug": "ac-delco",
              "brand": "AC DELCO",
              "aliasesList": ["ALDELCO", "ACDELCO"],
              "typesList": [2]
            }, {
              "id": 111,
              "total": 1,
              "slug": "allmakes",
              "brand": "ALLMAKES",
              "aliasesList": ["ALLMAKES", "ALLMAKES 4X4"],
              "typesList": [2]
            }, {
              "id": 209,
              "total": 1,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 211,
              "total": 1,
              "slug": "auger",
              "brand": "AUGER",
              "aliasesList": ["AUGER (ГЕРМАНИЯ)", "AUGER", "AUGER AUTOTECHNIK"],
              "typesList": [2]
            }, {
              "id": 347,
              "total": 1,
              "slug": "blue-print",
              "brand": "BLUE PRINT",
              "aliasesList": ["BLUE PRINT (ВЕЛИКОБРИТАНИЯ)", "BLUE PRINT", "ACL"],
              "typesList": [2]
            }, {
              "id": 586,
              "total": 1,
              "slug": "dello",
              "brand": "DELLO",
              "aliasesList": ["AUTOMEGA", "DELLO (ГЕРМАНИЯ)", "DELLO (AUTOMEGA)", "DELLO", "AUTOMEGADELLO"],
              "typesList": [2]
            }, {
              "id": 593,
              "total": 1,
              "slug": "denso",
              "brand": "DENSO",
              "aliasesList": ["ND", "DENSO EUROPE B.V", "DENSO", "DENSO PS", "NIPPONDENSO", "DENSO (ЯПОНИЯ)"],
              "typesList": [2]
            }, {
              "id": 629,
              "total": 1,
              "slug": "dominant",
              "brand": "DOMINANT",
              "aliasesList": ["DOMINANT"],
              "typesList": [2]
            }, {
              "id": 818,
              "total": 1,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 1027,
              "total": 1,
              "slug": "hans-pries",
              "brand": "HANS PRIES",
              "aliasesList": ["HANS PRIES (ГЕРМАНИЯ)", "HANS PRIES", "HANS-PRIS", "HANS PRIES (TOPRAN)", "HANS-PRAES", "TOPRAN"],
              "typesList": [2]
            }, {
              "id": 1180,
              "total": 1,
              "slug": "iveco",
              "brand": "IVECO",
              "aliasesList": ["IVECO", "OE IVECO", "IVECO OE", "IVECO-DAILY"],
              "typesList": [1]
            }, {
              "id": 1190,
              "total": 1,
              "slug": "japanparts",
              "brand": "JAPANPARTS",
              "aliasesList": ["JAPANPARTS (ИТАЛИЯ)", "JAPAN PARTS (К-Т 2ШТ.)", "JAPANPARTS", "JAPANSPARTS"],
              "typesList": [2]
            }, {
              "id": 1217,
              "total": 1,
              "slug": "jp-group",
              "brand": "JP GROUP",
              "aliasesList": ["JP GROUP (ДАНИЯ)", "JPG", "JP GROUPE", "JP GROUP", "GP GROUP", "JP"],
              "typesList": [2]
            }, {
              "id": 1778,
              "total": 1,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }, {
              "id": 1814,
              "total": 1,
              "slug": "porsche",
              "brand": "PORSCHE",
              "aliasesList": ["ПОРШЕ", "PORSCHE", "VW-PORSCHE"],
              "typesList": [1]
            }, {
              "id": 1863,
              "total": 1,
              "slug": "raon",
              "brand": "RAON",
              "aliasesList": ["RAON AUTO", "RAON"],
              "typesList": [2]
            }, {
              "id": 2287,
              "total": 1,
              "slug": "trucktec-automotive",
              "brand": "TRUCKTEC AUTOMOTIVE",
              "aliasesList": ["TRUCKTEC AUTOMOTIVE", "TRUCKTEC"],
              "typesList": [2]
            }, {
              "id": 2353,
              "total": 1,
              "slug": "vaz",
              "brand": "VAZ",
              "aliasesList": ["АВТОВАЗ\" ОАО Г", "\"АВТОВАЗ\" ОАО Г. ТОЛЬЯТТИ", "LADA", "LADA (РОССИЯ)", "LADA IMAGE", "AVTOVAZ", "ВАЗ ЛАДА", "ВАЗ Г", "ВАЗ (САМАРА)", "ВАЗ (ОАО АВТОВАЗ Г", "ВАЗ", "VAZ", "ЛАДА", "АО ЛАДА-ИМИДЖ", "АО ВАЗ", "АО «ЛАДА ИМИДЖ»", "НИВА", "АВТОВАЗ", "ОАО АВТОВАЗ", "ОАО АВТОВАЗ (РЕМКОМПЛЕКТЫ)", "ОАО ВАЗ", "GM-AVTOVAZ", "АВТОВАЗ ОАО"],
              "typesList": [1]
            }, {
              "id": 2358,
              "total": 1,
              "slug": "vemo-vaico",
              "brand": "VEMO (VAICO)",
              "aliasesList": ["VIEROL AG", "VIEROL", "VEMO", "VAICO/VEMO", "VAICO", "VEMO (VAICO)"],
              "typesList": [2]
            }, {
              "id": 2364,
              "total": 1,
              "slug": "avtoelektronika",
              "brand": "АВТОЭЛЕКТРОНИКА",
              "aliasesList": ["АВТОЭЛЕКТРОНИКА", "КЗЭИ", "ОАО АВТОЭЛЕКТРОНИКА"],
              "typesList": [2]
            }, {
              "id": 2387,
              "total": 1,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 4103,
              "total": 1,
              "slug": "sailing",
              "brand": "SAILING",
              "aliasesList": ["SAILING", "SAILING (КИТАЙ)"],
              "typesList": [2]
            }, {
              "id": 4792,
              "total": 1,
              "slug": "nsp",
              "brand": "NSP",
              "aliasesList": ["NSP"],
              "typesList": [2]
            }, {
              "id": 16208,
              "total": 1,
              "slug": "cartronic",
              "brand": "CARTRONIC",
              "aliasesList": ["CARTRONIC"],
              "typesList": [2]
            }, {
              "id": 16393,
              "total": 1,
              "slug": "modis",
              "brand": "MODIS",
              "aliasesList": ["MODIS"],
              "typesList": [2]
            }, {
              "id": 18082,
              "total": 1,
              "slug": "topcover",
              "brand": "TOPCOVER",
              "aliasesList": ["TOPCOVER"],
              "typesList": [2]
            }]
          }, {
            "id": 32201147,
            "name": "Дефлектор резонатора фильтра воздушного",
            "slug": "deflektor-rezonatora-filtra-vozdushnogo",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 4309,
              "total": 1,
              "slug": "junsite",
              "brand": "JUNSITE",
              "aliasesList": ["JUNSITE"],
              "typesList": [2]
            }]
          }, {
            "id": 25088780,
            "name": "Корпус воздушного фильтра",
            "slug": "korpus-vozdushnogo-filtra",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 4086,
              "total": 121,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 1542,
              "total": 81,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 1976,
              "total": 61,
              "slug": "sat",
              "brand": "SAT",
              "aliasesList": ["SAT-TYG", "SATA", "SAT (КИТАЙ)", "SAT", "SAT/TYC", "SAT PREMIUM"],
              "typesList": [2]
            }, {
              "id": 1092,
              "total": 59,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 1533,
              "total": 56,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 2166,
              "total": 48,
              "slug": "suzuki",
              "brand": "SUZUKI",
              "aliasesList": ["SUZUKI (ЯПОНИЯ)", "SUZUKI MOTO", "SUZUKI MOTORCYCLES", "CHANGHE SUZUKI", "SUZUKI ORGNL", "SUZUKI ORIGINAL", "SUZUKI ЗАПЧАСТИ", "SUZUKIM", "SUZUKI", "OE SUZUKI", "CHANGAN SUZUKI"],
              "typesList": [1]
            }, {
              "id": 863,
              "total": 44,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 1652,
              "total": 44,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 2156,
              "total": 32,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 1481,
              "total": 30,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 18,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 209,
              "total": 14,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 2446,
              "total": 14,
              "slug": "yamaha",
              "brand": "YAMAHA",
              "aliasesList": ["YAMAHA", "YAMAHA MOTORCYCLES"],
              "typesList": [2]
            }, {
              "id": 2184,
              "total": 12,
              "slug": "tangde",
              "brand": "TANGDE",
              "aliasesList": ["TANGDE", "TD"],
              "typesList": [2]
            }, {
              "id": 498,
              "total": 11,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 1501,
              "total": 9,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 9,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 351,
              "total": 8,
              "slug": "bmw",
              "brand": "BMW",
              "aliasesList": ["BMW MOTO", "БМВ", "OE BMW", "BMW (CN)", "BMW (ГЕРМАНИЯ)", "BMW"],
              "typesList": [1]
            }, {
              "id": 189,
              "total": 7,
              "slug": "at",
              "brand": "AT",
              "aliasesList": ["AT", "AUTO TECHNOLOGIES GROUP", "AUTO TECHNOLOGIES GROUP S.R.O."],
              "typesList": [2]
            }, {
              "id": 2387,
              "total": 7,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 491,
              "total": 6,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 1354,
              "total": 6,
              "slug": "land-rover",
              "brand": "LAND ROVER",
              "aliasesList": ["ROVER/LAND ROVER", "LAND ROVER", "ROVER/LANDROVER/MG", "LR ЗАПЧАСТИ", "ЛЭНД РОВЕР", "LAND ROVER", "OE LAND ROVER"],
              "typesList": [1]
            }, {
              "id": 4060,
              "total": 6,
              "slug": "casp",
              "brand": "CASP",
              "aliasesList": ["CASP (ТАЙВАНЬ, КИТАЙ)", "CASP"],
              "typesList": [2]
            }, {
              "id": 490,
              "total": 5,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 565,
              "total": 5,
              "slug": "daihatsu",
              "brand": "DAIHATSU",
              "aliasesList": ["DAIHATSU", "DAIHATSU (ЯПОНИЯ)"],
              "typesList": [1]
            }, {
              "id": 1073,
              "total": 4,
              "slug": "hino",
              "brand": "HINO",
              "aliasesList": ["HINO"],
              "typesList": [1]
            }, {
              "id": 1186,
              "total": 4,
              "slug": "jaguar",
              "brand": "JAGUAR",
              "aliasesList": ["JAGUAR", "ЯГУАР", "JAGUAR ЗАПЧАСТИ"],
              "typesList": [1]
            }, {
              "id": 607,
              "total": 3,
              "slug": "diamond",
              "brand": "DIAMOND",
              "aliasesList": ["DIAMOND", "DIAMOND AUTO PARTS", "DAP"],
              "typesList": [2]
            }, {
              "id": 631,
              "total": 3,
              "slug": "donaldson",
              "brand": "DONALDSON",
              "aliasesList": ["DONALDSON GESEIISCHAFT", "DONALDSON", "ﾆﾎﾝﾄﾞﾅﾙﾄﾞｿﾝ", "DONALDSON TRUCK"],
              "typesList": [2]
            }, {
              "id": 806,
              "total": 3,
              "slug": "feituo",
              "brand": "FEITUO",
              "aliasesList": ["FEITUO"],
              "typesList": [2]
            }, {
              "id": 818,
              "total": 3,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 1988,
              "total": 3,
              "slug": "scania",
              "brand": "SCANIA",
              "aliasesList": ["SCANIA", "SCANIA EESTI AS", "OE Scania"],
              "typesList": [1]
            }, {
              "id": 4631,
              "total": 3,
              "slug": "tsp",
              "brand": "TSP",
              "aliasesList": ["ТСП", "TSP"],
              "typesList": [2]
            }, {
              "id": 5123,
              "total": 3,
              "slug": "gelorad",
              "brand": "GELORAD",
              "aliasesList": ["GELORAD"],
              "typesList": [2]
            }, {
              "id": 191,
              "total": 2,
              "slug": "atek",
              "brand": "ATEK",
              "aliasesList": ["КИТАЙ,ATEK", "ATEK"],
              "typesList": [2]
            }, {
              "id": 646,
              "total": 2,
              "slug": "dpa",
              "brand": "DPA",
              "aliasesList": ["DPA"],
              "typesList": [2]
            }, {
              "id": 1173,
              "total": 2,
              "slug": "isuzu",
              "brand": "ISUZU",
              "aliasesList": ["ISUZU"],
              "typesList": [1]
            }, {
              "id": 1375,
              "total": 2,
              "slug": "lex",
              "brand": "LEX",
              "aliasesList": ["LEX"],
              "typesList": [2]
            }, {
              "id": 1444,
              "total": 2,
              "slug": "man",
              "brand": "MAN",
              "aliasesList": ["MAN OE", "MAN ORIGINAL", "MAN"],
              "typesList": [1]
            }, {
              "id": 1814,
              "total": 2,
              "slug": "porsche",
              "brand": "PORSCHE",
              "aliasesList": ["ПОРШЕ", "PORSCHE", "VW-PORSCHE"],
              "typesList": [1]
            }, {
              "id": 17735,
              "total": 2,
              "slug": "gk-avtokomponent",
              "brand": "ГК АВТОКОМПОНЕНТ",
              "aliasesList": ["ГК АВТОКОМПОНЕНТ"],
              "typesList": [2]
            }, {
              "id": 211,
              "total": 1,
              "slug": "auger",
              "brand": "AUGER",
              "aliasesList": ["AUGER (ГЕРМАНИЯ)", "AUGER", "AUGER AUTOTECHNIK"],
              "typesList": [2]
            }, {
              "id": 343,
              "total": 1,
              "slug": "blic",
              "brand": "BLIC",
              "aliasesList": ["BLIC"],
              "typesList": [2]
            }, {
              "id": 353,
              "total": 1,
              "slug": "body-parts",
              "brand": "BODY PARTS",
              "aliasesList": ["BODY", "BODY PARTS", "BODIPARTS"],
              "typesList": [2]
            }, {
              "id": 405,
              "total": 1,
              "slug": "bsg",
              "brand": "BSG",
              "aliasesList": ["BSG (ТУРЦИЯ)", "\"BSG\" BASBUG", "BASBUG (ТУРЦИЯ)", "BASBUG", "BSG"],
              "typesList": [2]
            }, {
              "id": 629,
              "total": 1,
              "slug": "dominant",
              "brand": "DOMINANT",
              "aliasesList": ["DOMINANT"],
              "typesList": [2]
            }, {
              "id": 796,
              "total": 1,
              "slug": "faw",
              "brand": "FAW",
              "aliasesList": ["FAW"],
              "typesList": [1]
            }, {
              "id": 926,
              "total": 1,
              "slug": "geely",
              "brand": "GEELY",
              "aliasesList": ["GEELY (КИТАЙ)", "GEELY"],
              "typesList": [1]
            }, {
              "id": 1027,
              "total": 1,
              "slug": "hans-pries",
              "brand": "HANS PRIES",
              "aliasesList": ["HANS PRIES (ГЕРМАНИЯ)", "HANS PRIES", "HANS-PRIS", "HANS PRIES (TOPRAN)", "HANS-PRAES", "TOPRAN"],
              "typesList": [2]
            }, {
              "id": 1180,
              "total": 1,
              "slug": "iveco",
              "brand": "IVECO",
              "aliasesList": ["IVECO", "OE IVECO", "IVECO OE", "IVECO-DAILY"],
              "typesList": [1]
            }, {
              "id": 1368,
              "total": 1,
              "slug": "lema",
              "brand": "LEMA",
              "aliasesList": ["LEMA (ГЕРМАНИЯ)", "LEMA"],
              "typesList": [2]
            }, {
              "id": 1382,
              "total": 1,
              "slug": "lifan",
              "brand": "LIFAN",
              "aliasesList": ["LIFAN", "ЛИФАН"],
              "typesList": [1]
            }, {
              "id": 1490,
              "total": 1,
              "slug": "mec-diesel",
              "brand": "MEC-DIESEL",
              "aliasesList": ["MEC-DIESEL", "MEC-DISEL"],
              "typesList": [2]
            }, {
              "id": 1778,
              "total": 1,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }, {
              "id": 2582,
              "total": 1,
              "slug": "zzvf",
              "brand": "ZZVF",
              "aliasesList": ["ZZVF (КИТАЙ)", "ZZVF"],
              "typesList": [2]
            }, {
              "id": 2587,
              "total": 1,
              "slug": "ssang-yong",
              "brand": "SSANG YONG",
              "aliasesList": ["SSANG YONG", "SSANGYOUNG", "OE SSANG YONG", "SSANGYONG (ОРИГИНАЛ)", "SSANG YONG MOTORS CORPORATION (КОРЕЯ", "SSANGYONG (GEN) KOREA", "ORIGINAL SSANGYONG", "SSANGYONG (КОРЕЯ)", "SY", "SANGYONG", "SSY"],
              "typesList": [1, 3]
            }, {
              "id": 4210,
              "total": 1,
              "slug": "hillport",
              "brand": "HILLPORT",
              "aliasesList": ["HILLPORT", "HILPORT"],
              "typesList": [2]
            }, {
              "id": 4309,
              "total": 1,
              "slug": "junsite",
              "brand": "JUNSITE",
              "aliasesList": ["JUNSITE"],
              "typesList": [2]
            }]
          }, {
            "id": 25176861,
            "name": "Корпус воздушного фильтра (нижняя часть)",
            "slug": "korpus-vozdushnogo-filtra-nizhnyaya-chast_2",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 1542,
              "total": 2,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 1,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1713,
              "total": 1,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 2156,
              "total": 1,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }]
          }, {
            "id": 25103310,
            "name": "Корпус фильтра воздушного",
            "slug": "korpus-filtra-vozdushnogo_1",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 863,
              "total": 44,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 17,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1542,
              "total": 13,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 1976,
              "total": 10,
              "slug": "sat",
              "brand": "SAT",
              "aliasesList": ["SAT-TYG", "SATA", "SAT (КИТАЙ)", "SAT", "SAT/TYC", "SAT PREMIUM"],
              "typesList": [2]
            }, {
              "id": 4086,
              "total": 8,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 353,
              "total": 7,
              "slug": "body-parts",
              "brand": "BODY PARTS",
              "aliasesList": ["BODY", "BODY PARTS", "BODIPARTS"],
              "typesList": [2]
            }, {
              "id": 191,
              "total": 6,
              "slug": "atek",
              "brand": "ATEK",
              "aliasesList": ["КИТАЙ,ATEK", "ATEK"],
              "typesList": [2]
            }, {
              "id": 4473,
              "total": 6,
              "slug": "jorden",
              "brand": "JORDEN",
              "aliasesList": ["JORDEN", "JORDEN (JH)"],
              "typesList": [2]
            }, {
              "id": 498,
              "total": 5,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 1092,
              "total": 5,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 2310,
              "total": 5,
              "slug": "tyg",
              "brand": "TYG",
              "aliasesList": ["TYG ", "TYG (TONG YANG)", "TYG (ТАЙВАНЬ)", "TYG-TAIWAN", "TYG"],
              "typesList": [2]
            }, {
              "id": 2587,
              "total": 4,
              "slug": "ssang-yong",
              "brand": "SSANG YONG",
              "aliasesList": ["SSANG YONG", "SSANGYOUNG", "OE SSANG YONG", "SSANGYONG (ОРИГИНАЛ)", "SSANG YONG MOTORS CORPORATION (КОРЕЯ", "SSANGYONG (GEN) KOREA", "ORIGINAL SSANGYONG", "SSANGYONG (КОРЕЯ)", "SY", "SANGYONG", "SSY"],
              "typesList": [1, 3]
            }, {
              "id": 209,
              "total": 3,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 211,
              "total": 3,
              "slug": "auger",
              "brand": "AUGER",
              "aliasesList": ["AUGER (ГЕРМАНИЯ)", "AUGER", "AUGER AUTOTECHNIK"],
              "typesList": [2]
            }, {
              "id": 2387,
              "total": 3,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 18082,
              "total": 3,
              "slug": "topcover",
              "brand": "TOPCOVER",
              "aliasesList": ["TOPCOVER"],
              "typesList": [2]
            }, {
              "id": 629,
              "total": 2,
              "slug": "dominant",
              "brand": "DOMINANT",
              "aliasesList": ["DOMINANT"],
              "typesList": [2]
            }, {
              "id": 818,
              "total": 2,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 1481,
              "total": 2,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 2,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 2353,
              "total": 2,
              "slug": "vaz",
              "brand": "VAZ",
              "aliasesList": ["АВТОВАЗ\" ОАО Г", "\"АВТОВАЗ\" ОАО Г. ТОЛЬЯТТИ", "LADA", "LADA (РОССИЯ)", "LADA IMAGE", "AVTOVAZ", "ВАЗ ЛАДА", "ВАЗ Г", "ВАЗ (САМАРА)", "ВАЗ (ОАО АВТОВАЗ Г", "ВАЗ", "VAZ", "ЛАДА", "АО ЛАДА-ИМИДЖ", "АО ВАЗ", "АО «ЛАДА ИМИДЖ»", "НИВА", "АВТОВАЗ", "ОАО АВТОВАЗ", "ОАО АВТОВАЗ (РЕМКОМПЛЕКТЫ)", "ОАО ВАЗ", "GM-AVTOVAZ", "АВТОВАЗ ОАО"],
              "typesList": [1]
            }, {
              "id": 17403,
              "total": 2,
              "slug": "grog",
              "brand": "GROG",
              "aliasesList": ["GROG"],
              "typesList": [2]
            }, {
              "id": 351,
              "total": 1,
              "slug": "bmw",
              "brand": "BMW",
              "aliasesList": ["BMW MOTO", "БМВ", "OE BMW", "BMW (CN)", "BMW (ГЕРМАНИЯ)", "BMW"],
              "typesList": [1]
            }, {
              "id": 490,
              "total": 1,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 607,
              "total": 1,
              "slug": "diamond",
              "brand": "DIAMOND",
              "aliasesList": ["DIAMOND", "DIAMOND AUTO PARTS", "DAP"],
              "typesList": [2]
            }, {
              "id": 609,
              "total": 1,
              "slug": "diesel-technic",
              "brand": "DIESEL TECHNIC",
              "aliasesList": ["DT-DIESEL TECHNIC", "DT (ГЕРМАНИЯ)", "DT", "DIESEL TECHNIC", "DT SPARE PARTS"],
              "typesList": [2]
            }, {
              "id": 646,
              "total": 1,
              "slug": "dpa",
              "brand": "DPA",
              "aliasesList": ["DPA"],
              "typesList": [2]
            }, {
              "id": 1100,
              "total": 1,
              "slug": "howo",
              "brand": "HOWO",
              "aliasesList": ["HOWO", "HOWO/SHAANXI"],
              "typesList": [2]
            }, {
              "id": 1173,
              "total": 1,
              "slug": "isuzu",
              "brand": "ISUZU",
              "aliasesList": ["ISUZU"],
              "typesList": [1]
            }, {
              "id": 1501,
              "total": 1,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 1959,
              "total": 1,
              "slug": "sampa",
              "brand": "SAMPA",
              "aliasesList": ["SAMPA"],
              "typesList": [2]
            }, {
              "id": 2146,
              "total": 1,
              "slug": "stellox",
              "brand": "STELLOX",
              "aliasesList": ["STELLOX", "STELOX"],
              "typesList": [2]
            }, {
              "id": 2247,
              "total": 1,
              "slug": "tong-yang",
              "brand": "TONG YANG",
              "aliasesList": ["TONG YANG (КИТАЙ)", "TONG YANG"],
              "typesList": [2]
            }, {
              "id": 4309,
              "total": 1,
              "slug": "junsite",
              "brand": "JUNSITE",
              "aliasesList": ["JUNSITE"],
              "typesList": [2]
            }, {
              "id": 5521,
              "total": 1,
              "slug": "sheft",
              "brand": "SHEFT",
              "aliasesList": ["SHEFT"],
              "typesList": [2]
            }, {
              "id": 16268,
              "total": 1,
              "slug": "forward",
              "brand": "FORWARD",
              "aliasesList": ["FORWARD", "FORWARD AVTOPARTS"],
              "typesList": [2]
            }, {
              "id": 17735,
              "total": 1,
              "slug": "gk-avtokomponent",
              "brand": "ГК АВТОКОМПОНЕНТ",
              "aliasesList": ["ГК АВТОКОМПОНЕНТ"],
              "typesList": [2]
            }, {
              "id": 18076,
              "total": 1,
              "slug": "siegel",
              "brand": "SIEGEL",
              "aliasesList": ["SIEGEL"],
              "typesList": [2]
            }]
          }, {
            "id": 26210937,
            "name": "Кронштейн резонатора фильтра воздушного",
            "slug": "kronshtejn-rezonatora-filtra-vozdushnogo",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 1652,
              "total": 2,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 1481,
              "total": 1,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 1,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 1886,
              "total": 1,
              "slug": "renault-samsung",
              "brand": "RENAULT-SAMSUNG",
              "aliasesList": ["SAMSUNG", "RENAULT-SAMSUNG"],
              "typesList": [2]
            }]
          }, {
            "id": 25090854,
            "name": "Крышка воздушного фильтра",
            "slug": "kryshka-vozdushnogo-filtra",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 4086,
              "total": 142,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 1533,
              "total": 78,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1542,
              "total": 27,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 1092,
              "total": 17,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 2166,
              "total": 17,
              "slug": "suzuki",
              "brand": "SUZUKI",
              "aliasesList": ["SUZUKI (ЯПОНИЯ)", "SUZUKI MOTO", "SUZUKI MOTORCYCLES", "CHANGHE SUZUKI", "SUZUKI ORGNL", "SUZUKI ORIGINAL", "SUZUKI ЗАПЧАСТИ", "SUZUKIM", "SUZUKI", "OE SUZUKI", "CHANGAN SUZUKI"],
              "typesList": [1]
            }, {
              "id": 1481,
              "total": 12,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 2446,
              "total": 12,
              "slug": "yamaha",
              "brand": "YAMAHA",
              "aliasesList": ["YAMAHA", "YAMAHA MOTORCYCLES"],
              "typesList": [2]
            }, {
              "id": 929,
              "total": 5,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1713,
              "total": 5,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 491,
              "total": 2,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 565,
              "total": 2,
              "slug": "daihatsu",
              "brand": "DAIHATSU",
              "aliasesList": ["DAIHATSU", "DAIHATSU (ЯПОНИЯ)"],
              "typesList": [1]
            }, {
              "id": 4631,
              "total": 2,
              "slug": "tsp",
              "brand": "TSP",
              "aliasesList": ["ТСП", "TSP"],
              "typesList": [2]
            }, {
              "id": 498,
              "total": 1,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 560,
              "total": 1,
              "slug": "daf",
              "brand": "DAF",
              "aliasesList": ["LEYLAND AUTO", "DAF", "DAF OE", "LDV", "LDV LIMITED", "LEYLAND", "LEYLAND-DAF"],
              "typesList": [1]
            }, {
              "id": 863,
              "total": 1,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 1550,
              "total": 1,
              "slug": "mopar-parts",
              "brand": "MOPAR PARTS",
              "aliasesList": ["MOPAR,КАНАДА", "MOPAR PARTS", "MOPAR"],
              "typesList": [2]
            }, {
              "id": 1652,
              "total": 1,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 1814,
              "total": 1,
              "slug": "porsche",
              "brand": "PORSCHE",
              "aliasesList": ["ПОРШЕ", "PORSCHE", "VW-PORSCHE"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 1,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 1959,
              "total": 1,
              "slug": "sampa",
              "brand": "SAMPA",
              "aliasesList": ["SAMPA"],
              "typesList": [2]
            }, {
              "id": 1976,
              "total": 1,
              "slug": "sat",
              "brand": "SAT",
              "aliasesList": ["SAT-TYG", "SATA", "SAT (КИТАЙ)", "SAT", "SAT/TYC", "SAT PREMIUM"],
              "typesList": [2]
            }, {
              "id": 2156,
              "total": 1,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 2387,
              "total": 1,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 2582,
              "total": 1,
              "slug": "zzvf",
              "brand": "ZZVF",
              "aliasesList": ["ZZVF (КИТАЙ)", "ZZVF"],
              "typesList": [2]
            }]
          }, {
            "id": 28143465,
            "name": "Крышка резонатора фильтра воздушного",
            "slug": "kryshka-rezonatora-filtra-vozdushnogo",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 1778,
              "total": 1,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }]
          }, {
            "id": 25334439,
            "name": "Патрубок резонатора фильтра воздушного",
            "slug": "patrubok-rezonatora-filtra-vozdushnogo",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 929,
              "total": 3,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 491,
              "total": 2,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 1652,
              "total": 2,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 189,
              "total": 1,
              "slug": "at",
              "brand": "AT",
              "aliasesList": ["AT", "AUTO TECHNOLOGIES GROUP", "AUTO TECHNOLOGIES GROUP S.R.O."],
              "typesList": [2]
            }, {
              "id": 1713,
              "total": 1,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 1,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }]
          }, {
            "id": 25101460,
            "name": "Патрубок фильтра воздушного",
            "slug": "patrubok-filtra-vozdushnogo",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 1501,
              "total": 15,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 2587,
              "total": 5,
              "slug": "ssang-yong",
              "brand": "SSANG YONG",
              "aliasesList": ["SSANG YONG", "SSANGYOUNG", "OE SSANG YONG", "SSANGYONG (ОРИГИНАЛ)", "SSANG YONG MOTORS CORPORATION (КОРЕЯ", "SSANGYONG (GEN) KOREA", "ORIGINAL SSANGYONG", "SSANGYONG (КОРЕЯ)", "SY", "SANGYONG", "SSY"],
              "typesList": [1, 3]
            }, {
              "id": 1652,
              "total": 4,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 209,
              "total": 2,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 1328,
              "total": 2,
              "slug": "kross",
              "brand": "KROSS",
              "aliasesList": ["KROSS"],
              "typesList": [2]
            }, {
              "id": 1481,
              "total": 2,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 155,
              "total": 1,
              "slug": "arirang",
              "brand": "ARIRANG",
              "aliasesList": ["ARIRANG"],
              "typesList": [2]
            }, {
              "id": 498,
              "total": 1,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 609,
              "total": 1,
              "slug": "diesel-technic",
              "brand": "DIESEL TECHNIC",
              "aliasesList": ["DT-DIESEL TECHNIC", "DT (ГЕРМАНИЯ)", "DT", "DIESEL TECHNIC", "DT SPARE PARTS"],
              "typesList": [2]
            }, {
              "id": 647,
              "total": 1,
              "slug": "dp-group",
              "brand": "DP GROUP",
              "aliasesList": ["DP", "DP GROUP"],
              "typesList": [2, 3]
            }, {
              "id": 802,
              "total": 1,
              "slug": "febest",
              "brand": "FEBEST",
              "aliasesList": ["FEBEST (ГЕРМАНИЯ)", "FEBEST", "FEBTST"],
              "typesList": [2]
            }, {
              "id": 918,
              "total": 1,
              "slug": "gates",
              "brand": "GATES",
              "aliasesList": ["GATES БЕЛЬГИЯ", "GATES", "GATES (OEM) GERMANY", "GATES (США)", "GATES CORP", "GATES-AU", "GATES / KETNER", "GATES-CN"],
              "typesList": [2]
            }, {
              "id": 929,
              "total": 1,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1027,
              "total": 1,
              "slug": "hans-pries",
              "brand": "HANS PRIES",
              "aliasesList": ["HANS PRIES (ГЕРМАНИЯ)", "HANS PRIES", "HANS-PRIS", "HANS PRIES (TOPRAN)", "HANS-PRAES", "TOPRAN"],
              "typesList": [2]
            }, {
              "id": 1136,
              "total": 1,
              "slug": "impergom",
              "brand": "IMPERGOM",
              "aliasesList": ["IMPERGOM", "IMPEGOM"],
              "typesList": [2]
            }, {
              "id": 1375,
              "total": 1,
              "slug": "lex",
              "brand": "LEX",
              "aliasesList": ["LEX"],
              "typesList": [2]
            }, {
              "id": 1542,
              "total": 1,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 1814,
              "total": 1,
              "slug": "porsche",
              "brand": "PORSCHE",
              "aliasesList": ["ПОРШЕ", "PORSCHE", "VW-PORSCHE"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 1,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 2387,
              "total": 1,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 4604,
              "total": 1,
              "slug": "brt",
              "brand": "БРТ",
              "aliasesList": ["ОАО БРТ", "БРТ Г", "БАЛАКОВОРЕЗИНОТЕХНИКА (ОАО БРТ)", "ОАО БАЛАКОВОРЕЗИНОТЕХНИКА", "БАЛАКОВО", "БАЛАКОВОРЕЗИНОТЕХНИКА ОАО", "БАЛАКОВОРЕЗИНОТЕХНИКА ОАО, БАЛАКОВО", "БРТ"],
              "typesList": [2]
            }]
          }, {
            "id": 33658479,
            "name": "Планка резонатора фильтра воздушного опорная",
            "slug": "planka-rezonatora-filtra-vozdushnogo-opornaya",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 1542,
              "total": 1,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }]
          }, {
            "id": 26357368,
            "name": "Прокладка резонатора фильтра воздушного",
            "slug": "prokladka-rezonatora-filtra-vozdushnogo",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 929,
              "total": 4,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 491,
              "total": 1,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 559,
              "total": 1,
              "slug": "daewoo",
              "brand": "DAEWOO",
              "aliasesList": ["UZ-DAEWOO", "DAEWOO (КОРЕЯ)", "DAEWOO MOTOR", "DAEWOOPARTS, PMC", "DAEWOO"],
              "typesList": [1, 4]
            }]
          }, {
            "id": 25092801,
            "name": "Резонатор фильтра воздушного",
            "slug": "rezonator-filtra-vozdushnogo",
            "groupsList": [10003],
            "manufacturers": [{
              "id": 1652,
              "total": 51,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 1542,
              "total": 43,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 863,
              "total": 32,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 1092,
              "total": 30,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 19,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1481,
              "total": 17,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 1533,
              "total": 16,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 4086,
              "total": 13,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 491,
              "total": 11,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 1713,
              "total": 10,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 2156,
              "total": 10,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 2166,
              "total": 8,
              "slug": "suzuki",
              "brand": "SUZUKI",
              "aliasesList": ["SUZUKI (ЯПОНИЯ)", "SUZUKI MOTO", "SUZUKI MOTORCYCLES", "CHANGHE SUZUKI", "SUZUKI ORGNL", "SUZUKI ORIGINAL", "SUZUKI ЗАПЧАСТИ", "SUZUKIM", "SUZUKI", "OE SUZUKI", "CHANGAN SUZUKI"],
              "typesList": [1]
            }, {
              "id": 191,
              "total": 7,
              "slug": "atek",
              "brand": "ATEK",
              "aliasesList": ["КИТАЙ,ATEK", "ATEK"],
              "typesList": [2]
            }, {
              "id": 4473,
              "total": 7,
              "slug": "jorden",
              "brand": "JORDEN",
              "aliasesList": ["JORDEN", "JORDEN (JH)"],
              "typesList": [2]
            }, {
              "id": 1778,
              "total": 5,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 5,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 498,
              "total": 3,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 2582,
              "total": 3,
              "slug": "zzvf",
              "brand": "ZZVF",
              "aliasesList": ["ZZVF (КИТАЙ)", "ZZVF"],
              "typesList": [2]
            }, {
              "id": 189,
              "total": 2,
              "slug": "at",
              "brand": "AT",
              "aliasesList": ["AT", "AUTO TECHNOLOGIES GROUP", "AUTO TECHNOLOGIES GROUP S.R.O."],
              "typesList": [2]
            }, {
              "id": 209,
              "total": 2,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 559,
              "total": 2,
              "slug": "daewoo",
              "brand": "DAEWOO",
              "aliasesList": ["UZ-DAEWOO", "DAEWOO (КОРЕЯ)", "DAEWOO MOTOR", "DAEWOOPARTS, PMC", "DAEWOO"],
              "typesList": [1, 4]
            }, {
              "id": 629,
              "total": 2,
              "slug": "dominant",
              "brand": "DOMINANT",
              "aliasesList": ["DOMINANT"],
              "typesList": [2]
            }, {
              "id": 920,
              "total": 2,
              "slug": "gaz",
              "brand": "GAZ",
              "aliasesList": ["ГАЗ 3302", "ГАЗ 2705", "ГАЗ 52", "ГАЗ 2217", "ГАЗ", "ВОЛГА-ОЙЛ", "ВОЛГА", "ГАЗ 53", "ГАЗ 66", "ГАЗ ОАО", "ГАЗ ООО", "ГАЗЕЛЬ", "ВАЛДАЙ", "ГОРЬКОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД ОАО, Н.Н", "ОАО ГАЗ", "УМЗ Г.УЛЬЯНОВСК", "УМЗ Г", "VOLGA", "УМЗ", "GAZ", "ГАЗ 4301", "ГАЗ 3309", "ГАЗ 3308", "ГАЗ 3307", "OE GAZ"],
              "typesList": [1, 3]
            }, {
              "id": 1354,
              "total": 2,
              "slug": "land-rover",
              "brand": "LAND ROVER",
              "aliasesList": ["ROVER/LAND ROVER", "LAND ROVER", "ROVER/LANDROVER/MG", "LR ЗАПЧАСТИ", "ЛЭНД РОВЕР", "LAND ROVER", "OE LAND ROVER"],
              "typesList": [1]
            }, {
              "id": 4064,
              "total": 2,
              "slug": "deppul",
              "brand": "DEPPUL",
              "aliasesList": ["DEPPUL (КИТАЙ)", "DEPPUL"],
              "typesList": [2]
            }, {
              "id": 4309,
              "total": 2,
              "slug": "junsite",
              "brand": "JUNSITE",
              "aliasesList": ["JUNSITE"],
              "typesList": [2]
            }, {
              "id": 17234,
              "total": 2,
              "slug": "sky-parts",
              "brand": "Sky Parts",
              "aliasesList": ["Sky Parts"],
              "typesList": [2]
            }, {
              "id": 490,
              "total": 1,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 806,
              "total": 1,
              "slug": "feituo",
              "brand": "FEITUO",
              "aliasesList": ["FEITUO"],
              "typesList": [2]
            }, {
              "id": 1173,
              "total": 1,
              "slug": "isuzu",
              "brand": "ISUZU",
              "aliasesList": ["ISUZU"],
              "typesList": [1]
            }, {
              "id": 1940,
              "total": 1,
              "slug": "ruei",
              "brand": "RUEI",
              "aliasesList": ["RUEI"],
              "typesList": [2]
            }, {
              "id": 2510,
              "total": 1,
              "slug": "transmaster",
              "brand": "TRANSMASTER",
              "aliasesList": ["TRANSMASTER UNIVERSAL", "TRANSMASTER", "TRANSMASTER UNIVERSA", "ТМ \"TRANSMASTER\""],
              "typesList": [2]
            }, {
              "id": 2587,
              "total": 1,
              "slug": "ssang-yong",
              "brand": "SSANG YONG",
              "aliasesList": ["SSANG YONG", "SSANGYOUNG", "OE SSANG YONG", "SSANGYONG (ОРИГИНАЛ)", "SSANG YONG MOTORS CORPORATION (КОРЕЯ", "SSANGYONG (GEN) KOREA", "ORIGINAL SSANGYONG", "SSANGYONG (КОРЕЯ)", "SY", "SANGYONG", "SSY"],
              "typesList": [1, 3]
            }, {
              "id": 4286,
              "total": 1,
              "slug": "rytson",
              "brand": "RYTSON",
              "aliasesList": ["RYTSON", "CHANGZHOU RYTSON"],
              "typesList": [2]
            }, {
              "id": 4308,
              "total": 1,
              "slug": "asparts",
              "brand": "ASPARTS",
              "aliasesList": ["ASPARTS"],
              "typesList": [2]
            }]
          }],
          "total": 15
        }, {
          "id": 10002,
          "name": "Запчасти впускного коллектора",
          "slug": "zapchasti-vpusknogo-kollektora",
          "groupsList": [{
            "id": 25090984,
            "name": "Впускной коллектор",
            "slug": "vpusknoj-kollektor",
            "groupsList": [10002],
            "manufacturers": [{
              "id": 1501,
              "total": 502,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 209,
              "total": 105,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 351,
              "total": 8,
              "slug": "bmw",
              "brand": "BMW",
              "aliasesList": ["BMW MOTO", "БМВ", "OE BMW", "BMW (CN)", "BMW (ГЕРМАНИЯ)", "BMW"],
              "typesList": [1]
            }, {
              "id": 863,
              "total": 8,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 1186,
              "total": 6,
              "slug": "jaguar",
              "brand": "JAGUAR",
              "aliasesList": ["JAGUAR", "ЯГУАР", "JAGUAR ЗАПЧАСТИ"],
              "typesList": [1]
            }, {
              "id": 1481,
              "total": 4,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 4086,
              "total": 4,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 498,
              "total": 3,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 818,
              "total": 3,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 1354,
              "total": 3,
              "slug": "land-rover",
              "brand": "LAND ROVER",
              "aliasesList": ["ROVER/LAND ROVER", "LAND ROVER", "ROVER/LANDROVER/MG", "LR ЗАПЧАСТИ", "ЛЭНД РОВЕР", "LAND ROVER", "OE LAND ROVER"],
              "typesList": [1]
            }, {
              "id": 1814,
              "total": 3,
              "slug": "porsche",
              "brand": "PORSCHE",
              "aliasesList": ["ПОРШЕ", "PORSCHE", "VW-PORSCHE"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 3,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 2387,
              "total": 3,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 926,
              "total": 2,
              "slug": "geely",
              "brand": "GEELY",
              "aliasesList": ["GEELY (КИТАЙ)", "GEELY"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 2,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1713,
              "total": 2,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 490,
              "total": 1,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 491,
              "total": 1,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 560,
              "total": 1,
              "slug": "daf",
              "brand": "DAF",
              "aliasesList": ["LEYLAND AUTO", "DAF", "DAF OE", "LDV", "LDV LIMITED", "LEYLAND", "LEYLAND-DAF"],
              "typesList": [1]
            }, {
              "id": 735,
              "total": 1,
              "slug": "era",
              "brand": "ERA",
              "aliasesList": ["ERA BENELUX", "ERA", "ERA ELETTRO"],
              "typesList": [2]
            }, {
              "id": 1444,
              "total": 1,
              "slug": "man",
              "brand": "MAN",
              "aliasesList": ["MAN OE", "MAN ORIGINAL", "MAN"],
              "typesList": [1]
            }, {
              "id": 1542,
              "total": 1,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 1652,
              "total": 1,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 2156,
              "total": 1,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 2353,
              "total": 1,
              "slug": "vaz",
              "brand": "VAZ",
              "aliasesList": ["АВТОВАЗ\" ОАО Г", "\"АВТОВАЗ\" ОАО Г. ТОЛЬЯТТИ", "LADA", "LADA (РОССИЯ)", "LADA IMAGE", "AVTOVAZ", "ВАЗ ЛАДА", "ВАЗ Г", "ВАЗ (САМАРА)", "ВАЗ (ОАО АВТОВАЗ Г", "ВАЗ", "VAZ", "ЛАДА", "АО ЛАДА-ИМИДЖ", "АО ВАЗ", "АО «ЛАДА ИМИДЖ»", "НИВА", "АВТОВАЗ", "ОАО АВТОВАЗ", "ОАО АВТОВАЗ (РЕМКОМПЛЕКТЫ)", "ОАО ВАЗ", "GM-AVTOVAZ", "АВТОВАЗ ОАО"],
              "typesList": [1]
            }, {
              "id": 2358,
              "total": 1,
              "slug": "vemo-vaico",
              "brand": "VEMO (VAICO)",
              "aliasesList": ["VIEROL AG", "VIEROL", "VEMO", "VAICO/VEMO", "VAICO", "VEMO (VAICO)"],
              "typesList": [2]
            }]
          }, {
            "id": 25089784,
            "name": "Коллектор впускной",
            "slug": "kollektor-vpusknoj",
            "groupsList": [10002],
            "manufacturers": [{
              "id": 863,
              "total": 166,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 4086,
              "total": 140,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 1652,
              "total": 125,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 1778,
              "total": 110,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }, {
              "id": 1533,
              "total": 59,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1542,
              "total": 52,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 209,
              "total": 49,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 35,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1481,
              "total": 35,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 1092,
              "total": 22,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 498,
              "total": 18,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 2446,
              "total": 15,
              "slug": "yamaha",
              "brand": "YAMAHA",
              "aliasesList": ["YAMAHA", "YAMAHA MOTORCYCLES"],
              "typesList": [2]
            }, {
              "id": 818,
              "total": 14,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 1501,
              "total": 12,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 2166,
              "total": 11,
              "slug": "suzuki",
              "brand": "SUZUKI",
              "aliasesList": ["SUZUKI (ЯПОНИЯ)", "SUZUKI MOTO", "SUZUKI MOTORCYCLES", "CHANGHE SUZUKI", "SUZUKI ORGNL", "SUZUKI ORIGINAL", "SUZUKI ЗАПЧАСТИ", "SUZUKIM", "SUZUKI", "OE SUZUKI", "CHANGAN SUZUKI"],
              "typesList": [1]
            }, {
              "id": 2156,
              "total": 9,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 7,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 300,
              "total": 4,
              "slug": "behr-hella",
              "brand": "BEHR (HELLA)",
              "aliasesList": ["BEHR-HELLA (ГЕРМАНИЯ)", "BEHR_", "BEHR THERMOT-TRONIK", "BEHR SERVICE", "BEHR HELLA SERVICE TRUCK", "BEHR HELLA SERVICE", "BEHR (HELLA)", "HELLA/BEHR-HELLA", "HELLA TRUCK", "HELLA (ГЕРМАНИЯ)", "HELLA"],
              "typesList": [2]
            }, {
              "id": 490,
              "total": 4,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 491,
              "total": 4,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 1180,
              "total": 4,
              "slug": "iveco",
              "brand": "IVECO",
              "aliasesList": ["IVECO", "OE IVECO", "IVECO OE", "IVECO-DAILY"],
              "typesList": [1]
            }, {
              "id": 189,
              "total": 2,
              "slug": "at",
              "brand": "AT",
              "aliasesList": ["AT", "AUTO TECHNOLOGIES GROUP", "AUTO TECHNOLOGIES GROUP S.R.O."],
              "typesList": [2]
            }, {
              "id": 559,
              "total": 2,
              "slug": "daewoo",
              "brand": "DAEWOO",
              "aliasesList": ["UZ-DAEWOO", "DAEWOO (КОРЕЯ)", "DAEWOO MOTOR", "DAEWOOPARTS, PMC", "DAEWOO"],
              "typesList": [1, 4]
            }, {
              "id": 926,
              "total": 2,
              "slug": "geely",
              "brand": "GEELY",
              "aliasesList": ["GEELY (КИТАЙ)", "GEELY"],
              "typesList": [1]
            }, {
              "id": 1186,
              "total": 2,
              "slug": "jaguar",
              "brand": "JAGUAR",
              "aliasesList": ["JAGUAR", "ЯГУАР", "JAGUAR ЗАПЧАСТИ"],
              "typesList": [1]
            }, {
              "id": 1354,
              "total": 2,
              "slug": "land-rover",
              "brand": "LAND ROVER",
              "aliasesList": ["ROVER/LAND ROVER", "LAND ROVER", "ROVER/LANDROVER/MG", "LR ЗАПЧАСТИ", "ЛЭНД РОВЕР", "LAND ROVER", "OE LAND ROVER"],
              "typesList": [1]
            }, {
              "id": 1713,
              "total": 2,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 2353,
              "total": 2,
              "slug": "vaz",
              "brand": "VAZ",
              "aliasesList": ["АВТОВАЗ\" ОАО Г", "\"АВТОВАЗ\" ОАО Г. ТОЛЬЯТТИ", "LADA", "LADA (РОССИЯ)", "LADA IMAGE", "AVTOVAZ", "ВАЗ ЛАДА", "ВАЗ Г", "ВАЗ (САМАРА)", "ВАЗ (ОАО АВТОВАЗ Г", "ВАЗ", "VAZ", "ЛАДА", "АО ЛАДА-ИМИДЖ", "АО ВАЗ", "АО «ЛАДА ИМИДЖ»", "НИВА", "АВТОВАЗ", "ОАО АВТОВАЗ", "ОАО АВТОВАЗ (РЕМКОМПЛЕКТЫ)", "ОАО ВАЗ", "GM-AVTOVAZ", "АВТОВАЗ ОАО"],
              "typesList": [1]
            }, {
              "id": 2358,
              "total": 2,
              "slug": "vemo-vaico",
              "brand": "VEMO (VAICO)",
              "aliasesList": ["VIEROL AG", "VIEROL", "VEMO", "VAICO/VEMO", "VAICO", "VEMO (VAICO)"],
              "typesList": [2]
            }, {
              "id": 3709,
              "total": 2,
              "slug": "zmz",
              "brand": "ZMZ",
              "aliasesList": ["ЗМЗ 405", "ОАО ЗАВОЛЖСКИЙ МОТОРНЫЙ ЗАВОД", "ОАО ЗМЗ", "ОАО ЗМЗ Г", "ОАО ЗМЗ Г.ЗАВОЛЖЬЕ", "ОАО ЗМЗ ПС", "ZMZ", "ЗМЗ 406", "ЗМЗ", "ЗМЗ (ЗАВОЛЖСКИЙ МОТОРНЫЙ ЗАВОД)", "ЗМЗ 402"],
              "typesList": [2]
            }, {
              "id": 424,
              "total": 1,
              "slug": "byd",
              "brand": "BYD",
              "aliasesList": ["BYD"],
              "typesList": [1]
            }, {
              "id": 565,
              "total": 1,
              "slug": "daihatsu",
              "brand": "DAIHATSU",
              "aliasesList": ["DAIHATSU", "DAIHATSU (ЯПОНИЯ)"],
              "typesList": [1]
            }, {
              "id": 1487,
              "total": 1,
              "slug": "meat-doria",
              "brand": "MEAT & DORIA",
              "aliasesList": ["MEAT&amp;DORIA", "MEAT AND DORIA", "MEAT & DORIA"],
              "typesList": [2]
            }, {
              "id": 1789,
              "total": 1,
              "slug": "pierburg",
              "brand": "PIERBURG",
              "aliasesList": ["PIERBURG", "Pierburg truck", "PIERBURG FRANCE"],
              "typesList": [2]
            }, {
              "id": 2037,
              "total": 1,
              "slug": "shaanxi",
              "brand": "SHAANXI",
              "aliasesList": ["SHAANXI"],
              "typesList": [1]
            }, {
              "id": 2168,
              "total": 1,
              "slug": "swag",
              "brand": "SWAG",
              "aliasesList": ["SWAG"],
              "typesList": [2]
            }, {
              "id": 2314,
              "total": 1,
              "slug": "uaz",
              "brand": "UAZ",
              "aliasesList": ["УЛЬЯНОВСК", "ОАО УЛЬЯНОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД", "UAZ", "УАЗ", "УЛЬЯНОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД ОАО, Г.У", "УЛЬЯНОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД", "ОАО УАЗ Г", "ОАО УАЗ Г.УЛЬЯНОВСК", "ООО УАЗ-АВТОКОМПОНЕНТ", "ОАО УАЗ", "УАЗ ОАО", "OE UAZ"],
              "typesList": [1]
            }, {
              "id": 2374,
              "total": 1,
              "slug": "vika",
              "brand": "VIKA",
              "aliasesList": ["VIKA"],
              "typesList": [2]
            }, {
              "id": 2387,
              "total": 1,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 2582,
              "total": 1,
              "slug": "zzvf",
              "brand": "ZZVF",
              "aliasesList": ["ZZVF (КИТАЙ)", "ZZVF"],
              "typesList": [2]
            }, {
              "id": 2587,
              "total": 1,
              "slug": "ssang-yong",
              "brand": "SSANG YONG",
              "aliasesList": ["SSANG YONG", "SSANGYOUNG", "OE SSANG YONG", "SSANGYONG (ОРИГИНАЛ)", "SSANG YONG MOTORS CORPORATION (КОРЕЯ", "SSANGYONG (GEN) KOREA", "ORIGINAL SSANGYONG", "SSANGYONG (КОРЕЯ)", "SY", "SANGYONG", "SSY"],
              "typesList": [1, 3]
            }, {
              "id": 3538,
              "total": 1,
              "slug": "uralaz",
              "brand": "URALAZ",
              "aliasesList": ["URAL", "УРАЛ", "URALAZ", "АЗ УРАЛ"],
              "typesList": [2]
            }, {
              "id": 3616,
              "total": 1,
              "slug": "mtz",
              "brand": "MTZ",
              "aliasesList": ["MTZ", "МТЗ", "BELARUS"],
              "typesList": [2]
            }, {
              "id": 4309,
              "total": 1,
              "slug": "junsite",
              "brand": "JUNSITE",
              "aliasesList": ["JUNSITE"],
              "typesList": [2]
            }, {
              "id": 4624,
              "total": 1,
              "slug": "ultra-power",
              "brand": "ULTRA-POWER",
              "aliasesList": ["ULTRA-POWER"],
              "typesList": [2]
            }, {
              "id": 4883,
              "total": 1,
              "slug": "mmz",
              "brand": "MMZ",
              "aliasesList": ["ММЗ ОАО ФИЛИАЛ В Г", "MMZ", "ММЗ ОАО", "ММЗ"],
              "typesList": [2]
            }, {
              "id": 17817,
              "total": 1,
              "slug": "oao-umz",
              "brand": "ОАО УМЗ",
              "aliasesList": ["ОАО УМЗ"],
              "typesList": [2]
            }]
          }, {
            "id": 25089653,
            "name": "Прокладка впускного коллектора",
            "slug": "prokladka-vpusknogo-kollektora",
            "groupsList": [10002],
            "manufacturers": [{
              "id": 1490,
              "total": 81,
              "slug": "mec-diesel",
              "brand": "MEC-DIESEL",
              "aliasesList": ["MEC-DIESEL", "MEC-DISEL"],
              "typesList": [2]
            }, {
              "id": 4086,
              "total": 78,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 2246,
              "total": 57,
              "slug": "tong-hong",
              "brand": "TONG HONG",
              "aliasesList": ["TONG HONG"],
              "typesList": [2]
            }, {
              "id": 2582,
              "total": 54,
              "slug": "zzvf",
              "brand": "ZZVF",
              "aliasesList": ["ZZVF (КИТАЙ)", "ZZVF"],
              "typesList": [2]
            }, {
              "id": 297,
              "total": 41,
              "slug": "beck-arnley",
              "brand": "BECK ARNLEY",
              "aliasesList": ["BECK ARNLEY", "BECK"],
              "typesList": [2]
            }, {
              "id": 720,
              "total": 35,
              "slug": "elwis-royal",
              "brand": "ELWIS ROYAL",
              "aliasesList": ["ELWIS ROYAL", "ELWIS ROIL", "ELWIS"],
              "typesList": [2]
            }, {
              "id": 1272,
              "total": 32,
              "slug": "kibi",
              "brand": "KIBI",
              "aliasesList": ["KIBI"],
              "typesList": [2]
            }, {
              "id": 718,
              "total": 27,
              "slug": "elring",
              "brand": "ELRING",
              "aliasesList": ["ELRLING", "ELRING", "ELRING TRUCK"],
              "typesList": [2]
            }, {
              "id": 2152,
              "total": 27,
              "slug": "stone",
              "brand": "STONE",
              "aliasesList": ["STONE"],
              "typesList": [2]
            }, {
              "id": 739,
              "total": 20,
              "slug": "eristic",
              "brand": "ERISTIC",
              "aliasesList": ["ERISTIC", "Eristik"],
              "typesList": [2]
            }, {
              "id": 79,
              "total": 17,
              "slug": "ajusa",
              "brand": "AJUSA",
              "aliasesList": ["AJUSA", "ADJUSA"],
              "typesList": [2]
            }, {
              "id": 803,
              "total": 17,
              "slug": "febi",
              "brand": "FEBI",
              "aliasesList": ["FEBI", "FEBI TRUCK", "FEBI BILSTEIN", "FEBI (ГЕРМАНИЯ)"],
              "typesList": [2]
            }, {
              "id": 1027,
              "total": 17,
              "slug": "hans-pries",
              "brand": "HANS PRIES",
              "aliasesList": ["HANS PRIES (ГЕРМАНИЯ)", "HANS PRIES", "HANS-PRIS", "HANS PRIES (TOPRAN)", "HANS-PRAES", "TOPRAN"],
              "typesList": [2]
            }, {
              "id": 1186,
              "total": 16,
              "slug": "jaguar",
              "brand": "JAGUAR",
              "aliasesList": ["JAGUAR", "ЯГУАР", "JAGUAR ЗАПЧАСТИ"],
              "typesList": [1]
            }, {
              "id": 1533,
              "total": 16,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1971,
              "total": 16,
              "slug": "sanwa",
              "brand": "SANWA",
              "aliasesList": ["SANWA GASKET", "ｻﾝﾜﾊﾟｯｷﾝｸﾞｺｳｷﾞｮｳ", "SANWA"],
              "typesList": [2]
            }, {
              "id": 586,
              "total": 15,
              "slug": "dello",
              "brand": "DELLO",
              "aliasesList": ["AUTOMEGA", "DELLO (ГЕРМАНИЯ)", "DELLO (AUTOMEGA)", "DELLO", "AUTOMEGADELLO"],
              "typesList": [2]
            }, {
              "id": 498,
              "total": 13,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 1375,
              "total": 12,
              "slug": "lex",
              "brand": "LEX",
              "aliasesList": ["LEX"],
              "typesList": [2]
            }, {
              "id": 1976,
              "total": 12,
              "slug": "sat",
              "brand": "SAT",
              "aliasesList": ["SAT-TYG", "SATA", "SAT (КИТАЙ)", "SAT", "SAT/TYC", "SAT PREMIUM"],
              "typesList": [2]
            }, {
              "id": 1778,
              "total": 10,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }, {
              "id": 16351,
              "total": 9,
              "slug": "parts-mall",
              "brand": "PARTS-MALL",
              "aliasesList": ["PARTS", "PARTS-MALL", "PART-MALL"],
              "typesList": [2]
            }, {
              "id": 490,
              "total": 8,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 8,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1542,
              "total": 8,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 2146,
              "total": 8,
              "slug": "stellox",
              "brand": "STELLOX",
              "aliasesList": ["STELLOX", "STELOX"],
              "typesList": [2]
            }, {
              "id": 209,
              "total": 7,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 1328,
              "total": 7,
              "slug": "kross",
              "brand": "KROSS",
              "aliasesList": ["KROSS"],
              "typesList": [2]
            }, {
              "id": 1652,
              "total": 7,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 863,
              "total": 6,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 2184,
              "total": 6,
              "slug": "tangde",
              "brand": "TANGDE",
              "aliasesList": ["TANGDE", "TD"],
              "typesList": [2]
            }, {
              "id": 189,
              "total": 5,
              "slug": "at",
              "brand": "AT",
              "aliasesList": ["AT", "AUTO TECHNOLOGIES GROUP", "AUTO TECHNOLOGIES GROUP S.R.O."],
              "typesList": [2]
            }, {
              "id": 1368,
              "total": 5,
              "slug": "lema",
              "brand": "LEMA",
              "aliasesList": ["LEMA (ГЕРМАНИЯ)", "LEMA"],
              "typesList": [2]
            }, {
              "id": 2374,
              "total": 5,
              "slug": "vika",
              "brand": "VIKA",
              "aliasesList": ["VIKA"],
              "typesList": [2]
            }, {
              "id": 211,
              "total": 4,
              "slug": "auger",
              "brand": "AUGER",
              "aliasesList": ["AUGER (ГЕРМАНИЯ)", "AUGER", "AUGER AUTOTECHNIK"],
              "typesList": [2]
            }, {
              "id": 1180,
              "total": 4,
              "slug": "iveco",
              "brand": "IVECO",
              "aliasesList": ["IVECO", "OE IVECO", "IVECO OE", "IVECO-DAILY"],
              "typesList": [1]
            }, {
              "id": 1516,
              "total": 4,
              "slug": "meyle",
              "brand": "MEYLE",
              "aliasesList": ["MEYLI", "MAYLI", "MEYLE (ГЕРМАНИЯ)", "MEYLE", "MYELE", "MAYLE"],
              "typesList": [2]
            }, {
              "id": 1606,
              "total": 4,
              "slug": "nakamoto",
              "brand": "NAKAMOTO",
              "aliasesList": ["NAKANOTO", "NAKAMOTO (ТАЙВАНЬ)", "NAKAMOTO"],
              "typesList": [2]
            }, {
              "id": 1755,
              "total": 4,
              "slug": "patron",
              "brand": "PATRON",
              "aliasesList": ["PATRON"],
              "typesList": [2]
            }, {
              "id": 559,
              "total": 3,
              "slug": "daewoo",
              "brand": "DAEWOO",
              "aliasesList": ["UZ-DAEWOO", "DAEWOO (КОРЕЯ)", "DAEWOO MOTOR", "DAEWOOPARTS, PMC", "DAEWOO"],
              "typesList": [1, 4]
            }, {
              "id": 629,
              "total": 3,
              "slug": "dominant",
              "brand": "DOMINANT",
              "aliasesList": ["DOMINANT"],
              "typesList": [2]
            }, {
              "id": 818,
              "total": 3,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 1092,
              "total": 3,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 1313,
              "total": 3,
              "slug": "kp",
              "brand": "KP",
              "aliasesList": ["KP", "KOKUSAN PARTS"],
              "typesList": [2]
            }, {
              "id": 1354,
              "total": 3,
              "slug": "land-rover",
              "brand": "LAND ROVER",
              "aliasesList": ["ROVER/LAND ROVER", "LAND ROVER", "ROVER/LANDROVER/MG", "LR ЗАПЧАСТИ", "ЛЭНД РОВЕР", "LAND ROVER", "OE LAND ROVER"],
              "typesList": [1]
            }, {
              "id": 1814,
              "total": 3,
              "slug": "porsche",
              "brand": "PORSCHE",
              "aliasesList": ["ПОРШЕ", "PORSCHE", "VW-PORSCHE"],
              "typesList": [1]
            }, {
              "id": 2370,
              "total": 3,
              "slug": "victor-reinz",
              "brand": "VICTOR REINZ",
              "aliasesList": ["VICTOR REINZ TRUCK", "VIKTOR REINZ", "REINZ", "REINZ VICTOR", "REINZ-DICHTUNGS-GMBH&CO", "VICTOR REINZ"],
              "typesList": [2]
            }, {
              "id": 491,
              "total": 2,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 533,
              "total": 2,
              "slug": "corteco",
              "brand": "CORTECO",
              "aliasesList": ["CORTECO (ГЕРМАНИЯ)", "ISEAL (CORTECO)", "CORTECO"],
              "typesList": [2]
            }, {
              "id": 609,
              "total": 2,
              "slug": "diesel-technic",
              "brand": "DIESEL TECHNIC",
              "aliasesList": ["DT-DIESEL TECHNIC", "DT (ГЕРМАНИЯ)", "DT", "DIESEL TECHNIC", "DT SPARE PARTS"],
              "typesList": [2]
            }, {
              "id": 668,
              "total": 2,
              "slug": "dyg",
              "brand": "DYG",
              "aliasesList": ["DYG", "DYG KOREA"],
              "typesList": [2]
            }, {
              "id": 873,
              "total": 2,
              "slug": "francecar",
              "brand": "FRANCECAR",
              "aliasesList": ["FRANCECAR"],
              "typesList": [2]
            }, {
              "id": 926,
              "total": 2,
              "slug": "geely",
              "brand": "GEELY",
              "aliasesList": ["GEELY (КИТАЙ)", "GEELY"],
              "typesList": [1]
            }, {
              "id": 955,
              "total": 2,
              "slug": "glaser",
              "brand": "GLASER",
              "aliasesList": ["GLESER", "SERVA (GLASER)", "GLASER"],
              "typesList": [2]
            }, {
              "id": 967,
              "total": 2,
              "slug": "goetze",
              "brand": "GOETZE",
              "aliasesList": ["GOETZE ENGINE", "GOETZE", "GOETZE TRUCK"],
              "typesList": [2]
            }, {
              "id": 987,
              "total": 2,
              "slug": "great-wall",
              "brand": "GREAT WALL",
              "aliasesList": ["GREAT WALL MOTORS ", "GREAT WALL (КИТАЙ)", " GREAT WALL", "GREAT WOOL", "GREAT WALL", "GW"],
              "typesList": [1]
            }, {
              "id": 1483,
              "total": 2,
              "slug": "mcbee",
              "brand": "MCBEE",
              "aliasesList": ["MCBEE"],
              "typesList": [2]
            }, {
              "id": 1713,
              "total": 2,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 2,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 1959,
              "total": 2,
              "slug": "sampa",
              "brand": "SAMPA",
              "aliasesList": ["SAMPA"],
              "typesList": [2]
            }, {
              "id": 2287,
              "total": 2,
              "slug": "trucktec-automotive",
              "brand": "TRUCKTEC AUTOMOTIVE",
              "aliasesList": ["TRUCKTEC AUTOMOTIVE", "TRUCKTEC"],
              "typesList": [2]
            }, {
              "id": 4065,
              "total": 2,
              "slug": "tork",
              "brand": "TORK",
              "aliasesList": ["TORK"],
              "typesList": [2]
            }, {
              "id": 5,
              "total": 1,
              "slug": "4max",
              "brand": "4MAX",
              "aliasesList": ["4MAX"],
              "typesList": [2]
            }, {
              "id": 172,
              "total": 1,
              "slug": "asam-sa",
              "brand": "ASAM-SA",
              "aliasesList": ["ASAM-SA", "ASAM"],
              "typesList": [2]
            }, {
              "id": 239,
              "total": 1,
              "slug": "autoplus",
              "brand": "AUTOPLUS",
              "aliasesList": ["AUTOPLUS"],
              "typesList": [2]
            }, {
              "id": 248,
              "total": 1,
              "slug": "auto-techteile",
              "brand": "AUTO TECHTEILE",
              "aliasesList": ["AUTO TECHTEILE"],
              "typesList": [2]
            }, {
              "id": 296,
              "total": 1,
              "slug": "bearmach",
              "brand": "BEARMACH",
              "aliasesList": ["BEARMACH"],
              "typesList": [2]
            }, {
              "id": 307,
              "total": 1,
              "slug": "bentley",
              "brand": "BENTLEY",
              "aliasesList": ["BENTLEY"],
              "typesList": [1]
            }, {
              "id": 560,
              "total": 1,
              "slug": "daf",
              "brand": "DAF",
              "aliasesList": ["LEYLAND AUTO", "DAF", "DAF OE", "LDV", "LDV LIMITED", "LEYLAND", "LEYLAND-DAF"],
              "typesList": [1]
            }, {
              "id": 565,
              "total": 1,
              "slug": "daihatsu",
              "brand": "DAIHATSU",
              "aliasesList": ["DAIHATSU", "DAIHATSU (ЯПОНИЯ)"],
              "typesList": [1]
            }, {
              "id": 633,
              "total": 1,
              "slug": "dongfeng",
              "brand": "DONGFENG",
              "aliasesList": ["DONGFENG (DFM)", "DONGFENG", "FENGXING (DONGFENG )", "DONFENG", "DONGFENG (DFAC)", "DONGFENG (DFAM)", "DONGFENG (DFL)", "DONGFENG FENGDU", "DONGFENG XIAOKANG"],
              "typesList": [1]
            }, {
              "id": 920,
              "total": 1,
              "slug": "gaz",
              "brand": "GAZ",
              "aliasesList": ["ГАЗ 3302", "ГАЗ 2705", "ГАЗ 52", "ГАЗ 2217", "ГАЗ", "ВОЛГА-ОЙЛ", "ВОЛГА", "ГАЗ 53", "ГАЗ 66", "ГАЗ ОАО", "ГАЗ ООО", "ГАЗЕЛЬ", "ВАЛДАЙ", "ГОРЬКОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД ОАО, Н.Н", "ОАО ГАЗ", "УМЗ Г.УЛЬЯНОВСК", "УМЗ Г", "VOLGA", "УМЗ", "GAZ", "ГАЗ 4301", "ГАЗ 3309", "ГАЗ 3308", "ГАЗ 3307", "OE GAZ"],
              "typesList": [1, 3]
            }, {
              "id": 1020,
              "total": 1,
              "slug": "han",
              "brand": "HAN",
              "aliasesList": ["HANGIL", "HAN"],
              "typesList": [2]
            }, {
              "id": 1073,
              "total": 1,
              "slug": "hino",
              "brand": "HINO",
              "aliasesList": ["HINO"],
              "typesList": [1]
            }, {
              "id": 1100,
              "total": 1,
              "slug": "howo",
              "brand": "HOWO",
              "aliasesList": ["HOWO", "HOWO/SHAANXI"],
              "typesList": [2]
            }, {
              "id": 1173,
              "total": 1,
              "slug": "isuzu",
              "brand": "ISUZU",
              "aliasesList": ["ISUZU"],
              "typesList": [1]
            }, {
              "id": 1189,
              "total": 1,
              "slug": "japan-cars",
              "brand": "JAPAN CARS",
              "aliasesList": ["JAPAN CARS", "JAPAN CARS (ЯПОНИЯ)", "JC"],
              "typesList": [2]
            }, {
              "id": 1198,
              "total": 1,
              "slug": "jeep",
              "brand": "JEEP",
              "aliasesList": ["JEEP"],
              "typesList": [2]
            }, {
              "id": 1217,
              "total": 1,
              "slug": "jp-group",
              "brand": "JP GROUP",
              "aliasesList": ["JP GROUP (ДАНИЯ)", "JPG", "JP GROUPE", "JP GROUP", "GP GROUP", "JP"],
              "typesList": [2]
            }, {
              "id": 1243,
              "total": 1,
              "slug": "kap",
              "brand": "KAP",
              "aliasesList": ["KAP", "KAP VOLUE"],
              "typesList": [2]
            }, {
              "id": 1331,
              "total": 1,
              "slug": "ksp",
              "brand": "KSP",
              "aliasesList": ["KSP"],
              "typesList": [2]
            }, {
              "id": 1382,
              "total": 1,
              "slug": "lifan",
              "brand": "LIFAN",
              "aliasesList": ["LIFAN", "ЛИФАН"],
              "typesList": [1]
            }, {
              "id": 1481,
              "total": 1,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 1734,
              "total": 1,
              "slug": "ossca",
              "brand": "OSSCA",
              "aliasesList": ["OSSCA", "OSSKA"],
              "typesList": [2]
            }, {
              "id": 1756,
              "total": 1,
              "slug": "payen",
              "brand": "PAYEN",
              "aliasesList": ["PAYEN -GLICO", "PAYEN", "PAYEN TRUCK"],
              "typesList": [2]
            }, {
              "id": 1896,
              "total": 1,
              "slug": "rheejin",
              "brand": "RHEEJIN",
              "aliasesList": ["RHEEJIN", "RHEEJIN METAL", "RHEE JIN KOREA", "RHEEJIN (NG) KOREA"],
              "typesList": [2]
            }, {
              "id": 2294,
              "total": 1,
              "slug": "citron",
              "brand": "ЦИТРОН",
              "aliasesList": ["TSN-ЦИТРОН", "ОАО ЦИТРОН", "ОАО ЦИТРОН Г", "РОССИЯ,TSN", "CITRON", "ТАЙВАНЬ,TSN", "МЕХАНИК", "TSITRON", "ЦИТРОН (Г", "TSN", "ЦИТРОН КОНЦЕРН ОАО, МОСКВА", "TSN (РОССИЯ)", "TSN TRUCK", "ЦИТРОНМЕХАНИК", "ЦИТРОН"],
              "typesList": [2]
            }, {
              "id": 2314,
              "total": 1,
              "slug": "uaz",
              "brand": "UAZ",
              "aliasesList": ["УЛЬЯНОВСК", "ОАО УЛЬЯНОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД", "UAZ", "УАЗ", "УЛЬЯНОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД ОАО, Г.У", "УЛЬЯНОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД", "ОАО УАЗ Г", "ОАО УАЗ Г.УЛЬЯНОВСК", "ООО УАЗ-АВТОКОМПОНЕНТ", "ОАО УАЗ", "УАЗ ОАО", "OE UAZ"],
              "typesList": [1]
            }, {
              "id": 2353,
              "total": 1,
              "slug": "vaz",
              "brand": "VAZ",
              "aliasesList": ["АВТОВАЗ\" ОАО Г", "\"АВТОВАЗ\" ОАО Г. ТОЛЬЯТТИ", "LADA", "LADA (РОССИЯ)", "LADA IMAGE", "AVTOVAZ", "ВАЗ ЛАДА", "ВАЗ Г", "ВАЗ (САМАРА)", "ВАЗ (ОАО АВТОВАЗ Г", "ВАЗ", "VAZ", "ЛАДА", "АО ЛАДА-ИМИДЖ", "АО ВАЗ", "АО «ЛАДА ИМИДЖ»", "НИВА", "АВТОВАЗ", "ОАО АВТОВАЗ", "ОАО АВТОВАЗ (РЕМКОМПЛЕКТЫ)", "ОАО ВАЗ", "GM-AVTOVAZ", "АВТОВАЗ ОАО"],
              "typesList": [1]
            }, {
              "id": 2387,
              "total": 1,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 2576,
              "total": 1,
              "slug": "5-825",
              "brand": "5'825",
              "aliasesList": ["OHNO", "ｵｵﾉｺﾞﾑｺｳｷﾞｮｳ", "5'825"],
              "typesList": [2]
            }, {
              "id": 4480,
              "total": 1,
              "slug": "dollex",
              "brand": "DOLLEX",
              "aliasesList": ["DOLLEX"],
              "typesList": [2]
            }, {
              "id": 4538,
              "total": 1,
              "slug": "rosteco",
              "brand": "ROSTECO",
              "aliasesList": ["ROSTECO"],
              "typesList": [2]
            }, {
              "id": 4604,
              "total": 1,
              "slug": "brt",
              "brand": "БРТ",
              "aliasesList": ["ОАО БРТ", "БРТ Г", "БАЛАКОВОРЕЗИНОТЕХНИКА (ОАО БРТ)", "ОАО БАЛАКОВОРЕЗИНОТЕХНИКА", "БАЛАКОВО", "БАЛАКОВОРЕЗИНОТЕХНИКА ОАО", "БАЛАКОВОРЕЗИНОТЕХНИКА ОАО, БАЛАКОВО", "БРТ"],
              "typesList": [2]
            }, {
              "id": 5236,
              "total": 1,
              "slug": "blumaq",
              "brand": "BLUMAQ",
              "aliasesList": ["BLUMAQ", "BLUMAQ CASTELLУN CENTRAL"],
              "typesList": [2]
            }, {
              "id": 5249,
              "total": 1,
              "slug": "bcm_1",
              "brand": "БЦМ",
              "aliasesList": ["БЦМ"],
              "typesList": [2]
            }, {
              "id": 18305,
              "total": 1,
              "slug": "oem-genuine-gasket",
              "brand": "OEM GENUINE GASKET",
              "aliasesList": ["OEM GENUINE GASKET"],
              "typesList": [2]
            }]
          }],
          "total": 3
        }, {
          "id": 10004,
          "name": "Запчасти дроссельной заслонки",
          "slug": "zapchasti-drosselnoj-zaslonki",
          "groupsList": [{
            "id": 25093253,
            "name": "Блок дроссельной заслонки",
            "slug": "blok-drosselnoj-zaslonki",
            "groupsList": [10004],
            "manufacturers": [{
              "id": 209,
              "total": 129,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 37,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1501,
              "total": 15,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 1734,
              "total": 6,
              "slug": "ossca",
              "brand": "OSSCA",
              "aliasesList": ["OSSCA", "OSSKA"],
              "typesList": [2]
            }, {
              "id": 1789,
              "total": 6,
              "slug": "pierburg",
              "brand": "PIERBURG",
              "aliasesList": ["PIERBURG", "Pierburg truck", "PIERBURG FRANCE"],
              "typesList": [2]
            }, {
              "id": 2374,
              "total": 6,
              "slug": "vika",
              "brand": "VIKA",
              "aliasesList": ["VIKA"],
              "typesList": [2]
            }, {
              "id": 296,
              "total": 3,
              "slug": "bearmach",
              "brand": "BEARMACH",
              "aliasesList": ["BEARMACH"],
              "typesList": [2]
            }, {
              "id": 491,
              "total": 2,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 1533,
              "total": 2,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1713,
              "total": 2,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 1778,
              "total": 2,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }, {
              "id": 2332,
              "total": 2,
              "slug": "united-motors",
              "brand": "UNITED MOTORS",
              "aliasesList": ["UNITED MOTORS"],
              "typesList": [2]
            }, {
              "id": 2354,
              "total": 2,
              "slug": "vdo",
              "brand": "VDO",
              "aliasesList": ["VDO", "SIEMENS VDO", "VDO SIEMENS", "SIEMENS"],
              "typesList": [2]
            }, {
              "id": 586,
              "total": 1,
              "slug": "dello",
              "brand": "DELLO",
              "aliasesList": ["AUTOMEGA", "DELLO (ГЕРМАНИЯ)", "DELLO (AUTOMEGA)", "DELLO", "AUTOMEGADELLO"],
              "typesList": [2]
            }, {
              "id": 1652,
              "total": 1,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 1755,
              "total": 1,
              "slug": "patron",
              "brand": "PATRON",
              "aliasesList": ["PATRON"],
              "typesList": [2]
            }, {
              "id": 1947,
              "total": 1,
              "slug": "sachs",
              "brand": "SACHS",
              "aliasesList": ["SACHS (ZF SRE)", "SACHS / ZF", "SACHS", "SACHS (ГЕРМАНИЯ)", "SACHS TRUCK"],
              "typesList": [2]
            }, {
              "id": 2168,
              "total": 1,
              "slug": "swag",
              "brand": "SWAG",
              "aliasesList": ["SWAG"],
              "typesList": [2]
            }]
          }, {
            "id": 25089119,
            "name": "Датчик положения дроссельной заслонки",
            "slug": "datchik-polozheniya-drosselnoj-zaslonki_1",
            "groupsList": [10004],
            "manufacturers": [{
              "id": 863,
              "total": 34,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 1652,
              "total": 27,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 366,
              "total": 21,
              "slug": "bosch",
              "brand": "BOSCH",
              "aliasesList": ["BOSCH TRUCK", "BOSCH (ГЕРМАНИЯ)", "РОБЕРТБОШ", "ROBERT BOSCH", "BOSCH (OEM) GERMANY", "BOSH", "BOSCH DIAGNOSTICS", "BOSCH MOBA, ГЕРМАНИЯ", "BOSCH", "BOSCH BATTERY"],
              "typesList": [2]
            }, {
              "id": 735,
              "total": 19,
              "slug": "era",
              "brand": "ERA",
              "aliasesList": ["ERA BENELUX", "ERA", "ERA ELETTRO"],
              "typesList": [2]
            }, {
              "id": 929,
              "total": 19,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 2134,
              "total": 19,
              "slug": "standard",
              "brand": "STANDARD",
              "aliasesList": ["STANDARD", "STANDARD MOTOR PRODUCTS", "STANDART"],
              "typesList": [2]
            }, {
              "id": 1542,
              "total": 18,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 1487,
              "total": 17,
              "slug": "meat-doria",
              "brand": "MEAT & DORIA",
              "aliasesList": ["MEAT&amp;DORIA", "MEAT AND DORIA", "MEAT & DORIA"],
              "typesList": [2]
            }, {
              "id": 1976,
              "total": 16,
              "slug": "sat",
              "brand": "SAT",
              "aliasesList": ["SAT-TYG", "SATA", "SAT (КИТАЙ)", "SAT", "SAT/TYC", "SAT PREMIUM"],
              "typesList": [2]
            }, {
              "id": 2582,
              "total": 16,
              "slug": "zzvf",
              "brand": "ZZVF",
              "aliasesList": ["ZZVF (КИТАЙ)", "ZZVF"],
              "typesList": [2]
            }, {
              "id": 498,
              "total": 15,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 1541,
              "total": 15,
              "slug": "mobiletron",
              "brand": "MOBILETRON",
              "aliasesList": ["MOBILITRON", "MOBLETRON", "MOBILETRON"],
              "typesList": [2]
            }, {
              "id": 732,
              "total": 13,
              "slug": "eps",
              "brand": "EPS",
              "aliasesList": ["EPS (ИТАЛИЯ)", "EPS(FACET)", "EPS"],
              "typesList": [2]
            }, {
              "id": 587,
              "total": 12,
              "slug": "delphi",
              "brand": "DELPHI",
              "aliasesList": ["DELPHI KOREA", "ﾃﾞﾙﾌｧｲ", "DELPHI", "DELPHI ELECTRICAL", "DELPHI DIESEL", "DELPHI [LOCKHEED AP]", "DELPHI (США)", "DELPHI (OEM) USA", "DELFI", "LOCKHEED (DELPHI)"],
              "typesList": [2]
            }, {
              "id": 4086,
              "total": 12,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 2166,
              "total": 11,
              "slug": "suzuki",
              "brand": "SUZUKI",
              "aliasesList": ["SUZUKI (ЯПОНИЯ)", "SUZUKI MOTO", "SUZUKI MOTORCYCLES", "CHANGHE SUZUKI", "SUZUKI ORGNL", "SUZUKI ORIGINAL", "SUZUKI ЗАПЧАСТИ", "SUZUKIM", "SUZUKI", "OE SUZUKI", "CHANGAN SUZUKI"],
              "typesList": [1]
            }, {
              "id": 2146,
              "total": 10,
              "slug": "stellox",
              "brand": "STELLOX",
              "aliasesList": ["STELLOX", "STELOX"],
              "typesList": [2]
            }, {
              "id": 2446,
              "total": 10,
              "slug": "yamaha",
              "brand": "YAMAHA",
              "aliasesList": ["YAMAHA", "YAMAHA MOTORCYCLES"],
              "typesList": [2]
            }, {
              "id": 76,
              "total": 9,
              "slug": "aisan",
              "brand": "AISAN",
              "aliasesList": ["AISAN"],
              "typesList": [2]
            }, {
              "id": 209,
              "total": 8,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 2294,
              "total": 8,
              "slug": "citron",
              "brand": "ЦИТРОН",
              "aliasesList": ["TSN-ЦИТРОН", "ОАО ЦИТРОН", "ОАО ЦИТРОН Г", "РОССИЯ,TSN", "CITRON", "ТАЙВАНЬ,TSN", "МЕХАНИК", "TSITRON", "ЦИТРОН (Г", "TSN", "ЦИТРОН КОНЦЕРН ОАО, МОСКВА", "TSN (РОССИЯ)", "TSN TRUCK", "ЦИТРОНМЕХАНИК", "ЦИТРОН"],
              "typesList": [2]
            }, {
              "id": 629,
              "total": 7,
              "slug": "dominant",
              "brand": "DOMINANT",
              "aliasesList": ["DOMINANT"],
              "typesList": [2]
            }, {
              "id": 1481,
              "total": 7,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 1916,
              "total": 7,
              "slug": "roers-parts",
              "brand": "ROERS PARTS",
              "aliasesList": ["ROERS PARTS"],
              "typesList": [2]
            }, {
              "id": 4206,
              "total": 7,
              "slug": "quattro-freni",
              "brand": "QUATTRO FRENI",
              "aliasesList": ["QUATTRO FRENI", "QUATRO", "QF"],
              "typesList": [2]
            }, {
              "id": 300,
              "total": 6,
              "slug": "behr-hella",
              "brand": "BEHR (HELLA)",
              "aliasesList": ["BEHR-HELLA (ГЕРМАНИЯ)", "BEHR_", "BEHR THERMOT-TRONIK", "BEHR SERVICE", "BEHR HELLA SERVICE TRUCK", "BEHR HELLA SERVICE", "BEHR (HELLA)", "HELLA/BEHR-HELLA", "HELLA TRUCK", "HELLA (ГЕРМАНИЯ)", "HELLA"],
              "typesList": [2]
            }, {
              "id": 575,
              "total": 6,
              "slug": "dashi",
              "brand": "DASHI",
              "aliasesList": ["DASHI"],
              "typesList": [2]
            }, {
              "id": 1083,
              "total": 6,
              "slug": "hoffer",
              "brand": "HOFFER",
              "aliasesList": ["HOFFER"],
              "typesList": [2]
            }, {
              "id": 1814,
              "total": 6,
              "slug": "porsche",
              "brand": "PORSCHE",
              "aliasesList": ["ПОРШЕ", "PORSCHE", "VW-PORSCHE"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 6,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 4308,
              "total": 6,
              "slug": "asparts",
              "brand": "ASPARTS",
              "aliasesList": ["ASPARTS"],
              "typesList": [2]
            }, {
              "id": 24,
              "total": 5,
              "slug": "ac-delco",
              "brand": "AC DELCO",
              "aliasesList": ["ALDELCO", "ACDELCO"],
              "typesList": [2]
            }, {
              "id": 586,
              "total": 5,
              "slug": "dello",
              "brand": "DELLO",
              "aliasesList": ["AUTOMEGA", "DELLO (ГЕРМАНИЯ)", "DELLO (AUTOMEGA)", "DELLO", "AUTOMEGADELLO"],
              "typesList": [2]
            }, {
              "id": 1092,
              "total": 5,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 1513,
              "total": 5,
              "slug": "metzger",
              "brand": "METZGER",
              "aliasesList": ["METZGER"],
              "typesList": [2]
            }, {
              "id": 2156,
              "total": 5,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 1173,
              "total": 4,
              "slug": "isuzu",
              "brand": "ISUZU",
              "aliasesList": ["ISUZU"],
              "typesList": [1]
            }, {
              "id": 1435,
              "total": 4,
              "slug": "magneti-marelli",
              "brand": "MAGNETI MARELLI",
              "aliasesList": ["MAGNETTI MARELI", "MAGNETI MARELI", "MAGNETI", "MAG", "M-MARELLI", "MAGNETI MARELLI", "MARELLI", "MARELLI EXCHANGE", "MARELLI MAGNETI", "MAGNETTI MARELLI", "MAGNETTI"],
              "typesList": [2]
            }, {
              "id": 1570,
              "total": 4,
              "slug": "motorcraft",
              "brand": "MOTORCRAFT",
              "aliasesList": ["ﾓｰﾀｰｸﾗﾌﾄ", "MOTORCRAFT"],
              "typesList": [2]
            }, {
              "id": 1940,
              "total": 4,
              "slug": "ruei",
              "brand": "RUEI",
              "aliasesList": ["RUEI"],
              "typesList": [2]
            }, {
              "id": 2353,
              "total": 4,
              "slug": "vaz",
              "brand": "VAZ",
              "aliasesList": ["АВТОВАЗ\" ОАО Г", "\"АВТОВАЗ\" ОАО Г. ТОЛЬЯТТИ", "LADA", "LADA (РОССИЯ)", "LADA IMAGE", "AVTOVAZ", "ВАЗ ЛАДА", "ВАЗ Г", "ВАЗ (САМАРА)", "ВАЗ (ОАО АВТОВАЗ Г", "ВАЗ", "VAZ", "ЛАДА", "АО ЛАДА-ИМИДЖ", "АО ВАЗ", "АО «ЛАДА ИМИДЖ»", "НИВА", "АВТОВАЗ", "ОАО АВТОВАЗ", "ОАО АВТОВАЗ (РЕМКОМПЛЕКТЫ)", "ОАО ВАЗ", "GM-AVTOVAZ", "АВТОВАЗ ОАО"],
              "typesList": [1]
            }, {
              "id": 2358,
              "total": 4,
              "slug": "vemo-vaico",
              "brand": "VEMO (VAICO)",
              "aliasesList": ["VIEROL AG", "VIEROL", "VEMO", "VAICO/VEMO", "VAICO", "VEMO (VAICO)"],
              "typesList": [2]
            }, {
              "id": 347,
              "total": 3,
              "slug": "blue-print",
              "brand": "BLUE PRINT",
              "aliasesList": ["BLUE PRINT (ВЕЛИКОБРИТАНИЯ)", "BLUE PRINT", "ACL"],
              "typesList": [2]
            }, {
              "id": 565,
              "total": 3,
              "slug": "daihatsu",
              "brand": "DAIHATSU",
              "aliasesList": ["DAIHATSU", "DAIHATSU (ЯПОНИЯ)"],
              "typesList": [1]
            }, {
              "id": 782,
              "total": 3,
              "slug": "facet",
              "brand": "FACET",
              "aliasesList": ["FACET (ИТАЛИЯ)", "FACET"],
              "typesList": [2]
            }, {
              "id": 1189,
              "total": 3,
              "slug": "japan-cars",
              "brand": "JAPAN CARS",
              "aliasesList": ["JAPAN CARS", "JAPAN CARS (ЯПОНИЯ)", "JC"],
              "typesList": [2]
            }, {
              "id": 1325,
              "total": 3,
              "slug": "kroner",
              "brand": "KRONER",
              "aliasesList": ["KRONER (KRONERAUTO)", "KRONER"],
              "typesList": [2]
            }, {
              "id": 1451,
              "total": 3,
              "slug": "manover",
              "brand": "MANOVER",
              "aliasesList": ["MANOVER"],
              "typesList": [2]
            }, {
              "id": 1533,
              "total": 3,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1550,
              "total": 3,
              "slug": "mopar-parts",
              "brand": "MOPAR PARTS",
              "aliasesList": ["MOPAR,КАНАДА", "MOPAR PARTS", "MOPAR"],
              "typesList": [2]
            }, {
              "id": 1755,
              "total": 3,
              "slug": "patron",
              "brand": "PATRON",
              "aliasesList": ["PATRON"],
              "typesList": [2]
            }, {
              "id": 1778,
              "total": 3,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }, {
              "id": 1789,
              "total": 3,
              "slug": "pierburg",
              "brand": "PIERBURG",
              "aliasesList": ["PIERBURG", "Pierburg truck", "PIERBURG FRANCE"],
              "typesList": [2]
            }, {
              "id": 2387,
              "total": 3,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 2410,
              "total": 3,
              "slug": "wells",
              "brand": "WELLS",
              "aliasesList": ["ADVANTECH", "WELLS"],
              "typesList": [2]
            }, {
              "id": 18153,
              "total": 3,
              "slug": "auto-7",
              "brand": "AUTO 7",
              "aliasesList": ["AUTO 7"],
              "typesList": [2]
            }, {
              "id": 818,
              "total": 2,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 873,
              "total": 2,
              "slug": "francecar",
              "brand": "FRANCECAR",
              "aliasesList": ["FRANCECAR"],
              "typesList": [2]
            }, {
              "id": 926,
              "total": 2,
              "slug": "geely",
              "brand": "GEELY",
              "aliasesList": ["GEELY (КИТАЙ)", "GEELY"],
              "typesList": [1]
            }, {
              "id": 1063,
              "total": 2,
              "slug": "herth-buss-jakoparts",
              "brand": "HERTH + BUSS JAKOPARTS",
              "aliasesList": ["H+B JAKOPARTS", "JAKOPARTS", "JAKOPARTS (NIPPARTS) HERTH+BUSS", "HEPTH BUSS", "HBJAKOPART", "JAKOPARTS (NIPPARTS)", "HERTH + BUSS JAKOPARTS"],
              "typesList": [2]
            }, {
              "id": 1217,
              "total": 2,
              "slug": "jp-group",
              "brand": "JP GROUP",
              "aliasesList": ["JP GROUP (ДАНИЯ)", "JPG", "JP GROUPE", "JP GROUP", "GP GROUP", "JP"],
              "typesList": [2]
            }, {
              "id": 1354,
              "total": 2,
              "slug": "land-rover",
              "brand": "LAND ROVER",
              "aliasesList": ["ROVER/LAND ROVER", "LAND ROVER", "ROVER/LANDROVER/MG", "LR ЗАПЧАСТИ", "ЛЭНД РОВЕР", "LAND ROVER", "OE LAND ROVER"],
              "typesList": [1]
            }, {
              "id": 1400,
              "total": 2,
              "slug": "logem",
              "brand": "LOGEM",
              "aliasesList": ["LOGEM"],
              "typesList": [2]
            }, {
              "id": 1516,
              "total": 2,
              "slug": "meyle",
              "brand": "MEYLE",
              "aliasesList": ["MEYLI", "MAYLI", "MEYLE (ГЕРМАНИЯ)", "MEYLE", "MYELE", "MAYLE"],
              "typesList": [2]
            }, {
              "id": 1734,
              "total": 2,
              "slug": "ossca",
              "brand": "OSSCA",
              "aliasesList": ["OSSCA", "OSSKA"],
              "typesList": [2]
            }, {
              "id": 1863,
              "total": 2,
              "slug": "raon",
              "brand": "RAON",
              "aliasesList": ["RAON AUTO", "RAON"],
              "typesList": [2]
            }, {
              "id": 2470,
              "total": 2,
              "slug": "zaz",
              "brand": "ZAZ",
              "aliasesList": ["ЗАЗ / CHANCE", "ЗАЗ", "ZAZ", "CHANCE"],
              "typesList": [2]
            }, {
              "id": 2514,
              "total": 2,
              "slug": "startvolt_1",
              "brand": "STARTVOLT",
              "aliasesList": ["STARTVOLT"],
              "typesList": [2]
            }, {
              "id": 4155,
              "total": 2,
              "slug": "startvolt",
              "brand": "СТАРТВОЛЬТ",
              "aliasesList": ["СТАРВОЛЬТ", "СТАРТВОЛЬТ"],
              "typesList": [2]
            }, {
              "id": 4178,
              "total": 2,
              "slug": "soate",
              "brand": "СОАТЭ",
              "aliasesList": ["СОАТЭ Г.СТАРЫЙ ОСКОЛ", "СОАТЭ", "ЗАО СОАТЭ", "ЗАО\"СОАТЭ\"Г", "СОАТЭ ОАО, СТАРЫЙ ОСКОЛ", "СОАТЭ Г"],
              "typesList": [2]
            }, {
              "id": 4403,
              "total": 2,
              "slug": "deko",
              "brand": "DEKO",
              "aliasesList": ["DEKO", "DEKO/RUYAN"],
              "typesList": [2]
            }, {
              "id": 4579,
              "total": 2,
              "slug": "hofer",
              "brand": "HOFER",
              "aliasesList": ["HOFER ГЕРМАНИЯ", "HOFER"],
              "typesList": [2]
            }, {
              "id": 4792,
              "total": 2,
              "slug": "nsp",
              "brand": "NSP",
              "aliasesList": ["NSP"],
              "typesList": [2]
            }, {
              "id": 27,
              "total": 1,
              "slug": "achim",
              "brand": "ACHIM",
              "aliasesList": ["ACHIM"],
              "typesList": [2]
            }, {
              "id": 119,
              "total": 1,
              "slug": "amd",
              "brand": "AMD",
              "aliasesList": ["AMD (КОРЕЯ)", "AMD"],
              "typesList": [2]
            }, {
              "id": 180,
              "total": 1,
              "slug": "asin",
              "brand": "ASIN",
              "aliasesList": ["ASIN"],
              "typesList": [2]
            }, {
              "id": 189,
              "total": 1,
              "slug": "at",
              "brand": "AT",
              "aliasesList": ["AT", "AUTO TECHNOLOGIES GROUP", "AUTO TECHNOLOGIES GROUP S.R.O."],
              "typesList": [2]
            }, {
              "id": 490,
              "total": 1,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 491,
              "total": 1,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 543,
              "total": 1,
              "slug": "crown",
              "brand": "CROWN",
              "aliasesList": ["CROWN"],
              "typesList": [2]
            }, {
              "id": 559,
              "total": 1,
              "slug": "daewoo",
              "brand": "DAEWOO",
              "aliasesList": ["UZ-DAEWOO", "DAEWOO (КОРЕЯ)", "DAEWOO MOTOR", "DAEWOOPARTS, PMC", "DAEWOO"],
              "typesList": [1, 4]
            }, {
              "id": 803,
              "total": 1,
              "slug": "febi",
              "brand": "FEBI",
              "aliasesList": ["FEBI", "FEBI TRUCK", "FEBI BILSTEIN", "FEBI (ГЕРМАНИЯ)"],
              "typesList": [2]
            }, {
              "id": 987,
              "total": 1,
              "slug": "great-wall",
              "brand": "GREAT WALL",
              "aliasesList": ["GREAT WALL MOTORS ", "GREAT WALL (КИТАЙ)", " GREAT WALL", "GREAT WOOL", "GREAT WALL", "GW"],
              "typesList": [1]
            }, {
              "id": 1012,
              "total": 1,
              "slug": "haima",
              "brand": "HAIMA",
              "aliasesList": ["HAIMA"],
              "typesList": [1]
            }, {
              "id": 1027,
              "total": 1,
              "slug": "hans-pries",
              "brand": "HANS PRIES",
              "aliasesList": ["HANS PRIES (ГЕРМАНИЯ)", "HANS PRIES", "HANS-PRIS", "HANS PRIES (TOPRAN)", "HANS-PRAES", "TOPRAN"],
              "typesList": [2]
            }, {
              "id": 1156,
              "total": 1,
              "slug": "inzi",
              "brand": "INZI",
              "aliasesList": ["INZI"],
              "typesList": [2]
            }, {
              "id": 1186,
              "total": 1,
              "slug": "jaguar",
              "brand": "JAGUAR",
              "aliasesList": ["JAGUAR", "ЯГУАР", "JAGUAR ЗАПЧАСТИ"],
              "typesList": [1]
            }, {
              "id": 1382,
              "total": 1,
              "slug": "lifan",
              "brand": "LIFAN",
              "aliasesList": ["LIFAN", "ЛИФАН"],
              "typesList": [1]
            }, {
              "id": 1467,
              "total": 1,
              "slug": "master-sport",
              "brand": "MASTER-SPORT",
              "aliasesList": ["MASTER-SPORT"],
              "typesList": [2]
            }, {
              "id": 1599,
              "total": 1,
              "slug": "mv-parts",
              "brand": "MV-PARTS",
              "aliasesList": ["MV-PARTS"],
              "typesList": [2]
            }, {
              "id": 1606,
              "total": 1,
              "slug": "nakamoto",
              "brand": "NAKAMOTO",
              "aliasesList": ["NAKANOTO", "NAKAMOTO (ТАЙВАНЬ)", "NAKAMOTO"],
              "typesList": [2]
            }, {
              "id": 1638,
              "total": 1,
              "slug": "ngk",
              "brand": "NGK",
              "aliasesList": ["NGK (OEM) KOREA", "NTK", "NGK", "NTK (NGK-GROUP)", "NGK SPARK PLUG CO., LTD.", "NGK SPARK PLUG", "NGK LASER PLATINUM PREMIUM", "NGK (ЮЖНАЯ КОРЕЯ)", "NGK-NTK"],
              "typesList": [2]
            }, {
              "id": 1676,
              "total": 1,
              "slug": "npa",
              "brand": "NPA",
              "aliasesList": ["NPA"],
              "typesList": [2]
            }, {
              "id": 1713,
              "total": 1,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 2168,
              "total": 1,
              "slug": "swag",
              "brand": "SWAG",
              "aliasesList": ["SWAG"],
              "typesList": [2]
            }, {
              "id": 2319,
              "total": 1,
              "slug": "ukorauto",
              "brand": "UKORAUTO",
              "aliasesList": ["UKORAUTO", "UKOR", "UKOR-AVTO"],
              "typesList": [2]
            }, {
              "id": 2495,
              "total": 1,
              "slug": "pekar",
              "brand": "PEKAR",
              "aliasesList": ["PEKAR ПА", "ПЕКАР", "PEKAR", "PEKAR-СПБ"],
              "typesList": [2]
            }, {
              "id": 2554,
              "total": 1,
              "slug": "s-tr",
              "brand": "S-TR",
              "aliasesList": ["S-TR"],
              "typesList": [2]
            }, {
              "id": 2587,
              "total": 1,
              "slug": "ssang-yong",
              "brand": "SSANG YONG",
              "aliasesList": ["SSANG YONG", "SSANGYOUNG", "OE SSANG YONG", "SSANGYONG (ОРИГИНАЛ)", "SSANG YONG MOTORS CORPORATION (КОРЕЯ", "SSANGYONG (GEN) KOREA", "ORIGINAL SSANGYONG", "SSANGYONG (КОРЕЯ)", "SY", "SANGYONG", "SSY"],
              "typesList": [1, 3]
            }, {
              "id": 4103,
              "total": 1,
              "slug": "sailing",
              "brand": "SAILING",
              "aliasesList": ["SAILING", "SAILING (КИТАЙ)"],
              "typesList": [2]
            }]
          }, {
            "id": 25091493,
            "name": "Датчик холостого хода",
            "slug": "datchik-holostogo-hoda",
            "groupsList": [10004],
            "manufacturers": [{
              "id": 1189,
              "total": 18,
              "slug": "japan-cars",
              "brand": "JAPAN CARS",
              "aliasesList": ["JAPAN CARS", "JAPAN CARS (ЯПОНИЯ)", "JC"],
              "typesList": [2]
            }, {
              "id": 498,
              "total": 11,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 735,
              "total": 7,
              "slug": "era",
              "brand": "ERA",
              "aliasesList": ["ERA BENELUX", "ERA", "ERA ELETTRO"],
              "typesList": [2]
            }, {
              "id": 1885,
              "total": 7,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 1916,
              "total": 7,
              "slug": "roers-parts",
              "brand": "ROERS PARTS",
              "aliasesList": ["ROERS PARTS"],
              "typesList": [2]
            }, {
              "id": 863,
              "total": 6,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 1652,
              "total": 6,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 2134,
              "total": 6,
              "slug": "standard",
              "brand": "STANDARD",
              "aliasesList": ["STANDARD", "STANDARD MOTOR PRODUCTS", "STANDART"],
              "typesList": [2]
            }, {
              "id": 24,
              "total": 5,
              "slug": "ac-delco",
              "brand": "AC DELCO",
              "aliasesList": ["ALDELCO", "ACDELCO"],
              "typesList": [2]
            }, {
              "id": 732,
              "total": 5,
              "slug": "eps",
              "brand": "EPS",
              "aliasesList": ["EPS (ИТАЛИЯ)", "EPS(FACET)", "EPS"],
              "typesList": [2]
            }, {
              "id": 1481,
              "total": 5,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 1976,
              "total": 5,
              "slug": "sat",
              "brand": "SAT",
              "aliasesList": ["SAT-TYG", "SATA", "SAT (КИТАЙ)", "SAT", "SAT/TYC", "SAT PREMIUM"],
              "typesList": [2]
            }, {
              "id": 76,
              "total": 4,
              "slug": "aisan",
              "brand": "AISAN",
              "aliasesList": ["AISAN"],
              "typesList": [2]
            }, {
              "id": 366,
              "total": 4,
              "slug": "bosch",
              "brand": "BOSCH",
              "aliasesList": ["BOSCH TRUCK", "BOSCH (ГЕРМАНИЯ)", "РОБЕРТБОШ", "ROBERT BOSCH", "BOSCH (OEM) GERMANY", "BOSH", "BOSCH DIAGNOSTICS", "BOSCH MOBA, ГЕРМАНИЯ", "BOSCH", "BOSCH BATTERY"],
              "typesList": [2]
            }, {
              "id": 1570,
              "total": 4,
              "slug": "motorcraft",
              "brand": "MOTORCRAFT",
              "aliasesList": ["ﾓｰﾀｰｸﾗﾌﾄ", "MOTORCRAFT"],
              "typesList": [2]
            }, {
              "id": 2156,
              "total": 4,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 189,
              "total": 3,
              "slug": "at",
              "brand": "AT",
              "aliasesList": ["AT", "AUTO TECHNOLOGIES GROUP", "AUTO TECHNOLOGIES GROUP S.R.O."],
              "typesList": [2]
            }, {
              "id": 929,
              "total": 3,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1092,
              "total": 3,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 1541,
              "total": 3,
              "slug": "mobiletron",
              "brand": "MOBILETRON",
              "aliasesList": ["MOBILITRON", "MOBLETRON", "MOBILETRON"],
              "typesList": [2]
            }, {
              "id": 1542,
              "total": 3,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 1863,
              "total": 3,
              "slug": "raon",
              "brand": "RAON",
              "aliasesList": ["RAON AUTO", "RAON"],
              "typesList": [2]
            }, {
              "id": 2582,
              "total": 3,
              "slug": "zzvf",
              "brand": "ZZVF",
              "aliasesList": ["ZZVF (КИТАЙ)", "ZZVF"],
              "typesList": [2]
            }, {
              "id": 490,
              "total": 2,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 873,
              "total": 2,
              "slug": "francecar",
              "brand": "FRANCECAR",
              "aliasesList": ["FRANCECAR"],
              "typesList": [2]
            }, {
              "id": 1533,
              "total": 2,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1778,
              "total": 2,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }, {
              "id": 2554,
              "total": 2,
              "slug": "s-tr",
              "brand": "S-TR",
              "aliasesList": ["S-TR"],
              "typesList": [2]
            }, {
              "id": 2587,
              "total": 2,
              "slug": "ssang-yong",
              "brand": "SSANG YONG",
              "aliasesList": ["SSANG YONG", "SSANGYOUNG", "OE SSANG YONG", "SSANGYONG (ОРИГИНАЛ)", "SSANG YONG MOTORS CORPORATION (КОРЕЯ", "SSANGYONG (GEN) KOREA", "ORIGINAL SSANGYONG", "SSANGYONG (КОРЕЯ)", "SY", "SANGYONG", "SSY"],
              "typesList": [1, 3]
            }, {
              "id": 4086,
              "total": 2,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 17844,
              "total": 2,
              "slug": "ooo-gruppa-omega",
              "brand": "ООО ГРУППА ОМЕГА",
              "aliasesList": ["ООО ГРУППА ОМЕГА"],
              "typesList": [2]
            }, {
              "id": 209,
              "total": 1,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 300,
              "total": 1,
              "slug": "behr-hella",
              "brand": "BEHR (HELLA)",
              "aliasesList": ["BEHR-HELLA (ГЕРМАНИЯ)", "BEHR_", "BEHR THERMOT-TRONIK", "BEHR SERVICE", "BEHR HELLA SERVICE TRUCK", "BEHR HELLA SERVICE", "BEHR (HELLA)", "HELLA/BEHR-HELLA", "HELLA TRUCK", "HELLA (ГЕРМАНИЯ)", "HELLA"],
              "typesList": [2]
            }, {
              "id": 383,
              "total": 1,
              "slug": "breckner",
              "brand": "BRECKNER",
              "aliasesList": ["BRECKNER"],
              "typesList": [2]
            }, {
              "id": 396,
              "total": 1,
              "slug": "britpart",
              "brand": "BRITPART",
              "aliasesList": ["BRITPART"],
              "typesList": [2]
            }, {
              "id": 543,
              "total": 1,
              "slug": "crown",
              "brand": "CROWN",
              "aliasesList": ["CROWN"],
              "typesList": [2]
            }, {
              "id": 629,
              "total": 1,
              "slug": "dominant",
              "brand": "DOMINANT",
              "aliasesList": ["DOMINANT"],
              "typesList": [2]
            }, {
              "id": 782,
              "total": 1,
              "slug": "facet",
              "brand": "FACET",
              "aliasesList": ["FACET (ИТАЛИЯ)", "FACET"],
              "typesList": [2]
            }, {
              "id": 1027,
              "total": 1,
              "slug": "hans-pries",
              "brand": "HANS PRIES",
              "aliasesList": ["HANS PRIES (ГЕРМАНИЯ)", "HANS PRIES", "HANS-PRIS", "HANS PRIES (TOPRAN)", "HANS-PRAES", "TOPRAN"],
              "typesList": [2]
            }, {
              "id": 1059,
              "total": 1,
              "slug": "henschel",
              "brand": "HENSCHEL",
              "aliasesList": ["HENSHEL", "HENSHEL (ГЕРМАНИЯ)", "HENSCHEL"],
              "typesList": [2]
            }, {
              "id": 1435,
              "total": 1,
              "slug": "magneti-marelli",
              "brand": "MAGNETI MARELLI",
              "aliasesList": ["MAGNETTI MARELI", "MAGNETI MARELI", "MAGNETI", "MAG", "M-MARELLI", "MAGNETI MARELLI", "MARELLI", "MARELLI EXCHANGE", "MARELLI MAGNETI", "MAGNETTI MARELLI", "MAGNETTI"],
              "typesList": [2]
            }, {
              "id": 1451,
              "total": 1,
              "slug": "manover",
              "brand": "MANOVER",
              "aliasesList": ["MANOVER"],
              "typesList": [2]
            }, {
              "id": 1487,
              "total": 1,
              "slug": "meat-doria",
              "brand": "MEAT & DORIA",
              "aliasesList": ["MEAT&amp;DORIA", "MEAT AND DORIA", "MEAT & DORIA"],
              "typesList": [2]
            }, {
              "id": 1501,
              "total": 1,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 1755,
              "total": 1,
              "slug": "patron",
              "brand": "PATRON",
              "aliasesList": ["PATRON"],
              "typesList": [2]
            }, {
              "id": 1809,
              "total": 1,
              "slug": "polcar",
              "brand": "POLCAR",
              "aliasesList": ["POLCAR (ПОЛЬША)", "POLCAR", "POLKAR"],
              "typesList": [2]
            }, {
              "id": 1934,
              "total": 1,
              "slug": "rover",
              "brand": "ROVER",
              "aliasesList": ["AUSTIN MAESTRO", "ROVER", "ROVER GPOUP", "MG ROVER", "MG MAESTRO"],
              "typesList": [2]
            }, {
              "id": 2353,
              "total": 1,
              "slug": "vaz",
              "brand": "VAZ",
              "aliasesList": ["АВТОВАЗ\" ОАО Г", "\"АВТОВАЗ\" ОАО Г. ТОЛЬЯТТИ", "LADA", "LADA (РОССИЯ)", "LADA IMAGE", "AVTOVAZ", "ВАЗ ЛАДА", "ВАЗ Г", "ВАЗ (САМАРА)", "ВАЗ (ОАО АВТОВАЗ Г", "ВАЗ", "VAZ", "ЛАДА", "АО ЛАДА-ИМИДЖ", "АО ВАЗ", "АО «ЛАДА ИМИДЖ»", "НИВА", "АВТОВАЗ", "ОАО АВТОВАЗ", "ОАО АВТОВАЗ (РЕМКОМПЛЕКТЫ)", "ОАО ВАЗ", "GM-AVTOVAZ", "АВТОВАЗ ОАО"],
              "typesList": [1]
            }, {
              "id": 2390,
              "total": 1,
              "slug": "vtr",
              "brand": "VTR",
              "aliasesList": ["VTR (ФИНЛЯНДИЯ)", "VTR"],
              "typesList": [2]
            }, {
              "id": 2410,
              "total": 1,
              "slug": "wells",
              "brand": "WELLS",
              "aliasesList": ["ADVANTECH", "WELLS"],
              "typesList": [2]
            }, {
              "id": 4206,
              "total": 1,
              "slug": "quattro-freni",
              "brand": "QUATTRO FRENI",
              "aliasesList": ["QUATTRO FRENI", "QUATRO", "QF"],
              "typesList": [2]
            }, {
              "id": 16208,
              "total": 1,
              "slug": "cartronic",
              "brand": "CARTRONIC",
              "aliasesList": ["CARTRONIC"],
              "typesList": [2]
            }, {
              "id": 17941,
              "total": 1,
              "slug": "fgup-kzta",
              "brand": "ФГУП КЗТА",
              "aliasesList": ["ФГУП КЗТА"],
              "typesList": [2]
            }]
          }, {
            "id": 25119061,
            "name": "Дроссель",
            "slug": "drossel",
            "groupsList": [10004],
            "manufacturers": [{
              "id": 209,
              "total": 19,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 1501,
              "total": 16,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 2314,
              "total": 4,
              "slug": "uaz",
              "brand": "UAZ",
              "aliasesList": ["УЛЬЯНОВСК", "ОАО УЛЬЯНОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД", "UAZ", "УАЗ", "УЛЬЯНОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД ОАО, Г.У", "УЛЬЯНОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД", "ОАО УАЗ Г", "ОАО УАЗ Г.УЛЬЯНОВСК", "ООО УАЗ-АВТОКОМПОНЕНТ", "ОАО УАЗ", "УАЗ ОАО", "OE UAZ"],
              "typesList": [1]
            }, {
              "id": 498,
              "total": 3,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 351,
              "total": 2,
              "slug": "bmw",
              "brand": "BMW",
              "aliasesList": ["BMW MOTO", "БМВ", "OE BMW", "BMW (CN)", "BMW (ГЕРМАНИЯ)", "BMW"],
              "typesList": [1]
            }, {
              "id": 818,
              "total": 2,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 926,
              "total": 2,
              "slug": "geely",
              "brand": "GEELY",
              "aliasesList": ["GEELY (КИТАЙ)", "GEELY"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 1,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1180,
              "total": 1,
              "slug": "iveco",
              "brand": "IVECO",
              "aliasesList": ["IVECO", "OE IVECO", "IVECO OE", "IVECO-DAILY"],
              "typesList": [1]
            }, {
              "id": 1186,
              "total": 1,
              "slug": "jaguar",
              "brand": "JAGUAR",
              "aliasesList": ["JAGUAR", "ЯГУАР", "JAGUAR ЗАПЧАСТИ"],
              "typesList": [1]
            }, {
              "id": 1734,
              "total": 1,
              "slug": "ossca",
              "brand": "OSSCA",
              "aliasesList": ["OSSCA", "OSSKA"],
              "typesList": [2]
            }, {
              "id": 1814,
              "total": 1,
              "slug": "porsche",
              "brand": "PORSCHE",
              "aliasesList": ["ПОРШЕ", "PORSCHE", "VW-PORSCHE"],
              "typesList": [1]
            }]
          }, {
            "id": 25091842,
            "name": "Дроссельная заслонка",
            "slug": "drosselnaya-zaslonka",
            "groupsList": [10004],
            "manufacturers": [{
              "id": 1652,
              "total": 44,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 209,
              "total": 28,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 351,
              "total": 25,
              "slug": "bmw",
              "brand": "BMW",
              "aliasesList": ["BMW MOTO", "БМВ", "OE BMW", "BMW (CN)", "BMW (ГЕРМАНИЯ)", "BMW"],
              "typesList": [1]
            }, {
              "id": 1501,
              "total": 17,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 12,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1713,
              "total": 11,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 4086,
              "total": 7,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 818,
              "total": 5,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 1481,
              "total": 5,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 498,
              "total": 4,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 1542,
              "total": 4,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 491,
              "total": 3,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 2156,
              "total": 3,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 2387,
              "total": 3,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 490,
              "total": 2,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 1533,
              "total": 2,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 2166,
              "total": 2,
              "slug": "suzuki",
              "brand": "SUZUKI",
              "aliasesList": ["SUZUKI (ЯПОНИЯ)", "SUZUKI MOTO", "SUZUKI MOTORCYCLES", "CHANGHE SUZUKI", "SUZUKI ORGNL", "SUZUKI ORIGINAL", "SUZUKI ЗАПЧАСТИ", "SUZUKIM", "SUZUKI", "OE SUZUKI", "CHANGAN SUZUKI"],
              "typesList": [1]
            }, {
              "id": 2354,
              "total": 2,
              "slug": "vdo",
              "brand": "VDO",
              "aliasesList": ["VDO", "SIEMENS VDO", "VDO SIEMENS", "SIEMENS"],
              "typesList": [2]
            }, {
              "id": 366,
              "total": 1,
              "slug": "bosch",
              "brand": "BOSCH",
              "aliasesList": ["BOSCH TRUCK", "BOSCH (ГЕРМАНИЯ)", "РОБЕРТБОШ", "ROBERT BOSCH", "BOSCH (OEM) GERMANY", "BOSH", "BOSCH DIAGNOSTICS", "BOSCH MOBA, ГЕРМАНИЯ", "BOSCH", "BOSCH BATTERY"],
              "typesList": [2]
            }, {
              "id": 543,
              "total": 1,
              "slug": "crown",
              "brand": "CROWN",
              "aliasesList": ["CROWN"],
              "typesList": [2]
            }, {
              "id": 559,
              "total": 1,
              "slug": "daewoo",
              "brand": "DAEWOO",
              "aliasesList": ["UZ-DAEWOO", "DAEWOO (КОРЕЯ)", "DAEWOO MOTOR", "DAEWOOPARTS, PMC", "DAEWOO"],
              "typesList": [1, 4]
            }, {
              "id": 926,
              "total": 1,
              "slug": "geely",
              "brand": "GEELY",
              "aliasesList": ["GEELY (КИТАЙ)", "GEELY"],
              "typesList": [1]
            }, {
              "id": 978,
              "total": 1,
              "slug": "gp",
              "brand": "GP",
              "aliasesList": ["GP", "GP-GERMANPARTS"],
              "typesList": [2]
            }, {
              "id": 1092,
              "total": 1,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 1186,
              "total": 1,
              "slug": "jaguar",
              "brand": "JAGUAR",
              "aliasesList": ["JAGUAR", "ЯГУАР", "JAGUAR ЗАПЧАСТИ"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 1,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 1945,
              "total": 1,
              "slug": "saab",
              "brand": "SAAB",
              "aliasesList": ["SAAB"],
              "typesList": [1]
            }, {
              "id": 2374,
              "total": 1,
              "slug": "vika",
              "brand": "VIKA",
              "aliasesList": ["VIKA"],
              "typesList": [2]
            }, {
              "id": 4308,
              "total": 1,
              "slug": "asparts",
              "brand": "ASPARTS",
              "aliasesList": ["ASPARTS"],
              "typesList": [2]
            }]
          }, {
            "id": 25372302,
            "name": "Дроссельный узел",
            "slug": "drosselnyj-uzel",
            "groupsList": [10004],
            "manufacturers": [{
              "id": 929,
              "total": 1,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }]
          }, {
            "id": 25089490,
            "name": "Заслонка дроссельная",
            "slug": "zaslonka-drosselnaya",
            "groupsList": [10004],
            "manufacturers": [{
              "id": 4086,
              "total": 299,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 1652,
              "total": 64,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 209,
              "total": 21,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 1533,
              "total": 17,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1713,
              "total": 17,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 76,
              "total": 14,
              "slug": "aisan",
              "brand": "AISAN",
              "aliasesList": ["AISAN"],
              "typesList": [2]
            }, {
              "id": 863,
              "total": 14,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 14,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1778,
              "total": 14,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }, {
              "id": 189,
              "total": 13,
              "slug": "at",
              "brand": "AT",
              "aliasesList": ["AT", "AUTO TECHNOLOGIES GROUP", "AUTO TECHNOLOGIES GROUP S.R.O."],
              "typesList": [2]
            }, {
              "id": 1542,
              "total": 13,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 366,
              "total": 12,
              "slug": "bosch",
              "brand": "BOSCH",
              "aliasesList": ["BOSCH TRUCK", "BOSCH (ГЕРМАНИЯ)", "РОБЕРТБОШ", "ROBERT BOSCH", "BOSCH (OEM) GERMANY", "BOSH", "BOSCH DIAGNOSTICS", "BOSCH MOBA, ГЕРМАНИЯ", "BOSCH", "BOSCH BATTERY"],
              "typesList": [2]
            }, {
              "id": 1789,
              "total": 12,
              "slug": "pierburg",
              "brand": "PIERBURG",
              "aliasesList": ["PIERBURG", "Pierburg truck", "PIERBURG FRANCE"],
              "typesList": [2]
            }, {
              "id": 1885,
              "total": 11,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 1487,
              "total": 9,
              "slug": "meat-doria",
              "brand": "MEAT & DORIA",
              "aliasesList": ["MEAT&amp;DORIA", "MEAT AND DORIA", "MEAT & DORIA"],
              "typesList": [2]
            }, {
              "id": 24,
              "total": 8,
              "slug": "ac-delco",
              "brand": "AC DELCO",
              "aliasesList": ["ALDELCO", "ACDELCO"],
              "typesList": [2]
            }, {
              "id": 498,
              "total": 7,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 1435,
              "total": 6,
              "slug": "magneti-marelli",
              "brand": "MAGNETI MARELLI",
              "aliasesList": ["MAGNETTI MARELI", "MAGNETI MARELI", "MAGNETI", "MAG", "M-MARELLI", "MAGNETI MARELLI", "MARELLI", "MARELLI EXCHANGE", "MARELLI MAGNETI", "MAGNETTI MARELLI", "MAGNETTI"],
              "typesList": [2]
            }, {
              "id": 1481,
              "total": 6,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 2446,
              "total": 6,
              "slug": "yamaha",
              "brand": "YAMAHA",
              "aliasesList": ["YAMAHA", "YAMAHA MOTORCYCLES"],
              "typesList": [2]
            }, {
              "id": 490,
              "total": 5,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 818,
              "total": 5,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 1092,
              "total": 5,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 2156,
              "total": 5,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 2374,
              "total": 5,
              "slug": "vika",
              "brand": "VIKA",
              "aliasesList": ["VIKA"],
              "typesList": [2]
            }, {
              "id": 99,
              "total": 4,
              "slug": "alfa-romeo",
              "brand": "ALFA ROMEO",
              "aliasesList": ["ALFAROME/FIAT/LANCI", "ALFA/FIAT/LANCIA", "ALFA ROMEO", "ALFA"],
              "typesList": [1]
            }, {
              "id": 4103,
              "total": 4,
              "slug": "sailing",
              "brand": "SAILING",
              "aliasesList": ["SAILING", "SAILING (КИТАЙ)"],
              "typesList": [2]
            }, {
              "id": 623,
              "total": 3,
              "slug": "dodge",
              "brand": "DODGE",
              "aliasesList": ["DODGE", "COLLECTION DODGE"],
              "typesList": [2]
            }, {
              "id": 2354,
              "total": 3,
              "slug": "vdo",
              "brand": "VDO",
              "aliasesList": ["VDO", "SIEMENS VDO", "VDO SIEMENS", "SIEMENS"],
              "typesList": [2]
            }, {
              "id": 2582,
              "total": 3,
              "slug": "zzvf",
              "brand": "ZZVF",
              "aliasesList": ["ZZVF (КИТАЙ)", "ZZVF"],
              "typesList": [2]
            }, {
              "id": 491,
              "total": 2,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 559,
              "total": 2,
              "slug": "daewoo",
              "brand": "DAEWOO",
              "aliasesList": ["UZ-DAEWOO", "DAEWOO (КОРЕЯ)", "DAEWOO MOTOR", "DAEWOOPARTS, PMC", "DAEWOO"],
              "typesList": [1, 4]
            }, {
              "id": 735,
              "total": 2,
              "slug": "era",
              "brand": "ERA",
              "aliasesList": ["ERA BENELUX", "ERA", "ERA ELETTRO"],
              "typesList": [2]
            }, {
              "id": 1173,
              "total": 2,
              "slug": "isuzu",
              "brand": "ISUZU",
              "aliasesList": ["ISUZU"],
              "typesList": [1]
            }, {
              "id": 1190,
              "total": 2,
              "slug": "japanparts",
              "brand": "JAPANPARTS",
              "aliasesList": ["JAPANPARTS (ИТАЛИЯ)", "JAPAN PARTS (К-Т 2ШТ.)", "JAPANPARTS", "JAPANSPARTS"],
              "typesList": [2]
            }, {
              "id": 1354,
              "total": 2,
              "slug": "land-rover",
              "brand": "LAND ROVER",
              "aliasesList": ["ROVER/LAND ROVER", "LAND ROVER", "ROVER/LANDROVER/MG", "LR ЗАПЧАСТИ", "ЛЭНД РОВЕР", "LAND ROVER", "OE LAND ROVER"],
              "typesList": [1]
            }, {
              "id": 1382,
              "total": 2,
              "slug": "lifan",
              "brand": "LIFAN",
              "aliasesList": ["LIFAN", "ЛИФАН"],
              "typesList": [1]
            }, {
              "id": 1501,
              "total": 2,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 2554,
              "total": 2,
              "slug": "s-tr",
              "brand": "S-TR",
              "aliasesList": ["S-TR"],
              "typesList": [2]
            }, {
              "id": 586,
              "total": 1,
              "slug": "dello",
              "brand": "DELLO",
              "aliasesList": ["AUTOMEGA", "DELLO (ГЕРМАНИЯ)", "DELLO (AUTOMEGA)", "DELLO", "AUTOMEGADELLO"],
              "typesList": [2]
            }, {
              "id": 629,
              "total": 1,
              "slug": "dominant",
              "brand": "DOMINANT",
              "aliasesList": ["DOMINANT"],
              "typesList": [2]
            }, {
              "id": 642,
              "total": 1,
              "slug": "dorman",
              "brand": "DORMAN",
              "aliasesList": ["DORMAN ENGINES", "DORMAN"],
              "typesList": [2]
            }, {
              "id": 743,
              "total": 1,
              "slug": "ersus",
              "brand": "ERSUS",
              "aliasesList": ["ERSUS"],
              "typesList": [2]
            }, {
              "id": 873,
              "total": 1,
              "slug": "francecar",
              "brand": "FRANCECAR",
              "aliasesList": ["FRANCECAR"],
              "typesList": [2]
            }, {
              "id": 920,
              "total": 1,
              "slug": "gaz",
              "brand": "GAZ",
              "aliasesList": ["ГАЗ 3302", "ГАЗ 2705", "ГАЗ 52", "ГАЗ 2217", "ГАЗ", "ВОЛГА-ОЙЛ", "ВОЛГА", "ГАЗ 53", "ГАЗ 66", "ГАЗ ОАО", "ГАЗ ООО", "ГАЗЕЛЬ", "ВАЛДАЙ", "ГОРЬКОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД ОАО, Н.Н", "ОАО ГАЗ", "УМЗ Г.УЛЬЯНОВСК", "УМЗ Г", "VOLGA", "УМЗ", "GAZ", "ГАЗ 4301", "ГАЗ 3309", "ГАЗ 3308", "ГАЗ 3307", "OE GAZ"],
              "typesList": [1, 3]
            }, {
              "id": 1107,
              "total": 1,
              "slug": "huco",
              "brand": "HUCO",
              "aliasesList": ["HÜCO", "HUECO", "HUGO", "HUCO"],
              "typesList": [2]
            }, {
              "id": 1198,
              "total": 1,
              "slug": "jeep",
              "brand": "JEEP",
              "aliasesList": ["JEEP"],
              "typesList": [2]
            }, {
              "id": 1217,
              "total": 1,
              "slug": "jp-group",
              "brand": "JP GROUP",
              "aliasesList": ["JP GROUP (ДАНИЯ)", "JPG", "JP GROUPE", "JP GROUP", "GP GROUP", "JP"],
              "typesList": [2]
            }, {
              "id": 1375,
              "total": 1,
              "slug": "lex",
              "brand": "LEX",
              "aliasesList": ["LEX"],
              "typesList": [2]
            }, {
              "id": 1490,
              "total": 1,
              "slug": "mec-diesel",
              "brand": "MEC-DIESEL",
              "aliasesList": ["MEC-DIESEL", "MEC-DISEL"],
              "typesList": [2]
            }, {
              "id": 1734,
              "total": 1,
              "slug": "ossca",
              "brand": "OSSCA",
              "aliasesList": ["OSSCA", "OSSKA"],
              "typesList": [2]
            }, {
              "id": 2166,
              "total": 1,
              "slug": "suzuki",
              "brand": "SUZUKI",
              "aliasesList": ["SUZUKI (ЯПОНИЯ)", "SUZUKI MOTO", "SUZUKI MOTORCYCLES", "CHANGHE SUZUKI", "SUZUKI ORGNL", "SUZUKI ORIGINAL", "SUZUKI ЗАПЧАСТИ", "SUZUKIM", "SUZUKI", "OE SUZUKI", "CHANGAN SUZUKI"],
              "typesList": [1]
            }, {
              "id": 2353,
              "total": 1,
              "slug": "vaz",
              "brand": "VAZ",
              "aliasesList": ["АВТОВАЗ\" ОАО Г", "\"АВТОВАЗ\" ОАО Г. ТОЛЬЯТТИ", "LADA", "LADA (РОССИЯ)", "LADA IMAGE", "AVTOVAZ", "ВАЗ ЛАДА", "ВАЗ Г", "ВАЗ (САМАРА)", "ВАЗ (ОАО АВТОВАЗ Г", "ВАЗ", "VAZ", "ЛАДА", "АО ЛАДА-ИМИДЖ", "АО ВАЗ", "АО «ЛАДА ИМИДЖ»", "НИВА", "АВТОВАЗ", "ОАО АВТОВАЗ", "ОАО АВТОВАЗ (РЕМКОМПЛЕКТЫ)", "ОАО ВАЗ", "GM-AVTOVAZ", "АВТОВАЗ ОАО"],
              "typesList": [1]
            }, {
              "id": 2358,
              "total": 1,
              "slug": "vemo-vaico",
              "brand": "VEMO (VAICO)",
              "aliasesList": ["VIEROL AG", "VIEROL", "VEMO", "VAICO/VEMO", "VAICO", "VEMO (VAICO)"],
              "typesList": [2]
            }, {
              "id": 2587,
              "total": 1,
              "slug": "ssang-yong",
              "brand": "SSANG YONG",
              "aliasesList": ["SSANG YONG", "SSANGYOUNG", "OE SSANG YONG", "SSANGYONG (ОРИГИНАЛ)", "SSANG YONG MOTORS CORPORATION (КОРЕЯ", "SSANGYONG (GEN) KOREA", "ORIGINAL SSANGYONG", "SSANGYONG (КОРЕЯ)", "SY", "SANGYONG", "SSY"],
              "typesList": [1, 3]
            }, {
              "id": 4308,
              "total": 1,
              "slug": "asparts",
              "brand": "ASPARTS",
              "aliasesList": ["ASPARTS"],
              "typesList": [2]
            }]
          }, {
            "id": 25090010,
            "name": "Клапан холостого хода",
            "slug": "klapan-holostogo-hoda",
            "groupsList": [10004],
            "manufacturers": [{
              "id": 863,
              "total": 76,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 4086,
              "total": 74,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 2134,
              "total": 26,
              "slug": "standard",
              "brand": "STANDARD",
              "aliasesList": ["STANDARD", "STANDARD MOTOR PRODUCTS", "STANDART"],
              "typesList": [2]
            }, {
              "id": 1533,
              "total": 24,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1652,
              "total": 21,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 1075,
              "total": 15,
              "slug": "hitachi",
              "brand": "HITACHI",
              "aliasesList": ["ﾋﾀﾁｵｰﾄﾊﾟｰﾂ&AMP;ｻｰﾋﾞｽ", "HITACHI", "ﾋﾀﾁｶｾｲｺｳｷﾞｮｳ"],
              "typesList": [2]
            }, {
              "id": 2410,
              "total": 14,
              "slug": "wells",
              "brand": "WELLS",
              "aliasesList": ["ADVANTECH", "WELLS"],
              "typesList": [2]
            }, {
              "id": 351,
              "total": 12,
              "slug": "bmw",
              "brand": "BMW",
              "aliasesList": ["BMW MOTO", "БМВ", "OE BMW", "BMW (CN)", "BMW (ГЕРМАНИЯ)", "BMW"],
              "typesList": [1]
            }, {
              "id": 1570,
              "total": 12,
              "slug": "motorcraft",
              "brand": "MOTORCRAFT",
              "aliasesList": ["ﾓｰﾀｰｸﾗﾌﾄ", "MOTORCRAFT"],
              "typesList": [2]
            }, {
              "id": 1092,
              "total": 11,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 10,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 586,
              "total": 8,
              "slug": "dello",
              "brand": "DELLO",
              "aliasesList": ["AUTOMEGA", "DELLO (ГЕРМАНИЯ)", "DELLO (AUTOMEGA)", "DELLO", "AUTOMEGADELLO"],
              "typesList": [2]
            }, {
              "id": 1778,
              "total": 8,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }, {
              "id": 2166,
              "total": 8,
              "slug": "suzuki",
              "brand": "SUZUKI",
              "aliasesList": ["SUZUKI (ЯПОНИЯ)", "SUZUKI MOTO", "SUZUKI MOTORCYCLES", "CHANGHE SUZUKI", "SUZUKI ORGNL", "SUZUKI ORIGINAL", "SUZUKI ЗАПЧАСТИ", "SUZUKIM", "SUZUKI", "OE SUZUKI", "CHANGAN SUZUKI"],
              "typesList": [1]
            }, {
              "id": 498,
              "total": 7,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 1375,
              "total": 7,
              "slug": "lex",
              "brand": "LEX",
              "aliasesList": ["LEX"],
              "typesList": [2]
            }, {
              "id": 1542,
              "total": 7,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 543,
              "total": 6,
              "slug": "crown",
              "brand": "CROWN",
              "aliasesList": ["CROWN"],
              "typesList": [2]
            }, {
              "id": 735,
              "total": 5,
              "slug": "era",
              "brand": "ERA",
              "aliasesList": ["ERA BENELUX", "ERA", "ERA ELETTRO"],
              "typesList": [2]
            }, {
              "id": 803,
              "total": 5,
              "slug": "febi",
              "brand": "FEBI",
              "aliasesList": ["FEBI", "FEBI TRUCK", "FEBI BILSTEIN", "FEBI (ГЕРМАНИЯ)"],
              "typesList": [2]
            }, {
              "id": 1107,
              "total": 4,
              "slug": "huco",
              "brand": "HUCO",
              "aliasesList": ["HÜCO", "HUECO", "HUGO", "HUCO"],
              "typesList": [2]
            }, {
              "id": 1550,
              "total": 4,
              "slug": "mopar-parts",
              "brand": "MOPAR PARTS",
              "aliasesList": ["MOPAR,КАНАДА", "MOPAR PARTS", "MOPAR"],
              "typesList": [2]
            }, {
              "id": 2358,
              "total": 4,
              "slug": "vemo-vaico",
              "brand": "VEMO (VAICO)",
              "aliasesList": ["VIEROL AG", "VIEROL", "VEMO", "VAICO/VEMO", "VAICO", "VEMO (VAICO)"],
              "typesList": [2]
            }, {
              "id": 74,
              "total": 3,
              "slug": "airtex",
              "brand": "AIRTEX",
              "aliasesList": ["AIRTEC", "AIRTEX"],
              "typesList": [2]
            }, {
              "id": 300,
              "total": 3,
              "slug": "behr-hella",
              "brand": "BEHR (HELLA)",
              "aliasesList": ["BEHR-HELLA (ГЕРМАНИЯ)", "BEHR_", "BEHR THERMOT-TRONIK", "BEHR SERVICE", "BEHR HELLA SERVICE TRUCK", "BEHR HELLA SERVICE", "BEHR (HELLA)", "HELLA/BEHR-HELLA", "HELLA TRUCK", "HELLA (ГЕРМАНИЯ)", "HELLA"],
              "typesList": [2]
            }, {
              "id": 1435,
              "total": 3,
              "slug": "magneti-marelli",
              "brand": "MAGNETI MARELLI",
              "aliasesList": ["MAGNETTI MARELI", "MAGNETI MARELI", "MAGNETI", "MAG", "M-MARELLI", "MAGNETI MARELLI", "MARELLI", "MARELLI EXCHANGE", "MARELLI MAGNETI", "MAGNETTI MARELLI", "MAGNETTI"],
              "typesList": [2]
            }, {
              "id": 1734,
              "total": 3,
              "slug": "ossca",
              "brand": "OSSCA",
              "aliasesList": ["OSSCA", "OSSKA"],
              "typesList": [2]
            }, {
              "id": 1976,
              "total": 3,
              "slug": "sat",
              "brand": "SAT",
              "aliasesList": ["SAT-TYG", "SATA", "SAT (КИТАЙ)", "SAT", "SAT/TYC", "SAT PREMIUM"],
              "typesList": [2]
            }, {
              "id": 2554,
              "total": 3,
              "slug": "s-tr",
              "brand": "S-TR",
              "aliasesList": ["S-TR"],
              "typesList": [2]
            }, {
              "id": 24,
              "total": 2,
              "slug": "ac-delco",
              "brand": "AC DELCO",
              "aliasesList": ["ALDELCO", "ACDELCO"],
              "typesList": [2]
            }, {
              "id": 76,
              "total": 2,
              "slug": "aisan",
              "brand": "AISAN",
              "aliasesList": ["AISAN"],
              "typesList": [2]
            }, {
              "id": 119,
              "total": 2,
              "slug": "amd",
              "brand": "AMD",
              "aliasesList": ["AMD (КОРЕЯ)", "AMD"],
              "typesList": [2]
            }, {
              "id": 629,
              "total": 2,
              "slug": "dominant",
              "brand": "DOMINANT",
              "aliasesList": ["DOMINANT"],
              "typesList": [2]
            }, {
              "id": 1173,
              "total": 2,
              "slug": "isuzu",
              "brand": "ISUZU",
              "aliasesList": ["ISUZU"],
              "typesList": [1]
            }, {
              "id": 1481,
              "total": 2,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 1713,
              "total": 2,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 2156,
              "total": 2,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 2294,
              "total": 2,
              "slug": "citron",
              "brand": "ЦИТРОН",
              "aliasesList": ["TSN-ЦИТРОН", "ОАО ЦИТРОН", "ОАО ЦИТРОН Г", "РОССИЯ,TSN", "CITRON", "ТАЙВАНЬ,TSN", "МЕХАНИК", "TSITRON", "ЦИТРОН (Г", "TSN", "ЦИТРОН КОНЦЕРН ОАО, МОСКВА", "TSN (РОССИЯ)", "TSN TRUCK", "ЦИТРОНМЕХАНИК", "ЦИТРОН"],
              "typesList": [2]
            }, {
              "id": 2354,
              "total": 2,
              "slug": "vdo",
              "brand": "VDO",
              "aliasesList": ["VDO", "SIEMENS VDO", "VDO SIEMENS", "SIEMENS"],
              "typesList": [2]
            }, {
              "id": 2390,
              "total": 2,
              "slug": "vtr",
              "brand": "VTR",
              "aliasesList": ["VTR (ФИНЛЯНДИЯ)", "VTR"],
              "typesList": [2]
            }, {
              "id": 2446,
              "total": 2,
              "slug": "yamaha",
              "brand": "YAMAHA",
              "aliasesList": ["YAMAHA", "YAMAHA MOTORCYCLES"],
              "typesList": [2]
            }, {
              "id": 4155,
              "total": 2,
              "slug": "startvolt",
              "brand": "СТАРТВОЛЬТ",
              "aliasesList": ["СТАРВОЛЬТ", "СТАРТВОЛЬТ"],
              "typesList": [2]
            }, {
              "id": 4206,
              "total": 2,
              "slug": "quattro-freni",
              "brand": "QUATTRO FRENI",
              "aliasesList": ["QUATTRO FRENI", "QUATRO", "QF"],
              "typesList": [2]
            }, {
              "id": 17403,
              "total": 2,
              "slug": "grog",
              "brand": "GROG",
              "aliasesList": ["GROG"],
              "typesList": [2]
            }, {
              "id": 45,
              "total": 1,
              "slug": "ae",
              "brand": "AE",
              "aliasesList": ["AE", "AE TRUCK"],
              "typesList": [2]
            }, {
              "id": 189,
              "total": 1,
              "slug": "at",
              "brand": "AT",
              "aliasesList": ["AT", "AUTO TECHNOLOGIES GROUP", "AUTO TECHNOLOGIES GROUP S.R.O."],
              "typesList": [2]
            }, {
              "id": 209,
              "total": 1,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 232,
              "total": 1,
              "slug": "automotor",
              "brand": "AUTOMOTOR",
              "aliasesList": ["AUTOMOTOR"],
              "typesList": [2]
            }, {
              "id": 405,
              "total": 1,
              "slug": "bsg",
              "brand": "BSG",
              "aliasesList": ["BSG (ТУРЦИЯ)", "\"BSG\" BASBUG", "BASBUG (ТУРЦИЯ)", "BASBUG", "BSG"],
              "typesList": [2]
            }, {
              "id": 490,
              "total": 1,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 491,
              "total": 1,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 575,
              "total": 1,
              "slug": "dashi",
              "brand": "DASHI",
              "aliasesList": ["DASHI"],
              "typesList": [2]
            }, {
              "id": 1189,
              "total": 1,
              "slug": "japan-cars",
              "brand": "JAPAN CARS",
              "aliasesList": ["JAPAN CARS", "JAPAN CARS (ЯПОНИЯ)", "JC"],
              "typesList": [2]
            }, {
              "id": 1400,
              "total": 1,
              "slug": "logem",
              "brand": "LOGEM",
              "aliasesList": ["LOGEM"],
              "typesList": [2]
            }, {
              "id": 1599,
              "total": 1,
              "slug": "mv-parts",
              "brand": "MV-PARTS",
              "aliasesList": ["MV-PARTS"],
              "typesList": [2]
            }, {
              "id": 1789,
              "total": 1,
              "slug": "pierburg",
              "brand": "PIERBURG",
              "aliasesList": ["PIERBURG", "Pierburg truck", "PIERBURG FRANCE"],
              "typesList": [2]
            }, {
              "id": 1802,
              "total": 1,
              "slug": "pmc",
              "brand": "PMC",
              "aliasesList": ["PMC"],
              "typesList": [2]
            }, {
              "id": 1885,
              "total": 1,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 1916,
              "total": 1,
              "slug": "roers-parts",
              "brand": "ROERS PARTS",
              "aliasesList": ["ROERS PARTS"],
              "typesList": [2]
            }, {
              "id": 1934,
              "total": 1,
              "slug": "rover",
              "brand": "ROVER",
              "aliasesList": ["AUSTIN MAESTRO", "ROVER", "ROVER GPOUP", "MG ROVER", "MG MAESTRO"],
              "typesList": [2]
            }, {
              "id": 2342,
              "total": 1,
              "slug": "valeo",
              "brand": "VALEO",
              "aliasesList": ["VALEO KOREA", "VALEO (ФРАНЦИЯ)", "VALEO SERVICE EASTERN", "VALEO (OEM) KOREA", "VALEO ( ШВЕЦИЯ)", "VALEO PHC", "VALEO ELECTRIC AND ELECTRONIC SYSTEMS SP", "VALEO PHP", "VALEO (ЮЖНАЯ КОРЕЯ)", "VALEO", "ｳﾞｧﾚｵﾕﾆｼｱﾄﾗﾝｽﾐｯｼｮﾝ", "VALEO TRUCK"],
              "typesList": [2]
            }, {
              "id": 2387,
              "total": 1,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 2395,
              "total": 1,
              "slug": "wahler",
              "brand": "WAHLER",
              "aliasesList": ["WAHLER"],
              "typesList": [2]
            }, {
              "id": 2514,
              "total": 1,
              "slug": "startvolt_1",
              "brand": "STARTVOLT",
              "aliasesList": ["STARTVOLT"],
              "typesList": [2]
            }, {
              "id": 2587,
              "total": 1,
              "slug": "ssang-yong",
              "brand": "SSANG YONG",
              "aliasesList": ["SSANG YONG", "SSANGYOUNG", "OE SSANG YONG", "SSANGYONG (ОРИГИНАЛ)", "SSANG YONG MOTORS CORPORATION (КОРЕЯ", "SSANGYONG (GEN) KOREA", "ORIGINAL SSANGYONG", "SSANGYONG (КОРЕЯ)", "SY", "SANGYONG", "SSY"],
              "typesList": [1, 3]
            }, {
              "id": 2891,
              "total": 1,
              "slug": "long",
              "brand": "LONG",
              "aliasesList": ["LONG"],
              "typesList": [2]
            }, {
              "id": 4065,
              "total": 1,
              "slug": "tork",
              "brand": "TORK",
              "aliasesList": ["TORK"],
              "typesList": [2]
            }, {
              "id": 5260,
              "total": 1,
              "slug": "at_1",
              "brand": "АТ",
              "aliasesList": ["АТ"],
              "typesList": [2]
            }, {
              "id": 16351,
              "total": 1,
              "slug": "parts-mall",
              "brand": "PARTS-MALL",
              "aliasesList": ["PARTS", "PARTS-MALL", "PART-MALL"],
              "typesList": [2]
            }]
          }, {
            "id": 25090331,
            "name": "Прокладка дроссельной заслонки",
            "slug": "prokladka-drosselnoj-zaslonki",
            "groupsList": [10004],
            "manufacturers": [{
              "id": 4086,
              "total": 57,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 863,
              "total": 38,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 297,
              "total": 18,
              "slug": "beck-arnley",
              "brand": "BECK ARNLEY",
              "aliasesList": ["BECK ARNLEY", "BECK"],
              "typesList": [2]
            }, {
              "id": 1976,
              "total": 16,
              "slug": "sat",
              "brand": "SAT",
              "aliasesList": ["SAT-TYG", "SATA", "SAT (КИТАЙ)", "SAT", "SAT/TYC", "SAT PREMIUM"],
              "typesList": [2]
            }, {
              "id": 1542,
              "total": 11,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 1272,
              "total": 7,
              "slug": "kibi",
              "brand": "KIBI",
              "aliasesList": ["KIBI"],
              "typesList": [2]
            }, {
              "id": 209,
              "total": 6,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 498,
              "total": 4,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 4,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1375,
              "total": 3,
              "slug": "lex",
              "brand": "LEX",
              "aliasesList": ["LEX"],
              "typesList": [2]
            }, {
              "id": 1652,
              "total": 3,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 1713,
              "total": 3,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 1814,
              "total": 3,
              "slug": "porsche",
              "brand": "PORSCHE",
              "aliasesList": ["ПОРШЕ", "PORSCHE", "VW-PORSCHE"],
              "typesList": [1]
            }, {
              "id": 16351,
              "total": 3,
              "slug": "parts-mall",
              "brand": "PARTS-MALL",
              "aliasesList": ["PARTS", "PARTS-MALL", "PART-MALL"],
              "typesList": [2]
            }, {
              "id": 1092,
              "total": 2,
              "slug": "honda",
              "brand": "HONDA",
              "aliasesList": ["HONDA ХОДОВКА", "HONDA", "ACURA", "HONDA ДВИГАТЕЛЬ", "HONDA USA", "OE HONDA", "HONDA MOTOR CO., LTD.", "HONDA/ACURA", "HONDA MOTOR CO", "HONDA MOTO", "HONDA OEM", "HONDA (ЯПОНИЯ)", "HONDA - HONDA"],
              "typesList": [1]
            }, {
              "id": 1382,
              "total": 2,
              "slug": "lifan",
              "brand": "LIFAN",
              "aliasesList": ["LIFAN", "ЛИФАН"],
              "typesList": [1]
            }, {
              "id": 2576,
              "total": 2,
              "slug": "5-825",
              "brand": "5'825",
              "aliasesList": ["OHNO", "ｵｵﾉｺﾞﾑｺｳｷﾞｮｳ", "5'825"],
              "typesList": [2]
            }, {
              "id": 4538,
              "total": 2,
              "slug": "rosteco",
              "brand": "ROSTECO",
              "aliasesList": ["ROSTECO"],
              "typesList": [2]
            }, {
              "id": 24,
              "total": 1,
              "slug": "ac-delco",
              "brand": "AC DELCO",
              "aliasesList": ["ALDELCO", "ACDELCO"],
              "typesList": [2]
            }, {
              "id": 490,
              "total": 1,
              "slug": "chery",
              "brand": "CHERY",
              "aliasesList": ["CHERI", "CHERRY", "CHERY (КИТАЙ)", "CHERY"],
              "typesList": [1]
            }, {
              "id": 491,
              "total": 1,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 718,
              "total": 1,
              "slug": "elring",
              "brand": "ELRING",
              "aliasesList": ["ELRLING", "ELRING", "ELRING TRUCK"],
              "typesList": [2]
            }, {
              "id": 739,
              "total": 1,
              "slug": "eristic",
              "brand": "ERISTIC",
              "aliasesList": ["ERISTIC", "Eristik"],
              "typesList": [2]
            }, {
              "id": 818,
              "total": 1,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 926,
              "total": 1,
              "slug": "geely",
              "brand": "GEELY",
              "aliasesList": ["GEELY (КИТАЙ)", "GEELY"],
              "typesList": [1]
            }, {
              "id": 1152,
              "total": 1,
              "slug": "international",
              "brand": "INTERNATIONAL",
              "aliasesList": ["INTERNATIONAL"],
              "typesList": [2]
            }, {
              "id": 1186,
              "total": 1,
              "slug": "jaguar",
              "brand": "JAGUAR",
              "aliasesList": ["JAGUAR", "ЯГУАР", "JAGUAR ЗАПЧАСТИ"],
              "typesList": [1]
            }, {
              "id": 1243,
              "total": 1,
              "slug": "kap",
              "brand": "KAP",
              "aliasesList": ["KAP", "KAP VOLUE"],
              "typesList": [2]
            }, {
              "id": 1354,
              "total": 1,
              "slug": "land-rover",
              "brand": "LAND ROVER",
              "aliasesList": ["ROVER/LAND ROVER", "LAND ROVER", "ROVER/LANDROVER/MG", "LR ЗАПЧАСТИ", "ЛЭНД РОВЕР", "LAND ROVER", "OE LAND ROVER"],
              "typesList": [1]
            }, {
              "id": 1501,
              "total": 1,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 1802,
              "total": 1,
              "slug": "pmc",
              "brand": "PMC",
              "aliasesList": ["PMC"],
              "typesList": [2]
            }, {
              "id": 1896,
              "total": 1,
              "slug": "rheejin",
              "brand": "RHEEJIN",
              "aliasesList": ["RHEEJIN", "RHEEJIN METAL", "RHEE JIN KOREA", "RHEEJIN (NG) KOREA"],
              "typesList": [2]
            }, {
              "id": 2152,
              "total": 1,
              "slug": "stone",
              "brand": "STONE",
              "aliasesList": ["STONE"],
              "typesList": [2]
            }, {
              "id": 2156,
              "total": 1,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 2246,
              "total": 1,
              "slug": "tong-hong",
              "brand": "TONG HONG",
              "aliasesList": ["TONG HONG"],
              "typesList": [2]
            }, {
              "id": 2353,
              "total": 1,
              "slug": "vaz",
              "brand": "VAZ",
              "aliasesList": ["АВТОВАЗ\" ОАО Г", "\"АВТОВАЗ\" ОАО Г. ТОЛЬЯТТИ", "LADA", "LADA (РОССИЯ)", "LADA IMAGE", "AVTOVAZ", "ВАЗ ЛАДА", "ВАЗ Г", "ВАЗ (САМАРА)", "ВАЗ (ОАО АВТОВАЗ Г", "ВАЗ", "VAZ", "ЛАДА", "АО ЛАДА-ИМИДЖ", "АО ВАЗ", "АО «ЛАДА ИМИДЖ»", "НИВА", "АВТОВАЗ", "ОАО АВТОВАЗ", "ОАО АВТОВАЗ (РЕМКОМПЛЕКТЫ)", "ОАО ВАЗ", "GM-AVTOVAZ", "АВТОВАЗ ОАО"],
              "typesList": [1]
            }, {
              "id": 2370,
              "total": 1,
              "slug": "victor-reinz",
              "brand": "VICTOR REINZ",
              "aliasesList": ["VICTOR REINZ TRUCK", "VIKTOR REINZ", "REINZ", "REINZ VICTOR", "REINZ-DICHTUNGS-GMBH&CO", "VICTOR REINZ"],
              "typesList": [2]
            }, {
              "id": 2387,
              "total": 1,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 2470,
              "total": 1,
              "slug": "zaz",
              "brand": "ZAZ",
              "aliasesList": ["ЗАЗ / CHANCE", "ЗАЗ", "ZAZ", "CHANCE"],
              "typesList": [2]
            }, {
              "id": 2582,
              "total": 1,
              "slug": "zzvf",
              "brand": "ZZVF",
              "aliasesList": ["ZZVF (КИТАЙ)", "ZZVF"],
              "typesList": [2]
            }, {
              "id": 4537,
              "total": 1,
              "slug": "ena",
              "brand": "ENA",
              "aliasesList": ["ENA"],
              "typesList": [2]
            }]
          }],
          "total": 9
        }, {
          "id": 10007,
          "name": "Турбонаддув",
          "slug": "turbonadduv",
          "groupsList": [{
            "id": 25087229,
            "name": "Интеркулер",
            "slug": "interkuler",
            "groupsList": [10007],
            "manufacturers": [{
              "id": 256,
              "total": 422,
              "slug": "ava-quality-cooling",
              "brand": "AVA QUALITY COOLING",
              "aliasesList": ["AVA TRUCK", "AVA QUALITY COOLING", "AVA(ВЕНЕСУАЛА)", "AVA COOLING", "AVA", "AVA COOLING SYSTEMS"],
              "typesList": [2]
            }, {
              "id": 2348,
              "total": 236,
              "slug": "van-wezel",
              "brand": "VAN WEZEL",
              "aliasesList": ["VAN WEZEL"],
              "typesList": [2]
            }, {
              "id": 1679,
              "total": 158,
              "slug": "nrf",
              "brand": "NRF",
              "aliasesList": ["NRF(НИДЕРЛАНДЫ)", "NRF", "NRF truck"],
              "typesList": [2]
            }, {
              "id": 1653,
              "total": 139,
              "slug": "nissens",
              "brand": "NISSENS",
              "aliasesList": ["NISSEN", "NISSENS (ДАНИЯ)", "NISSENS", "NISSENS TRUCK"],
              "typesList": [2]
            }, {
              "id": 2342,
              "total": 130,
              "slug": "valeo",
              "brand": "VALEO",
              "aliasesList": ["VALEO KOREA", "VALEO (ФРАНЦИЯ)", "VALEO SERVICE EASTERN", "VALEO (OEM) KOREA", "VALEO ( ШВЕЦИЯ)", "VALEO PHC", "VALEO ELECTRIC AND ELECTRONIC SYSTEMS SP", "VALEO PHP", "VALEO (ЮЖНАЯ КОРЕЯ)", "VALEO", "ｳﾞｧﾚｵﾕﾆｼｱﾄﾗﾝｽﾐｯｼｮﾝ", "VALEO TRUCK"],
              "typesList": [2]
            }, {
              "id": 2310,
              "total": 98,
              "slug": "tyg",
              "brand": "TYG",
              "aliasesList": ["TYG ", "TYG (TONG YANG)", "TYG (ТАЙВАНЬ)", "TYG-TAIWAN", "TYG"],
              "typesList": [2]
            }, {
              "id": 2380,
              "total": 67,
              "slug": "vite",
              "brand": "VITE",
              "aliasesList": ["VITE"],
              "typesList": [2]
            }, {
              "id": 2112,
              "total": 64,
              "slug": "spectra-premium",
              "brand": "SPECTRA PREMIUM",
              "aliasesList": ["SPECTRA", "SPECTRA PREMIUM"],
              "typesList": [2]
            }, {
              "id": 2214,
              "total": 50,
              "slug": "termal",
              "brand": "TERMAL",
              "aliasesList": ["TERMAL", "Termal truck"],
              "typesList": [2]
            }, {
              "id": 300,
              "total": 35,
              "slug": "behr-hella",
              "brand": "BEHR (HELLA)",
              "aliasesList": ["BEHR-HELLA (ГЕРМАНИЯ)", "BEHR_", "BEHR THERMOT-TRONIK", "BEHR SERVICE", "BEHR HELLA SERVICE TRUCK", "BEHR HELLA SERVICE", "BEHR (HELLA)", "HELLA/BEHR-HELLA", "HELLA TRUCK", "HELLA (ГЕРМАНИЯ)", "HELLA"],
              "typesList": [2]
            }, {
              "id": 863,
              "total": 33,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 1421,
              "total": 32,
              "slug": "luzar",
              "brand": "LUZAR",
              "aliasesList": ["LZR", "LUZAR", "ЛУЗАР"],
              "typesList": [2]
            }, {
              "id": 889,
              "total": 24,
              "slug": "frigair",
              "brand": "FRIGAIR",
              "aliasesList": ["FRIGAIR"],
              "typesList": [2]
            }, {
              "id": 1236,
              "total": 24,
              "slug": "kale-oto-radyator",
              "brand": "KALE OTO RADYATOR",
              "aliasesList": ["KALE OTO RADYATÖR", "KALE OTO RADYATOR"],
              "typesList": [2]
            }, {
              "id": 2146,
              "total": 23,
              "slug": "stellox",
              "brand": "STELLOX",
              "aliasesList": ["STELLOX", "STELOX"],
              "typesList": [2]
            }, {
              "id": 1501,
              "total": 20,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 593,
              "total": 16,
              "slug": "denso",
              "brand": "DENSO",
              "aliasesList": ["ND", "DENSO EUROPE B.V", "DENSO", "DENSO PS", "NIPPONDENSO", "DENSO (ЯПОНИЯ)"],
              "typesList": [2]
            }, {
              "id": 1814,
              "total": 16,
              "slug": "porsche",
              "brand": "PORSCHE",
              "aliasesList": ["ПОРШЕ", "PORSCHE", "VW-PORSCHE"],
              "typesList": [1]
            }, {
              "id": 629,
              "total": 12,
              "slug": "dominant",
              "brand": "DOMINANT",
              "aliasesList": ["DOMINANT"],
              "typesList": [2]
            }, {
              "id": 2582,
              "total": 12,
              "slug": "zzvf",
              "brand": "ZZVF",
              "aliasesList": ["ZZVF (КИТАЙ)", "ZZVF"],
              "typesList": [2]
            }, {
              "id": 209,
              "total": 10,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 1652,
              "total": 10,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 2287,
              "total": 8,
              "slug": "trucktec-automotive",
              "brand": "TRUCKTEC AUTOMOTIVE",
              "aliasesList": ["TRUCKTEC AUTOMOTIVE", "TRUCKTEC"],
              "typesList": [2]
            }, {
              "id": 4329,
              "total": 8,
              "slug": "various-mfr",
              "brand": "VARIOUS MFR",
              "aliasesList": ["VARIOUS MFR", "VARIOUS"],
              "typesList": [2]
            }, {
              "id": 1435,
              "total": 7,
              "slug": "magneti-marelli",
              "brand": "MAGNETI MARELLI",
              "aliasesList": ["MAGNETTI MARELI", "MAGNETI MARELI", "MAGNETI", "MAG", "M-MARELLI", "MAGNETI MARELLI", "MARELLI", "MARELLI EXCHANGE", "MARELLI MAGNETI", "MAGNETTI MARELLI", "MAGNETTI"],
              "typesList": [2]
            }, {
              "id": 2387,
              "total": 7,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 1533,
              "total": 6,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1976,
              "total": 6,
              "slug": "sat",
              "brand": "SAT",
              "aliasesList": ["SAT-TYG", "SATA", "SAT (КИТАЙ)", "SAT", "SAT/TYC", "SAT PREMIUM"],
              "typesList": [2]
            }, {
              "id": 560,
              "total": 5,
              "slug": "daf",
              "brand": "DAF",
              "aliasesList": ["LEYLAND AUTO", "DAF", "DAF OE", "LDV", "LDV LIMITED", "LEYLAND", "LEYLAND-DAF"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 5,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 2374,
              "total": 5,
              "slug": "vika",
              "brand": "VIKA",
              "aliasesList": ["VIKA"],
              "typesList": [2]
            }, {
              "id": 498,
              "total": 4,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 1542,
              "total": 4,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 1778,
              "total": 4,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }, {
              "id": 2224,
              "total": 4,
              "slug": "thermotec",
              "brand": "THERMOTEC",
              "aliasesList": ["THERMOTEC"],
              "typesList": [2]
            }, {
              "id": 351,
              "total": 3,
              "slug": "bmw",
              "brand": "BMW",
              "aliasesList": ["BMW MOTO", "БМВ", "OE BMW", "BMW (CN)", "BMW (ГЕРМАНИЯ)", "BMW"],
              "typesList": [1]
            }, {
              "id": 586,
              "total": 3,
              "slug": "dello",
              "brand": "DELLO",
              "aliasesList": ["AUTOMEGA", "DELLO (ГЕРМАНИЯ)", "DELLO (AUTOMEGA)", "DELLO", "AUTOMEGADELLO"],
              "typesList": [2]
            }, {
              "id": 1027,
              "total": 3,
              "slug": "hans-pries",
              "brand": "HANS PRIES",
              "aliasesList": ["HANS PRIES (ГЕРМАНИЯ)", "HANS PRIES", "HANS-PRIS", "HANS PRIES (TOPRAN)", "HANS-PRAES", "TOPRAN"],
              "typesList": [2]
            }, {
              "id": 1100,
              "total": 3,
              "slug": "howo",
              "brand": "HOWO",
              "aliasesList": ["HOWO", "HOWO/SHAANXI"],
              "typesList": [2]
            }, {
              "id": 1734,
              "total": 3,
              "slug": "ossca",
              "brand": "OSSCA",
              "aliasesList": ["OSSCA", "OSSKA"],
              "typesList": [2]
            }, {
              "id": 1755,
              "total": 3,
              "slug": "patron",
              "brand": "PATRON",
              "aliasesList": ["PATRON"],
              "typesList": [2]
            }, {
              "id": 1809,
              "total": 3,
              "slug": "polcar",
              "brand": "POLCAR",
              "aliasesList": ["POLCAR (ПОЛЬША)", "POLCAR", "POLKAR"],
              "typesList": [2]
            }, {
              "id": 248,
              "total": 2,
              "slug": "auto-techteile",
              "brand": "AUTO TECHTEILE",
              "aliasesList": ["AUTO TECHTEILE"],
              "typesList": [2]
            }, {
              "id": 633,
              "total": 2,
              "slug": "dongfeng",
              "brand": "DONGFENG",
              "aliasesList": ["DONGFENG (DFM)", "DONGFENG", "FENGXING (DONGFENG )", "DONFENG", "DONGFENG (DFAC)", "DONGFENG (DFAM)", "DONGFENG (DFL)", "DONGFENG FENGDU", "DONGFENG XIAOKANG"],
              "typesList": [1]
            }, {
              "id": 818,
              "total": 2,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 920,
              "total": 2,
              "slug": "gaz",
              "brand": "GAZ",
              "aliasesList": ["ГАЗ 3302", "ГАЗ 2705", "ГАЗ 52", "ГАЗ 2217", "ГАЗ", "ВОЛГА-ОЙЛ", "ВОЛГА", "ГАЗ 53", "ГАЗ 66", "ГАЗ ОАО", "ГАЗ ООО", "ГАЗЕЛЬ", "ВАЛДАЙ", "ГОРЬКОВСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД ОАО, Н.Н", "ОАО ГАЗ", "УМЗ Г.УЛЬЯНОВСК", "УМЗ Г", "VOLGA", "УМЗ", "GAZ", "ГАЗ 4301", "ГАЗ 3309", "ГАЗ 3308", "ГАЗ 3307", "OE GAZ"],
              "typesList": [1, 3]
            }, {
              "id": 929,
              "total": 2,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1508,
              "total": 2,
              "slug": "metalcaucho",
              "brand": "METALCAUCHO",
              "aliasesList": ["METALCAUCHO"],
              "typesList": [2]
            }, {
              "id": 2037,
              "total": 2,
              "slug": "shaanxi",
              "brand": "SHAANXI",
              "aliasesList": ["SHAANXI"],
              "typesList": [1]
            }, {
              "id": 3906,
              "total": 2,
              "slug": "dynamax",
              "brand": "DYNAMAX",
              "aliasesList": ["DYNAMAX", "DYNAMAX-KOREA"],
              "typesList": [2]
            }, {
              "id": 4872,
              "total": 2,
              "slug": "taspo",
              "brand": "TASPO",
              "aliasesList": ["TASPO", "ТАСПО"],
              "typesList": [2]
            }, {
              "id": 353,
              "total": 1,
              "slug": "body-parts",
              "brand": "BODY PARTS",
              "aliasesList": ["BODY", "BODY PARTS", "BODIPARTS"],
              "typesList": [2]
            }, {
              "id": 1041,
              "total": 1,
              "slug": "hcc",
              "brand": "HCC",
              "aliasesList": ["HCC (OEM) KOREA", "HCC (ЮЖНАЯ КОРЕЯ)", "HCC KOREA", "HCC"],
              "typesList": [2]
            }, {
              "id": 1180,
              "total": 1,
              "slug": "iveco",
              "brand": "IVECO",
              "aliasesList": ["IVECO", "OE IVECO", "IVECO OE", "IVECO-DAILY"],
              "typesList": [1]
            }, {
              "id": 1287,
              "total": 1,
              "slug": "klokkerholm",
              "brand": "KLOKKERHOLM",
              "aliasesList": ["KLOKKERHOL", "KLOKKERHOLM"],
              "typesList": [2]
            }, {
              "id": 1354,
              "total": 1,
              "slug": "land-rover",
              "brand": "LAND ROVER",
              "aliasesList": ["ROVER/LAND ROVER", "LAND ROVER", "ROVER/LANDROVER/MG", "LR ЗАПЧАСТИ", "ЛЭНД РОВЕР", "LAND ROVER", "OE LAND ROVER"],
              "typesList": [1]
            }, {
              "id": 1444,
              "total": 1,
              "slug": "man",
              "brand": "MAN",
              "aliasesList": ["MAN OE", "MAN ORIGINAL", "MAN"],
              "typesList": [1]
            }, {
              "id": 1480,
              "total": 1,
              "slug": "maz",
              "brand": "MAZ",
              "aliasesList": ["ОАО МАЗ", "МАЗ-4370", "MAZ", "МАЗ", "МАЗ (АЛЬТЕРНАТИВА)", "МАЗ ОАО"],
              "typesList": [2]
            }, {
              "id": 1481,
              "total": 1,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 1487,
              "total": 1,
              "slug": "meat-doria",
              "brand": "MEAT & DORIA",
              "aliasesList": ["MEAT&amp;DORIA", "MEAT AND DORIA", "MEAT & DORIA"],
              "typesList": [2]
            }, {
              "id": 1490,
              "total": 1,
              "slug": "mec-diesel",
              "brand": "MEC-DIESEL",
              "aliasesList": ["MEC-DIESEL", "MEC-DISEL"],
              "typesList": [2]
            }, {
              "id": 1527,
              "total": 1,
              "slug": "mini",
              "brand": "MINI",
              "aliasesList": ["MINI COOPER", "MINI"],
              "typesList": [1]
            }, {
              "id": 2156,
              "total": 1,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 2309,
              "total": 1,
              "slug": "tyc",
              "brand": "TYC",
              "aliasesList": ["TYC (ТАЙВАНЬ)", "TYC", "TYС"],
              "typesList": [2]
            }, {
              "id": 2358,
              "total": 1,
              "slug": "vemo-vaico",
              "brand": "VEMO (VAICO)",
              "aliasesList": ["VIEROL AG", "VIEROL", "VEMO", "VAICO/VEMO", "VAICO", "VEMO (VAICO)"],
              "typesList": [2]
            }, {
              "id": 3097,
              "total": 1,
              "slug": "camc",
              "brand": "CAMC",
              "aliasesList": ["CAMC"],
              "typesList": [2]
            }, {
              "id": 4086,
              "total": 1,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 5219,
              "total": 1,
              "slug": "shaaz",
              "brand": "ШААЗ",
              "aliasesList": ["ОАО \"ШАДРИНСКИЙ АВТОАГРЕГАТНЫЙ ЗАВОД\"", "ШААЗ"],
              "typesList": [2]
            }]
          }, {
            "id": 25090682,
            "name": "Патрубок интеркулера",
            "slug": "patrubok-interkulera",
            "groupsList": [10007],
            "manufacturers": [{
              "id": 498,
              "total": 18,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 1864,
              "total": 17,
              "slug": "rapro",
              "brand": "RAPRO",
              "aliasesList": ["RAPRO"],
              "typesList": [2]
            }, {
              "id": 1027,
              "total": 11,
              "slug": "hans-pries",
              "brand": "HANS PRIES",
              "aliasesList": ["HANS PRIES (ГЕРМАНИЯ)", "HANS PRIES", "HANS-PRIS", "HANS PRIES (TOPRAN)", "HANS-PRAES", "TOPRAN"],
              "typesList": [2]
            }, {
              "id": 211,
              "total": 10,
              "slug": "auger",
              "brand": "AUGER",
              "aliasesList": ["AUGER (ГЕРМАНИЯ)", "AUGER", "AUGER AUTOTECHNIK"],
              "typesList": [2]
            }, {
              "id": 818,
              "total": 10,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 1217,
              "total": 9,
              "slug": "jp-group",
              "brand": "JP GROUP",
              "aliasesList": ["JP GROUP (ДАНИЯ)", "JPG", "JP GROUPE", "JP GROUP", "GP GROUP", "JP"],
              "typesList": [2]
            }, {
              "id": 1885,
              "total": 9,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 2587,
              "total": 9,
              "slug": "ssang-yong",
              "brand": "SSANG YONG",
              "aliasesList": ["SSANG YONG", "SSANGYOUNG", "OE SSANG YONG", "SSANGYONG (ОРИГИНАЛ)", "SSANG YONG MOTORS CORPORATION (КОРЕЯ", "SSANGYONG (GEN) KOREA", "ORIGINAL SSANGYONG", "SSANGYONG (КОРЕЯ)", "SY", "SANGYONG", "SSY"],
              "typesList": [1, 3]
            }, {
              "id": 863,
              "total": 8,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 1375,
              "total": 8,
              "slug": "lex",
              "brand": "LEX",
              "aliasesList": ["LEX"],
              "typesList": [2]
            }, {
              "id": 1501,
              "total": 8,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 1100,
              "total": 7,
              "slug": "howo",
              "brand": "HOWO",
              "aliasesList": ["HOWO", "HOWO/SHAANXI"],
              "typesList": [2]
            }, {
              "id": 2019,
              "total": 7,
              "slug": "sem-lastik",
              "brand": "SEM LASTIK",
              "aliasesList": ["SEM OTOMOTIV (SEM LASTIK)", "SEM LASTIK", "S-EM"],
              "typesList": [2]
            }, {
              "id": 2374,
              "total": 5,
              "slug": "vika",
              "brand": "VIKA",
              "aliasesList": ["VIKA"],
              "typesList": [2]
            }, {
              "id": 405,
              "total": 4,
              "slug": "bsg",
              "brand": "BSG",
              "aliasesList": ["BSG (ТУРЦИЯ)", "\"BSG\" BASBUG", "BASBUG (ТУРЦИЯ)", "BASBUG", "BSG"],
              "typesList": [2]
            }, {
              "id": 609,
              "total": 4,
              "slug": "diesel-technic",
              "brand": "DIESEL TECHNIC",
              "aliasesList": ["DT-DIESEL TECHNIC", "DT (ГЕРМАНИЯ)", "DT", "DIESEL TECHNIC", "DT SPARE PARTS"],
              "typesList": [2]
            }, {
              "id": 1354,
              "total": 4,
              "slug": "land-rover",
              "brand": "LAND ROVER",
              "aliasesList": ["ROVER/LAND ROVER", "LAND ROVER", "ROVER/LANDROVER/MG", "LR ЗАПЧАСТИ", "ЛЭНД РОВЕР", "LAND ROVER", "OE LAND ROVER"],
              "typesList": [1]
            }, {
              "id": 1508,
              "total": 4,
              "slug": "metalcaucho",
              "brand": "METALCAUCHO",
              "aliasesList": ["METALCAUCHO"],
              "typesList": [2]
            }, {
              "id": 1516,
              "total": 4,
              "slug": "meyle",
              "brand": "MEYLE",
              "aliasesList": ["MEYLI", "MAYLI", "MEYLE (ГЕРМАНИЯ)", "MEYLE", "MYELE", "MAYLE"],
              "typesList": [2]
            }, {
              "id": 1542,
              "total": 4,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 1959,
              "total": 4,
              "slug": "sampa",
              "brand": "SAMPA",
              "aliasesList": ["SAMPA"],
              "typesList": [2]
            }, {
              "id": 2184,
              "total": 4,
              "slug": "tangde",
              "brand": "TANGDE",
              "aliasesList": ["TANGDE", "TD"],
              "typesList": [2]
            }, {
              "id": 209,
              "total": 3,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 248,
              "total": 3,
              "slug": "auto-techteile",
              "brand": "AUTO TECHTEILE",
              "aliasesList": ["AUTO TECHTEILE"],
              "typesList": [2]
            }, {
              "id": 2037,
              "total": 3,
              "slug": "shaanxi",
              "brand": "SHAANXI",
              "aliasesList": ["SHAANXI"],
              "typesList": [1]
            }, {
              "id": 2146,
              "total": 3,
              "slug": "stellox",
              "brand": "STELLOX",
              "aliasesList": ["STELLOX", "STELOX"],
              "typesList": [2]
            }, {
              "id": 629,
              "total": 2,
              "slug": "dominant",
              "brand": "DOMINANT",
              "aliasesList": ["DOMINANT"],
              "typesList": [2]
            }, {
              "id": 1368,
              "total": 2,
              "slug": "lema",
              "brand": "LEMA",
              "aliasesList": ["LEMA (ГЕРМАНИЯ)", "LEMA"],
              "typesList": [2]
            }, {
              "id": 1493,
              "total": 2,
              "slug": "mega",
              "brand": "MEGA",
              "aliasesList": ["MEGA"],
              "typesList": [2]
            }, {
              "id": 1814,
              "total": 2,
              "slug": "porsche",
              "brand": "PORSCHE",
              "aliasesList": ["ПОРШЕ", "PORSCHE", "VW-PORSCHE"],
              "typesList": [1]
            }, {
              "id": 2168,
              "total": 2,
              "slug": "swag",
              "brand": "SWAG",
              "aliasesList": ["SWAG"],
              "typesList": [2]
            }, {
              "id": 2287,
              "total": 2,
              "slug": "trucktec-automotive",
              "brand": "TRUCKTEC AUTOMOTIVE",
              "aliasesList": ["TRUCKTEC AUTOMOTIVE", "TRUCKTEC"],
              "typesList": [2]
            }, {
              "id": 189,
              "total": 1,
              "slug": "at",
              "brand": "AT",
              "aliasesList": ["AT", "AUTO TECHNOLOGIES GROUP", "AUTO TECHNOLOGIES GROUP S.R.O."],
              "typesList": [2]
            }, {
              "id": 396,
              "total": 1,
              "slug": "britpart",
              "brand": "BRITPART",
              "aliasesList": ["BRITPART"],
              "typesList": [2]
            }, {
              "id": 491,
              "total": 1,
              "slug": "chevrolet",
              "brand": "CHEVROLET",
              "aliasesList": ["CHEVROLET LANOS", "OE CHEVROLET", "CHEVROLET (SGM)", "CHEVROLET (SGMW)", "CHEVROLET DAT", "CHEVROLET"],
              "typesList": [2]
            }, {
              "id": 551,
              "total": 1,
              "slug": "cummins",
              "brand": "CUMMINS",
              "aliasesList": ["CUMM", "CUMMINS", "CUMMINS CH", "CUMMINS TURBO TECHNOLOGIES LTD", "CUMMINS INC"],
              "typesList": [1]
            }, {
              "id": 560,
              "total": 1,
              "slug": "daf",
              "brand": "DAF",
              "aliasesList": ["LEYLAND AUTO", "DAF", "DAF OE", "LDV", "LDV LIMITED", "LEYLAND", "LEYLAND-DAF"],
              "typesList": [1]
            }, {
              "id": 633,
              "total": 1,
              "slug": "dongfeng",
              "brand": "DONGFENG",
              "aliasesList": ["DONGFENG (DFM)", "DONGFENG", "FENGXING (DONGFENG )", "DONFENG", "DONGFENG (DFAC)", "DONGFENG (DFAM)", "DONGFENG (DFL)", "DONGFENG FENGDU", "DONGFENG XIAOKANG"],
              "typesList": [1]
            }, {
              "id": 793,
              "total": 1,
              "slug": "fast",
              "brand": "FAST",
              "aliasesList": ["FAST"],
              "typesList": [2]
            }, {
              "id": 1180,
              "total": 1,
              "slug": "iveco",
              "brand": "IVECO",
              "aliasesList": ["IVECO", "OE IVECO", "IVECO OE", "IVECO-DAILY"],
              "typesList": [1]
            }, {
              "id": 1198,
              "total": 1,
              "slug": "jeep",
              "brand": "JEEP",
              "aliasesList": ["JEEP"],
              "typesList": [2]
            }, {
              "id": 1238,
              "total": 1,
              "slug": "kamaz",
              "brand": "KAMAZ",
              "aliasesList": ["ПАО КАМАЗ", "KAMAZ", "КАМАЗ", "КАМАЗ (АЛЬТЕРНАТИВА)"],
              "typesList": [1]
            }, {
              "id": 1443,
              "total": 1,
              "slug": "malo",
              "brand": "MALO",
              "aliasesList": ["AKRON", "MALO", "MALÒ", "AKRONMALO"],
              "typesList": [2]
            }, {
              "id": 1533,
              "total": 1,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1713,
              "total": 1,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 1778,
              "total": 1,
              "slug": "peugeot-citroen",
              "brand": "PEUGEOT/CITROEN",
              "aliasesList": ["CITROEN -&GT; PEUGEOT", "PSA", "OEM CITROEN", "СИТРОЕН", "OEM PEUGEOT", "CITROEN", "CITROEN-PEUGEOT", "OE PEUGEOT", "CITROEN (DF-PSA)", "PEUGEOT", "PEUGEOT/CITROEN", "PEUGEOT (PSA)", "PEUGEOT / CITROEN (ФРАНЦИЯ)", "CITROËN"],
              "typesList": [1]
            }, {
              "id": 2156,
              "total": 1,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 2224,
              "total": 1,
              "slug": "thermotec",
              "brand": "THERMOTEC",
              "aliasesList": ["THERMOTEC"],
              "typesList": [2]
            }, {
              "id": 2387,
              "total": 1,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 2496,
              "total": 1,
              "slug": "zil",
              "brand": "ZIL",
              "aliasesList": ["ЗИЛ (АЛЬТЕРНАТИВА)", "АМО ЗИЛ ОАО", "ЗИЛ", "ZIL"],
              "typesList": [2]
            }, {
              "id": 4116,
              "total": 1,
              "slug": "starkmeister",
              "brand": "STARKMEISTER",
              "aliasesList": ["STARKMEISTER", "STARKMEISTER AIR SPR"],
              "typesList": [2]
            }, {
              "id": 4434,
              "total": 1,
              "slug": "yumak",
              "brand": "YUMAK",
              "aliasesList": ["YUMAK"],
              "typesList": [2]
            }, {
              "id": 5385,
              "total": 1,
              "slug": "wosm",
              "brand": "WOSM",
              "aliasesList": ["WOSM"],
              "typesList": [2]
            }, {
              "id": 18076,
              "total": 1,
              "slug": "siegel",
              "brand": "SIEGEL",
              "aliasesList": ["SIEGEL"],
              "typesList": [2]
            }]
          }, {
            "id": 25099541,
            "name": "Радиатор интеркулера",
            "slug": "radiator-interkulera_1",
            "groupsList": [10007],
            "manufacturers": [{
              "id": 1976,
              "total": 25,
              "slug": "sat",
              "brand": "SAT",
              "aliasesList": ["SAT-TYG", "SATA", "SAT (КИТАЙ)", "SAT", "SAT/TYC", "SAT PREMIUM"],
              "typesList": [2]
            }, {
              "id": 209,
              "total": 20,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 1542,
              "total": 14,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 256,
              "total": 12,
              "slug": "ava-quality-cooling",
              "brand": "AVA QUALITY COOLING",
              "aliasesList": ["AVA TRUCK", "AVA QUALITY COOLING", "AVA(ВЕНЕСУАЛА)", "AVA COOLING", "AVA", "AVA COOLING SYSTEMS"],
              "typesList": [2]
            }, {
              "id": 1421,
              "total": 9,
              "slug": "luzar",
              "brand": "LUZAR",
              "aliasesList": ["LZR", "LUZAR", "ЛУЗАР"],
              "typesList": [2]
            }, {
              "id": 498,
              "total": 8,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 88,
              "total": 4,
              "slug": "aks-dasis",
              "brand": "AKS DASIS",
              "aliasesList": ["AKS DASIS", "DASIS"],
              "typesList": [2]
            }, {
              "id": 1501,
              "total": 4,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 1809,
              "total": 4,
              "slug": "polcar",
              "brand": "POLCAR",
              "aliasesList": ["POLCAR (ПОЛЬША)", "POLCAR", "POLKAR"],
              "typesList": [2]
            }, {
              "id": 2342,
              "total": 4,
              "slug": "valeo",
              "brand": "VALEO",
              "aliasesList": ["VALEO KOREA", "VALEO (ФРАНЦИЯ)", "VALEO SERVICE EASTERN", "VALEO (OEM) KOREA", "VALEO ( ШВЕЦИЯ)", "VALEO PHC", "VALEO ELECTRIC AND ELECTRONIC SYSTEMS SP", "VALEO PHP", "VALEO (ЮЖНАЯ КОРЕЯ)", "VALEO", "ｳﾞｧﾚｵﾕﾆｼｱﾄﾗﾝｽﾐｯｼｮﾝ", "VALEO TRUCK"],
              "typesList": [2]
            }, {
              "id": 2587,
              "total": 4,
              "slug": "ssang-yong",
              "brand": "SSANG YONG",
              "aliasesList": ["SSANG YONG", "SSANGYOUNG", "OE SSANG YONG", "SSANGYONG (ОРИГИНАЛ)", "SSANG YONG MOTORS CORPORATION (КОРЕЯ", "SSANGYONG (GEN) KOREA", "ORIGINAL SSANGYONG", "SSANGYONG (КОРЕЯ)", "SY", "SANGYONG", "SSY"],
              "typesList": [1, 3]
            }, {
              "id": 172,
              "total": 3,
              "slug": "asam-sa",
              "brand": "ASAM-SA",
              "aliasesList": ["ASAM-SA", "ASAM"],
              "typesList": [2]
            }, {
              "id": 351,
              "total": 3,
              "slug": "bmw",
              "brand": "BMW",
              "aliasesList": ["BMW MOTO", "БМВ", "OE BMW", "BMW (CN)", "BMW (ГЕРМАНИЯ)", "BMW"],
              "typesList": [1]
            }, {
              "id": 353,
              "total": 3,
              "slug": "body-parts",
              "brand": "BODY PARTS",
              "aliasesList": ["BODY", "BODY PARTS", "BODIPARTS"],
              "typesList": [2]
            }, {
              "id": 818,
              "total": 3,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 863,
              "total": 3,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 929,
              "total": 2,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1180,
              "total": 2,
              "slug": "iveco",
              "brand": "IVECO",
              "aliasesList": ["IVECO", "OE IVECO", "IVECO OE", "IVECO-DAILY"],
              "typesList": [1]
            }, {
              "id": 1217,
              "total": 2,
              "slug": "jp-group",
              "brand": "JP GROUP",
              "aliasesList": ["JP GROUP (ДАНИЯ)", "JPG", "JP GROUPE", "JP GROUP", "GP GROUP", "JP"],
              "typesList": [2]
            }, {
              "id": 1354,
              "total": 2,
              "slug": "land-rover",
              "brand": "LAND ROVER",
              "aliasesList": ["ROVER/LAND ROVER", "LAND ROVER", "ROVER/LANDROVER/MG", "LR ЗАПЧАСТИ", "ЛЭНД РОВЕР", "LAND ROVER", "OE LAND ROVER"],
              "typesList": [1]
            }, {
              "id": 1652,
              "total": 2,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 1679,
              "total": 2,
              "slug": "nrf",
              "brand": "NRF",
              "aliasesList": ["NRF(НИДЕРЛАНДЫ)", "NRF", "NRF truck"],
              "typesList": [2]
            }, {
              "id": 4086,
              "total": 2,
              "slug": "toyota",
              "brand": "TOYOTA",
              "aliasesList": ["Тайота", "TOYOTA", "ТОЙОТА МОТОР", "TOYOTA GENUINE PARTS ЯПОНИЯ", "TOYOTA MOTOR CORPORATION", "LEXUS", "TOYOTA (ЯПОНИЯ)", "TOYOTA ДВИГАТЕЛЬ", "TOYOTA КУЗОВ", "TOYOTA КУЗОВНОЕ", "TOYOTA ХОДОВКА", "OE TOYOTA", "OEM TOYOTA", "TY", "TOYOTA-LEXUS", "TOYOTA-ТАЙЛАНД", "ТОЙОТА МОТОР ЮРОП НВ", "ТОЙОТА МОТОР КОРПОРЕЙШН", "TOYOTA - TOYOTA"],
              "typesList": [1]
            }, {
              "id": 889,
              "total": 1,
              "slug": "frigair",
              "brand": "FRIGAIR",
              "aliasesList": ["FRIGAIR"],
              "typesList": [2]
            }, {
              "id": 987,
              "total": 1,
              "slug": "great-wall",
              "brand": "GREAT WALL",
              "aliasesList": ["GREAT WALL MOTORS ", "GREAT WALL (КИТАЙ)", " GREAT WALL", "GREAT WOOL", "GREAT WALL", "GW"],
              "typesList": [1]
            }, {
              "id": 1186,
              "total": 1,
              "slug": "jaguar",
              "brand": "JAGUAR",
              "aliasesList": ["JAGUAR", "ЯГУАР", "JAGUAR ЗАПЧАСТИ"],
              "typesList": [1]
            }, {
              "id": 1375,
              "total": 1,
              "slug": "lex",
              "brand": "LEX",
              "aliasesList": ["LEX"],
              "typesList": [2]
            }, {
              "id": 1493,
              "total": 1,
              "slug": "mega",
              "brand": "MEGA",
              "aliasesList": ["MEGA"],
              "typesList": [2]
            }, {
              "id": 1533,
              "total": 1,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1653,
              "total": 1,
              "slug": "nissens",
              "brand": "NISSENS",
              "aliasesList": ["NISSEN", "NISSENS (ДАНИЯ)", "NISSENS", "NISSENS TRUCK"],
              "typesList": [2]
            }, {
              "id": 1885,
              "total": 1,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 1988,
              "total": 1,
              "slug": "scania",
              "brand": "SCANIA",
              "aliasesList": ["SCANIA", "SCANIA EESTI AS", "OE Scania"],
              "typesList": [1]
            }]
          }, {
            "id": 25089347,
            "name": "Турбина",
            "slug": "turbina",
            "groupsList": [10007],
            "manufacturers": [{
              "id": 1501,
              "total": 19,
              "slug": "mercedes-benz",
              "brand": "MERCEDES-BENZ",
              "aliasesList": ["MERCEDES-BENZ", "MERCEDERS", "MB OE", "MB", "ORIGINAL MER", "MERCEDES", "MERCEDES-BENZ (ГЕРМАНИЯ)", "МЕРСЕДЕС-БЕНЦ", "МВ - MERCEDES", "MERSEDES", "MERSEDES BENZ", "МВ", "ZUBEHOR", "MERCE", "OE MERCEDES BENZ", "OE MB"],
              "typesList": [1]
            }, {
              "id": 361,
              "total": 12,
              "slug": "borg-warner",
              "brand": "BORG WARNER",
              "aliasesList": ["BORG WARNER (ГЕРМАНИЯ)", "BORG WARNER", "BORGWARNER TURBO SYSTEMS GMBH", "BORGWARNER (KKK)"],
              "typesList": [2]
            }, {
              "id": 1100,
              "total": 8,
              "slug": "howo",
              "brand": "HOWO",
              "aliasesList": ["HOWO", "HOWO/SHAANXI"],
              "typesList": [2]
            }, {
              "id": 2387,
              "total": 7,
              "slug": "volvo",
              "brand": "VOLVO",
              "aliasesList": ["VOLVO (ШВЕЦИЯ)", "VOLVO", "VOLVO ЗАПЧАСТИ", "OE VOLVO", "VOLVO OE", "VOLVO CONSTRUCTION", "VOLVO TRUCKS NAMERICA INC"],
              "typesList": [1]
            }, {
              "id": 209,
              "total": 6,
              "slug": "vag",
              "brand": "VAG",
              "aliasesList": ["VW", "VOLKSWAGEN", "ФОЛЬКСВАГЕН ЛЕГКОВЫЕ МОСКВА", "SKODA ACC", "SKODA AUTO A", "VAG", "VOLKSWAGEN AG", "VW / AUDI", "AUDI/ VW", "AUDI ACC", "AUDI (ГЕРМАНИЯ)", "SEAT", "AUDI", "ФОЛЬКСВАГЕН ГРУПП", "WV", "VAG (VW,AUDI,SKODA,SEAT)", "SKODA", "VAG (ГЕРМАНИЯ)", "VAG ОРИГИНАЛЬНЫЕ ЗАПЧАСТИ", "VW/SEAT", "VW ACC", "OE VAG"],
              "typesList": [1]
            }, {
              "id": 498,
              "total": 6,
              "slug": "chrysler",
              "brand": "CHRYSLER",
              "aliasesList": ["CHRYSLER-JEEP-DODGE", "CHRYSLER USA", "CHRYSLER / DODGE / JEEP", "CHRYSLER (США)", "CHRYS", "СHRYSLER", "CHRYSLER"],
              "typesList": [1]
            }, {
              "id": 863,
              "total": 6,
              "slug": "ford",
              "brand": "FORD",
              "aliasesList": ["FORD EUROPA", "FORD EUROPE", "FORD OTOSAN", "OE FORD", "FORD AUSTRALIA", "FORD RACING", "FORD TRANSIT", "FORD EU", "FORD EUR", "ФОРД", "FORD -O.E.", "FORD NEW HOLLAND", "COLLECTION FORD", "FORD (CHANGAN)", "FORD (JMC)", "FORD", "FORD (США)", "CHANGAN FORD", "FORD-USA", "FORD US", "FORD ASIA / OZEANIA", "OEM FORD", "FORD MOTOR CO"],
              "typesList": [1]
            }, {
              "id": 917,
              "total": 6,
              "slug": "garrett",
              "brand": "GARRETT",
              "aliasesList": ["GARRETT REMAN", "GARRET", "REMAN", "GARRETTS (OEM)", "GARRETT(Б/У, REBUILD)", "GARRETT", "GARRET KOREA", "GARRETT PERFORMANCE", "GARRETT ENGINE BOOSTING SYSTEMS"],
              "typesList": [2]
            }, {
              "id": 1127,
              "total": 6,
              "slug": "ihi",
              "brand": "IHI",
              "aliasesList": ["IHI"],
              "typesList": [2]
            }, {
              "id": 1439,
              "total": 6,
              "slug": "mahle",
              "brand": "MAHLE",
              "aliasesList": ["MAHLEТЕРМОСТАТЫ", "KNEHT", "KNECHT-MAHLE", "KNECHT(MAHLE FILTERS)", "KNECHT(MAHLE FILTER)", "KNECHT", "MAHLE", "MAHLE-KNECHT", "MAHLE-PISTON", "KNTCHT", "MAHLE (ГЕРМАНИЯ)", "MAHLE KNECHTKITTO", "MAHLE KOLBEN", "MAHLE ORIGINAL", "MAHLE POLSKA SP", "MAHLE/KNECHT (ГЕРМАНИЯ)", "MAHLE-FILTER", "MAHLE TRUCK", "KNECHT TRUCK", "MAHLE/KNECHT TRUCK", "KNECHT/MAHLE TRUCK", "MAHLE-CLEVITE"],
              "typesList": [2]
            }, {
              "id": 1533,
              "total": 6,
              "slug": "mitsubishi",
              "brand": "MITSUBISHI",
              "aliasesList": ["MITSUBISHI TURBOCHARGERS", "MITSUBISHI (ЯПОНИЯ)", "MITSUBISHI (OEM) JAPAN", "MITSU", "МИТСУБИШИ", "MITSUBISHI", "МИТСУБИШИ MITSUBISHI", "MMC", "MITSUBISHI TURBOCHARGER", "MITSUBISHI OE", "MITSUBISHI MOTORS", "MITSUBISHI MOTOR CO.", "OE MITSUBISHI", "MITSUBISHI CORPORATION"],
              "typesList": [1]
            }, {
              "id": 1599,
              "total": 5,
              "slug": "mv-parts",
              "brand": "MV-PARTS",
              "aliasesList": ["MV-PARTS"],
              "typesList": [2]
            }, {
              "id": 551,
              "total": 3,
              "slug": "cummins",
              "brand": "CUMMINS",
              "aliasesList": ["CUMM", "CUMMINS", "CUMMINS CH", "CUMMINS TURBO TECHNOLOGIES LTD", "CUMMINS INC"],
              "typesList": [1]
            }, {
              "id": 818,
              "total": 3,
              "slug": "fiat",
              "brand": "FIAT",
              "aliasesList": ["OE FIAT", "FIAT / LANCIA / ALFA", "FIAT GROUP", "DUCATO 244", "FIAT-ALFA-LANCIA", "DUCATO", "ALFAROMEO/FIAT/LANCIA", "ФИАТ", "DUCATO 250", "FIAT", "FIAT-ALFA ROMEO-LANCIA"],
              "typesList": [1]
            }, {
              "id": 1180,
              "total": 3,
              "slug": "iveco",
              "brand": "IVECO",
              "aliasesList": ["IVECO", "OE IVECO", "IVECO OE", "IVECO-DAILY"],
              "typesList": [1]
            }, {
              "id": 1652,
              "total": 3,
              "slug": "nissan",
              "brand": "NISSAN",
              "aliasesList": ["NISSAN (РОССИЯ)", "NISSAN (ЯПОНИЯ)", "NISSAN OEM", "NISSAN ДВИГАТЕЛЬ", "NISSAN КУЗОВ", "NISSAN-DATSUN", "NISSANDIESEL", "NISSAN-INFINITI", "OE NISSAN", "DATSUN", "НИССАН", "НИССАН NISSAN", "НИССАН ГРУПП", "UD NISSAN DIESEL", "NISSAN", "ALMERA N16", "INFINITY", "INFINITI", "NISSAN - INFINITY"],
              "typesList": [1]
            }, {
              "id": 405,
              "total": 2,
              "slug": "bsg",
              "brand": "BSG",
              "aliasesList": ["BSG (ТУРЦИЯ)", "\"BSG\" BASBUG", "BASBUG (ТУРЦИЯ)", "BASBUG", "BSG"],
              "typesList": [2]
            }, {
              "id": 609,
              "total": 2,
              "slug": "diesel-technic",
              "brand": "DIESEL TECHNIC",
              "aliasesList": ["DT-DIESEL TECHNIC", "DT (ГЕРМАНИЯ)", "DT", "DIESEL TECHNIC", "DT SPARE PARTS"],
              "typesList": [2]
            }, {
              "id": 1217,
              "total": 2,
              "slug": "jp-group",
              "brand": "JP GROUP",
              "aliasesList": ["JP GROUP (ДАНИЯ)", "JPG", "JP GROUPE", "JP GROUP", "GP GROUP", "JP"],
              "typesList": [2]
            }, {
              "id": 1481,
              "total": 2,
              "slug": "mazda",
              "brand": "MAZDA",
              "aliasesList": ["ｼﾞｪﾈﾘｯｸﾊﾟｰﾂ", "MAZDA", "MAZDA (ЯПОНИЯ)", "OE MAZDA", "Мazda"],
              "typesList": [1]
            }, {
              "id": 1989,
              "total": 2,
              "slug": "scan-tech",
              "brand": "SCAN-TECH",
              "aliasesList": ["SCAN-TECH"],
              "typesList": [2]
            }, {
              "id": 2037,
              "total": 2,
              "slug": "shaanxi",
              "brand": "SHAANXI",
              "aliasesList": ["SHAANXI"],
              "typesList": [1]
            }, {
              "id": 2156,
              "total": 2,
              "slug": "subaru",
              "brand": "SUBARU",
              "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
              "typesList": [1]
            }, {
              "id": 351,
              "total": 1,
              "slug": "bmw",
              "brand": "BMW",
              "aliasesList": ["BMW MOTO", "БМВ", "OE BMW", "BMW (CN)", "BMW (ГЕРМАНИЯ)", "BMW"],
              "typesList": [1]
            }, {
              "id": 465,
              "total": 1,
              "slug": "case-ih",
              "brand": "CASE IH",
              "aliasesList": ["CASE IH", "CASE", "CASE CONSTRUCTION"],
              "typesList": [2]
            }, {
              "id": 722,
              "total": 1,
              "slug": "emmerre",
              "brand": "EMMERRE",
              "aliasesList": ["EMMERRE (ИТАЛИЯ)", "EMMERRE SRL", "EMMERRE"],
              "typesList": [2]
            }, {
              "id": 929,
              "total": 1,
              "slug": "general-motors",
              "brand": "GENERAL MOTORS",
              "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
              "typesList": [1]
            }, {
              "id": 1173,
              "total": 1,
              "slug": "isuzu",
              "brand": "ISUZU",
              "aliasesList": ["ISUZU"],
              "typesList": [1]
            }, {
              "id": 1186,
              "total": 1,
              "slug": "jaguar",
              "brand": "JAGUAR",
              "aliasesList": ["JAGUAR", "ЯГУАР", "JAGUAR ЗАПЧАСТИ"],
              "typesList": [1]
            }, {
              "id": 1190,
              "total": 1,
              "slug": "japanparts",
              "brand": "JAPANPARTS",
              "aliasesList": ["JAPANPARTS (ИТАЛИЯ)", "JAPAN PARTS (К-Т 2ШТ.)", "JAPANPARTS", "JAPANSPARTS"],
              "typesList": [2]
            }, {
              "id": 1280,
              "total": 1,
              "slug": "kkk",
              "brand": "KKK",
              "aliasesList": ["3K", "KKK"],
              "typesList": [2]
            }, {
              "id": 1354,
              "total": 1,
              "slug": "land-rover",
              "brand": "LAND ROVER",
              "aliasesList": ["ROVER/LAND ROVER", "LAND ROVER", "ROVER/LANDROVER/MG", "LR ЗАПЧАСТИ", "ЛЭНД РОВЕР", "LAND ROVER", "OE LAND ROVER"],
              "typesList": [1]
            }, {
              "id": 1444,
              "total": 1,
              "slug": "man",
              "brand": "MAN",
              "aliasesList": ["MAN OE", "MAN ORIGINAL", "MAN"],
              "typesList": [1]
            }, {
              "id": 1542,
              "total": 1,
              "slug": "mobis",
              "brand": "MOBIS",
              "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
              "typesList": [1]
            }, {
              "id": 1713,
              "total": 1,
              "slug": "opel",
              "brand": "OPEL",
              "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
              "typesList": [1]
            }, {
              "id": 1885,
              "total": 1,
              "slug": "renault",
              "brand": "RENAULT",
              "aliasesList": ["RVI", "OEM RENAULT", "RENAULT ОРИГИНАЛ", "OE RENAULT", "RENAULT TRUCKS", "RENAULT", "RENAULT S", "RENAULT (ФРАНЦИЯ)", "RENAULT (РЕНО)", "RVI OE"],
              "typesList": [1]
            }, {
              "id": 2000,
              "total": 1,
              "slug": "schwitzer",
              "brand": "SCHWITZER",
              "aliasesList": ["SCHWITZER"],
              "typesList": [2]
            }, {
              "id": 2374,
              "total": 1,
              "slug": "vika",
              "brand": "VIKA",
              "aliasesList": ["VIKA"],
              "typesList": [2]
            }, {
              "id": 18111,
              "total": 1,
              "slug": "sdlg",
              "brand": "SDLG",
              "aliasesList": ["SDLG"],
              "typesList": [2]
            }]
          }],
          "total": 4
        }],
        "total": 31
      }, {
        "id": 2,
        "name": "Выхлопная система",
        "slug": "vyhlopnaya-sistema",
        "groupsList": [{
          "id": 10006,
          "name": "Глушитель",
          "slug": "glushitel",
          "groupsList": [],
          "total": 16
        }, {
          "id": 10008,
          "name": "Система рециркуляции отработанных газов (EGR)",
          "slug": "sistema-recirkulyacii-otrabotannyh-gazov-egr",
          "groupsList": [],
          "total": 1
        }],
        "total": 17
      }, {
        "id": 3,
        "name": "Детали салона",
        "slug": "detali-salona",
        "groupsList": [{
          "id": 10016,
          "name": "Бардачок",
          "slug": "bardachok",
          "groupsList": [],
          "total": 3
        }, {
          "id": 10014,
          "name": "Коврики | Чехлы",
          "slug": "kovriki-chehly",
          "groupsList": [],
          "total": 4
        }, {
          "id": 10010,
          "name": "Кресла | Диваны",
          "slug": "kresla-divany",
          "groupsList": [],
          "total": 5
        }, {
          "id": 10009,
          "name": "Обшивки | Панели",
          "slug": "obshivki-paneli",
          "groupsList": [],
          "total": 2
        }, {
          "id": 10015,
          "name": "Освещение салона",
          "slug": "osveshhenie-salona",
          "groupsList": [],
          "total": 2
        }, {
          "id": 10013,
          "name": "Солнцезащитные козырьки",
          "slug": "solncezashhitnye-kozyrki",
          "groupsList": [],
          "total": 4
        }, {
          "id": 10012,
          "name": "Стеклоподъемники",
          "slug": "steklopodemniki",
          "groupsList": [],
          "total": 9
        }, {
          "id": 10011,
          "name": "Торпеда",
          "slug": "torpeda",
          "groupsList": [],
          "total": 1
        }],
        "total": 30
      }, {
        "id": 4,
        "name": "Дополнительное оборудование",
        "slug": "dopolnitelnoe-oborudovanie",
        "groupsList": [{
          "id": 10036,
          "name": "Автовинил",
          "slug": "avtovinil",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10024,
          "name": "Аксессуары",
          "slug": "aksessuary",
          "groupsList": [],
          "total": 2
        }, {
          "id": 10022,
          "name": "Аудио",
          "slug": "audio",
          "groupsList": [],
          "total": 13
        }, {
          "id": 10029,
          "name": "Багажники | Крепления",
          "slug": "bagazhniki-krepleniya",
          "groupsList": [],
          "total": 4
        }, {
          "id": 10019,
          "name": "Вспомогательные системы | Парктроники",
          "slug": "vspomogatelnye-sistemy-parktroniki",
          "groupsList": [],
          "total": 5
        }, {
          "id": 10020,
          "name": "Датчики | Подиумы",
          "slug": "datchiki-podiumy",
          "groupsList": [],
          "total": 3
        }, {
          "id": 10033,
          "name": "Дефлекторы",
          "slug": "deflektory",
          "groupsList": [],
          "total": 3
        }, {
          "id": 10018,
          "name": "Защиты",
          "slug": "zashhity",
          "groupsList": [],
          "total": 3
        }, {
          "id": 10035,
          "name": "Кенгурин | Защита бампера",
          "slug": "kengurin-zashhita-bampera",
          "groupsList": [],
          "total": 2
        }, {
          "id": 10034,
          "name": "Кожух (чехол) запасного колеса",
          "slug": "kozhuh-chehol-zapasnogo-kolesa",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10031,
          "name": "Лебедки",
          "slug": "lebedki",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10021,
          "name": "Люки",
          "slug": "lyuki",
          "groupsList": [],
          "total": 3
        }, {
          "id": 10026,
          "name": "Мультимедия | Навигация",
          "slug": "multimediya-navigaciya",
          "groupsList": [],
          "total": 8
        }, {
          "id": 10027,
          "name": "Охранные системы",
          "slug": "ohrannye-sistemy",
          "groupsList": [],
          "total": 5
        }, {
          "id": 10032,
          "name": "Предпусковые подогреватели",
          "slug": "predpuskovye-podogrevateli",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10030,
          "name": "Фаркопы",
          "slug": "farkopy",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10023,
          "name": "Шумоизоляция",
          "slug": "shumoizolyaciya",
          "groupsList": [],
          "total": 4
        }],
        "total": 60
      }, {
        "id": 5,
        "name": "Жидкости | Автохимия",
        "slug": "zhidkosti-avtohimiya",
        "groupsList": [{
          "id": 10037,
          "name": "Жидкости",
          "slug": "zhidkosti",
          "groupsList": [],
          "total": 2
        }, {
          "id": 10038,
          "name": "Масла",
          "slug": "masla",
          "groupsList": [],
          "total": 4
        }],
        "total": 6
      }, {
        "id": 6,
        "name": "Запчасти двигателя",
        "slug": "zapchasti-dvigatelya",
        "groupsList": [{
          "id": 10046,
          "name": "Блок управления",
          "slug": "blok-upravleniya",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10045,
          "name": "ГРМ | ГБЦ",
          "slug": "grm-gbc",
          "groupsList": [],
          "total": 31
        }, {
          "id": 10039,
          "name": "Двигатель",
          "slug": "dvigatel",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10047,
          "name": "Запчасти блока цилиндров",
          "slug": "zapchasti-bloka-cilindrov",
          "groupsList": [],
          "total": 10
        }, {
          "id": 10040,
          "name": "Опоры двигателя",
          "slug": "opory-dvigatelya",
          "groupsList": [],
          "total": 8
        }, {
          "id": 10042,
          "name": "Поршневая группа | КШМ",
          "slug": "porshnevaya-gruppa-kshm",
          "groupsList": [],
          "total": 21
        }, {
          "id": 10043,
          "name": "Расходники",
          "slug": "rashodniki",
          "groupsList": [],
          "total": 5
        }, {
          "id": 10044,
          "name": "Система смазки",
          "slug": "sistema-smazki",
          "groupsList": [],
          "total": 11
        }, {
          "id": 10041,
          "name": "Щупы | Крышки",
          "slug": "shhupy-kryshki",
          "groupsList": [],
          "total": 4
        }],
        "total": 92
      }, {
        "id": 7,
        "name": "Запчасти кузова",
        "slug": "zapchasti-kuzova",
        "groupsList": [{
          "id": 10054,
          "name": "Багажник",
          "slug": "bagazhnik",
          "groupsList": [],
          "total": 12
        }, {
          "id": 10052,
          "name": "Бампера",
          "slug": "bampera",
          "groupsList": [],
          "total": 23
        }, {
          "id": 10050,
          "name": "Двери",
          "slug": "dveri",
          "groupsList": [],
          "total": 15
        }, {
          "id": 10049,
          "name": "Зеркала",
          "slug": "zerkala",
          "groupsList": [],
          "total": 4
        }, {
          "id": 10051,
          "name": "Капот и оснащение",
          "slug": "kapot-i-osnashhenie",
          "groupsList": [],
          "total": 48
        }, {
          "id": 10053,
          "name": "Крылья",
          "slug": "krylya",
          "groupsList": [],
          "total": 6
        }, {
          "id": 10058,
          "name": "Кузов по частям",
          "slug": "kuzov-po-chastyam",
          "groupsList": [],
          "total": 29
        }, {
          "id": 10048,
          "name": "Молдинги | Накладки",
          "slug": "moldingi-nakladki",
          "groupsList": [],
          "total": 20
        }, {
          "id": 10056,
          "name": "Омыватели",
          "slug": "omyvateli",
          "groupsList": [],
          "total": 11
        }, {
          "id": 10055,
          "name": "Решетки радиатора",
          "slug": "reshetki-radiatora",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10057,
          "name": "Стеклоочистители",
          "slug": "stekloochistiteli",
          "groupsList": [],
          "total": 10
        }, {
          "id": 10059,
          "name": "Экстерьер",
          "slug": "eksterer",
          "groupsList": [],
          "total": 13
        }],
        "total": 192
      }, {
        "id": 8,
        "name": "Оптика",
        "slug": "optika",
        "groupsList": [{
          "id": 10061,
          "name": "Габариты",
          "slug": "gabarity",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10065,
          "name": "Ксенон | Би-ксенон",
          "slug": "ksenon-bi-ksenon",
          "groupsList": [],
          "total": 3
        }, {
          "id": 10063,
          "name": "Лампы | Светодиоды",
          "slug": "lampy-svetodiody",
          "groupsList": [],
          "total": 2
        }, {
          "id": 10062,
          "name": "Поворотники",
          "slug": "povorotniki",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10060,
          "name": "Фары",
          "slug": "fary",
          "groupsList": [],
          "total": 9
        }, {
          "id": 10064,
          "name": "Фонари",
          "slug": "fonari",
          "groupsList": [],
          "total": 6
        }],
        "total": 22
      }, {
        "id": 9,
        "name": "Подвеска",
        "slug": "podveska",
        "groupsList": [{
          "id": 10068,
          "name": "Запчасти подвески",
          "slug": "zapchasti-podveski",
          "groupsList": [],
          "total": 33
        }, {
          "id": 10067,
          "name": "Мосты",
          "slug": "mosty",
          "groupsList": [],
          "total": 11
        }, {
          "id": 10066,
          "name": "Стойки амортизационные",
          "slug": "stojki-amortizacionnye",
          "groupsList": [],
          "total": 15
        }],
        "total": 59
      }, {
        "id": 23,
        "name": "Прочее",
        "slug": "prochee",
        "groupsList": [{
          "id": 10134,
          "name": "Прочее",
          "slug": "prochee",
          "groupsList": [],
          "total": 486
        }],
        "total": 486
      }, {
        "id": 10,
        "name": "Рулевое управление",
        "slug": "rulevoe-upravlenie",
        "groupsList": [{
          "id": 10070,
          "name": "Рулевой механизм",
          "slug": "rulevoj-mehanizm",
          "groupsList": [],
          "total": 18
        }, {
          "id": 10071,
          "name": "Руль | Рулевая колонка",
          "slug": "rul-rulevaya-kolonka",
          "groupsList": [],
          "total": 8
        }, {
          "id": 10069,
          "name": "Усилитель рулевого управления",
          "slug": "usilitel-rulevogo-upravleniya",
          "groupsList": [],
          "total": 3
        }],
        "total": 29
      }, {
        "id": 11,
        "name": "Система зажигания",
        "slug": "sistema-zazhiganiya",
        "groupsList": [{
          "id": 10074,
          "name": "Замки зажигания",
          "slug": "zamki-zazhiganiya",
          "groupsList": [],
          "total": 7
        }, {
          "id": 10072,
          "name": "Катушки зажигания",
          "slug": "katushki-zazhiganiya",
          "groupsList": [],
          "total": 9
        }, {
          "id": 10073,
          "name": "Свечи зажигания",
          "slug": "svechi-zazhiganiya",
          "groupsList": [],
          "total": 6
        }],
        "total": 22
      }, {
        "id": 12,
        "name": "Система отопления и кондиционирования",
        "slug": "sistema-otopleniya-i-kondicionirovaniya",
        "groupsList": [{
          "id": 10076,
          "name": "Запчасти кондиционера",
          "slug": "zapchasti-kondicionera",
          "groupsList": [],
          "total": 21
        }, {
          "id": 10077,
          "name": "Запчасти печки",
          "slug": "zapchasti-pechki",
          "groupsList": [],
          "total": 15
        }, {
          "id": 10075,
          "name": "Климат-контроль",
          "slug": "klimat-kontrol",
          "groupsList": [],
          "total": 1
        }],
        "total": 37
      }, {
        "id": 13,
        "name": "Система охлаждения",
        "slug": "sistema-ohlazhdeniya",
        "groupsList": [{
          "id": 10078,
          "name": "Вентиляторы",
          "slug": "ventilyatory",
          "groupsList": [],
          "total": 10
        }, {
          "id": 10080,
          "name": "Насос системы охлаждения",
          "slug": "nasos-sistemy-ohlazhdeniya",
          "groupsList": [],
          "total": 3
        }, {
          "id": 10079,
          "name": "Радиаторы",
          "slug": "radiatory",
          "groupsList": [],
          "total": 11
        }, {
          "id": 10081,
          "name": "Термостаты",
          "slug": "termostaty",
          "groupsList": [],
          "total": 4
        }, {
          "id": 10082,
          "name": "Шланги",
          "slug": "shlangi",
          "groupsList": [],
          "total": 1
        }],
        "total": 29
      }, {
        "id": 14,
        "name": "Системы безопасности",
        "slug": "sistemy-bezopasnosti",
        "groupsList": [{
          "id": 10085,
          "name": "Ремни безопасности",
          "slug": "remni-bezopasnosti",
          "groupsList": [],
          "total": 3
        }, {
          "id": 10083,
          "name": "Система пассивной безопасности (SRS)",
          "slug": "sistema-passivnoj-bezopasnosti-srs",
          "groupsList": [],
          "total": 6
        }, {
          "id": 10084,
          "name": "Система стабилизации движения (ESP)",
          "slug": "sistema-stabilizacii-dvizheniya-esp",
          "groupsList": [],
          "total": 1
        }],
        "total": 10
      }, {
        "id": 15,
        "name": "Стекла",
        "slug": "stekla",
        "groupsList": [{
          "id": 10088,
          "name": "Боковые",
          "slug": "bokovye",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10089,
          "name": "Задние",
          "slug": "zadnie",
          "groupsList": [],
          "total": 3
        }, {
          "id": 10087,
          "name": "Лобовые (ветровые)",
          "slug": "lobovye-vetrovye",
          "groupsList": [],
          "total": 2
        }, {
          "id": 10086,
          "name": "Уплотнители",
          "slug": "uplotniteli",
          "groupsList": [],
          "total": 3
        }, {
          "id": 10090,
          "name": "Форточки",
          "slug": "fortochki",
          "groupsList": [],
          "total": 1
        }],
        "total": 10
      }, {
        "id": 16,
        "name": "Топливная система",
        "slug": "toplivnaya-sistema",
        "groupsList": [{
          "id": 10093,
          "name": "Запчасти карбюратора",
          "slug": "zapchasti-karbyuratora",
          "groupsList": [],
          "total": 2
        }, {
          "id": 10092,
          "name": "Система впрыска",
          "slug": "sistema-vpryska",
          "groupsList": [],
          "total": 10
        }, {
          "id": 10095,
          "name": "Топливные баки",
          "slug": "toplivnye-baki",
          "groupsList": [],
          "total": 15
        }, {
          "id": 10094,
          "name": "Топливные насосы",
          "slug": "toplivnye-nasosy",
          "groupsList": [],
          "total": 6
        }],
        "total": 33
      }, {
        "id": 17,
        "name": "Тормозная система",
        "slug": "tormoznaya-sistema",
        "groupsList": [{
          "id": 10098,
          "name": "ABS",
          "slug": "abs",
          "groupsList": [],
          "total": 5
        }, {
          "id": 10097,
          "name": "Стояночная тормозная система",
          "slug": "stoyanochnaya-tormoznaya-sistema",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10096,
          "name": "Тормозные диски | Барабаны",
          "slug": "tormoznye-diski-barabany",
          "groupsList": [],
          "total": 7
        }, {
          "id": 10101,
          "name": "Тормозные колодки | Запчасти ",
          "slug": "tormoznye-kolodki-zapchasti-",
          "groupsList": [],
          "total": 8
        }, {
          "id": 10100,
          "name": "Тормозные шланги",
          "slug": "tormoznye-shlangi",
          "groupsList": [],
          "total": 7
        }, {
          "id": 10099,
          "name": "Цилиндры | Усилители",
          "slug": "cilindry-usiliteli",
          "groupsList": [],
          "total": 5
        }],
        "total": 33
      }, {
        "id": 18,
        "name": "Трансмиссия | КПП",
        "slug": "transmissiya-kpp",
        "groupsList": [{
          "id": 10111,
          "name": "Автоматическая КПП (Типтроник)",
          "slug": "avtomaticheskaya-kpp-tiptronik",
          "groupsList": [],
          "total": 2
        }, {
          "id": 10102,
          "name": "АКПП (Автоматическая КПП)",
          "slug": "akpp-avtomaticheskaya-kpp",
          "groupsList": [],
          "total": 41
        }, {
          "id": 10105,
          "name": "Дифференциалы",
          "slug": "differencialy",
          "groupsList": [],
          "total": 26
        }, {
          "id": 10109,
          "name": "Запчасти вариатора (CVT)",
          "slug": "zapchasti-variatora-cvt",
          "groupsList": [],
          "total": 12
        }, {
          "id": 10107,
          "name": "Запчасти раздаточной коробки",
          "slug": "zapchasti-razdatochnoj-korobki",
          "groupsList": [],
          "total": 4
        }, {
          "id": 10108,
          "name": "Запчасти сцепления",
          "slug": "zapchasti-scepleniya",
          "groupsList": [],
          "total": 13
        }, {
          "id": 10106,
          "name": "Карданный вал | Редуктор",
          "slug": "kardannyj-val-reduktor",
          "groupsList": [],
          "total": 11
        }, {
          "id": 10104,
          "name": "МКПП (Механическая КПП)",
          "slug": "mkpp-mehanicheskaya-kpp",
          "groupsList": [],
          "total": 16
        }, {
          "id": 10112,
          "name": "Педали",
          "slug": "pedali",
          "groupsList": [],
          "total": 5
        }, {
          "id": 10103,
          "name": "Привод | ШРУС",
          "slug": "privod-shrus",
          "groupsList": [],
          "total": 7
        }, {
          "id": 10110,
          "name": "Роботизированная КПП (Робот)",
          "slug": "robotizirovannaya-kpp-robot",
          "groupsList": [],
          "total": 8
        }],
        "total": 145
      }, {
        "id": 19,
        "name": "Фильтры",
        "slug": "filtry",
        "groupsList": [{
          "id": 10113,
          "name": "Воздушные",
          "slug": "vozdushnye",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10115,
          "name": "Масляные",
          "slug": "maslyanye",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10116,
          "name": "Салонные",
          "slug": "salonnye",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10114,
          "name": "Топливные",
          "slug": "toplivnye",
          "groupsList": [],
          "total": 4
        }],
        "total": 7
      }, {
        "id": 20,
        "name": "Шины | Диски",
        "slug": "shiny-diski",
        "groupsList": [{
          "id": 10120,
          "name": "Диски",
          "slug": "diski",
          "groupsList": [],
          "total": 2
        }, {
          "id": 10118,
          "name": "Запаски | Докатки",
          "slug": "zapaski-dokatki",
          "groupsList": [],
          "total": 10
        }, {
          "id": 10117,
          "name": "Колпаки",
          "slug": "kolpaki",
          "groupsList": [],
          "total": 3
        }, {
          "id": 10119,
          "name": "Шины",
          "slug": "shiny",
          "groupsList": [],
          "total": 1
        }],
        "total": 16
      }, {
        "id": 21,
        "name": "Электрооборудование",
        "slug": "elektrooborudovanie",
        "groupsList": [{
          "id": 10124,
          "name": "Аккумуляторы",
          "slug": "akkumulyatory",
          "groupsList": [],
          "total": 3
        }, {
          "id": 10122,
          "name": "Генераторы",
          "slug": "generatory",
          "groupsList": [],
          "total": 8
        }, {
          "id": 10126,
          "name": "Запчасти звукового сигнала",
          "slug": "zapchasti-zvukovogo-signala",
          "groupsList": [],
          "total": 1
        }, {
          "id": 10123,
          "name": "Предохранители | Реле | Датчики",
          "slug": "predohraniteli-rele-datchiki",
          "groupsList": [],
          "total": 10
        }, {
          "id": 10127,
          "name": "Приборная панель | Переключатели | Кнопки",
          "slug": "pribornaya-panel-pereklyuchateli-knopki",
          "groupsList": [],
          "total": 14
        }, {
          "id": 10125,
          "name": "Проводка",
          "slug": "provodka",
          "groupsList": [],
          "total": 7
        }, {
          "id": 10121,
          "name": "Стартеры",
          "slug": "startery",
          "groupsList": [],
          "total": 8
        }, {
          "id": 10128,
          "name": "Электромоторы",
          "slug": "elektromotory",
          "groupsList": [],
          "total": 1
        }],
        "total": 52
      }],
    selectedBrandsCurrent: [
      {
        id: 366,
        name: "BOSCH"
      },
      {
        id: 4155,
        name: "СТАРТВОЛЬТ"
      },
      {
        id: 2514,
        name: "STARTVOLT"
      },
      {
        id: 2582,
        name: "ZZVF"
      },
      {
        id: 2134,
        name: "STANDARD"
      },
      {
        id: 587,
        name: "DELPHI"
      },
      {
        id: 498,
        name: "CHRYSLER"
      },
      {
        id: 16264,
        name: "DODA"
      },
      {
        id: 735,
        name: "ERA"
      },
      {
        id: 4086,
        name: "TOYOTA"
      },
      {
        id: 2112,
        name: "SPECTRA PREMIUM"
      },
      {
        id: 1487,
        name: "MEAT & DORIA"
      },
      {
        id: 1550,
        name: "MOPAR PARTS"
      },
      {
        id: 1976,
        name: "SAT"
      },
      {
        id: 863,
        name: "FORD"
      },
      {
        id: 1533,
        name: "MITSUBISHI"
      },
      {
        id: 1789,
        name: "PIERBURG"
      },
      {
        id: 1916,
        name: "ROERS PARTS"
      },
      {
        id: 2168,
        name: "SWAG"
      },
      {
        id: 490,
        name: "CHERY"
      },
      {
        id: 559,
        name: "DAEWOO"
      },
      {
        id: 783,
        name: "FAE"
      },
      {
        id: 920,
        name: "GAZ"
      },
      {
        id: 2168,
        name: "SWAG"
      },
      {
        id: 490,
        name: "CHERY"
      },
      {
        id: 559,
        name: "DAEWOO"
      },
      {
        id: 783,
        name: "FAE"
      },
      {
        id: 920,
        name: "GAZ"
      }
    ],
    filters: {
      "partManufacturer": [
        {
        "id": 929,
        "slug": "general-motors",
        "brand": "GENERAL MOTORS",
        "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
        "typesList": [1]
      }, {
        "id": 1542,
        "slug": "mobis",
        "brand": "MOBIS",
        "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
        "typesList": [1]
      }, {
        "id": 1713,
        "slug": "opel",
        "brand": "OPEL",
        "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
        "typesList": [1]
      }, {
        "id": 2156,
        "slug": "subaru",
        "brand": "SUBARU",
        "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
        "typesList": [1]
      }],
      "city": [{
        "id": "596",
        "name": "Барнаул",
        "level": 2,
        "slug": "barnaul",
        "prepositionalName": "Барнауле",
        "parent": {
          "id": "586",
          "name": "Алтайский край"
        }
      }, {
        "id": "738",
        "name": "Белгород",
        "level": 2,
        "slug": "belgorod",
        "prepositionalName": "Белгороде",
        "parent": {
          "id": "728",
          "name": "Белгородская область"
        }
      }, {
        "id": "872",
        "name": "Братск",
        "level": 2,
        "slug": "bratsk",
        "prepositionalName": "Братске",
        "parent": {
          "id": "852",
          "name": "Иркутская область"
        }
      }, {
        "id": "664",
        "name": "Владивосток",
        "level": 2,
        "slug": "vladivostok",
        "prepositionalName": "Владивостоке",
        "parent": {
          "id": "654",
          "name": "Приморский край"
        }
      }, {
        "id": "833",
        "name": "Воронеж",
        "level": 2,
        "slug": "voronezh",
        "prepositionalName": "Воронеже",
        "parent": {
          "id": "817",
          "name": "Воронежская область"
        }
      }, {
        "id": "1134",
        "name": "Домодедово",
        "level": 2,
        "slug": "domodedovo",
        "prepositionalName": "Домодедово",
        "parent": {
          "id": "1048",
          "name": "Московская область"
        }
      }, {
        "id": "602",
        "name": "Ейск",
        "level": 2,
        "slug": "ejsk",
        "prepositionalName": "Ейске",
        "parent": {
          "id": "599",
          "name": "Краснодарский край"
        }
      }, {
        "id": "1490",
        "name": "Екатеринбург",
        "level": 2,
        "slug": "ekaterinburg",
        "prepositionalName": "Екатеринбурге",
        "parent": {
          "id": "1443",
          "name": "Свердловская область"
        }
      }, {
        "id": "1838",
        "name": "Зеленоград",
        "level": 2,
        "slug": "zelenograd",
        "prepositionalName": "Зеленограде",
        "parent": {
          "id": "1651",
          "name": "Москва"
        }
      }, {
        "id": "850",
        "name": "Иваново",
        "level": 2,
        "slug": "ivanovo",
        "prepositionalName": "Иваново",
        "parent": {
          "id": "834",
          "name": "Ивановская область"
        }
      }, {
        "id": "943",
        "name": "Кемерово",
        "level": 2,
        "slug": "kemerovo",
        "prepositionalName": "Кемерово",
        "parent": {
          "id": "928",
          "name": "Кемеровская область"
        }
      }, {
        "id": "621",
        "name": "Краснодар",
        "level": 2,
        "slug": "krasnodar",
        "prepositionalName": "Краснодаре",
        "parent": {
          "id": "599",
          "name": "Краснодарский край"
        }
      }, {
        "id": "1139",
        "name": "Лобня",
        "level": 2,
        "slug": "lobnya",
        "prepositionalName": "Лобне",
        "parent": {
          "id": "1048",
          "name": "Московская область"
        }
      }, {
        "id": "1140",
        "name": "Люберцы",
        "level": 2,
        "slug": "lyubercy",
        "prepositionalName": "Люберцах",
        "parent": {
          "id": "1048",
          "name": "Московская область"
        }
      }, {
        "id": "1654",
        "name": "Москва",
        "level": 2,
        "slug": "moskva_russia",
        "prepositionalName": "Москве",
        "parent": {
          "id": "1651",
          "name": "Москва"
        }
      }, {
        "id": "1145",
        "name": "Мытищи",
        "level": 2,
        "slug": "mytishhi",
        "prepositionalName": "Мытищах",
        "parent": {
          "id": "1048",
          "name": "Московская область"
        }
      }, {
        "id": "1200",
        "name": "Нижний Новгород",
        "level": 2,
        "slug": "nizhnij-novgorod",
        "prepositionalName": "Нижнем Новгороде",
        "parent": {
          "id": "1172",
          "name": "Нижегородская область"
        }
      }, {
        "id": "1243",
        "name": "Новосибирск",
        "level": 2,
        "slug": "novosibirsk",
        "prepositionalName": "Новосибирске",
        "parent": {
          "id": "1228",
          "name": "Новосибирская область"
        }
      }, {
        "id": "1324",
        "name": "Пермь",
        "level": 2,
        "slug": "perm",
        "prepositionalName": "Перми",
        "parent": {
          "id": "1300",
          "name": "Пермский край"
        }
      }, {
        "id": "1361",
        "name": "Ростов-на-Дону",
        "level": 2,
        "slug": "rostov-na-donu",
        "prepositionalName": "Ростове-на-Дону",
        "parent": {
          "id": "1342",
          "name": "Ростовская область"
        }
      }, {
        "id": "1377",
        "name": "Рязань",
        "level": 2,
        "slug": "ryazan",
        "prepositionalName": "Рязани",
        "parent": {
          "id": "1366",
          "name": "Рязанская область"
        }
      }, {
        "id": "1386",
        "name": "Самара",
        "level": 2,
        "slug": "samara",
        "prepositionalName": "Самаре",
        "parent": {
          "id": "1379",
          "name": "Самарская область"
        }
      }, {
        "id": "1851",
        "name": "Санкт-Петербург",
        "level": 2,
        "slug": "sankt-peterburg",
        "prepositionalName": "Санкт-Петербурге",
        "parent": {
          "id": "1840",
          "name": "Санкт-Петербург"
        }
      }, {
        "id": "1087",
        "name": "Сходня",
        "level": 2,
        "slug": "shodnya",
        "prepositionalName": "Сходне",
        "parent": {
          "id": "1048",
          "name": "Московская область"
        }
      }, {
        "id": "1948",
        "name": "Тарко-Сале",
        "level": 2,
        "slug": "tarko-sale",
        "prepositionalName": "Тарко-Сале",
        "parent": {
          "id": "1945",
          "name": "Ямало-Ненецкий автономный округ"
        }
      }, {
        "id": "665",
        "name": "Уссурийск",
        "level": 2,
        "slug": "ussurijsk",
        "prepositionalName": "Уссурийске",
        "parent": {
          "id": "654",
          "name": "Приморский край"
        }
      }, {
        "id": "693",
        "name": "Хабаровск",
        "level": 2,
        "slug": "habarovsk",
        "prepositionalName": "Хабаровске",
        "parent": {
          "id": "687",
          "name": "Хабаровский край"
        }
      }, {
        "id": "1133",
        "name": "Химки",
        "level": 2,
        "slug": "himki",
        "prepositionalName": "Химках",
        "parent": {
          "id": "1048",
          "name": "Московская область"
        }
      }, {
        "id": "1625",
        "name": "Челябинск",
        "level": 2,
        "slug": "chelyabinsk",
        "prepositionalName": "Челябинске",
        "parent": {
          "id": "1595",
          "name": "Челябинская область"
        }
      }]
    },
    listParts: [
      {
        "id": 13236725,
        "total": 49,
        "minPrice": 960,
        "maxPrice": 3426,
        "vendorCode": "281124A003",
        "type": 1,
        "name": "Корпус воздушного фильтра (нижняя часть)",
        "aliasesList": [],
        "description": "",
        "manufacturerId": 1542,
        "manufacturer": {
          "id": 1542,
          "slug": "mobis",
          "brand": "MOBIS",
          "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
          "typesList": [1]
        }
      }, {
        "id": 13236733,
        "total": 44,
        "minPrice": 1229,
        "maxPrice": 2967,
        "vendorCode": "46052SG020",
        "type": 1,
        "name": "Корпус воздушного фильтра (нижняя часть)",
        "aliasesList": [],
        "description": "",
        "manufacturerId": 2156,
        "manufacturer": {
          "id": 2156,
          "slug": "subaru",
          "brand": "SUBARU",
          "aliasesList": ["SUBARU ЭЛЕКТРИКА", "SUBARU ДВИГАТЕЛЬ", "SUBARY", "SUBARU КУЗОВ", "FHI", "SUBARU (ЯПОНИЯ)", "SUBARU", "SUBARU ПОДВЕСКА", "OE SUBARU"],
          "typesList": [1]
        }
      }, {
        "id": 13236724,
        "total": 31,
        "minPrice": 1790,
        "maxPrice": 4424,
        "vendorCode": "281123J100",
        "type": 1,
        "name": "Корпус воздушного фильтра (нижняя часть)",
        "aliasesList": [],
        "description": "",
        "manufacturerId": 1542,
        "manufacturer": {
          "id": 1542,
          "slug": "mobis",
          "brand": "MOBIS",
          "aliasesList": ["MOBIS_KIA", "НМС", "KIA_HYUNDAI", "KIA/HYUNDAI/MOBIS", "KIA MOTORS", "KIA BONGO", "KIA", "ORIGINAL HYUNDAI-KIA", "MOBIS_HUYNDAI", "HYUNDAI OILBANK", "HYUNDAI/KIA/MOBIS", "MOBIS/HYUNDAI/KIA", "MOBIS KOREA", "OEM KOREA", "MOBIS HYUNDAI", "MOBIS (ОРИГИНАЛ)", "MOBIS", "HYUNDAY-KIA", "HYUNDAY", "HYUNDAI", "HYUNDAI-KIA", "HYUNDAI/KIA (GEN) KOREA", "HMC", "HYN/KIA", "OE HYUNDAI", "КИА", "КИА KIA", "HYUDAI-KIA", "HYU", "HYNDAI KIA", "HYUNDAI MOTOR COMPANY", "HYUNDAI MOTOR CORPORATION", "HYUNDAI / KIA (КОРЕЯ)"],
          "typesList": [1]
        }
      }, {
        "id": 13236726,
        "total": 1,
        "minPrice": 3092,
        "maxPrice": 3092,
        "vendorCode": "4802867",
        "type": 1,
        "name": "Корпус воздушного фильтра (нижняя часть)",
        "aliasesList": [],
        "description": "",
        "manufacturerId": 929,
        "manufacturer": {
          "id": 929,
          "slug": "general-motors",
          "brand": "GENERAL MOTORS",
          "aliasesList": ["ORIGINAL GM", "GM (ОРИГИНАЛ)", "GM(GENERAL MOTORS)", "GM УЗБЕКИСТАН", "GMDAT", "GM CHEVROLET OPEL", "GM/DAEWOO/CHEVROLET", "GM KOREA", "GM", "GM-UZ", "GM DAEWOO", "GM/CHEVROLET/DAEWOO", "CHEVROLET / DAEWOO", "GENERAL MOTORS (США)", "GENERAL MOTORS (GM)", "OE GENERAL MOTORS", "GENERAL MOTORS", "CHEVROLET NIVA", "DW-MOTORS", "OEM", "OEM GENERAL MOTORS", "OE GM", "GeniralMotorc", "GM-АвтоВАЗ", "GM USA"],
          "typesList": [1]
        }
      }, {
        "id": 13236727,
        "total": 1,
        "minPrice": 2951,
        "maxPrice": 2951,
        "vendorCode": "4802867",
        "type": 1,
        "name": "Корпус воздушного фильтра (нижняя часть)",
        "aliasesList": [],
        "description": "",
        "manufacturerId": 1713,
        "manufacturer": {
          "id": 1713,
          "slug": "opel",
          "brand": "OPEL",
          "aliasesList": ["OPEL/VAUXHALL", "OPEL", "OPEL/CHEVROLET/DAEWOO", "OPEL\\SHEVROLE"],
          "typesList": [1]
        }
      }],
  }
};