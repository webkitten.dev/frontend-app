export default () => [
  {
    href: '/check-auto',
    domain: 'main',
    title: 'Проверка по VIN-номеру',
  },
  {
    href: '/check-auto/example',
    domain: 'main',
    title: 'Пример отчета',
  },
  {
    href: '/check-auto/questions',
    domain: 'main',
    title: 'Вопросы и ответы',
  },
  {
    href: '/about-us',
    domain: 'main',
    title: 'Юридическая информация',
  },
];
