export default () => [
  {
    href: '/',
    domain: 'helper',
    title: 'Вызвать автоконсьержа',
  },
  {
    href: '/partner',
    domain: 'helper',
    title: 'Стать партнером',
  },
  {
    href: '/about',
    domain: 'helper',
    title: 'О сервисе',
  },
  {
    href: '/faq/help-finding-parts',
    domain: 'helper',
    title: 'FAQ',
  },
];
