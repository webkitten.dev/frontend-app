import partsSections from './parts';
import helperSections from './helper';
import checkAutoSections from './checkAuto';

export default {
  partsSections,
  helperSections,
  checkAutoSections,
};
