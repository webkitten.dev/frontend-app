export default () => [
  {
    href: '/poisk-po-nomeru',
    domain: 'main',
    title: 'Поиск по номеру',
  },
  {
    href: '/do/part',
    domain: 'main',
    title: 'Контрактные (б/у)',
  },
  {
    href: '/request-to-spare',
    domain: 'main',
    title: 'VIN-запрос',
  },
  {
    href: '/catalog',
    domain: 'main',
    title: 'Каталоги',
  },
  {
    href: '/categories',
    domain: 'main',
    title: 'Каталоги',
  },
  {
    href: '/tyres',
    domain: 'main',
    title: 'Шины и диски',
  },
];
