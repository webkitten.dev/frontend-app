export const Placement = Object.freeze({
  FRONT: 'Перед',
  REAR: 'Зад',
  LEFT: 'Лево',
  RIGHT: 'Право',
  TOP: 'Верх',
  BOTTOM: 'Низ',
  ANY: '',
});
