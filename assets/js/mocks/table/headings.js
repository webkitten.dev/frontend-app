// TODO: Improve лучше блок с sorting вынести в отдельный объект.
// И делать merge с этим на странице.
export const currentOffer = [
  {
    name: 'names',
    title: 'Бренд, артикул, название у продавца',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'comment',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'createdAd',
    title: 'Добавлено',
    titleMobile: 'по дате публикации',
    classes: '',
    sorting: {
      has: true,
      criteria: 'created_at',
    },
  },
  {
    name: 'delivery',
    title: 'Срок доставки',
    titleMobile: 'по сроку доставки',
    classes: '',
    sorting: {
      has: false,
      criteria: 'quantity',
    },
  },
  {
    name: 'sellerBasic',
    title: 'Продавец',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'price',
    title: 'Стоимость',
    titleMobile: 'по стоимости',
    classes: '_align-right',
    sorting: {
      has: true,
      criteria: 'price',
    },
  },
  {
    name: 'buttonsOrder',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
];
export const currentSeller = [
  {
    name: 'names',
    title: 'Бренд, артикул, название у продавца',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'createdAd',
    title: 'Добавлено',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'delivery',
    title: 'Срок доставки',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'characteristics',
    title: 'Характеристики товара',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'price',
    title: 'Стоимость',
    classes: '_align-right',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'buttonsOrder',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
];

export const applicability = [
  {
    name: 'brand',
    title: 'Марка',
    sorting: {
      has: true,
      criteria: 'brand',
    },
  },
  {
    name: 'model_name',
    title: 'Модель',
    sorting: {
      has: true,
      criteria: 'model_name',
    },
  },
  {
    name: 'type_name',
    title: 'Модификация',
    sorting: {
      has: true,
      criteria: 'type_name',
    },
  },
];

export const applicabilityExpert = [
  {
    name: 'manufacturer',
    title: 'Марка',
    sorting: {
      has: true,
      criteria: 'manufacturer',
    },
  },
  {
    name: 'model',
    title: 'Модель',
    sorting: {
      has: true,
      criteria: 'model',
    },
  },
  {
    name: 'type',
    title: 'Модификация',
    sorting: {
      has: true,
      criteria: 'type',
    },
  },
];

export const ContractParts = [
  {
    name: 'img',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'partName',
    title: 'Данные о товаре',
    classes: '__all-wrap',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'condition',
    title: 'Состояние',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'createdAd',
    title: 'Добавлено',
    titleMobile: 'по дате добавления',
    classes: '',
    sorting: {
      has: true,
      criteria: 'date',
    },
  },
  {
    name: 'delivery',
    title: 'Срок доставки',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'sellerBasic',
    title: 'Продавец',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'price',
    title: 'Стоимость',
    titleMobile: 'по стоимости',
    classes: '',
    sorting: {
      has: true,
      criteria: 'price',
    },
  },
  {
    name: 'buttonsOrder',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
];

export const Tyres = [
  {
    name: 'img',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'manufacturer',
    title: 'Производитель',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'model',
    title: 'Модель',
    classes: '',
    sorting: {
      has: true,
      criteria: 'model_name',
    },
  },
  {
    name: 'size',
    title: 'Размеры',
    classes: '',
    sorting: {
      has: false,
      criteria: 'date',
    },
  },
  {
    name: 'type',
    title: 'Сезон',
    classes: '',
    sorting: {
      has: true,
      criteria: 'season',
    },
  },
  {
    name: 'price',
    title: 'Стоимость',
    titleMobile: 'по стоимости',
    classes: '',
    sorting: {
      has: true,
      criteria: 'min_price',
    },
  },
  {
    name: 'actions',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
];

export const Wheels = [
  {
    name: 'img',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'manufacturer',
    title: 'Производитель',
    classes: '',
    sorting: {
      has: true,
      criteria: 'part_manufacturer_brand',
    },
  },
  {
    name: 'model',
    title: 'Модель',
    classes: '',
    sorting: {
      has: true,
      criteria: 'model_name',
    },
  },
  {
    name: 'size',
    title: 'Размеры',
    classes: '',
    sorting: {
      has: false,
      criteria: 'date',
    },
  },
  {
    name: 'type',
    title: 'Тип',
    classes: '',
    sorting: {
      has: true,
      criteria: 'type',
    },
  },
  {
    name: 'price',
    title: 'Стоимость',
    titleMobile: 'по стоимости',
    classes: '',
    sorting: {
      has: true,
      criteria: 'min_price',
    },
  },
  {
    name: 'actions',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
];

export const priceUpload = [
  {
    name: 'number',
    title: '№',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'createdAd',
    title: 'Дата создания',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'name',
    title: 'Название',
    classes: '__all-wrap',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'template',
    title: 'Шаблон',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'typeUpload',
    title: 'Тип загрузки',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'typePart',
    title: 'Тип запчастей',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'link',
    title: 'Ссылка на файл',
    classes: '__all-wrap',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'actions',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
];

export const priceTemplate = [
  {
    name: 'number',
    title: '№',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'createdAd',
    title: 'Дата создания',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'name',
    title: 'Название шаблона',
    classes: '__all-wrap',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'active',
    title: 'Включен',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'typePart',
    title: 'Тип запчастей',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'format',
    title: 'Формат шаблона',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'listUpload',
    title: 'Автозагрузки по шаблону',
    classes: '__all-wrap',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'actions',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
];

export const priceHistory = [
  {
    name: 'number',
    title: '№',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'createdAd',
    title: 'Дата создания',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'fileName',
    title: 'Имя файла',
    classes: '__all-wrap',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'shop',
    title: 'Магазин',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'typeUpload',
    title: 'Тип загрузки',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'status',
    title: 'Статус',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'errors',
    title: 'Ошибки',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
];


export const characteristicsLabels = {
  quantity: 'Количество',
  condition: 'Состояние',
  color: 'Цвет',
  vendorCode: 'Артикул',
  carManufacturer: 'Марка',
  carModel: 'Модель',
  carChassis: 'Кузов',
  carEngine: 'Двигатель',
  carYears: 'Год',
  partManufacturer: 'Производитель',
  inStock: 'Наличие',
  placement: 'Расположение',
  deliveryTime: 'Срок доставки',
};
