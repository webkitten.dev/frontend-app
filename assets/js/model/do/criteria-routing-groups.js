export default Object.freeze({
  params: [
    'city',
    'carManufacturer',
    'carModel',
  ],
  query: Object.freeze({
    shop: 'shop',
    metro: 'metro',
    chassisList: 'chassis',
    enginesList: 'engines',
    text: 'text',
    yearsList: 'years',
    priceMin: 'price_min',
    priceMax: 'price_max',
    condition: 'condition',
    merchantType: 'merchant_type',
    imagesPresence: 'images_presence',
    placementList: 'placement',
  }),
});
