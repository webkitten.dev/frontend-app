export default Object.freeze({
  city: 'c_',
  carManufacturer: 'mf_',
  carModel: 'm_',
});
