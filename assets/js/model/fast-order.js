import Item from '~/assets/js/model/item/item';

export default class FastOrder {
  constructor(data) {
    this.part = new Item({
      ...data,
      quantity: 1,
    });
    this.seller = {
      id: data.shop_id,
      name: data.shop_name,
      city: data.city_name,
      street: data.street,
      building: data.building,
      office: data.office,
      quantity_string: data.quantity_string,
      deliverTime: data.delivery_string,
      availableCount: data.quantity ? data.quantity : 999,
    };
  }
}
