import { has, camelCase, cloneDeep } from 'lodash-es';
import parseFromUTC from '~/assets/js/helpers/parseFromUTC';

class ContractCartItem {
  address = '';

  brand = '';

  charCode = '';

  city = '';

  commentName = '';

  createdAt = '';

  customerComment = '';

  delivery = '';

  deliveryFrom = 0;

  deliveryRetailCondition = '';

  deliveryString = '';

  deliveryTo = 0;

  deliveryWholesaleCondition = '';

  discountCheck = 0;

  inStock = false;

  isWholesalePrice = false;

  itemId = 0;

  manufacturerId = 0;

  maxQuantity = 0;

  name = '';

  nameStandard = '';

  organizationId = 0;

  price = 0;

  quantity = 0;

  seller = '';

  shopId = 0;

  userId = 0;

  vendorCode = '';

  partId = 0;

  constructor(properties) {
    const props = cloneDeep(properties);

    Object.keys(props).forEach((property) => {
      const newProp = camelCase(property);

      if (has(this, newProp)) {
        this[newProp] = props[property];
      }

      switch (true) {
        case (property === 'is_wholesale'):
          this.isWholesalePrice = props.is_wholesale;
          break;
        case (property === 'item_name'):
          this.name = props.item_name;
          break;
        case (property === 'part_name'):
          this.nameStandard = props.part_name;
          break;
        case (property === 'city_name'):
          this.city = props.city_name;
          break;
        case (property === 'part_manufacturer_id'):
          this.manufacturerId = props.part_manufacturer_id;
          break;
        case (property === 'shop_name'):
          this.seller = props.shop_name;
          break;
        default:
          break;
      }
    });
  }
}

export default class Item {
  constructor(model) {
    this.checked = false;
    this.contractModal = new ContractCartItem(model);
    this.id = this.getGUID();
  }

  getGUID() {
    if (this.contractModal) {
      if (
        has(this.contractModal, 'itemId')
        && has(this.contractModal, 'isWholesalePrice')
      ) {
        return this.contractModal.itemId.toString() + Number(this.contractModal.isWholesalePrice);
      }

      if (
        has(this.contractModal, 'item_id')
        && has(this.contractModal, 'is_wholesale')
      ) {
        return this.contractModal.item_id.toString() + Number(this.contractModal.is_wholesale);
      }
    }
    return null;
  }

  static isOwnedByUser(userId, item) {
    if (userId && item.contractModal) {
      return Number(userId) === item.contractModal.userId;
    }

    return false;
  }

  static isOutdated(item, itemOutdatedThreshold) {
    const target = parseFromUTC(item.contractModal.createdAt);
    const now = new Date();
    const timeDiff = Math.abs(now.getTime() - target.getTime());
    const diffDays = timeDiff / (1000 * 3600 * 24);
    return diffDays > itemOutdatedThreshold;
  }
}
