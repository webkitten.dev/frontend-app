import { get } from 'lodash-es';

export default class Organization {
  constructor(information) {
    this.id = get(information, 'id', '');
    this.city = get(information, 'city', null);
    this.street = get(information, 'street', '');
    this.building = get(information, 'building', '');
    this.office = get(information, 'office', '');
    this.coordLat = get(information, 'coordLat', '');
    this.coordLon = get(information, 'coordLng', '');
    this.paymentMethods = get(information, 'paymentMethods', []);
    this.organization = get(information, 'legal', []);
    this.organizationOptions = get(information, 'organization.legals', []);
  }
}
