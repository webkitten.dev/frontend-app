import { get } from 'lodash-es';
import Organization from './organization';
import Phone from '~/assets/js/model/phone/phone';

export default class Shop extends Organization {
  constructor(information) {
    super(information);
    this.aditionalContacts = get(information, 'shopAdditionalContacts', []);
    this.phones = Shop.getPhoneNumbers(information);
    this.name = get(information, 'shopName', '');
    this.email = get(information, 'shopEmail', '');
    this.emailIsConfirmed = get(information, 'emailIsConfirmed', false);
    this.siteIsConfirmed = get(information, 'siteIsConfirmed', false);
    this.scheduleIsConfirmed = get(information, 'scheduleIsConfirmed', false);
    this.addressIsConfirmed = get(information, 'addressIsConfirmed', false);
    this.site = get(information, 'shopSite', '');
    this.shopAttributes = get(information, 'shopAttributes', []);
    this.tradingManufacturers = get(information, 'tradingManufacturers', []);
    this.officialMarks = get(information, 'officialDealers', []);
    this.additionalInfo = get(information, 'shopAdditionalInfo', '');
    this.extUrlItemUrl = get(information, 'shopUrl.itemUrl', '');
    this.extUrlItemRedirect = get(information, 'itemRedirect', false);
    this.extUrlOrderRedirect = get(information, 'orderRedirect', false);
  }

  static getPhoneNumbers(information) {
    if (get(information, 'shopPhones', []).length) {
      return information.shopPhones.map(phone => new Phone(phone));
    }
    return [new Phone()];
  }
}
