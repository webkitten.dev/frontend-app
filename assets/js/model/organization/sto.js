import { has, get } from 'lodash-es';
import Organization from './organization';
import Phone from '~/assets/js/model/phone/phone';

export default class Sto extends Organization {
  constructor(information) {
    super(information);
    this.name = get(information, 'name', '');
    this.email = get(information, 'email', '');
    this.site = get(information, 'site', '');
    this.metro = get(information, 'metro', '');
    this.hourPriceMin = get(information, 'hourPriceMin', '');
    this.hourPriceMax = get(information, 'hourPriceMax', '');
    this.phones = has(information, 'phones')
      ? Sto.getPhoneNumbers(information)
      : [new Phone()];
    this.aditionalContacts = get(information, 'additionalContacts', []);
    this.additionalInfo = get(information, 'additionalInfo', '');
    this.spares = get(information, 'spares', false);
    this.nonSpecifiedServiceGroup = get(information, 'nonSpecifiedServiceGroup', false);
    this.sendNotification = get(information, 'sendNotifications', false);
    this.brands = get(information, 'brands', []);
    this.taskBrands = get(information, 'taskBrands', []);
    this.serviceGroups = get(information, 'serviceGroups', []);
    this.services = get(information, 'services', []);
    this.taskServiceGroups = get(information, 'taskServiceGroups', []);
    this.taskServices = get(information, 'taskServices', []);
    this.emailIsConfirmed = get(information, 'emailIsConfirmed', false);
    this.siteIsConfirmed = get(information, 'siteIsConfirmed', false);
    this.scheduleIsConfirmed = get(information, 'scheduleIsConfirmed', false);
    this.addressIsConfirmed = get(information, 'addressIsConfirmed', false);
  }

  static getPhoneNumbers(information) {
    return information.phones.map(phone => new Phone(phone));
  }
}
