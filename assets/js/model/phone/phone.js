import { has, get } from 'lodash-es';
import Validator from '~/assets/js/validator';

export default class Phone {
  constructor(model) {
    this.model = {
      phoneId: get(model, 'id', null),
      phone: has(model, 'shopPhone')
        ? get(model, 'shopPhone', null)
        : get(model, 'phone', null),
      comment: get(model, 'comment', ''),
      position: get(model, 'position', 0),
      phoneIsConfirmed: get(model, 'phoneIsConfirmed', false),
    };
    this.isMobile = this.model.phone ? Validator.isMobilePhoneNumber(this.model.phone) : false;
    this.hasChange = false;
    // Локальный дубликат номера телефона
    this.isDuplicate = false;
    // Номер уже есть в DB
    this.isSet = false;
    this.isValid = true;
  }
}
