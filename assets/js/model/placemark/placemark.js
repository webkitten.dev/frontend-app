export default class Placemark {
  constructor(model) {
    this.lat = model.lat || model.coord_lat || null;
    this.lng = model.lng || model.coord_lng || null;
    this.id = model.id || model.item_id || null;
  }
}
