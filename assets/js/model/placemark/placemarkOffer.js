import Placemark from './placemark';

export default class BalloonShop extends Placemark {
  constructor(model) {
    super(model);
    this.price = model.price || 0;
    this.oldPrice = model.old_price || 0;
    this.discountCheck = model.discount_check || 0;
    this.sellerQuantity = true;
    this.sellerURL = `${model.shop_id}/${model.organization_id}#${model.part_id}`;
    this.inStock = model.in_stock || '';
    this.quantity = model.quantity || 0;
    this.deliveryString = model.delivery_string || '';
    this.avgRating = model.avg_rating || '';
    this.feedbackCount = model.feedback_count || '';
    this.shopId = model.shop_id || '';
    this.shopName = model.shop_name || '';
    this.organizationId = model.organization_id || '';
    this.partId = model.part_id || '';
    this.contactIsConfirmed = model.contact_is_confirmed || '';
    this.cityName = model.city_name || '';
    this.isNetShop = model.is_netshop || '';
    this.street = model.street || '';
    this.building = model.building || '';
    this.office = model.office || '';
    this.scheduleDayIsSet = model.schedule_day_isset || '';
    this.schedule = {
      schedule_day_isset: model.schedule_day_isset,
      current_date_time_with_tz: model.current_date_time_with_tz,
      around_the_clock: model.around_the_clock,
      opening_date_time: model.opening_date_time,
      close_date_time: model.close_date_time,
      dinner_begin_date_time: model.dinner_begin_date_time,
      dinner_end_date_time: model.dinner_end_date_time,
    };
  }
}
