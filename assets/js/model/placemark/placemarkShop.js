import Placemark from './placemark';

export default class PlacemarkShop extends Placemark {
  constructor(model) {
    super(model);
    this.name = model.shop_name || '';
    this.site = model.shop_site || '';
    this.name = model.shop_name || '';
    this.city = model.city_name || '';
    this.building = model.building || '';
    this.street = model.street || '';
    this.phones = model.phones || [];
    this.fullAddress = this.address;
  }

  get address() {
    const address = [];
    this.city && address.push(this.city);
    this.building && address.push(this.building);
    this.street && address.push(this.street);

    return address.join(', ');
  }
}
