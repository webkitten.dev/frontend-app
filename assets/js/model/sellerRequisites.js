import Requisites from '~/assets/js/enums/seller/requisites';

export default class SellerRequisites {
  constructor(model) {
    this.name = {
      field: Requisites.NAME,
      value: model.name || '',
    };
    this.inn = {
      field: Requisites.INN,
      value: model.inn || '',
    };
    this.kpp = {
      field: Requisites.KPP,
      value: model.kpp || '',
    };
    this.directorName = {
      field: Requisites.DIRECTOR_NAME,
      value: model.directorName || '',
    };
    this.ogrn = {
      field: Requisites.OGRN,
      value: model.ogrn || '',
    };
    this.fullName = {
      field: Requisites.FULL_NAME,
      value: model.fullName || '',
    };
  }
}
