export default Object.freeze({
  season: null,
  brand: null,
  model: null,
  width: null,
  profile: null,
  diameter: null,
  carManufacturer: null,
  carModel: null,
  carType: null,
  carYear: null,
});
