export default Object.freeze({
  season: 's',
  brand: 'b',
  model: 'm',
  width: 'w',
  profile: 'p',
  diameter: 'd',
  carManufacturer: 'cman',
  carModel: 'cmod',
  carType: 'ct',
  carYear: 'cy',
});
