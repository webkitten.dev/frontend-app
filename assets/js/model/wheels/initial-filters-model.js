export default Object.freeze({
  type: null,
  brand: null,
  width: null,
  diameter: null,
  drill: null,
  offset: null,
  centerDiameter: null,
  carManufacturer: null,
  carModel: null,
  carType: null,
  carYear: null,
});
