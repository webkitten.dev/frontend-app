export default Object.freeze({
  type: 't',
  brand: 'b',
  width: 'w',
  diameter: 'di',
  drill: 'dr',
  offset: 'o',
  centerDiameter: 'dia',
  carManufacturer: 'cman',
  carModel: 'cmod',
  carType: 'ct',
  carYear: 'cy',
});
