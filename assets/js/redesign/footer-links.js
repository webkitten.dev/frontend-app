export const listFooterNavigation = [
  {
    title: 'Покупателям',
    list: [
      {
        name: 'Как и где найти запчасть',
        href: '/faq/find-spare-parts-for-car',
        domain: 'helper',
      },
      {
        name: 'Для чего нужен номер запчасти?',
        href: '/faq/what-part-number-for',
        domain: 'helper',
      },
      {
        name: 'Как найти оригинальную запчасть?',
        href: '/faq/kak-najti-originalnuyu-zapchast-na-avto',
        domain: 'helper',
      },
      {
        name: 'Как найти запчасть без VIN-номера?',
        href: '/faq/kak-najti-zapchasti-dlya-avto-esli-net-vin-koda',
        domain: 'helper',
      },
      {
        name: 'Помощь в поиске запчастей',
        href: '/faq/help-finding-parts',
        domain: 'helper',
      },
      {
        name: 'Помощь в подборе автосервиса',
        href: '/faq/help-selection-car-service',
        domain: 'helper',
      },
      {
        name: 'Частые вопросы',
        href: '/faq/help-finding-parts',
        domain: 'helper',
      },
    ],
  },
  {
    title: 'Продавцам',
    list: [
      {
        name: 'Правила размещения на сайте',
        href: '/about-us/for-providers',
        domain: 'main',
      },
      {
        name: 'Что нельзя продавать через наш сайт',
        href: '/about-us/prohibited',
        domain: 'main',
      },
      {
        name: 'Правила работы с запросами покупателей',
        href: '/about-us/offer',
        domain: 'main',
      },
      {
        name: 'Стать партнером "Автоконсьержа"',
        href: '/partner/scheme',
        domain: 'helper',
      },
    ],
  },
  {
    title: 'Каталоги',
    list: [
      {
        name: 'Каталоги оригиналов',
        href: '/catalog/oem',
        domain: 'main',
      },
      {
        name: 'Каталоги заменителей',
        href: '/catalog/am',
        domain: 'main',
      },
      {
        name: 'Каталоги по виду запчасти',
        href: '/categories',
        domain: 'main',
      },
    ],
  },
  {
    title: 'Услуги',
    list: [
      {
        name: 'Автоконсъерж',
        href: '/',
        domain: 'helper',
      },
      {
        name: 'Поиск СТО',
        href: '/',
        domain: 'sto',
      },
      {
        name: 'Проверка авто',
        href: '/check-auto',
        domain: 'main',
      },
    ],
  },
  {
    title: 'О нас',
    list: [
      {
        name: 'О проекте «ГисАвто»',
        href: '/about-us/about-us',
        domain: 'main',
      },
      {
        name: 'Блог',
        href: 'https://blog.gisauto.ru',
        domain: '',
      },
      {
        name: 'Реквизиты',
        href: '/about-us/requisites',
        domain: 'main',
      },
      {
        name: 'Контакты',
        href: '/about-us/contacts',
        domain: 'main',
      },
    ],
  },
];

export const listFooterLinks = [
  {
    title: 'Пользовательское соглашение',
    href: '/about-us/regulations',
    domain: 'main',
  },
  {
    title: 'Политика конфиденциальности',
    href: '/about-us/agreement',
    domain: 'main',
  },
  {
    title: '© 2015–2019  GISAUTO',
  },
];
