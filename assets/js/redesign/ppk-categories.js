export const ppkCategories = [
  {
    title: 'Запчасти для ТО',
    href: '/categories/zapchasti-dlya-to',
    childs: [
      {
        title: 'Внеплановое ТО',
        href: '/categories/zapchasti-dlya-to/vneplanovoe-to',
      },
      {
        title: 'Аккумулятор',
        href: '/categories/zapchasti-dlya-to/vneplanovoe-to/akkumulyator',
      },
      {
        title: 'Барабан тормозной',
        href: '/categories/zapchasti-dlya-to/vneplanovoe-to/baraban-tormoznoj',
      },
    ],
  },
  {
    title: 'Впускная система',
    href: '/categories/vpusknaya-sistema',
    childs: [
      {
        title: 'Запчасти воздуховода',
        href: '/categories/vpusknaya-sistema/zapchasti-vozduhovoda',
      },
      {
        title: 'Запчасти впускного коллектора',
        href: '/categories/vpusknaya-sistema/zapchasti-vpusknogo-kollektora',
      },
      {
        title: 'Запчасти дроссельной заслонки',
        href: '/categories/vpusknaya-sistema/zapchasti-drosselnoj-zaslonki',
      },
    ],
  },
  {
    title: 'Выхлопная система',
    href: '/categories/vyhlopnaya-sistema',
    childs: [
      {
        title: 'Глушитель',
        href: '/categories/vyhlopnaya-sistema/glushitel',
      },
      {
        title: 'Сажевый фильтр',
        href: '/categories/vyhlopnaya-sistema/glushitel/sazhevyj-filtr',
      },
      {
        title: 'Система рециркуляции отработанных газов (EGR)',
        href: '/categories/vyhlopnaya-sistema/sistema-recirkulyacii-otrabotannyh-gazov-egr',
      },
    ],
  },
  {
    title: 'Детали салона',
    href: '/categories/detali-salona',
    childs: [
      {
        title: 'Бардачок',
        href: '/categories/detali-salona/bardachok',
      },
      {
        title: 'Коврики | Чехлы',
        href: '/categories/detali-salona/kovriki-chehly',
      },
      {
        title: 'Кресла | Диваны',
        href: '/categories/detali-salona/kresla-divany',
      },
    ],
  },
  {
    title: 'Дополнительное оборудование',
    href: '/categories/dopolnitelnoe-oborudovanie',
    childs: [
      {
        title: 'Автовинил',
        href: '/categories/dopolnitelnoe-oborudovanie/avtovinil',
      },
      {
        title: 'Аксессуары',
        href: '/categories/dopolnitelnoe-oborudovanie/aksessuary',
      },
      {
        title: 'Аудио',
        href: '/categories/dopolnitelnoe-oborudovanie/audio',
      },
    ],
  },
  {
    title: 'Запчасти двигателя',
    href: '/categories/zapchasti-dvigatelya',
    childs: [
      {
        title: 'Блок управления',
        href: '/categories/zapchasti-dvigatelya/blok-upravleniya',
      },
      {
        title: 'Двигатель',
        href: '/categories/zapchasti-dvigatelya/dvigatel',
      },
      {
        title: 'Запчасти блока цилиндров',
        href: '/categories/zapchasti-dvigatelya/zapchasti-bloka-cilindrov',
      },
    ],
  },
  {
    title: 'Запчасти кузова',
    href: '/categories/zapchasti-kuzova',
    childs: [
      {
        title: 'Багажник',
        href: '/categories/zapchasti-kuzova/bagazhnik',
      },
      {
        title: 'Бампера',
        href: '/categories/zapchasti-kuzova/bampera',
      },
      {
        title: 'Кронштейн заднего бампера',
        href: '/categories/zapchasti-kuzova/bampera/kronshtejn-zadnego-bampera',
      },
    ],
  },
  {
    title: 'Оптика',
    href: '/categories/optika',
    childs: [
      {
        title: 'Габариты',
        href: '/categories/optika/gabarity',
      },
      {
        title: 'Поворотники',
        href: '/categories/optika/povorotniki',
      },
      {
        title: 'Фары',
        href: '/categories/optika/fary',
      },
    ],
  },
  {
    title: 'Подвеска',
    href: '/categories/podveska',
    childs: [
      {
        title: 'Балка передняя поперечная',
        href: '/categories/podveska/zapchasti-podveski/balka-perednyaya-poperechnaya',
      },
      {
        title: 'Балка подвески',
        href: '/categories/podveska/zapchasti-podveski/balka-podveski',
      },
      {
        title: 'Блок клапанов пневмоподвески',
        href: '/categories/podveska/zapchasti-podveski/blok-klapanov-pnevmopodveski',
      },
    ],
  },
  {
    title: 'Рулевое управление',
    href: '/categories/rulevoe-upravlenie',
    childs: [
      {
        title: 'Амортизатор рейки рулевой',
        href: '/categories/rulevoe-upravlenie/rulevoj-mehanizm/amortizator-rejki-rulevoj',
      },
      {
        title: 'Блок управления рулевой рейкой',
        href: '/categories/rulevoe-upravlenie/rulevoj-mehanizm/blok-upravleniya-rulevoj-rejkoj',
      },
      {
        title: 'Шаровая опора',
        href: '/categories/rulevoe-upravlenie/rulevoj-mehanizm/sharovaya-opora_1',
      },
    ],
  },
  {
    title: 'Система зажигания',
    href: '/categories/sistema-zazhiganiya',
    childs: [
      {
        title: 'Замки зажигания',
        href: '/categories/sistema-zazhiganiya/zamki-zazhiganiya',
      },
      {
        title: 'Катушки зажигания',
        href: '/categories/sistema-zazhiganiya/katushki-zazhiganiya',
      },
      {
        title: 'Распределитель зажигания',
        href: '/categories/sistema-zazhiganiya/raspredelitel-zazhiganiya',
      },
    ],
  },
  {
    title: 'Система охлаждения',
    href: '/categories/sistema-ohlazhdeniya',
    childs: [
      {
        title: 'Вентиляторы',
        href: '/categories/sistema-ohlazhdeniya/ventilyatory',
      },
      {
        title: 'Насос системы охлаждения',
        href: '/categories/sistema-ohlazhdeniya/nasos-sistemy-ohlazhdeniya',
      },
      {
        title: 'Радиаторы',
        href: '/categories/sistema-ohlazhdeniya/radiatory',
      },
    ],
  },
  {
    title: 'Системы безопасности',
    href: '/categories/sistemy-bezopasnosti',
    childs: [
      {
        title: 'Ремни безопасности',
        href: '/categories/sistemy-bezopasnosti/remni-bezopasnosti',
      },
      {
        title: 'Система пассивной безопасности (SRS)',
        href: '/categories/sistemy-bezopasnosti/sistema-passivnoj-bezopasnosti-srs',
      },
      {
        title: 'Система стабилизации движения (ESP)',
        href: '/categories/sistemy-bezopasnosti/sistema-stabilizacii-dvizheniya-esp',
      },
    ],
  },
  {
    title: 'Стекла',
    href: '/categories/stekla',
    childs: [
      {
        title: 'Боковые',
        href: '/categories/stekla/bokovye',
      },
      {
        title: 'Задние',
        href: '/categories/stekla/zadnie',
      },
      {
        title: 'Уплотнители',
        href: '/categories/stekla/uplotniteli',
      },
    ],
  },
  {
    title: 'Топливная система',
    href: '/categories/toplivnaya-sistema',
    childs: [
      {
        title: 'Запчасти карбюратора',
        href: '/categories/toplivnaya-sistema/zapchasti-karbyuratora',
      },
      {
        title: 'Система впрыска',
        href: '/categories/toplivnaya-sistema/sistema-vpryska',
      },
      {
        title: 'Топливные баки',
        href: '/categories/toplivnaya-sistema/toplivnye-baki',
      },
    ],
  },
  {
    title: 'Тормозная система',
    href: '/categories/tormoznaya-sistema',
    childs: [
      {
        title: 'ABS',
        href: '/categories/tormoznaya-sistema/abs',
      },
      {
        title: 'Стояночная тормозная система',
        href: '/categories/tormoznaya-sistema/stoyanochnaya-tormoznaya-sistema',
      },
      {
        title: 'Тормозной механизм | Суппорт',
        href: '/categories/tormoznaya-sistema/tormoznoj-mehanizm-support',
      },
    ],
  },
  {
    title: 'Трансмиссия | КПП',
    href: '/categories/transmissiya-kpp',
    childs: [
      {
        title: 'Блокиратор АКПП tiptronic',
        href: '/categories/transmissiya-kpp/avtomaticheskaya-kpp-tiptronik/blokirator-akpp-tiptronic',
      },
      {
        title: 'Фильтр АКПП tiptronic',
        href: '/categories/transmissiya-kpp/avtomaticheskaya-kpp-tiptronik/filtr-akpp-tiptronic',
      },
      {
        title: 'АКПП (Автоматическая КПП)',
        href: '/categories/transmissiya-kpp/akpp-avtomaticheskaya-kpp',
      },
    ],
  },
  {
    title: 'Шины | Диски',
    href: '/categories/shiny-diski',
    childs: [
      {
        title: 'Диски',
        href: '/categories/shiny-diski/diski',
      },
      {
        title: 'Запаски | Докатки',
        href: '/categories/shiny-diski/zapaski-dokatki',
      },
      {
        title: 'Колпаки',
        href: '/categories/shiny-diski/kolpaki',
      },
    ],
  },
  {
    title: 'Электрооборудование',
    href: '/categories/elektrooborudovanie',
    childs: [
      {
        title: 'Генераторы',
        href: '/categories/elektrooborudovanie/generatory',
      },
      {
        title: 'Запчасти звукового сигнала',
        href: '/categories/elektrooborudovanie/zapchasti-zvukovogo-signala',
      },
      {
        title: 'Предохранители | Реле | Датчики',
        href: '/categories/elektrooborudovanie/predohraniteli-rele-datchiki',
      },
    ],
  },
  {
    title: 'Прочее',
    href: '/categories/prochee',
    childs: [
      {
        title: 'Втулка направляющая',
        href: '/categories/prochee/prochee/vtulka-napravlyayushhaya',
      },
      {
        title: 'Герметик',
        href: '/categories/prochee/prochee/germetik',
      },
      {
        title: 'Насос вакуумный',
        href: '/categories/prochee/prochee/nasos-vakuumnyj',
      },
    ],
  },
];
