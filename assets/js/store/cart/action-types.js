export const ADD_ITEMS = 'addItems';
export const GET = 'get';
export const UPDATE = 'update';
export const CLEAR = 'clear';
export const SELECT_ALL_ITEMS = 'selectAllItems';
