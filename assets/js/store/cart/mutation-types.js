export const SET_IS_ADDED = 'SET_IS_ADDED';
export const SET_ITEMS = 'SET_ITEMS';
export const SET_SELECT_ITEM_BY_ID = 'SET_SELECT_ITEM_BY_ID';
export const DELETE_ITEM_BY_ID = 'DELETE_ITEM_BY_ID';
export const SET_IS_PREORDER = 'SET_IS_PREORDER';
export const SET_QUANTITY_ITEM = 'SET_QUANTITY_ITEM';
export const SET_COMMENTS = 'SET_COMMENTS';
