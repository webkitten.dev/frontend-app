export const GET_SHOP_ONLINE_STATUS = 'getShopOnlineStatus';
export const SHOW_ROOM = 'showRoom';
export const OPEN = 'open';
export const CLOSE = 'close';
export const LOADED = 'load';
export const OPEN_TECHSUPPORT_ROOM = 'openTechsupportRoom';
