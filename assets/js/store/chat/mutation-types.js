export const SET_LOAD = 'SET_LOAD';
export const SET_OPEN = 'SET_OPEN';
export const SET_CLOSE = 'SET_CLOSE';
export const SET_NEW_MESSAGES = 'SET_NEW_MESSAGES';
export const SET_OPEN_DIALOG_ONLOAD = 'SET_OPEN_DIALOG_ONLOAD';
