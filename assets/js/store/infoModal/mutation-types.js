export const SET_SHOW = 'SET_SHOW';
export const SET_HIDE = 'SET_HIDE';
export const SET_TOGGLE = 'SET_TOGGLE';
export const SET_TEXT = 'SET_TEXT';
export const SET_BUTTON_TEXT = 'SET_BUTTON_TEXT';
export const SET_BUTTON_URL = 'SET_BUTTON_URL';
export const SET_BUTTON_DOMAIN = 'SET_BUTTON_DOMAIN';
export const SET_BUTTON_VISIBILITY = 'SET_BUTTON_VISIBILITY';
