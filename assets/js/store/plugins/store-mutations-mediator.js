import * as USER_MUTATIONS from '~/assets/js/store/user/mutation-types';
import * as MODULES from '~/assets/js/store/type-modules/type-modules';
import * as CART_ACTIONS from '~/assets/js/store/cart/action-types';

export default function storeMutationsMediator(store) {
  store.subscribe(({ type, payload }) => {
    switch (type) {
      case `${MODULES.USER}/${USER_MUTATIONS.SET_USER}`:
        if (payload) {
          const user = store.state.user.auth;
          store.dispatch(`${MODULES.CART}/${CART_ACTIONS.GET}`, user);
        }
        break;
      default:
    }
  });
}
