export const AUTO_DETECT_LOCATION = 'autoDetectLocation';
export const DISABLE_AUTO_DETECT_LOCATION = 'disableAutoDetectLocation';
export const SET_LOCATION = 'setLocation';
export const LOAD_POPULAR_PART_GROUPS = 'loadPopularPartGroups';
