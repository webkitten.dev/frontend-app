export const SET_DOMAIN = 'SET_DOMAIN';
export const SET_SESSION_ID = 'SET_SESSION_ID';
export const SET_NAVIGATION = 'SET_NAVIGATION';
export const SET_LOCATION = 'SET_LOCATION';
export const SET_LOCATION_AUTO_DETECT_DISABLE = 'SET_LOCATION_AUTO_DETECT_DISABLE';
export const SET_FAST_ORDER = 'SET_FAST_ORDER';
export const SET_IS_BOT = 'SET_IS_BOT';
export const SET_POPULAR_PART_GROUPS = 'SET_POPULAR_PART_GROUPS';
