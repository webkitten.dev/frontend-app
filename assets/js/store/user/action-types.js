export const LOGIN = 'login';
export const LOGOUT = 'logout';
export const GET_COUNTERS = 'getCounters';
export const LOAD_STAT = 'loadStat';
export const REFRESH_USER = 'refreshUser';
export const REFETCH_USER = 'refetchUser';
