export default [
  {
    name: 'brand',
    title: 'Марка',
    sorting: {
      has: true,
      criteria: 'brand',
    },
  },
  {
    name: 'model_name',
    title: 'Модель',
    sorting: {
      has: true,
      criteria: 'model_name',
    },
  },
  {
    name: 'type_name',
    title: 'Модификация',
    sorting: {
      has: true,
      criteria: 'type_name',
    },
  },
];
