export default [
  {
    name: 'img',
    title: '',
    classes: '',
    sorting: { has: false, criteria: '' },
  },
  {
    name: 'brand',
    title: 'Производитель',
    classes: '',
    sorting: { has: false, criteria: '' },
  },
  {
    name: 'number',
    title: 'Номер',
    classes: '',
    sorting: { has: false, criteria: '' },
  },
  {
    name: 'partName',
    title: 'Название',
    classes: '',
    sorting: { has: false, criteria: '' },
  },
  {
    name: 'price',
    title: 'Стоимость',
    classes: '',
    sorting: { has: false, criteria: '' },
  },
  {
    name: 'sellerCounterAll',
    title: 'Всего на сайте',
    classes: '',
    sorting: { has: false, criteria: '' },
  },
  {
    name: 'topMobile',
    title: '',
    classes: '',
    sorting: { has: false, criteria: '' },
  },
];
