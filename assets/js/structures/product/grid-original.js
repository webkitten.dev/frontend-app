export default [
  {
    name: 'names',
    title: 'Бренд, артикул, название у продавца',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'comment',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'createdAd',
    title: 'Добавлено',
    titleMobile: 'по дате публикации',
    classes: '',
    sorting: {
      has: true,
      criteria: 'created_at',
    },
  },
  {
    name: 'delivery',
    title: 'Срок доставки',
    titleMobile: 'по сроку доставки',
    classes: '',
    sorting: {
      has: false,
      criteria: 'quantity',
    },
  },
  {
    name: 'sellerBasic',
    title: 'Продавец',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
  {
    name: 'price',
    title: 'Стоимость',
    titleMobile: 'по стоимости',
    classes: '_align-right',
    sorting: {
      has: true,
      criteria: 'price',
    },
  },
  {
    name: 'buttonsOrder',
    title: '',
    classes: '',
    sorting: {
      has: false,
      criteria: '',
    },
  },
];
