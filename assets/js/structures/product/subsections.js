import Subsections from '~/assets/js/enums/product/subsections';

export default [
  {
    name: Subsections.ORIGIN,
    title: 'Предложения продавцов',
    has: false,
  },
  {
    name: Subsections.ANALOG,
    title: 'Аналоги и заменители',
    has: false,
  },
  {
    name: Subsections.CHARACTERISTICS,
    title: 'Характеристики',
    has: true,
  },
  {
    name: Subsections.APPLICABILITY,
    title: 'Применяемость',
    has: true,
  },
];
