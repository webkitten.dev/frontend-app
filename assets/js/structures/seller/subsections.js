import Subsections from '~/assets/js/enums/seller/subsections';

export default [
  // {
  //   name: Subsections.ORIGIN,
  //   title: 'Новые',
  // },
  // {
  //   name: Subsections.CONTRACT,
  //   title: 'Контрактные б/у',
  // },
  // {
  //   name: Subsections.RATING,
  //   title: 'Рейтинг',
  // },
  {
    name: Subsections.FEEDBACK,
    title: 'Отзывы',
  },
  {
    name: Subsections.REQUISITES,
    title: 'Реквизиты',
  },
  {
    name: Subsections.BRANCHES,
    title: 'Филиалы',
  },
];
