import { extend, isEmpty } from 'lodash-es';

class Validator {
  constructor(value = null, type = null, options = {}) {
    this.options = extend({
      onReckoningReturn: false,
    }, options);
    this.value = value;
    this.type = type;
  }

  check(inputValue = null, inputType = null, inputOptions = {}) {
    const value = inputValue || this.value;
    const type = inputType || this.type;
    const options = extend({
      layout: 'eng',
    }, inputOptions);
    switch (type) {
      case 'vin':
        return Validator.checkVin(value);
      case 'frame':
        return Validator.checkFrame(value);
      case 'phone':
        return Validator.checkPhone(value);
      case 'email':
        return Validator.checkEmail(value);
      case 'notBlank':
        return Validator.notBlank(value);
      case 'string':
        return (options.layout === 'eng' && Validator.stringLayoutEng(value))
          || (options.layout === 'rus' && Validator.stringLayoutRus(value))
          || (options.layout === 'num' && Validator.stringLayoutNum(value));
      default:
        console.warn(`Validator type "${type}" is not find`);
        return this.options.onReckoningReturn;
    }
  }

  /**
  * Не пустое значение
  * @param value
  * @returns {boolean}
  */
  static notBlank(value) {
    return typeof value !== 'undefined'
      && typeof value !== 'function'
      && value !== null
      && (
        (typeof value === 'string' && value.length)
        || typeof value !== 'string'
      );
  }

  /**
  * Это строка?
  * @param value
  * @return {boolean}
  */
  static isString(value) {
    return typeof value === 'string';
  }

  /**
  * Выполнить проверку
  * @param value
  * @returns {boolean}
  */
  static checkVin(value) {
    return Validator.notBlank(value)
      && Boolean(value.match(/^[0-9A-HJ-NPR-Z]{17}(\n|$)/gi));
  }

  /**
  * Выполнить проверку
  * @param value
  * @returns {boolean}
  */
  static checkFrame(value) {
    return Validator.notBlank(value)
      && value.length !== 17
      && value.length >= 8
      && value.length <= 20
      && Boolean(value.match(/^[\d\w]+[- ]{1}[\d\w]+(\n|$)/gi));
  }

  /**
  * Проверить номер телефона
  * @param value
  * @returns {boolean}
  */
  static checkPhone(value) {
    return Validator.notBlank(value) && value.toString().match(/^[\d\s()+-]+$/);
  }

  /**
   * Проверка является ли номер миобильным. Пока что только для России.
   * @param value
   */
  static isMobilePhoneNumber(value) {
    // TODO: регулярки для не-России?
    value = value.replace(/[^+\d]/g, '');
    return /^(\+?7|8)9\d{9}$/g.test(value);
  }

  /**
  * Проверка Email
  * @param value
  * @returns {boolean}
  */
  static checkEmail(value) {
    return typeof Validator.notBlank(value)
      && Validator.isString(value)
      && !(// см комментарий в src/GisautoWebBundle/Resources/public/js/app/modules/Validator.js
        !value.match(/^(?!.*[-._]{2,})(?!.*?_.*?_.*?_.*?_.*?_.*?_)(?!.*?-.*?-.*?-.*?-)(?!.*?\..*?\..*?\..*?\..*?\.)(?!.*[-._]+$)[-._a-zа-яё0-9]{1,30}@[-._a-zа-яё0-9]{1,50}$/igm)
        || !value.match(/^(([a-zа-яё0-9]+|[a-zа-яё0-9][-._a-zа-яё0-9]*[a-zа-яё0-9])@([a-zа-яё0-9]+\.|[a-zа-яё0-9][-._a-zа-яё0-9]*[a-zа-яё0-9]\.){1,4}[a-zа-я]{2,})$/igm)
        || value.match(/^(.*([0-9]{1,3}\.){3}[0-9]{1,3}.*)$/igm)
      );
  }

  static stringLayoutEng(value) {
    return typeof value === 'string' && !value.match(/[а-яА-Я]{1,}/g);
  }

  static stringLayoutRus(value) {
    return typeof value === 'string' && !value.match(/[a-zA-Z]{1,}/g);
  }

  static stringLayoutNum(value) {
    return typeof value === 'number'
      || (typeof value === 'string' && !value.match(/[0-9]{0,}/g));
  }

  static viber(value) {
    return /^\+?\d{11,15}$/gi.test(value.replace(/\s|-|\(|\)|\+/gi, '').toLowerCase());
  }

  static skype(value) {
    // eslint-disable-next-line no-useless-escape
    return /^[a-zA-Z][A-Za-z\d\,\-\.\_\:]{5,256}$/gi.test(value);
  }

  static icq(value) {
    return /^\d{5,11}$/gi.test(value);
  }

  static telegram(value) {
    // eslint-disable-next-line no-useless-escape
    return /^[a-z0-9\_]{5,256}$/gi.test(value.replace(/\+|@|\s/gi, '').toLowerCase());
  }

  static whatsApp(value) {
    // eslint-disable-next-line no-useless-escape
    return /^\+?\d{11,15}$/gi.test(value.replace(/\s|-|\(|\)|\+/gi, '').toLowerCase());
  }

  static checkMessengers(item) {
    switch (item.type.id) {
      case '162':
        return !isEmpty(item.name) && !Validator.viber(item.name);
      case '58':
        return !isEmpty(item.name) && !Validator.skype(item.name);
      case '59':
        return !isEmpty(item.name) && !Validator.icq(item.name);
      case '60':
        return !isEmpty(item.name) && !Validator.telegram(item.name);
      case '161':
        return !isEmpty(item.name) && !Validator.whatsApp(item.name);
      default:
        return !isEmpty(item.name);
    }
  }
}

export default Validator;
