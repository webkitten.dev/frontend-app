export type Connection<T> = {
  pageInfo: PageInfo;
  edges: Edge<T>[];
};

type PageInfo = {
  // hasNextPage: boolean;
  count: number;
};

type Edge<T> = {
  // cursor: string;
  node: T;
};
