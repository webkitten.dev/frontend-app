export class ListResponse<T> {
  public items: T[] = [];
  public count: number = 0;
}
