import GraphQLQuery from 'graphql-query-builder';

import { Request as CommonRequest } from '../../../common-bundle/essence/request';

export interface Request extends CommonRequest {
  requestPayload: GraphQLQuery;
}
