import GraphQLQuery from 'graphql-query-builder';
import { AxiosRequestConfig } from 'axios';
import { IncomingHttpHeaders } from 'http';
import { isNil, isObject } from 'lodash';

import {
  LoggerContainer,
  LoggerCategories,
} from '../../../common-bundle/logger';
const logger = LoggerContainer.get(LoggerCategories.MONOLITH);

import { Request } from '../entities/request';
import { AbstractRepository as CommonAbstractRepository } from '../../../common-bundle/io/abstract-repository';

export abstract class AbstractRepository extends CommonAbstractRepository {
  protected readonly queryNamespace?: string;
  protected readonly endpoint!: string;
  protected readonly headers: IncomingHttpHeaders = {};
  protected readonly additionalFields: { [key: string]: string[] } = {};

  protected request(requestConfig: Request): Promise<any> {
    const timestamp = Number(new Date());
    logger.verbose(
      `request ${timestamp} %s`,
      `${requestConfig.requestType} {${requestConfig.requestPayload.toString()}}`,
    );
    return new Promise((resolve, reject) => {
      super.request(requestConfig)
        .then(({ data }) => {
          logger.verbose(`response ${timestamp} %j`, data);
          resolve(data);
        })
        .catch(e => reject(e));
    });
  }

  protected getRequestParams(query: GraphQLQuery, method: string): Request {
    const requestConfig: AxiosRequestConfig = {
      method,
      url: this.endpoint,
      headers: {
        ...this.headers,
        'Content-Type': 'application/json',
      },
    };
    const requestType = method === 'post' ? 'mutation' : 'query';
    switch (method) {
      case 'get':
        requestConfig.url += `?query=query{${encodeURIComponent(query.toString())}}`;
        break;
      case 'post':
        requestConfig.data = {
          query: `mutation{${query.toString()}}`,
        };
        break;
    }
    return {
      requestType,
      prepared: requestConfig,
      requestPayload: query,
    };
  }

  protected getQuery(inputEndpoint: string, params: object, inputFields: any[], innerQuery = false): GraphQLQuery {
    const fields = inputFields;
    const queryParams: { [key: string]: any } = this.omitNilProperty(Object.assign({}, params));
    let endpoint = inputEndpoint;
    if (!innerQuery) {
      if (this.queryNamespace) {
        endpoint = `${this.queryNamespace}_${endpoint}`;
      }
      endpoint = `data:${endpoint}`;
      Object.keys(this.additionalFields).forEach((name) => {
        const additionalField = new GraphQLQuery('meta');
        additionalField.find(this.additionalFields[name]);
        fields.push(additionalField);
      });
    }
    const query = new GraphQLQuery(endpoint, queryParams);
    query.find(fields);
    return query;
  }

  /***
   * Удаляем из объекта все поля что null или undefined
   * @param value type Object
   * @private
   */
  // @todo unit-test
  protected omitNilProperty(value: any): any {
    switch (true) {
      case (Array.isArray(value)):
        return value
          .filter((v: any) => !isNil(v))
          .map((v: any) => this.omitNilProperty(v));
      case (isObject(value)):
        const result: any = {};
        for (const key of Object.keys(value)) {
          if (!isNil(value[key])) {
            result[key] = this.omitNilProperty(value[key]);
          }
        }
        return result;
      default:
        if (!isNil(value)) {
          return value;
        }
    }
  }
}
