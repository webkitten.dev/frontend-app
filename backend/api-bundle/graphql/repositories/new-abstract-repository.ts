import { request } from 'graphql-request';

import {
  LoggerContainer,
  LoggerCategories,
} from '../../../common-bundle/logger';
const logger = LoggerContainer.get(LoggerCategories.GRAPHQL);

import { Connection } from '../entities/connection';
import { ListResponse } from '../entities/list-response';

export class AbstractRepository {
  protected readonly endpoint = process.env.SERVICE_GRAPHQL_URI as string;

  protected request<T>(query: string, variables?: object): Promise<T> {
    const timestamp = Number(new Date());
    const queryFormatted = query.replace(/\r?\n|\r/g, '').replace(/\s+/g, ' ');
    return new Promise(async (resolve, reject) => {
      logger.verbose(`request ${timestamp} %s, variables %j`, queryFormatted, variables || {});
      request<{ data: T }>(this.endpoint, query, variables)
        .then(({ data }) => {
          logger.verbose(`response ${timestamp} %j`, data);
          resolve(data);
        })
        .catch((e) => {
          logger.warn('error <%j> in %s, variables %j', e, queryFormatted, variables || {});
          reject(e);
        });
    });
  }

  protected _list<T>(query: string, variables?: object): Promise<ListResponse<T>> {
    return this.request<Connection<T>>(query, variables).then(data => ({
      items: data.edges.map(edge => edge.node),
      count: data.pageInfo.count,
    }));
  }

  protected _get<T>(query: string, variables?: object): Promise<T> {
    return this.request<T>(query, variables);
  }
}
