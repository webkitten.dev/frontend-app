// tslint:disable
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  Datetime: any,
  Time: any,
};

export type Address = {
  __typename?: 'Address',
  street: Scalars['String'],
  building: Scalars['String'],
  office: Scalars['String'],
  isConfirmed: Scalars['Boolean'],
};

export type AuthData = {
  __typename?: 'AuthData',
  tokens: TokensBearer,
};

export type CarManufacturer = Nameable & {
  __typename?: 'CarManufacturer',
  id: Scalars['ID'],
  name: Scalars['String'],
  slug: Scalars['String'],
  models: CarModelsConnection,
};


export type CarManufacturerModelsArgs = {
  skip?: Scalars['Int'],
  first?: Scalars['Int']
};

export type CarManufacturerEdge = {
  __typename?: 'CarManufacturerEdge',
  cursor: Scalars['String'],
  node: CarManufacturer,
};

export type CarManufacturersConnection = {
  __typename?: 'CarManufacturersConnection',
  pageInfo: PageInfo,
  edges: Array<CarManufacturerEdge>,
};

export type CarModel = Nameable & {
  __typename?: 'CarModel',
  id: Scalars['ID'],
  name: Scalars['String'],
  slug: Scalars['String'],
  manufacturer: CarManufacturer,
};

export type CarModelEdge = {
  __typename?: 'CarModelEdge',
  cursor: Scalars['String'],
  node: CarModel,
};

export type CarModelsConnection = {
  __typename?: 'CarModelsConnection',
  pageInfo: PageInfo,
  edges: Array<CarModelEdge>,
};

export type Company = {
  __typename?: 'Company',
  id: Scalars['ID'],
};

export type Contact = {
  __typename?: 'Contact',
  kind: ContactType,
  value: Scalars['String'],
  isConfirmed: Scalars['Boolean'],
};

export type Contactable = {
  websites?: Maybe<Array<Contact>>,
  emails?: Maybe<Array<Contact>>,
  phones?: Maybe<Array<Contact>>,
  messengers?: Maybe<Array<Contact>>,
};

export enum ContactType {
  Unknown = 'UNKNOWN',
  Email = 'EMAIL',
  Phone = 'PHONE',
  Website = 'WEBSITE',
  Icq = 'ICQ',
  Skype = 'SKYPE',
  Telegram = 'TELEGRAM',
  Viber = 'VIBER',
  Whatsapp = 'WHATSAPP'
}

export type ContractPart = Nameable & {
  __typename?: 'ContractPart',
  id: Scalars['ID'],
  statusUpdatedAt: Scalars['Datetime'],
  activeUntilAt: Scalars['Datetime'],
  createdAt: Scalars['Datetime'],
  actualAt: Scalars['Datetime'],
  status: PartAdStatus,
  name: Scalars['String'],
  description: Scalars['String'],
  city?: Maybe<Geo>,
  metro?: Maybe<Geo>,
  shop?: Maybe<Shop>,
  part?: Maybe<Part>,
  car: CarModel,
  color: Scalars['String'],
  cost: Scalars['Float'],
  quantity: Scalars['Int'],
  partCondition: PartCondition,
  seller: Contactable,
  isSet: Scalars['Boolean'],
  inStock: Scalars['Boolean'],
  partPlacement: Array<Placement>,
  images: Array<Image>,
  carChassis: Array<Scalars['String']>,
  carEngines: Array<Scalars['String']>,
  carYears?: Maybe<YearsRange>,
  statisticsData: ContractPartStatisticsData,
};

export type ContractPartEdge = {
  __typename?: 'ContractPartEdge',
  cursor: Scalars['String'],
  node: ContractPart,
};

export type ContractPartsConnection = {
  __typename?: 'ContractPartsConnection',
  pageInfo: PageInfo,
  edges: Array<ContractPartEdge>,
};

export enum ContractPartsSort {
  ActiveUntilAtAsc = 'activeUntilAt_ASC',
  ActiveUntilAtDesc = 'activeUntilAt_DESC',
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  ActualAtAsc = 'actualAt_ASC',
  ActualAtDesc = 'actualAt_DESC',
  CostAsc = 'cost_ASC',
  CostDesc = 'cost_DESC',
  HasImageAsc = 'hasImage_ASC',
  HasImageDesc = 'hasImage_DESC'
}

export type ContractPartsStat = {
  __typename?: 'ContractPartsStat',
  id: Scalars['ID'],
  group: Nameable,
  viewAd: Scalars['Int'],
  viewShopCard: Scalars['Int'],
  viewPhone: Scalars['Int'],
  viewEmail: Scalars['Int'],
  viewMessenger: Scalars['Int'],
  viewMap: Scalars['Int'],
  viewWebsite: Scalars['Int'],
  clickChat: Scalars['Int'],
};

export type ContractPartsStatConnection = {
  __typename?: 'ContractPartsStatConnection',
  pageInfo: PageInfo,
  edges: Array<ContractPartsStatEdge>,
};

export type ContractPartsStatEdge = {
  __typename?: 'ContractPartsStatEdge',
  cursor: Scalars['String'],
  node: ContractPartsStat,
};

export type ContractPartsStatFiltersConnection = {
  __typename?: 'ContractPartsStatFiltersConnection',
  pageInfo: PageInfo,
  edges: Array<ContractPartsStatFiltersEdge>,
};

export type ContractPartsStatFiltersEdge = {
  __typename?: 'ContractPartsStatFiltersEdge',
  cursor: Scalars['String'],
  node: Nameable,
};

export enum ContractPartsStatisticsFilters {
  City = 'CITY',
  Shop = 'SHOP',
  User = 'USER',
  CarManufacturer = 'CAR_MANUFACTURER',
  CarModel = 'CAR_MODEL',
  CarYear = 'CAR_YEAR',
  CarChassis = 'CAR_CHASSIS',
  CarEngine = 'CAR_ENGINE',
  Title = 'TITLE'
}

export enum ContractPartsStatisticsGroup {
  None = 'NONE',
  DateDay = 'DATE_DAY',
  DateWeek = 'DATE_WEEK',
  DateMonth = 'DATE_MONTH',
  DateYear = 'DATE_YEAR',
  Shop = 'SHOP',
  CarManufacturer = 'CAR_MANUFACTURER',
  CarModel = 'CAR_MODEL',
  Item = 'ITEM',
  Engine = 'ENGINE',
  Chassis = 'CHASSIS',
  Year = 'YEAR',
  User = 'USER',
  City = 'CITY'
}

export enum ContractPartsStatisticsSort {
  GroupAsc = 'group_ASC',
  GroupDesc = 'group_DESC',
  ViewAdAsc = 'viewAd_ASC',
  ViewAdDesc = 'viewAd_DESC',
  ViewShopCardAsc = 'viewShopCard_ASC',
  ViewShopCardDesc = 'viewShopCard_DESC',
  SendOrderAsc = 'sendOrder_ASC',
  SendOrderDesc = 'sendOrder_DESC',
  ViewPhoneAsc = 'viewPhone_ASC',
  ViewPhoneDesc = 'viewPhone_DESC',
  ViewEmailAsc = 'viewEmail_ASC',
  ViewEmailDesc = 'viewEmail_DESC',
  ViewMessengerAsc = 'viewMessenger_ASC',
  ViewMessengerDesc = 'viewMessenger_DESC',
  ViewMapAsc = 'viewMap_ASC',
  ViewMapDesc = 'viewMap_DESC',
  ViewWebsiteAsc = 'viewWebsite_ASC',
  ViewWebsiteDesc = 'viewWebsite_DESC',
  ClickChatAsc = 'clickChat_ASC',
  ClickChatDesc = 'clickChat_DESC'
}

export type ContractPartStatisticsData = {
  __typename?: 'ContractPartStatisticsData',
  adId: Scalars['ID'],
  adName: Scalars['String'],
  userId: Scalars['ID'],
  shopId: Scalars['ID'],
  geoId: Scalars['ID'],
  brandId: Scalars['ID'],
  brandName: Scalars['String'],
  modelId: Scalars['ID'],
  modelName: Scalars['String'],
  chassis: Array<Scalars['String']>,
  engines: Array<Scalars['String']>,
  years: Array<Scalars['Int']>,
};


export enum DayOfWeek {
  Unknown = 'UNKNOWN',
  Monday = 'MONDAY',
  Tuesday = 'TUESDAY',
  Wednesday = 'WEDNESDAY',
  Thursday = 'THURSDAY',
  Friday = 'FRIDAY',
  Saturday = 'SATURDAY',
  Sunday = 'SUNDAY'
}

export type Entry = Nameable & {
  __typename?: 'Entry',
  id: Scalars['ID'],
  name: Scalars['String'],
};

export type Fullname = {
  __typename?: 'Fullname',
  first?: Maybe<Scalars['String']>,
  middle?: Maybe<Scalars['String']>,
  last?: Maybe<Scalars['String']>,
};

export type Geo = Nameable & {
  __typename?: 'Geo',
  id: Scalars['ID'],
  parentId?: Maybe<Scalars['String']>,
  name: Scalars['String'],
  slug: Scalars['String'],
  prepositionalName: Scalars['String'],
  level: GeoLevel,
  timezoneName?: Maybe<Scalars['String']>,
  timezoneOffset?: Maybe<Scalars['Int']>,
  position?: Maybe<Point>,
};

export type GeoGetConnection = {
  __typename?: 'GeoGetConnection',
  pageInfo: PageInfo,
  edges: Array<GeoGetEdge>,
};

export type GeoGetEdge = {
  __typename?: 'GeoGetEdge',
  cursor: Scalars['String'],
  node: Geo,
};

export enum GeoLevel {
  All = 'ALL',
  Country = 'COUNTRY',
  Area = 'AREA',
  City = 'CITY',
  Metro = 'METRO'
}

export type Image = {
  __typename?: 'Image',
  id: Scalars['ID'],
  isCover: Scalars['Boolean'],
  small?: Maybe<ImageVariant>,
  medium?: Maybe<ImageVariant>,
  big?: Maybe<ImageVariant>,
  extra?: Maybe<ImageVariant>,
};

export type ImageVariant = {
  __typename?: 'ImageVariant',
  url: Scalars['String'],
};

export type LegacySchedule = {
  __typename?: 'LegacySchedule',
  current_date_time_with_tz?: Maybe<Scalars['Datetime']>,
  schedule_day_isset: Scalars['Boolean'],
  around_the_clock: Scalars['Boolean'],
  opening_date_time?: Maybe<Scalars['Datetime']>,
  close_date_time?: Maybe<Scalars['Datetime']>,
  dinner_begin_date_time?: Maybe<Scalars['Datetime']>,
  dinner_end_date_time?: Maybe<Scalars['Datetime']>,
};

export enum MerchantType {
  Any = 'ANY',
  Person = 'PERSON',
  Shop = 'SHOP'
}

export enum MinOrderUnit {
  Dontuse = 'DONTUSE',
  Rub = 'RUB',
  Unit = 'UNIT'
}

export type Mutation = {
  __typename?: 'Mutation',
  Auth_loginByEmail: AuthData,
  Auth_exchangeToken: AuthData,
  Auth_revokeToken: Scalars['Boolean'],
  PricelistTemplate_addPartTemplate: PartPricelistTemplate,
};


export type MutationAuth_LoginByEmailArgs = {
  email: Scalars['String'],
  password: Scalars['String']
};


export type MutationAuth_ExchangeTokenArgs = {
  token: Scalars['String']
};


export type MutationAuth_RevokeTokenArgs = {
  token: Scalars['String']
};


export type MutationPricelistTemplate_AddPartTemplateArgs = {
  fields: PartPricelistTemplateInput
};

export type Nameable = {
  id: Scalars['ID'],
  name: Scalars['String'],
};

export type NewPart = {
  __typename?: 'NewPart',
  id: Scalars['ID'],
};

export type NewPartsList = {
  __typename?: 'NewPartsList',
  items: Array<NewPart>,
  total: Scalars['Int'],
};

export type PageInfo = {
  __typename?: 'PageInfo',
  hasNextPage: Scalars['Boolean'],
  count: Scalars['Int'],
};

export type Part = Nameable & {
  __typename?: 'Part',
  id: Scalars['ID'],
  name: Scalars['String'],
  aliases: Array<Scalars['String']>,
  manufacturer: PartManufacturer,
  vendorCode: Scalars['String'],
};

export enum PartAdStatus {
  Any = 'ANY',
  Active = 'ACTIVE',
  Archived = 'ARCHIVED',
  Deleted = 'DELETED'
}

export enum PartCondition {
  Any = 'ANY',
  New = 'NEW',
  Used = 'USED'
}

export type PartGroup = {
  __typename?: 'PartGroup',
  id: Scalars['ID'],
  name: Scalars['String'],
  slug: Scalars['String'],
  subgroups?: Maybe<Array<PartSubgroup>>,
};

export type PartGroupTreeConnection = {
  __typename?: 'PartGroupTreeConnection',
  pageInfo: PageInfo,
  edges: Array<PartGroupTreeEdge>,
};

export type PartGroupTreeEdge = {
  __typename?: 'PartGroupTreeEdge',
  cursor: Scalars['String'],
  node: PartGroup,
};

export type PartManufacturer = Nameable & {
  __typename?: 'PartManufacturer',
  id: Scalars['ID'],
  name: Scalars['String'],
  slug: Scalars['String'],
};

export type PartManufacturersList = {
  __typename?: 'PartManufacturersList',
  items: Array<PartManufacturer>,
  total: Scalars['Int'],
};

export type PartName = {
  __typename?: 'PartName',
  id: Scalars['ID'],
  name: Scalars['String'],
  slug: Scalars['String'],
};

export type PartPricelistTemplate = PricelistTemplate & {
  __typename?: 'PartPricelistTemplate',
  ID: Scalars['ID'],
  templateName: Scalars['String'],
  company: Company,
  firstLine: Scalars['Int'],
  minOrder: Scalars['Int'],
  minOderUnit: MinOrderUnit,
  isWholesaleOnly: Scalars['Boolean'],
  isAvailable: Scalars['Boolean'],
  pricelistType: PricelistType,
  partManufacturer: PricelistTemplateRequiredField,
  partName: PricelistTemplateRequiredField,
  vendorCode: PricelistTemplateRequiredField,
  retailPrice: PricelistTemplateRequiredField,
  wholesalePrice: PricelistTemplateOptionalField,
  quantity: PricelistTemplateOptionalField,
  deliveryTime: PricelistTemplateOptionalField,
  commentRetailPrice: PricelistTemplateOptionalField,
  commentWholesalePrice: PricelistTemplateOptionalField,
  deliveryRetailCondition: PricelistTemplateOptionalField,
  deliveryWholesaleCondition: PricelistTemplateOptionalField,
  commentName: PricelistTemplateOptionalField,
};

export type PartPricelistTemplateInput = {
  common: PricelistTemplateCommonInput,
  partManufacturer: PricelistTemplateFieldRequiredInput,
  partName: PricelistTemplateFieldRequiredInput,
  vendorCode: PricelistTemplateFieldRequiredInput,
  retailPrice: PricelistTemplateFieldRequiredInput,
  wholesalePrice: PricelistTemplateFieldOptionalInput,
  quantity: PricelistTemplateFieldOptionalInput,
  deliveryTime: PricelistTemplateFieldOptionalInput,
  commentRetailPrice: PricelistTemplateFieldOptionalInput,
  commentWholesalePrice: PricelistTemplateFieldOptionalInput,
  deliveryRetailCondition: PricelistTemplateFieldOptionalInput,
  deliveryWholesaleCondition: PricelistTemplateFieldOptionalInput,
  commentName: PricelistTemplateFieldOptionalInput,
};

export type PartsList = {
  __typename?: 'PartsList',
  items: Array<Part>,
  total: Scalars['Int'],
};

export type PartSubgroup = {
  __typename?: 'PartSubgroup',
  id: Scalars['ID'],
  name: Scalars['String'],
  slug: Scalars['String'],
  partnames?: Maybe<Array<PartName>>,
};

export type Period = {
  __typename?: 'Period',
  begin: Scalars['Time'],
  end: Scalars['Time'],
};

export enum Placement {
  Any = 'ANY',
  Front = 'FRONT',
  Rear = 'REAR',
  Left = 'LEFT',
  Right = 'RIGHT',
  Top = 'TOP',
  Bottom = 'BOTTOM'
}

export type Point = {
  __typename?: 'Point',
  latitude: Scalars['Float'],
  longitude: Scalars['Float'],
};

export type PricelistTemplate = {
  ID: Scalars['ID'],
  templateName: Scalars['String'],
  company: Company,
  firstLine: Scalars['Int'],
  minOrder: Scalars['Int'],
  minOderUnit: MinOrderUnit,
  isWholesaleOnly: Scalars['Boolean'],
  isAvailable: Scalars['Boolean'],
  pricelistType: PricelistType,
};

export type PricelistTemplateCommonInput = {
  templateName: Scalars['String'],
  companyId: Scalars['ID'],
  firstLine: Scalars['Int'],
  minOrder: Scalars['Int'],
  minOderUnit: MinOrderUnit,
  isWholesaleOnly: Scalars['Boolean'],
  isAvailable: Scalars['Boolean'],
};

export type PricelistTemplateConnection = {
  __typename?: 'PricelistTemplateConnection',
  pageInfo: PageInfo,
  edges: PricelistTemplateEdge,
};

export type PricelistTemplateEdge = {
  __typename?: 'PricelistTemplateEdge',
  cursor: Scalars['String'],
  node: PricelistTemplate,
};

export type PricelistTemplateFieldOptionalInput = {
  columnNumber?: Maybe<Scalars['Int']>,
  customValue?: Maybe<Scalars['String']>,
};

export type PricelistTemplateFieldRequiredInput = {
  columnNumber: Scalars['Int'],
  customValue?: Maybe<Scalars['String']>,
};

export type PricelistTemplateOptionalField = {
  __typename?: 'PricelistTemplateOptionalField',
  columnNumber?: Maybe<Scalars['Int']>,
  customValue?: Maybe<Scalars['String']>,
};

export type PricelistTemplateRequiredField = {
  __typename?: 'PricelistTemplateRequiredField',
  columnNumber: Scalars['Int'],
  customValue?: Maybe<Scalars['String']>,
};

export enum PricelistType {
  Any = 'ANY',
  Part = 'PART',
  Tyre = 'TYRE',
  Wheel = 'WHEEL',
  Contract = 'CONTRACT'
}

export type Profile = {
  __typename?: 'Profile',
  fullname: Fullname,
};

export type Query = {
  __typename?: 'Query',
  self: User,
  NewParts_list: NewPartsList,
  ContractParts_list: ContractPartsConnection,
  Parts_list: PartsList,
  PartManufacturers_list: PartManufacturersList,
  PartGroup_getPopular: PartGroupTreeConnection,
  PartGroup_getTree: PartGroupTreeConnection,
  CarModels_list: CarModelsConnection,
  CarManufacturers_list: CarManufacturersConnection,
  Statistics_contractParts: ContractPartsStatConnection,
  Statistics_contractPartsFilters: ContractPartsStatFiltersConnection,
  Statistics_searchByNumber: SearchByNumberStatConnection,
  Statistics_searchByNumberFilters: SearchByNumberStatFiltersConnection,
  SearchByNumber_getLastRequested: SearchByNumberGetLastRequestedConnection,
  Geo_get: GeoGetConnection,
  Geo_resolve?: Maybe<Geo>,
};


export type QueryContractParts_ListArgs = {
  itemStatus?: Maybe<PartAdStatus>,
  ids?: Maybe<Array<Scalars['ID']>>,
  excludeIds?: Maybe<Array<Scalars['ID']>>,
  partCondition?: Maybe<PartCondition>,
  merchantType?: Maybe<MerchantType>,
  shopId?: Maybe<Scalars['ID']>,
  cityId?: Maybe<Scalars['ID']>,
  metroId?: Maybe<Scalars['ID']>,
  carManufacturerId?: Maybe<Scalars['ID']>,
  carModelId?: Maybe<Scalars['ID']>,
  priceRange?: Maybe<Range>,
  carYears?: Maybe<Array<Scalars['Int']>>,
  carChassis?: Maybe<Scalars['String']>,
  carEngine?: Maybe<Scalars['String']>,
  text?: Maybe<Scalars['String']>,
  isSet?: Maybe<Scalars['Boolean']>,
  inStock?: Maybe<Scalars['Boolean']>,
  hasImage?: Maybe<Scalars['Boolean']>,
  partPlacement?: Maybe<Array<Placement>>,
  skip?: Maybe<Scalars['Int']>,
  first?: Maybe<Scalars['Int']>,
  sort?: Maybe<Array<ContractPartsSort>>
};


export type QueryParts_ListArgs = {
  ids?: Maybe<Array<Scalars['ID']>>
};


export type QueryPartGroup_GetTreeArgs = {
  groupIds?: Maybe<Array<Scalars['ID']>>,
  subgroupIds?: Maybe<Array<Scalars['ID']>>,
  partNameIds?: Maybe<Array<Scalars['ID']>>
};


export type QueryCarModels_ListArgs = {
  skip?: Maybe<Scalars['Int']>,
  first?: Maybe<Scalars['Int']>,
  ids?: Maybe<Array<Scalars['ID']>>,
  manufacturerIds?: Maybe<Array<Scalars['ID']>>
};


export type QueryCarManufacturers_ListArgs = {
  skip?: Maybe<Scalars['Int']>,
  first?: Maybe<Scalars['Int']>,
  ids?: Maybe<Array<Scalars['ID']>>
};


export type QueryStatistics_ContractPartsArgs = {
  userIds?: Maybe<Array<Scalars['ID']>>,
  citiesIds?: Maybe<Array<Scalars['ID']>>,
  shopIds?: Maybe<Array<Scalars['ID']>>,
  manufacturerIds?: Maybe<Array<Scalars['ID']>>,
  modelIds?: Maybe<Array<Scalars['ID']>>,
  itemIds?: Maybe<Array<Scalars['ID']>>,
  chassis?: Maybe<Array<Scalars['String']>>,
  engines?: Maybe<Array<Scalars['String']>>,
  years?: Maybe<Array<Scalars['Int']>>,
  skip?: Maybe<Scalars['Int']>,
  first?: Maybe<Scalars['Int']>,
  group: ContractPartsStatisticsGroup,
  sort?: Maybe<ContractPartsStatisticsSort>,
  fromDate?: Maybe<Scalars['Datetime']>,
  toDate?: Maybe<Scalars['Datetime']>
};


export type QueryStatistics_ContractPartsFiltersArgs = {
  userIds?: Maybe<Array<Scalars['ID']>>,
  field: ContractPartsStatisticsFilters,
  fromDate?: Maybe<Scalars['Datetime']>,
  toDate?: Maybe<Scalars['Datetime']>
};


export type QueryStatistics_SearchByNumberArgs = {
  userIds?: Maybe<Array<Scalars['ID']>>,
  citiesIds?: Maybe<Array<Scalars['ID']>>,
  shopIds?: Maybe<Array<Scalars['ID']>>,
  brandIds?: Maybe<Array<Scalars['ID']>>,
  partIds?: Maybe<Array<Scalars['ID']>>,
  skip?: Maybe<Scalars['Int']>,
  first?: Maybe<Scalars['Int']>,
  group: SearchByNumberStatisticsGroup,
  sort?: Maybe<SearchByNumberStatisticsSort>,
  fromDate?: Maybe<Scalars['Datetime']>,
  toDate?: Maybe<Scalars['Datetime']>
};


export type QueryStatistics_SearchByNumberFiltersArgs = {
  userIds?: Maybe<Array<Scalars['ID']>>,
  field: SearchByNumberStatisticsFilters,
  fromDate?: Maybe<Scalars['Datetime']>,
  toDate?: Maybe<Scalars['Datetime']>
};


export type QuerySearchByNumber_GetLastRequestedArgs = {
  geoIds?: Maybe<Array<Scalars['ID']>>,
  isOrganization: Scalars['Boolean']
};


export type QueryGeo_GetArgs = {
  ids?: Maybe<Array<Scalars['ID']>>,
  slugs?: Maybe<Array<Scalars['String']>>,
  names?: Maybe<Array<Scalars['String']>>,
  parentId?: Maybe<Scalars['ID']>,
  level?: Maybe<GeoLevel>
};


export type QueryGeo_ResolveArgs = {
  ip: Scalars['String']
};

export type Range = {
  min?: Maybe<Scalars['Int']>,
  max?: Maybe<Scalars['Int']>,
};

export type Rating = {
  __typename?: 'Rating',
  votes: Scalars['Int'],
  rate: Scalars['Float'],
};

export type Schedule = {
  __typename?: 'Schedule',
  workdays?: Maybe<Array<Workday>>,
  isConfirmed: Scalars['Boolean'],
  legacy: LegacySchedule,
};

export type SearchByNumberGetLastRequestedConnection = {
  __typename?: 'SearchByNumberGetLastRequestedConnection',
  pageInfo: PageInfo,
  edges: Array<SearchByNumberGetLastRequestedEdge>,
};

export type SearchByNumberGetLastRequestedEdge = {
  __typename?: 'SearchByNumberGetLastRequestedEdge',
  cursor: Scalars['String'],
  node: SearchByNumberLastRequested,
};

export type SearchByNumberLastRequested = {
  __typename?: 'SearchByNumberLastRequested',
  partId: Scalars['ID'],
  createdAt: Scalars['String'],
  vendorCode: Scalars['String'],
  brand: Scalars['String'],
  partName: Scalars['String'],
  priceMin: Scalars['Int'],
  priceMax: Scalars['Int'],
  userLocationCount: Scalars['Int'],
  allOffersCount: Scalars['Int'],
};

export type SearchByNumberStat = {
  __typename?: 'SearchByNumberStat',
  id: Scalars['ID'],
  group: Nameable,
  viewShopCard: Scalars['Int'],
  sendOrderCart: Scalars['Int'],
  sendOrderQuick: Scalars['Int'],
  sendOrder: Scalars['Int'],
  viewPhone: Scalars['Int'],
  viewEmail: Scalars['Int'],
  viewMessenger: Scalars['Int'],
  viewMap: Scalars['Int'],
  viewWebsite: Scalars['Int'],
  clickOrderQuick: Scalars['Int'],
  clickAddToCart: Scalars['Int'],
  clickOrder: Scalars['Int'],
  clickExternalURLName: Scalars['Int'],
  clickExternalURLOrder: Scalars['Int'],
  clickChat: Scalars['Int'],
};

export type SearchByNumberStatConnection = {
  __typename?: 'SearchByNumberStatConnection',
  pageInfo: PageInfo,
  edges: Array<SearchByNumberStatEdge>,
};

export type SearchByNumberStatEdge = {
  __typename?: 'SearchByNumberStatEdge',
  cursor: Scalars['String'],
  node: SearchByNumberStat,
};

export type SearchByNumberStatFiltersConnection = {
  __typename?: 'SearchByNumberStatFiltersConnection',
  pageInfo: PageInfo,
  edges: Array<SearchByNumberStatFiltersEdge>,
};

export type SearchByNumberStatFiltersEdge = {
  __typename?: 'SearchByNumberStatFiltersEdge',
  cursor: Scalars['String'],
  node: Nameable,
};

export enum SearchByNumberStatisticsFilters {
  City = 'CITY',
  Shop = 'SHOP',
  User = 'USER',
  PartManufacturer = 'PART_MANUFACTURER',
  Part = 'PART',
  Item = 'ITEM'
}

export enum SearchByNumberStatisticsGroup {
  None = 'NONE',
  DateDay = 'DATE_DAY',
  DateWeek = 'DATE_WEEK',
  DateMonth = 'DATE_MONTH',
  DateYear = 'DATE_YEAR',
  Shop = 'SHOP',
  Brand = 'BRAND',
  Part = 'PART',
  User = 'USER',
  City = 'CITY'
}

export enum SearchByNumberStatisticsSort {
  GroupAsc = 'group_ASC',
  GroupDesc = 'group_DESC',
  ViewShopCardAsc = 'viewShopCard_ASC',
  ViewShopCardDesc = 'viewShopCard_DESC',
  SendOrderCartAsc = 'sendOrderCart_ASC',
  SendOrderCartDesc = 'sendOrderCart_DESC',
  SendOrderQuickAsc = 'sendOrderQuick_ASC',
  SendOrderQuickDesc = 'sendOrderQuick_DESC',
  SendOrderAsc = 'sendOrder_ASC',
  SendOrderDesc = 'sendOrder_DESC',
  ViewPhoneAsc = 'viewPhone_ASC',
  ViewPhoneDesc = 'viewPhone_DESC',
  ViewEmailAsc = 'viewEmail_ASC',
  ViewEmailDesc = 'viewEmail_DESC',
  ViewMessengerAsc = 'viewMessenger_ASC',
  ViewMessengerDesc = 'viewMessenger_DESC',
  ViewMapAsc = 'viewMap_ASC',
  ViewMapDesc = 'viewMap_DESC',
  ViewWebsiteAsc = 'viewWebsite_ASC',
  ViewWebsiteDesc = 'viewWebsite_DESC',
  ClickOrderQuickAsc = 'clickOrderQuick_ASC',
  ClickOrderQuickDesc = 'clickOrderQuick_DESC',
  ClickAddToCartAsc = 'clickAddToCart_ASC',
  ClickAddToCartDesc = 'clickAddToCart_DESC',
  ClickOrderAsc = 'clickOrder_ASC',
  ClickOrderDesc = 'clickOrder_DESC',
  ClickExternalUrlNameAsc = 'clickExternalURLName_ASC',
  ClickExternalUrlNameDesc = 'clickExternalURLName_DESC',
  ClickExternalUrlOrderAsc = 'clickExternalURLOrder_ASC',
  ClickExternalUrlOrderDesc = 'clickExternalURLOrder_DESC',
  ClickChatAsc = 'clickChat_ASC',
  ClickChatDesc = 'clickChat_DESC'
}

export type Shop = Nameable & Contactable & {
  __typename?: 'Shop',
  id: Scalars['ID'],
  name: Scalars['String'],
  company: Company,
  isNetshop: Scalars['Boolean'],
  address?: Maybe<Address>,
  websites?: Maybe<Array<Contact>>,
  emails?: Maybe<Array<Contact>>,
  phones?: Maybe<Array<Contact>>,
  messengers?: Maybe<Array<Contact>>,
  schedule?: Maybe<Schedule>,
  rating: Rating,
};


export type ShopScheduleArgs = {
  cityId?: Maybe<Scalars['ID']>
};

export enum SortOrder {
  Asc = 'ASC',
  Desc = 'DESC'
}


export type TokensBearer = {
  __typename?: 'TokensBearer',
  refresh: Scalars['String'],
  access: Scalars['String'],
};

export type User = Nameable & Contactable & {
  __typename?: 'User',
  id: Scalars['ID'],
  name: Scalars['String'],
  profile: Profile,
  websites?: Maybe<Array<Contact>>,
  emails?: Maybe<Array<Contact>>,
  phones?: Maybe<Array<Contact>>,
  messengers?: Maybe<Array<Contact>>,
};

export type Workday = {
  __typename?: 'Workday',
  dayOfWeek: DayOfWeek,
  hours?: Maybe<Period>,
  breaks?: Maybe<Array<Period>>,
};

export type YearsRange = {
  __typename?: 'YearsRange',
  min: Scalars['Int'],
  max: Scalars['Int'],
};
