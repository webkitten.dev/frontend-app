export class CountableResult<T> {
  public data: T[];
  public total: number;

  constructor(input: { data: T[], total: number }) {
    this.data = input.data;
    this.total = input.total;
  }
}
