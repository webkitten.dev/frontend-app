import {
  status as grpcStatus,
  Client,
  InterceptingCall,
  Listener,
  MessageRequester,
  Metadata,
  Requester,
  StatusObject,
} from 'grpc';
import { Message as JsPbMessage } from 'google-protobuf';

import {
  LoggerContainer,
  LoggerCategories,
} from '../../../common-bundle/logger';
const logger = LoggerContainer.get(LoggerCategories.GRPC);

import { sleep } from '../../../common-bundle/utils';

export abstract class AbstractRepository<T extends Client> {
  protected client: T;
  protected retryMax = 1;
  protected retryTimeout = 500; // ms

  constructor() {
    this.client = this.getClient();
  }

  protected invoke<REQ extends JsPbMessage, RES extends JsPbMessage>(
    method: Function,
  ): (request: REQ) => Promise<RES> {
    return request => new Promise((resolve, reject) => {
      method.call(this.client, request, (err: any, result: RES) => {
        if (err) {
          reject(err);
        }
        resolve(result);
      });
    });
  }

  protected getInterceptors(): Function[] {
    return [
      this.retryInterceptor.bind(this),
      this.debugInterceptor.bind(this),
    ];
  }

  protected retryInterceptor(options: { [key: string]: any }, nextCall: Function): InterceptingCall {
    const [
      namespace,
      methodName,
    ] = options.method_definition.path.split('/').filter(Boolean);

    let savedMetadata: Metadata;
    let savedSendMessage: MessageRequester;
    let savedReceiveMessage: any;
    let savedMessageNext: Function;
    const requester: Requester = {
      start: (metadata: Metadata, listener: Listener, next: Function) => {
        savedMetadata = metadata;
        const newListener = {
          onReceiveMessage: (message: any, next: Function) => {
            savedReceiveMessage = message;
            savedMessageNext = next;
            next(message);
          },
          onReceiveStatus: (status: StatusObject, next: Function) => {
            let retries = 0;
            const retry = async (message: any, metadata: Metadata) => {
              retries += 1;
              await sleep(this.retryTimeout);
              const newCall = nextCall(options);
              newCall.start(metadata, {
                onReceiveMessage: (message: any) => {
                  savedReceiveMessage = message;
                },
                onReceiveStatus: (status: StatusObject) => {
                  if (status.code !== grpcStatus.OK) {
                    if (retries < this.retryMax) {
                      retry(message, metadata);
                    } else {
                      logger.error(`${namespace} ${methodName} error %j in %j`, status, message.toObject());
                      savedMessageNext(savedReceiveMessage);
                      next(status);
                    }
                  } else {
                    savedMessageNext(savedReceiveMessage);
                    next({ code: grpcStatus.OK });
                  }
                },
              });
              newCall.sendMessage(message);
              newCall.halfClose();
            };
            if (status.code !== grpcStatus.OK) {
              retry(savedSendMessage, savedMetadata);
            } else {
              savedMessageNext(savedReceiveMessage);
              next(status);
            }
          },
        };
        next(metadata, newListener);
      },
      sendMessage: (message: any, next: Function) => {
        savedSendMessage = message;
        next(message);
      },
    };
    return new InterceptingCall(nextCall(options), requester);
  }

  protected debugInterceptor(options: { [key: string]: any }, nextCall: Function): InterceptingCall {
    const [
      namespace,
      methodName,
    ] = options.method_definition.path.split('/').filter(Boolean);
    const timestamp = Number(new Date());

    const requester: Requester = {
      start: (metadata: Metadata, listener: Listener, next: Function) => {
        const newListener = {
          onReceiveMessage: (message: any, next: Function) => {
            if (this.checkMessageToObject(message)) {
              logger.verbose(`${namespace} ${methodName} ${timestamp} response %j`, message.toObject());
            }
            next(message);
          },
        };
        next(metadata, newListener);
      },
      sendMessage: (message: any, next: Function) => {
        if (this.checkMessageToObject(message)) {
          logger.verbose(`${namespace} ${methodName} ${timestamp} request %j`, message.toObject());
        }
        next(message);
      },
    };

    return new InterceptingCall(nextCall(options), requester);
  }

  private checkMessageToObject(message: any): boolean {
    return message
      && typeof message === 'object'
      && typeof message.toObject === 'function';
  }

  protected abstract getClient(): T;
}
