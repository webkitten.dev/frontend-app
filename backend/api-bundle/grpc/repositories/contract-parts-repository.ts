import { credentials } from 'grpc';
import {
  get as _get,
  set as _set,
  has as _has,
  each as _each,
  isNil as _isNil,
  maxBy as _maxBy,
  sortBy as _sortBy,
} from 'lodash';

import * as utils from '../../../common-bundle/utils';

import { ContractPartsClient } from '@gisauto/contract-parts-service-proto/service_grpc_pb';
import {
  Range,
  Ordering,
  Pagination,
  FindPartCriteria,
  FindMerchantsCriteria,
  Part,
  PartCondition,
  MerchantType,
  ImagesPresence,
  ItemStatus,
  Placement,
} from '@gisauto/contract-parts-service-proto/types_pb';

import {
  FindPartsByIdsRequest,
  FindPartsByIdsResponse,
  FindPartsByCriteriaRequest,
  FindPartsByCriteriaResponse,
  GetPartsFacetFilterRequest,
  GetPartsFacetFilterResponse,
  CountPartsByCriteriaRequest,
  CountPartsByCriteriaResponse,
  CountMerchantsByCriteriaRequest,
  CountMerchantsByCriteriaResponse,
  GetPartsSearchSuggestionsRequest,
  GetPartsSearchSuggestionsResponse,
} from '@gisauto/contract-parts-service-proto/messages_pb';

import { CountableResult } from '../entities/countable-result';
import { AbstractRepository } from './abstract-repository';

export class ContractPartsRepository extends AbstractRepository<ContractPartsClient> {
  public async findPartsByIds(ids: number[]): Promise<Part.AsObject[]> {
    const criteria = new FindPartsByIdsRequest.Criteria();
    criteria.setIdsList(ids && ids.length ? ids.filter(Boolean) : []);

    const request = new FindPartsByIdsRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    FindPartsByIdsRequest,
    FindPartsByIdsResponse>(ContractPartsClient.prototype.findPartsByIds)(request);
    return response!.toObject().dataList;
  }

  public async findPartsByCriteria(
    inputCriteria: { [key: string]: any },
    inputOrdering: FindPartsByCriteriaRequest.Sort | null,
    limit?: number,
    offset?: number,
  ): Promise<CountableResult<Part.AsObject>> {
    const criteria = this.buildCriteriaFromReq(inputCriteria);
    criteria.setStatus(ItemStatus.STATUS_ACTIVE); // в списке не нужно выводить архивные

    let ordering;
    if (inputOrdering === null) {
      ordering = new FindPartsByCriteriaRequest.Sort();
      ordering.setHasImage(Ordering.DESC);
      ordering.setActualAt(Ordering.DESC);
    } else {
      ordering = inputOrdering;
    }

    const pagination = new Pagination();
    pagination.setPerPage(limit ? Number(limit) : 1);
    offset && pagination.setOffset(Number(offset));

    const request = new FindPartsByCriteriaRequest();
    request.setCriteria(criteria);
    ordering && request.setOrdering(ordering);
    request.setPagination(pagination);

    const response = await this.invoke<
    FindPartsByCriteriaRequest,
    FindPartsByCriteriaResponse>(ContractPartsClient.prototype.findPartsByCriteria)(request);
    return new CountableResult<Part.AsObject>({
      data: response!.toObject().dataList,
      total: response!.toObject().total,
    });
  }

  public async getPartsFacetFilter(
    inputCriteria: { [key: string]: any },
  ): Promise<GetPartsFacetFilterResponse.AsObject> {
    const criteria = this.buildCriteriaFromReq(inputCriteria);
    criteria.setStatus(ItemStatus.STATUS_ACTIVE); // в фильтрах не нужно выводить архивные

    const request = new GetPartsFacetFilterRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    GetPartsFacetFilterRequest,
    GetPartsFacetFilterResponse>(ContractPartsClient.prototype.getPartsFacetFilter)(request);
    return response!.toObject();
  }

  public async countParts(): Promise<number> {
    const criteria = new FindPartCriteria();
    criteria.setStatus(ItemStatus.STATUS_ACTIVE);

    const request = new CountPartsByCriteriaRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    CountPartsByCriteriaRequest,
    CountPartsByCriteriaResponse>(ContractPartsClient.prototype.countPartsByCriteria)(request);
    return response!.toObject().total;
  }

  public async countMerchants(): Promise<number> {
    const criteria = new FindMerchantsCriteria();
    criteria.setStatus(ItemStatus.STATUS_ACTIVE);

    const request = new CountMerchantsByCriteriaRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    CountMerchantsByCriteriaRequest,
    CountMerchantsByCriteriaResponse>(ContractPartsClient.prototype.countMerchantsByCriteria)(request);
    return response!.toObject().total;
  }

  public async getPartsSearchSuggestions(query: string): Promise<string[]> {
    const criteria = new GetPartsSearchSuggestionsRequest.Criteria();
    criteria.setText(query);

    const request = new GetPartsSearchSuggestionsRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    GetPartsSearchSuggestionsRequest,
    GetPartsSearchSuggestionsResponse>(ContractPartsClient.prototype.getPartsSearchSuggestions)(request);
    return response!.toObject().suggestionsList;
  }

  protected buildCriteriaFromReq(rawCriteria: { [key: string]: any }): FindPartCriteria {
    const criteria = new FindPartCriteria();
    if (_isNil(rawCriteria)) {
      return criteria;
    }

    switch (Number(_get(rawCriteria, 'condition', 0))) {
      case PartCondition.CONDITION_NEW: criteria.setCondition(PartCondition.CONDITION_NEW); break;
      case PartCondition.CONDITION_USED: criteria.setCondition(PartCondition.CONDITION_USED); break;
      default: criteria.setCondition(PartCondition.CONDITION_ANY); break;
    }
    switch (Number(_get(rawCriteria, 'merchantType', 0))) {
      case MerchantType.MERCHANT_PERSON: criteria.setMerchantType(MerchantType.MERCHANT_PERSON); break;
      case MerchantType.MERCHANT_SHOP: criteria.setMerchantType(MerchantType.MERCHANT_SHOP); break;
      default: criteria.setMerchantType(MerchantType.MERCHANT_ANY); break;
    }
    switch (Number(_get(rawCriteria, 'imagesPresence', 0))) {
      case ImagesPresence.IMAGES_PRESENT: criteria.setImagesPresence(ImagesPresence.IMAGES_PRESENT); break;
      default: criteria.setImagesPresence(ImagesPresence.IMAGES_ANY); break;
    }

    const placements = _get(rawCriteria, 'placementList', [])
      .map(Number)
      .filter(Boolean)
      .filter((placement: number) => placement in Object.values(Placement));
    placements && criteria.setPlacementList(placements);

    const priceRange = new Range();
    rawCriteria.priceMin && priceRange.setStart(utils.toSafeInt(rawCriteria.priceMin));
    rawCriteria.priceMax && priceRange.setEnd(utils.toSafeInt(rawCriteria.priceMax));
    criteria.setPriceRange(priceRange);

    rawCriteria.carManufacturer && criteria.setCarManufacturerId(+rawCriteria.carManufacturer);
    rawCriteria.carModel && criteria.setCarModelId(+rawCriteria.carModel);
    rawCriteria.shop && criteria.setShopId(+rawCriteria.shop);
    rawCriteria.city && criteria.setGeoId(+rawCriteria.city);
    rawCriteria.metro && criteria.setMetroId(+rawCriteria.metro);
    rawCriteria.chassisList && criteria.setChassis(rawCriteria.chassisList.toString());
    rawCriteria.enginesList && criteria.setEngine(rawCriteria.enginesList.toString());
    rawCriteria.text && criteria.setText(rawCriteria.text.toString());
    if (rawCriteria.yearsList && Array.isArray(rawCriteria.yearsList)) {
      criteria.setYearsList(rawCriteria.yearsList);
    }
    if (rawCriteria.excludeIdsList && Array.isArray(rawCriteria.excludeIdsList)) {
      criteria.setExcludeIdsList(rawCriteria.excludeIdsList);
    }

    return criteria;
  }

  protected getClient(): ContractPartsClient {
    return new ContractPartsClient(
      new URL(process.env.SERVICE_CONTRACTPARTS_URI as string).host,
      credentials.createInsecure(),
      { interceptors: this.getInterceptors() },
    );
  }
}
