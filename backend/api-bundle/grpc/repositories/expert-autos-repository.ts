import { AutosClient } from '@gisauto/expert-service-proto/autos/service_grpc_pb';
import {
  Ordering,
} from '@gisauto/expert-service-proto/common/ordering_pb';
import {
  AutoManufacturer,
  AutoModel,
  ManyPartApplicabilities,
} from '@gisauto/expert-service-proto/autos/types_pb';
import {
  FindManufacturersByIdsRequest,
  FindManufacturersByIdsResponse,
  FindManufacturersByCriteriaRequest,
  FindManufacturersByCriteriaResponse,
  FindModelsByIdsRequest,
  FindModelsByIdsResponse,
  FindModelsByCriteriaRequest,
  FindModelsByCriteriaResponse,
  FindTypesApplicabilityRequest,
  FindTypesApplicabilityResponse,
} from '@gisauto/expert-service-proto/autos/messages_pb';

import { ExpertRepository } from './expert-repository';
import { Pagination } from '@gisauto/part-group-service-proto/types_pb';

export class ExpertAutosRepository extends ExpertRepository {
  public async findTypesApplicability(partIds: number[]): Promise<[number, ManyPartApplicabilities.AsObject][]> {
    const criteria = new FindTypesApplicabilityRequest.Criteria();
    criteria.setIdsList(partIds.filter(Boolean));

    const request = new FindTypesApplicabilityRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    FindTypesApplicabilityRequest,
    FindTypesApplicabilityResponse>(AutosClient.prototype.findTypesApplicability)(request);
    return response!.toObject().dataMap;
  }

  public async findManufacturersByIds(
    ids: number[],
    inputOrdering?: FindManufacturersByIdsRequest.Sort,
  ): Promise<AutoManufacturer.AsObject[]> {
    const criteria = new FindManufacturersByIdsRequest.Criteria();
    criteria.setIdsList(ids.filter(Boolean));

    let ordering = inputOrdering;
    if (!ordering) {
      ordering = new FindManufacturersByIdsRequest.Sort();
      ordering.setBrand(Ordering.ASC);
    }

    const request = new FindManufacturersByIdsRequest();
    request.setCriteria(criteria);
    request.setOrdering(ordering);

    const response = await this.invoke<
    FindManufacturersByIdsRequest,
    FindManufacturersByIdsResponse>(AutosClient.prototype.findManufacturersByIds)(request);
    return response!.toObject().dataList;
  }

  public async findManufacturersByCriteria(
    inputCriteria?: { [key: string]: any },
    inputOrdering?: FindManufacturersByCriteriaRequest.Sort,
  ): Promise<AutoManufacturer.AsObject[]> {
    const criteria = new FindManufacturersByCriteriaRequest.Criteria();
    if (inputCriteria) {
      if (inputCriteria.hasModels) {
        criteria.setMinModels(1);
      }

      if (inputCriteria.ids && inputCriteria.ids.length) {
        criteria.setIdsList(inputCriteria.ids.filter(Boolean));
      }
      // Hotfix 1.29.4
      criteria.setWithHidden(inputCriteria.hasHidden);
    }

    let ordering = inputOrdering;
    if (!ordering) {
      ordering = new FindManufacturersByCriteriaRequest.Sort();
      ordering.setBrand(Ordering.ASC);
    }

    // Hotfix 1.29.1
    const pagination = new Pagination();
    pagination.setPerPage(1000);

    const request = new FindManufacturersByCriteriaRequest();
    request.setCriteria(criteria);
    request.setOrdering(ordering);
    request.setPagination(pagination);

    const response = await this.invoke<
    FindManufacturersByCriteriaRequest,
    FindManufacturersByCriteriaResponse>(AutosClient.prototype.findManufacturersByCriteria)(request);
    return response!.toObject().dataList;
  }

  public async findModelsByIds(
    ids: number[],
    inputOrdering?: FindModelsByIdsRequest.Sort,
  ): Promise<AutoModel.AsObject[]> {
    const criteria = new FindModelsByIdsRequest.Criteria();
    criteria.setIdsList(ids.filter(Boolean));

    let ordering = inputOrdering;
    if (!ordering) {
      ordering = new FindModelsByIdsRequest.Sort();
      ordering.setName(Ordering.ASC);
    }

    const request = new FindModelsByIdsRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    FindModelsByIdsRequest,
    FindModelsByIdsResponse>(AutosClient.prototype.findModelsByIds)(request);
    return response!.toObject().dataList;
  }

  public async findModelsByCriteria(
    inputCriteria?: { [key: string]: any },
    inputOrdering?: FindModelsByCriteriaRequest.Sort,
  ): Promise<AutoModel.AsObject[]> {
    const criteria = new FindModelsByCriteriaRequest.Criteria();
    if (inputCriteria && inputCriteria.manufacturerIds && inputCriteria.manufacturerIds.length) {
      criteria.setManufacturerIdsList(inputCriteria.manufacturerIds.filter(Boolean));
    }
    // Hotfix 1.29.4
    if (inputCriteria && inputCriteria.hasHidden) {
      criteria.setWithHidden(inputCriteria.hasHidden);
    }

    let ordering = inputOrdering;
    if (!ordering) {
      ordering = new FindModelsByCriteriaRequest.Sort();
      ordering.setName(Ordering.ASC);
    }
    // Hotfix 1.29.2
    const pagination = new Pagination();
    pagination.setPerPage(1000);

    const request = new FindModelsByCriteriaRequest();
    request.setCriteria(criteria);
    request.setOrdering(ordering);
    request.setPagination(pagination);

    const response = await this.invoke<
    FindModelsByCriteriaRequest,
    FindModelsByCriteriaResponse>(AutosClient.prototype.findModelsByCriteria)(request);
    return response!.toObject().dataList;
  }
}
