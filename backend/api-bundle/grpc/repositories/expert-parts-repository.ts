import { PartsClient } from '@gisauto/expert-service-proto/parts/service_grpc_pb';
import {
  PartManufacturer,
  PartGroup,
  PartName,
  Part,
} from '@gisauto/expert-service-proto/parts/types_pb';
import {
  FindManufacturerByIdsRequest,
  FindManufacturerByIdsResponse,
  FindManufacturerByCriteriaRequest,
  FindManufacturerByCriteriaResponse,
  FindPartGroupsByCriteriaRequest,
  FindPartGroupsByCriteriaResponse,
  FindPartNamesByCriteriaRequest,
  FindPartNamesByCriteriaResponse,
  FindPartByCriteriaRequest,
  FindPartByCriteriaResponse,
  FindManufacturerByPartNameIdsRequest,
  FindManufacturerByPartNameIdsResponse,
} from '@gisauto/expert-service-proto/parts/messages_pb';

import { ExpertRepository } from './expert-repository';

export class ExpertPartsRepository extends ExpertRepository {
  public async findManufacturerBySlugs(slugs: string[]): Promise<PartManufacturer.AsObject[]> {
    const criteria = new FindManufacturerByCriteriaRequest.Criteria();
    criteria.setSlugsList(slugs.filter(Boolean));

    const request = new FindManufacturerByCriteriaRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    FindManufacturerByCriteriaRequest,
    FindManufacturerByCriteriaResponse>(PartsClient.prototype.findManufacturerByCriteria)(request);
    return response!.toObject().dataList;
  }

  public async findManufacturerByIds(ids: number[]): Promise<PartManufacturer.AsObject[]> {
    const criteria = new FindManufacturerByIdsRequest.Criteria();
    criteria.setIdsList(ids.filter(Boolean));

    const request = new FindManufacturerByIdsRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    FindManufacturerByIdsRequest,
    FindManufacturerByIdsResponse>(PartsClient.prototype.findManufacturerByIds)(request);
    return response!.toObject().dataList;
  }

  public async findPartGroupsBySlugs(slugs?: string[]): Promise<PartGroup.AsObject[]> {
    const criteria = new FindPartGroupsByCriteriaRequest.Criteria();
    criteria.setSlugsList(slugs && slugs.length ? slugs.filter(Boolean) : []);

    const request = new FindPartGroupsByCriteriaRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    FindPartGroupsByCriteriaRequest,
    FindPartGroupsByCriteriaResponse>(PartsClient.prototype.findPartGroupsByCriteria)(request);
    return response!.toObject().dataList;
  }

  public async findPartNamesByGroupSlugs(slugs: string[]): Promise<PartName.AsObject[]> {
    const criteria = new FindPartNamesByCriteriaRequest.Criteria();
    criteria.setGroupSlugsList(slugs && slugs.length ? slugs.filter(Boolean) : []);

    const request = new FindPartNamesByCriteriaRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    FindPartNamesByCriteriaRequest,
    FindPartNamesByCriteriaResponse>(PartsClient.prototype.findPartNamesByCriteria)(request);
    return response!.toObject().dataList;
  }

  public async findPartNamesBySubGroupIds(ids: number[]): Promise<PartName.AsObject[]> {
    const criteria = new FindPartNamesByCriteriaRequest.Criteria();
    criteria.setGroupIdsList(ids);

    const request = new FindPartNamesByCriteriaRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
      FindPartNamesByCriteriaRequest,
      FindPartNamesByCriteriaResponse>(PartsClient.prototype.findPartNamesByCriteria)(request);
    return response!.toObject().dataList;
  }

  public async findPartNamesBySlugs(slugs: string[]): Promise<PartName.AsObject[]> {
    const criteria = new FindPartNamesByCriteriaRequest.Criteria();
    criteria.setSlugsList(slugs && slugs.length ? slugs.filter(Boolean) : []);

    const request = new FindPartNamesByCriteriaRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    FindPartNamesByCriteriaRequest,
    FindPartNamesByCriteriaResponse>(PartsClient.prototype.findPartNamesByCriteria)(request);
    return response!.toObject().dataList;
  }

  public async findPartNamesByIds(ids: number[]): Promise<PartName.AsObject[]> {
    const criteria = new FindPartNamesByCriteriaRequest.Criteria();
    criteria.setIdsList(ids && ids.length ? ids.filter(Boolean) : []);

    const request = new FindPartNamesByCriteriaRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    FindPartNamesByCriteriaRequest,
    FindPartNamesByCriteriaResponse>(PartsClient.prototype.findPartNamesByCriteria)(request);
    return response!.toObject().dataList;
  }

  public async findPartByIds(ids: number[]): Promise<Part.AsObject[]> {
    const criteria = new FindPartByCriteriaRequest.Criteria();
    criteria.setIdsList(ids.filter(Boolean));

    const request = new FindPartByCriteriaRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    FindPartByCriteriaRequest,
    FindPartByCriteriaResponse>(PartsClient.prototype.findPartByCriteria)(request);
    return response!.toObject().dataList;
  }

  public async findManufacturersByPartNameId(partNameId: number)
    : Promise<PartManufacturer.AsObject[]> {
    const criteria = new FindManufacturerByPartNameIdsRequest.Criteria();
    criteria.setIdsList([partNameId]);

    const request = new FindManufacturerByPartNameIdsRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    FindManufacturerByPartNameIdsRequest,
    FindManufacturerByPartNameIdsResponse>(PartsClient.prototype.findManufacturerByPartNameIds)(request);

    const rawResult = response!.toObject().dataMap.shift();
    if (!rawResult) {
      return [];
    }

    const [, { manufacturersList: result }] = rawResult;
    return result;
  }
}
