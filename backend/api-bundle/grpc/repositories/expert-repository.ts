import { credentials } from 'grpc';

import { PartsClient } from '@gisauto/expert-service-proto/parts/service_grpc_pb';
import { AbstractRepository } from './abstract-repository';

export abstract class ExpertRepository extends AbstractRepository<PartsClient> {
  protected getClient(): PartsClient {
    return new PartsClient(
      new URL(process.env.SERVICE_EXPERT_URI as string).host,
      credentials.createInsecure(),
      { interceptors: this.getInterceptors() },
    );
  }
}
