import { credentials } from 'grpc';

import { PartGroupsClient } from '@gisauto/part-group-service-proto/service_grpc_pb';
import {
  Part,
  PartManufacturer,
  Pagination,
  FindPartCriteria,
  Ordering,
} from '@gisauto/part-group-service-proto/types_pb';
import {
  FindPartsByCriteriaRequest,
  FindPartsByCriteriaResponse,
  GetPartsFilterRequest,
  GetPartsFilterResponse,
  FindPartManufacturersByPartNameIdRequest,
  FindPartManufacturersByPartNameIdResponse,
} from '@gisauto/part-group-service-proto/messages_pb';

import { AbstractRepository } from './abstract-repository';
import { CountableResult } from '../entities/countable-result';

export class PartGroupRepository extends AbstractRepository<PartGroupsClient> {
  public async findPartsByCriteria(
    inputCriteria: FindPartCriteria,
    limit: number,
    offset: number,
    order: {
      field: string | null,
      direction: string | null,
    },
  ): Promise<CountableResult<Part.AsObject> & { sourceCityId: number }> {
    const pagination = new Pagination();
    offset && pagination.setOffset(offset);
    pagination.setPerPage(limit || 1);

    let ordering = null;
    if (order.field && order.direction) {
      ordering = new FindPartsByCriteriaRequest.Sort();
      let orderDirection;
      switch (order.direction) {
        case 'asc': orderDirection = Ordering.ASC; break;
        case 'desc': orderDirection = Ordering.DESC; break;
        default: orderDirection = Ordering.NONE; break;
      }
      switch (order.field) {
        case 'name': ordering.setBrand(orderDirection); break;
        case 'count': ordering.setCount(orderDirection); break;
      }
    }

    const request = new FindPartsByCriteriaRequest();
    request.setCriteria(inputCriteria);
    request.setPagination(pagination);
    ordering && request.setOrdering(ordering);

    const response = await this.invoke<
    FindPartsByCriteriaRequest,
    FindPartsByCriteriaResponse>(PartGroupsClient.prototype.findPartsByCriteria)(request);

    const rawResult = response!.toObject();
    return {
      ...new CountableResult<Part.AsObject>({
        data: rawResult.dataList,
        total: rawResult.total,
      }),
      sourceCityId: rawResult.sourceCityId,
    };
  }

  public async findFilterByCriteria(
    inputCriteria: FindPartCriteria,
  ): Promise<GetPartsFilterResponse.AsObject> {
    const request = new GetPartsFilterRequest();
    request.setCriteria(inputCriteria);

    const response = await this.invoke<
    GetPartsFilterRequest,
    GetPartsFilterResponse>(PartGroupsClient.prototype.findFilterByCriteria)(request);
    return response!.toObject();
  }

  public async findPartManufacturersByPartNameId(partNameId: number): Promise<PartManufacturer.AsObject[]> {
    const criteria = new FindPartManufacturersByPartNameIdRequest.Criteria();
    criteria.setPartNameId(partNameId);
    const request = new FindPartManufacturersByPartNameIdRequest();
    request.setCriteria(criteria);

    const response = await this.invoke<
    FindPartManufacturersByPartNameIdRequest,
    FindPartManufacturersByPartNameIdResponse>(PartGroupsClient.prototype.findPartManufacturersByPartNameId)(request);

    return response!.toObject().dataList;
  }

  protected getClient(): PartGroupsClient {
    return new PartGroupsClient(
      new URL(process.env.SERVICE_PARTGROUPS_URI as string).host,
      credentials.createInsecure(),
      { interceptors: this.getInterceptors() },
    );
  }
}
