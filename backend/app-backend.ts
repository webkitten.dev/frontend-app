process.env.NODE_ENV = process.env.NODE_ENV || 'production';

const HOST = process.env.API_HOST || '0.0.0.0';
const PORT = process.env.API_PORT ? Number(process.env.API_PORT) : 3000;

import init from './init';

init(HOST, PORT, true);
