process.env.NODE_ENV = process.env.NODE_ENV || 'production';

const HOST = process.env.HOST || '0.0.0.0';
const PORT = process.env.PORT ? Number(process.env.PORT) : 3000;

import init from './init';
import nuxt from './nuxt';

const state = init(HOST, PORT, true);
nuxt(state.app);
