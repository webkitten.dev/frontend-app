process.env.NODE_ENV = process.env.NODE_ENV || 'production';

import { Builder } from 'nuxt';

import nuxt from './nuxt';

if (process.env.NODE_ENV === 'development') {
  process.exit(1);
}

nuxt(null, Builder);
