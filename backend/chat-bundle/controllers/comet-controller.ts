import { Response } from 'express';
import { sign } from 'jsonwebtoken';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

export const token = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const payload = { user_id: req.user!.id };
    const secret = `${process.env.COMET_CHAT_TOKEN}${process.env.COMET_CHAT_ID}`;
    const token = sign(payload, secret, { expiresIn: 3600 * 24 * 12 });

    utils.sendSuccess(res)({
      token,
      user_id: payload.user_id,
    });
  }),
];
