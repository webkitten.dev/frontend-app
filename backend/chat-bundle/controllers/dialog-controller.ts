import { Response } from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { ChatRepository } from '../repositories/chat-repository';

const chatRepository = new ChatRepository();

import { DialogRequest } from '../entities/dialog-request';
import { DialogResponse } from '../entities/dialog-response';
import { DialogContractPartRequest } from '../entities/dialog-contract-part-request';

export const fromRequest = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const request: DialogRequest = new DialogRequest({
      userId: req.user!.id,
      id: +req.body.elementId!,
    });

    const data: object = new DialogResponse(await chatRepository
      .createDialogFromRequest(request)
      .catch(utils.throwError(500, 'api error')));
    utils.sendSuccess(res)(data);
  }),
];

export const fromShopOrder = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const request: DialogRequest = new DialogRequest({
      userId: req.user!.id,
      id: +req.body.elementId!,
    });

    const data: object = new DialogResponse(await chatRepository
      .createDialogFromShopOrder(request)
      .catch(utils.throwError(500, 'api error')));
    utils.sendSuccess(res)(data);
  }),
];

export const fromContractPart = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const request: DialogContractPartRequest = new DialogContractPartRequest({
      userId: req.user!.id,
      id: +req.body.elementId!,
      shopId: +req.body.shopId!,
    });

    const data: object = new DialogResponse(await chatRepository
      .createDialogFromContractPart(request)
      .catch(utils.throwError(500, 'api error')));
    utils.sendSuccess(res)(data);
  }),
];

export const fromShop = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const request: DialogRequest = new DialogRequest({
      userId: req.user!.id,
      id: +req.body.elementId!,
    });

    const data: object = new DialogResponse(await chatRepository
      .createDialogFromShop(request)
      .catch(utils.throwError(500, 'api error')));
    utils.sendSuccess(res)(data);
  }),
];
