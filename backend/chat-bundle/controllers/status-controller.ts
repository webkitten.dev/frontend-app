import {
  Request,
  Response,
} from 'express';
import * as utils from '../../common-bundle/utils';
import { map } from 'lodash';

import { ChatRepository } from '../repositories/chat-repository';

const chatRepository = new ChatRepository();

export const onlineShops = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const ids: number[] = map(req.query.id, Number);
  const data = await chatRepository
    .onlineShopList(ids)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});
