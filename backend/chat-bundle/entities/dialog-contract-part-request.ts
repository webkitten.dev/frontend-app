import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class DialogContractPartRequest extends BaseEntity<DialogContractPartRequest> {
  public userId: number | undefined;
  public id: number | undefined;
  public shopId: number | undefined;
}
