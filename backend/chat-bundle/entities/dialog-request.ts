import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class DialogRequest extends BaseEntity<DialogRequest> {
  public userId: number | undefined;
  public id: number | undefined;
}
