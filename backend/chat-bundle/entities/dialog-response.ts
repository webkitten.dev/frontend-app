import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class DialogResponse extends BaseEntity<DialogResponse> {
  public roomId: number | undefined;
  public type: string | undefined;
}
