import { AbstractRepository } from '../../main-bundle/repositories/abstract-repository';
import { DialogRequest } from '../entities/dialog-request';
import { DialogContractPartRequest } from '../entities/dialog-contract-part-request';

export class ChatRepository extends AbstractRepository {
  protected readonly queryNamespace = 'Chat';

  public createDialogFromRequest(params: DialogRequest): Promise<object> {
    const query: GraphQLQuery = this.getQuery('createFromRequest', params, [
      'roomId',
      'type',
    ]);
    return this.request(this.getRequestParams(query, 'post'));
  }

  public createDialogFromShopOrder(params: DialogRequest): Promise<object> {
    const query: GraphQLQuery = this.getQuery('createFromShopOrder', params, [
      'roomId',
      'type',
    ]);
    return this.request(this.getRequestParams(query, 'post'));
  }

  public createDialogFromContractPart(params: DialogContractPartRequest): Promise<object> {
    const query: GraphQLQuery = this.getQuery('createFromContractPart', params, [
      'roomId',
      'type',
    ]);
    return this.request(this.getRequestParams(query, 'post'));
  }

  public createDialogFromShop(params: DialogRequest): Promise<object> {
    const query: GraphQLQuery = this.getQuery('createFromShop', params, [
      'roomId',
      'type',
    ]);
    return this.request(this.getRequestParams(query, 'post'));
  }

  public onlineShopList(shopIds: number[]): Promise<number[]> {
    const query: GraphQLQuery = this.getQuery('onlineShopList', { shopIds }, [
      'list',
    ]);
    return this.request(this.getRequestParams(query, 'get'));
  }
}
