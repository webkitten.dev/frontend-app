import { Router } from 'express';

import Comet from './modules/comet-router';
import Dialog from './modules/dialog-router';
import Status from './modules/status-router';

const router = Router();
router.use(Comet.prefix, Comet.router);
router.use(Dialog.prefix, Dialog.router);
router.use(Status.prefix, Status.router);

export default router;
