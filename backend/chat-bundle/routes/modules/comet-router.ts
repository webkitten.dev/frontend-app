import { Router } from 'express';
import * as controller from '../../controllers/comet-controller';

const router = Router();
router.get('/token', controller.token);

export default {
  router,
  prefix: '/comet',
};
