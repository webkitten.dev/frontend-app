import { Router } from 'express';
import * as controller from '../../controllers/dialog-controller';

const router = Router();
router.post('/fromRequest', controller.fromRequest);
router.post('/fromShopOrder', controller.fromShopOrder);
router.post('/fromContractPart', controller.fromContractPart);
router.post('/fromShop', controller.fromShop);

export default {
  router,
  prefix: '/dialog',
};
