import { Router } from 'express';
import * as controller from '../../controllers/status-controller';

const router = Router();
router.get('/onlineShops', controller.onlineShops);

export default {
  router,
  prefix: '/status',
};
