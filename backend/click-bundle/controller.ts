import { Response } from 'express';

import * as utils from '../common-bundle/utils';
import * as jwt from '../common-bundle/jwt';

import { ContractPartClick } from './entities/contract-part-click';
import { NewPartClick } from './entities/new-part-click';
import { ShopRepository } from '../main-bundle/repositories/shop-repository';
import { ClickRepository } from './repository';

const shopRepository = new ShopRepository();
const clickRepository = new ClickRepository();

export const newPart = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    if (!req.body.type || !Number(req.body.shopId)) {
      utils.throwError(400, 'bad request')(new Error('Некорректный набор параметров'));
      return;
    }

    const userId = await shopRepository
      .getUserId(Number(req.body.shopId))
      .catch(utils.throwError(500, 'api error'));
    if (!userId) {
      utils.throwError(500, 'api error')(new Error('Не удалось определить userId'));
      return;
    }

    const object = new NewPartClick({
      userId,
      action: req.body.type,
      shopId: req.body.shopId,
      brandId: req.body.brandId,
      brandName: req.body.brandName,
      partId: req.body.partId,
      partName: req.body.partName,
      vendorCode: req.body.partNumber,
      itemId: req.body.itemId,
      itemName: req.body.itemName,
      clientId: req.body.clientId
        ? req.body.clientId
        : (req.user ? String(req.user.id) : ''),
      headers: {
        ...req.headers,
        'x-real-ip': utils.getIpFromRequest(req),
        'x-session-id': utils.getHashedSessionId(req),
      },
    });

    const result = await clickRepository.send(object);

    utils.sendSuccess(res)(result);
  }),
];

export const contractPart = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    if (!req.body.type || !Number(req.body.adId)) {
      utils.throwError(400, 'bad request')(new Error('Некорректный набор параметров'));
      return;
    }

    const object = new ContractPartClick({
      action: req.body.type,
      adId: req.body.adId,
      adName: req.body.adName,
      userId: req.body.userId,
      shopId: req.body.shopId,
      geoId: req.body.geoId,
      brandId: req.body.brandId,
      brandName: req.body.brandName,
      modelId: req.body.modelId,
      modelName: req.body.modelName,
      years: req.body.years,
      chassis: req.body.chassis,
      engines: req.body.engines,
      clientId: req.body.clientId
        ? req.body.clientId
        : (req.session!.authUser ? req.session!.authUser.id : ''),
      headers: {
        ...req.headers,
        'x-real-ip': utils.getIpFromRequest(req),
        'x-session-id': utils.getHashedSessionId(req),
      },
    });

    const result = await clickRepository.send(object);

    utils.sendSuccess(res)(result);
  }),
];
