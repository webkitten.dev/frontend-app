import {
  pickBy as _pickBy,
} from 'lodash';
import { IncomingHttpHeaders } from 'http';

import { IEntity } from '../../common-bundle/essence/i-entity';

export interface IAbstractClickData {
  action: string;
  clientId: string;
}

export abstract class AbstractClick implements IEntity {
  protected readonly apiPath!: string;
  protected headers: IncomingHttpHeaders = {};
  protected apiData!: IAbstractClickData;

  constructor(
    data: {
      action: string,
      clientId: string,
      headers?: IncomingHttpHeaders,
    },
  ) {
    if (!data.action) {
      throw new Error('click without action not acceptable');
    }
    this.apiData = {
      action: String(data.action) || '',
      clientId: String(data.clientId) || '',
    };
    this.headers = data.headers || {};
  }

  public getApiPath(): string {
    return this.apiPath;
  }

  public getApiData() {
    return this.apiData;
  }

  public getHeaders(): IncomingHttpHeaders {
    return _pickBy(
      this.headers,
      (value, key) => ['referer', 'user-agent', 'x-real-ip', 'x-session-id'].includes(key),
    );
  }
}
