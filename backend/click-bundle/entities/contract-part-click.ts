import config from 'config';
import { IncomingHttpHeaders } from 'http';

import { AbstractClick, IAbstractClickData } from './abstract-click';

const apiPath: string = config.get('api.statscollector.contractPartPath');

interface IClickData extends IAbstractClickData {
  adId: string;
  adName: string;
  userId: string;
  shopId: string;
  geoId: string;
  brandId: number;
  brandName: string;
  modelId: number;
  modelName: string;
  chassis: string[];
  engines: string[];
  years: number[];
}

export class ContractPartClick extends AbstractClick {
  protected readonly apiPath = apiPath;
  protected apiData!: IClickData;

  constructor(
    data: {
      action: string,
      clientId: string,
      adId: string,
      adName: string,
      userId: string,
      shopId: string,
      geoId: string,
      brandId: number,
      brandName: string,
      modelId: number,
      modelName: string,
      chassis: string[],
      engines: string[],
      years: number[],
      headers?: IncomingHttpHeaders,
    },
  ) {
    super({ action: data.action, clientId: data.clientId, headers: data.headers });
    this.apiData.adId = data.adId ? String(data.adId) : '';
    this.apiData.adName = data.adName ? String(data.adName) : '';
    this.apiData.userId = data.userId ? String(data.userId) : '';
    this.apiData.shopId = data.shopId ? String(data.shopId) : '';
    this.apiData.geoId = data.geoId ? String(data.geoId) : '';
    this.apiData.brandId = data.brandId ? Number(data.brandId) : 0;
    this.apiData.brandName = data.brandName ? String(data.brandName) : '';
    this.apiData.modelId = data.modelId ? Number(data.modelId) : 0;
    this.apiData.modelName = data.modelName ? String(data.modelName) : '';
    if (data.chassis && Array.isArray(data.chassis)) {
      this.apiData.chassis = data.chassis.map(String);
    } else {
      this.apiData.chassis = [];
    }
    if (data.engines && Array.isArray(data.engines)) {
      this.apiData.engines = data.engines.map(String);
    } else {
      this.apiData.engines = [];
    }
    if (data.years && Array.isArray(data.years)) {
      this.apiData.years = data.years.map(Number);
    } else {
      this.apiData.years = [];
    }
  }
}
