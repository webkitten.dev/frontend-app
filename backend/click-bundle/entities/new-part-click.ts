import config from 'config';
import { IncomingHttpHeaders } from 'http';

import { AbstractClick, IAbstractClickData } from './abstract-click';

const apiPath: string = config.get('api.statscollector.newPartPath');

interface IClickData extends IAbstractClickData {
  userId: string;
  shopId: string;
  brandId: number;
  brandName: string;
  partId: string;
  partName: string;
  itemId: string;
  itemName: string;
  vendorCode: string;
}

export class NewPartClick extends AbstractClick {
  protected readonly apiPath = apiPath;
  protected apiData!: IClickData;

  constructor(
    data: {
      action: string,
      clientId: string
      userId: string | number,
      shopId: string,
      brandId: number,
      brandName: string,
      partId: string,
      partName: string,
      itemId: string,
      itemName: string,
      vendorCode: string,
      headers?: IncomingHttpHeaders,
    },
  ) {
    super({ action: data.action, clientId: data.clientId, headers: data.headers });
    this.apiData.userId = data.userId ? String(data.userId) : '';
    this.apiData.shopId = data.shopId ? String(data.shopId) : '';
    this.apiData.brandId = data.brandId ? Number(data.brandId) : 0;
    this.apiData.brandName = data.brandName ? String(data.brandName) : '';
    this.apiData.partId = data.partId ? String(data.partId) : '';
    this.apiData.partName = data.partName ? String(data.partName) : '';
    this.apiData.itemId = data.itemId ? String(data.itemId) : '';
    this.apiData.itemName = data.itemName ? String(data.itemName) : '';
    this.apiData.vendorCode = data.vendorCode ? String(data.vendorCode) : '';
  }
}
