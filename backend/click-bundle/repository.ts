import config from 'config';
import { AxiosRequestConfig } from 'axios';

import {
  LoggerContainer,
  LoggerCategories,
} from '../common-bundle/logger';
const logger = LoggerContainer.get(LoggerCategories.CLICK);

import { AbstractClick } from './entities/abstract-click';
import { AbstractRepository as CommonAbstractRepository } from '../common-bundle/io/abstract-repository';

export class ClickRepository extends CommonAbstractRepository {
  public send<T extends AbstractClick>(request: T): Promise<boolean> {
    const timestamp = Number(new Date());
    return new Promise((resolve) => {
      const requestConfig: AxiosRequestConfig = {
        url: [
          process.env.SERVICE_STATSCOLLECTOR_URI,
          request.getApiPath(),
          config.get('api.statscollector.clickSuffix'),
        ].join(''),
        method: 'post',
        data: request.getApiData(),
        headers: request.getHeaders() || {},
      };
      logger.verbose(
        `request ${request.getApiPath()} ${timestamp} %j, headers %j`,
        request.getApiData(),
        requestConfig.headers,
      );
      this.request({
        prepared: requestConfig,
        requestType: 'click',
        requestPayload: request.getApiData(),
      })
        .then((resolved) => { logger.verbose(resolved); resolve(true); })
        .catch((resolved) => { logger.warn(resolved); resolve(false); });
    });
  }
}
