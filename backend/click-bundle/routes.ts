import { Router } from 'express';
import * as controller from './controller';

const router = Router();
router.post('/newPart', controller.newPart);
router.post('/contractPart', controller.contractPart);

export default router;
