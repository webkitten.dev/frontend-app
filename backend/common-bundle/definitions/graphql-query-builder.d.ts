declare class GraphQLQuery {
  constructor(queryName: string, params?: object);
  public find(find: string | object | []): any;
  public toString(): string;
}

declare module 'graphql-query-builder' {
  export = GraphQLQuery;
}
