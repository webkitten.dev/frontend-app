import { IEntity } from './i-entity';

export abstract class BaseEntity<T> implements IEntity {
  constructor(data?: Partial<T>) {
    if (data) {
      Object.assign(this, data);
    }
  }
}
