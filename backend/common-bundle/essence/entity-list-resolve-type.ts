export class EntityListResolveType<T> {
  public data: T[] = [];
  public count: number | null = null;
}
