export abstract class IdentifiableType {
  public id: number | string | undefined;
}
