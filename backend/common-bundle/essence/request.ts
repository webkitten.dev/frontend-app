import { AxiosRequestConfig } from 'axios';

export interface Request {
  prepared: AxiosRequestConfig;
  requestType: string;
  requestPayload: object;
}
