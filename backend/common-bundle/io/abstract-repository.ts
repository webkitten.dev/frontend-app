import http from 'http';
import https from 'https';
import { AxiosInstance, default as axios } from 'axios';

import config from 'config';

import {
  LoggerContainer,
  LoggerCategories,
} from '../../common-bundle/logger';
const logger = LoggerContainer.get(LoggerCategories.APP);

import { Request } from '../essence/request';

export abstract class AbstractRepository {
  protected requestHandler: AxiosInstance;

  constructor() {
    if (config.get('api.scheme') === 'https') {
      this.requestHandler = axios.create({
        httpsAgent: new https.Agent({
          rejectUnauthorized: !config.get('api.ignoreSslErrors'),
        }),
      });
    } else {
      this.requestHandler = axios.create({
        httpAgent: new http.Agent(),
      });
    }
  }

  protected request(requestConfig: Request): Promise<any> {
    return new Promise((resolve, reject) => {
      this.requestHandler.request(requestConfig.prepared)
        .then((response) => {
          resolve(response.data);
        })
        .catch((e) => {
          logger.error(
            'error <%s> in %s, data %j',
            e.message,
            requestConfig.prepared.url,
            requestConfig.requestPayload,
          );
          reject(new Error(e.message));
        });
    });
  }
}
