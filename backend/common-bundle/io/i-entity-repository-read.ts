import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';

export interface IEntityRead<T> {
  count(): Promise<number>;
  list(
    criteria?: object,
    limit?: number,
    offset?: number,
    orderField?: string,
    orderDirection?: string,
  ): Promise<EntityListResolveType<T>>;
  get(criteria: object): Promise<T>;
}
