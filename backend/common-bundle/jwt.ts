import fs from 'fs';
import {
  Request as ExpressRequest,
  Response as ExpressResponse,
  NextFunction as ExpressNextFunction,
} from 'express';
import jwt from 'jsonwebtoken';
import expressJwt from 'express-jwt';
import { get as _get } from 'lodash';
import config from 'config';

let privateKey: expressJwt.secretType;
if (process.env.JWT_PRIVATE_KEY) {
  privateKey = process.env.JWT_PRIVATE_KEY as string;
} else if (process.env.JWT_PRIVATE_KEY_PATH) {
  privateKey = fs.readFileSync(process.env.JWT_PRIVATE_KEY_PATH);
} else {
  console.error('no jwt private key specified');
  process.exit(1);
}

let publicKey: expressJwt.secretType;
if (process.env.JWT_PUBLIC_KEY) {
  publicKey = process.env.JWT_PUBLIC_KEY as string;
} else if (process.env.JWT_PUBLIC_KEY_PATH) {
  publicKey = fs.readFileSync(process.env.JWT_PUBLIC_KEY_PATH);
} else {
  console.error('no jwt public key specified');
  process.exit(1);
}

interface Payload {
  username: string;
  id: number;
  exp: number;
}

interface MiddlewareOptions {
  token: string | null;
  anonym?: boolean;
}

export interface Request extends ExpressRequest {
  user?: Payload;
}

export const sign = (userId: number, username: string) => {
  return jwt.sign({ username, id: userId }, privateKey, {
    expiresIn: config.get('jwt.expiresIn'),
    algorithm: config.get('jwt.algorithm'),
  });
};

const middlewareFactory = (options: MiddlewareOptions) => {
  return expressJwt({
    getToken: () => options.token,
    secret: publicKey,
    credentialsRequired: !Boolean(_get(options, 'anonym', false)),
  });
};

const getActualToken = (req: ExpressRequest, res: ExpressResponse, next: ExpressNextFunction): string | null => {
  const cookies = require('cookie-universal')(req, res);
  const cookieName = config.get('jwt.cookieName');
  let token = cookies.get(cookieName) || null;
  if (!token) {
    return null;
  }

  const payload = jwt.decode(token) as Payload;
  let expiresIn;
  if (!payload || !payload.exp) {
    expiresIn = 1;
  } else {
    expiresIn = payload.exp;
  }
  expiresIn = expiresIn * 1000;

  const refreshAt = config.get<number>('jwt.refreshAt') * 1000;
  const left = expiresIn - new Date().getTime();
  if (left <= 0) {
    cookies.remove(cookieName, config.get('jwt.cookieOptions'));
    return null;
  }
  if (left <= refreshAt) {
    token = sign(payload.id, payload.username);
    cookies.set(cookieName, token, config.get('jwt.cookieOptions'));
  }

  return token;
};

export const middlewareAuth = (req: ExpressRequest, res: ExpressResponse, next: ExpressNextFunction) => {
  const token = getActualToken(req, res, next);
  const middleware = middlewareFactory({ token, anonym: false });
  return middleware(req, res, next);
};

export const middlewareAnonym = (req: ExpressRequest, res: ExpressResponse, next: ExpressNextFunction) => {
  const token = getActualToken(req, res, next);
  const middleware = middlewareFactory({ token, anonym: true });
  return middleware(req, res, next);
};
