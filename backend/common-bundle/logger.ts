import {
  transports,
  format,
  Container,
} from 'winston';

const categories = Object.freeze({
  MONOLITH: 'monolith',
  GRPC: 'grpc',
  GRAPHQL: 'graphql',
  APP: 'app',
  CLICK: 'click',
});

const loggerFactory = (label: string) => {
  return {
    format: format.combine(
      format.label({ label }),
      format.timestamp({ format: 'YYYY-MM-DDTHH:mm:ss.SSSZ' }),
      format.splat(),
      format.printf(({ timestamp, label, level, message }) => [
        `${timestamp}`,
        `[${label}]`,
        `[${level}]`,
        message,
      ].filter(Boolean).join(' ')),
    ),
    transports: [new transports.Console({ level: process.env.LOG_LEVEL || 'debug' })],
  };
};

const container = new Container();
container.add(categories.MONOLITH, loggerFactory(categories.MONOLITH));
container.add(categories.GRPC, loggerFactory(categories.GRPC));
container.add(categories.GRAPHQL, loggerFactory(categories.GRAPHQL));
container.add(categories.APP, loggerFactory(categories.APP));
container.add(categories.CLICK, loggerFactory(categories.CLICK));

export { container as LoggerContainer, categories as LoggerCategories };
