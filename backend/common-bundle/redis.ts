import {
  LoggerContainer,
  LoggerCategories,
} from './logger';
const logger = LoggerContainer.get(LoggerCategories.APP);

const redisParams = new URL(process.env.REDIS_API_CACHE_URI as string);
const redis = require('async-redis').createClient({
  port: Number(redisParams.port),
  host: redisParams.hostname,
  db: Number(redisParams.pathname.replace('/', '')),
});

redis.monitor();
redis.on('monitor', (time: number, args: any[], rawReply: object) => {
  logger.silly('%o: %o', args, rawReply);
});

redis.asyncSetex = async (key: string, period: number, value: string | number) => {
  try {
    await redis.setex(key, period, JSON.stringify(value));
  } catch (e) {
    logger.error(e.message);
  }
};

redis.asyncGet = async (value: string) => {
  const data = await redis.get(value);
  return JSON.parse(data);
};

export default redis;
