import md5 from 'md5';
import {
  Request,
  Response,
  NextFunction,
} from 'express';
import {
  some as _some,
  find as _find,
} from 'lodash';
import config from 'config';

import { IdentifiableType } from './essence/identifiable-type';

export const throwError = (
  errorCode: number,
  errorType?: string,
  errorMessage?: string,
) => (inputError?: { [key: string]: any }) => {
  if (inputError && inputError.errorCode) {
    throw inputError;
  }
  let error;
  if (!inputError) {
    error = new Error(errorMessage || 'Default Error');
  } else {
    error = inputError;
  }
  error.errorCode = errorCode;
  if (errorType) {
    error.errorType = errorType;
  }
  delete error.config;
  delete error.request;
  throw error;
};

export const throwIf = <T>(
  fn: (check: any) => boolean,
  errorCode: number,
  errorType: string,
  error: Error,
) => (result: T) => {
  if (fn(result)) {
    return throwError(errorCode, errorType)(error);
  }
  return result;
};

export const sendSuccess = (
  res: Response,
  message?: string,
) => (data: any) => {
  res.status(200).json({ data, message, type: 'success' });
};

export const sendError = (
  res: Response,
  status?: number,
  message?: string,
) => (error?: { [key: string]: any }) => {
  res.status(status || error && error.errorCode || 500).json({
    error,
    type: 'error',
    message: message || error && error.message,
  });
};

export const getIpFromRequest = (req: Request): string => {
  if (req.headers['x-dev-ip']) {
    return req.headers['x-dev-ip']!.toString();
  }
  switch (process.env.NODE_ENV) {
    case 'production':
      if (req.headers['x-real-ip']) {
        return req.headers['x-real-ip']!.toString();
      }
      return req.connection.remoteAddress || req.socket.remoteAddress || '';
    default:
      return '109.202.29.4';
  }
};

export const isBot = (req: Request): boolean => {
  const userAgent: string = req.headers['user-agent']!.toString();
  const bots = config.get<string[]>('botUserAgents');
  const regExp = new RegExp(bots.join('|'), 'gi');
  return regExp.test(userAgent);
};

export const wrapAsyncMethod = (
  fn: (req: Request, res: Response, next: NextFunction) => Promise<any>,
) => {
  return (req: Request, res: Response, next: NextFunction) => {
    fn(req, res, next).catch(next);
  };
};

export const remapIdentifiableCollections = <T extends IdentifiableType, U extends IdentifiableType>(
  oldValues: T[],
  newValues: U[],
): (T & U)[] => {
  if (
    !Array.isArray(newValues) || !newValues.length ||
    !Array.isArray(oldValues) || !oldValues.length
  ) {
    return [];
  }
  return oldValues
    .filter((value: T) => _some(newValues, ({ id }) => Number(id) === Number(value.id)))
    .map((value: T) => {
      const newValue: U = _find(newValues, ({ id }) => Number(id) === Number(value.id))!;
      return {
        ...value,
        ...newValue,
      };
    });
  // return _unionBy(newValues, oldValues as unknown as ArrayLike<T>, 'id') as (T & U)[];
};

export const sleep = async (ms: number) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

export const toSafeInt = (n: number) => {
  let result = Number(n);
  if (result > Number.MAX_SAFE_INTEGER) {
    result = Number.MAX_SAFE_INTEGER;
  }
  return result;
};

export const getHashedSessionId = (req: Request): string => {
  return md5(req.sessionID || '');
};
