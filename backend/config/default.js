const defer = require('config/defer').deferConfig;

module.exports = {
  api: {
    scheme: 'https',
    ignoreSslErrors: false,
    baseUrl: '/_backend',
    main: {
      tokenHeader: 'X-Fawesome-Token',
    },
    statscollector: {
      newPartPath: '/search-by-number',
      contractPartPath: '/contract-parts',
      clickSuffix: '/click',
    },
  },
  redis: {
    cacheAges: { // секунд
      counters: 60 * 60 * 1,
    },
  },
  domains: {
    base: process.env.HOST_BASE || 'gisauto.ru',
    main: process.env.HOST_MAIN || 'gisauto.ru',
    sto: process.env.HOST_STO || 'sto.gisauto.ru',
    helper: process.env.HOST_HELPER || 'helper.gisauto.ru',
    chat: process.env.HOST_CHAT || 'chat.gisauto.ru',
  },
  itemOutdatedTreshold: 14,
  imagesBaseUrl: process.env.IMAGES_BASE_PATH,
  socials: {
    telegram: 'GisautoHelper',
    whatsapp: '79639434194',
    viber: '79639434194',
    vk: 'gisauto',
    ok: 'gisautoru',
    instagram: 'gisauto.ru',
  },
  contacts: {
    info: {
      phone: '+7 (800) 700-41-91',
      email: 'info@gisauto.ru',
    },
  },
  botUserAgents: [
    'YandexBot',
    'YandexMobileBot',
    'Googlebot',
    'bingbot',
    'Mail.RU_Bot',
  ],
  jwt: {
    algorithm: 'RS256',
    expiresIn: 60 * 60 * 24 * 31,
    refreshAt: 60 * 60 * 24 * 7,
    cookieName: 'BEARER',
    cookieOptions: {
      path: '/',
      domain: `.${process.env.HOST_BASE || 'gisauto.ru'}`,
      httpOnly: true,
      secure: true,
      maxAge: 60 * 60 * 24 * 365 * 1000,
    },
  },
  allStatsAllowedUserIds: [
    9263, // office@gisauto.ru
    19075, // giss.ivanov.01@mail.ru
    15485, // july2urgis@mail.ru
    11204, // gregoryk@bgs.ru
    23762, // vorobyeva.ksenya@gmail.com
    22805, // vorobyeva.ksenya@test.com
    24104, // vorobyevak@mail.com
    21797, // 3751290@gmail.com
  ],
  siteWarning: process.env.SITE_WARNING,
  client: {
    domains: defer(function domains() { return this.domains; }),
    itemOutdatedTreshold: defer(function itemOutdatedTreshold() { return this.itemOutdatedTreshold; }),
    imagesBaseUrl: defer(function imagesBaseUrl() { return this.imagesBaseUrl; }),
    socials: defer(function socials() { return this.socials; }),
    contacts: defer(function contacts() { return this.contacts; }),
    allStatsAllowedUserIds: defer(function allStatsAllowedUserIds() { return this.allStatsAllowedUserIds; }),
    botUserAgents: defer(function botUserAgents() { return this.botUserAgents; }),
    siteWarning: defer(function siteWarning() { return this.siteWarning; }),
  },
};
