import { Application as ExpressApplication } from 'express';
import shutdown from 'http-shutdown';

import {
  LoggerContainer,
  LoggerCategories,
} from './common-bundle/logger';
const logger = LoggerContainer.get(LoggerCategories.APP);

import server from './server';

export default (host: string, port: number, withBackend: boolean = true) => {
  const state: {
    app: ExpressApplication,
    server: any,
  } = {
    app: server(withBackend),
    server: null,
  };

  state.server = shutdown(state.app.listen(port, host, () => {
    logger.info(`Running on ${host}:${port}`);
  }));

  const stop = () => {
    state.server!.shutdown(() => {
      logger.info(`Stopped on ${host}:${port}`);
      process.exit(0);
    });
  };
  process.on('SIGINT', stop);
  process.on('SIGTERM', stop);

  return state;
};
