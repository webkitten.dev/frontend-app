import { Response } from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { Abuse } from '../entities/abuse';
import { AbuseRepository } from '../repositories/abuse-repository';
import { BooleanResult } from '../entities/boolean-result';

const repository: AbuseRepository = new AbuseRepository();

export const create = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data: BooleanResult = await repository
      .createAbuse(new Abuse({
        userId: req.user ? req.user.id : null,
        shopId: Number(req.body.shopId),
        comment: req.body.comment,
      }))
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data);
  }),
];
