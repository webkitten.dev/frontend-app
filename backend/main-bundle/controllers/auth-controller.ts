import {
  Request,
  Response,
} from 'express';
import {
  isEmpty as _isEmpty,
} from 'lodash';
import config from 'config';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { UserRepository } from '../repositories/user-repository';

const repository = new UserRepository();

export const login = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  if (!req.body.username || !req.body.password) {
    utils.throwError(401, 'bad credentials')(new Error('Login or password missing'));
  }

  const data = await repository
    .get({ username: req.body.username, password: req.body.password })
    .catch(utils.throwError(500, 'api error'));

  if (_isEmpty(data)) {
    utils.throwError(401, 'bad credentials')(new Error('Неверный логин или пароль'));
  }

  req.session!.authUser = data;

  const token = jwt.sign(Number(data.id!), data.username!);
  res.cookie(config.get('jwt.cookieName'), token, config.get('jwt.cookieOptions'));

  utils.sendSuccess(res)(data);
});

export const logout = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  delete req.session!.authUser;
  res.cookie(config.get('jwt.cookieName'), '', config.get('jwt.cookieOptions'));
  utils.sendSuccess(res)({ done: true });
});
