import {
  Request,
  Response,
} from 'express';
import * as utils from '../../common-bundle/utils';

import { CarManufacturerRepository } from '../repositories/car-manufacturer-repository';
import { ExpertAutosRepository } from '../../api-bundle/grpc/repositories/expert-autos-repository';

const carManufacturerRepository = new CarManufacturerRepository();

export const list = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const hasModels = req.query.hasModels ? true : false;
  const expertAutosRepository = new ExpertAutosRepository();
  try {
    const result = await expertAutosRepository.findManufacturersByCriteria({ hasModels });
    utils.sendSuccess(res)(result);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
});

export const get = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const id = Number(req.query.id) || 0;
  const expertAutosRepository = new ExpertAutosRepository();
  try {
    const result = await expertAutosRepository.findManufacturersByIds([id]);
    utils.sendSuccess(res)(result);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
});

export const getWithRegion = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const data = await carManufacturerRepository
    .getWithRegion()
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});
