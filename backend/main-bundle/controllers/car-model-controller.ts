import {
  Request,
  Response,
} from 'express';
import * as utils from '../../common-bundle/utils';

import { ExpertAutosRepository } from '../../api-bundle/grpc/repositories/expert-autos-repository';

export const getByManufacturerId = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const manufacturerId = Number(req.query.manufacturerId) || 0;
  const expertAutosRepository = new ExpertAutosRepository();
  try {
    const result = await expertAutosRepository.findModelsByCriteria({ manufacturerIds: [manufacturerId] });
    utils.sendSuccess(res)(result);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
});
