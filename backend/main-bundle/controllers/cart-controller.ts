import {
  Request,
  Response,
  NextFunction,
} from 'express';
import * as utils from '../../common-bundle/utils';

import { CartItem } from '../entities/cart-item';

export const count = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  utils.sendSuccess(res)(req.session!.cart ? req.session!.cart.length : 0);
});

export const get = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  req.session!.cart = req.session!.cart || [];

  utils.sendSuccess(res)(req.session!.cart);
});

export const update = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  req.session!.cart = req.body.cartItemsList;

  utils.sendSuccess(res)(req.session!.cart);
});

export const add = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  req.session!.cart = req.session!.cart || [];

  const hasItem: CartItem | undefined | null = req.session!.cart
    .find((item: CartItem) => item.itemId === req.body.cartItem.itemId &&
      item.isWholesalePrice === req.body.cartItem.isWholesalePrice);

  if (hasItem) {
    hasItem.quantity! += 1;
  } else {
    req.session!.cart.push(req.body.cartItem);
  }

  utils.sendSuccess(res)(req.session!.cart);
});
