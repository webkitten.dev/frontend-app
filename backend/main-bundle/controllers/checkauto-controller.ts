import {
  Request,
  Response,
  NextFunction,
} from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { CheckautoRepository } from '../repositories/checkauto-repository';
const repository: CheckautoRepository = new CheckautoRepository();

export const info = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: object = await repository
    .getInfo()
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});

export const shortReportInfo = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: { [key: string]: any } = await repository
    .getShortReportInfo(req.query.id)
    .catch(utils.throwError(500, 'api error'));
  try {
    data.info = JSON.parse(data.info);
  } catch (jsonError) {
    utils.throwError(500, 'api response json decode error')(jsonError);
  }
  utils.sendSuccess(res)(data);
});

export const fullReportInfo = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: { [key: string]: any } = await repository
    .getFullReportInfo(req.query.id)
    .catch(utils.throwError(500, 'api error'));
  try {
    data.info = JSON.parse(data.info);
  } catch (jsonError) {
    utils.throwError(500, 'api response json decode error')(jsonError);
  }
  utils.sendSuccess(res)(data);
});

export const init = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: object = await repository
    .init(req.query.vin)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});

export const create = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: object = await repository
    .create(req.body.vin, req.body.captcha, req.body.session)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});

export const updateRestrictions = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: object = await repository
    .updateRestrictions(req.body.id, req.body.captcha, req.body.session)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});

export const createPayment = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data: object = await repository
      .createPayment(
        req.body.vinRequestDataId,
        req.body.email,
        req.body.phone,
        req.body.register,
        req.user ? req.user.id : null,
        req.body.promocode,
      )
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data);
  }),
];

export const promoCodeInfo = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data: object = await repository
      .promoCodeInfo(
        req.query.code,
        req.user ? req.user.id : null,
      )
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data);
  }),
];
