import {
  Request,
  Response,
} from 'express';
import config from 'config';
import * as utils from '../../common-bundle/utils';

import redis from '../../common-bundle/redis';
import { CitySellerRepository } from '../repositories/city-seller-repository';
const repository = new CitySellerRepository();

export const count = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const count = await redis.asyncGet('citySellerCount');
  if (count) {
    utils.sendSuccess(res)(count);
    return true;
  }

  const data = await repository
    .count()
    .catch(utils.throwError(500, 'api error'));
  redis.asyncSetex('citySellerCount', config.get('redis.cacheAges.counters'), data);
  utils.sendSuccess(res)(data);
});
