import { Response } from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { ConciergePartnershipRequestRepository } from '../repositories/concierge-partnership-request-repository';
import { ConciergePartnershipRequest } from '../entities/concierge-partnership-request';
const repository: ConciergePartnershipRequestRepository = new ConciergePartnershipRequestRepository();

export const create = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const cookies = require('cookie-universal')(req, res, true);
    const object = new ConciergePartnershipRequest({
      geoTagId: +cookies.get('currentLocation') || null,
      userId: req.user ? req.user.id : null,
      userIp: utils.getIpFromRequest(req),
      contactPerson: req.body.data.contactPerson || null,
      phone: req.body.data.phone,
      email: req.body.data.email || null,
      organizationCityId: req.body.data.city ? +req.body.data.city : 0,
    });
    const data: object = await repository
      .create(object)
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data);
  }),
];
