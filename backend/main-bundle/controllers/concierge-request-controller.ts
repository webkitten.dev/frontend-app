import { Response } from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { ConciergeRequestRepository } from '../repositories/concierge-request-repository';
import { ConciergeRequest } from '../entities/concierge-request';
const repository = new ConciergeRequestRepository();

export const create = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const cookies = require('cookie-universal')(req, res, true);
    const object = new ConciergeRequest({
      geoTagId: +cookies.get('currentLocation') || null,
      userId: req.user ? req.user.id : null,
      userIp: utils.getIpFromRequest(req),
      phone: req.body.data.phone,
      problem: req.body.data.problem,
    });
    const data: object = await repository
      .create(object)
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data);
  }),
];
