import {
  Request,
  Response,
} from 'express';
import * as utils from '../../common-bundle/utils';
import redis from '../../common-bundle/redis';

import config from 'config';
import {
  get as _get,
  maxBy as _maxBy,
  sortBy as _sortBy,
} from 'lodash';

import {
  GeoLevel,
  PartCondition,
  MerchantType,
  Placement,
  ContractPartsSort,
} from '../../api-bundle/graphql/types';
import { CountableType } from '../../common-bundle/essence/countable-type';

import { UserRepository } from '../repositories/user-repository';
import { ShopRepository } from '../repositories/shop-repository';
import { GeoRepository } from '../repositories/geo-repository';
import { ContractPartsRepository } from '../../api-bundle/grpc/repositories/contract-parts-repository';
import { ContractPartsRepository as NewContractPartsRepository  } from '../repositories/contract-parts-repository';
import { ExpertAutosRepository } from '../../api-bundle/grpc/repositories/expert-autos-repository';
import { ExpertPartsRepository } from '../../api-bundle/grpc/repositories/expert-parts-repository';

import {
  Part,
  PartCondition as GrpcPartCondition,
  MerchantType as GrpcMerchantType,
  Placement as GrpcPlacement,
} from '@gisauto/contract-parts-service-proto/types_pb';

export const get = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const id = req.query.id ? Number(req.query.id) : 0;

  const contractPartsRepository = new ContractPartsRepository();
  const expertAutosRepository = new ExpertAutosRepository();
  const expertPartsRepository = new ExpertPartsRepository();

  const userRepository = new UserRepository();
  const shopRepository = new ShopRepository();
  const geoRepository = new GeoRepository();

  let result: Part.AsObject[];
  try {
    result = await contractPartsRepository.findPartsByIds([id]);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
  if (!result.length) {
    utils.throwError(404, 'not found')(new Error(`item not found by id ${id}`));
    return;
  }
  const item = result.shift()!;
  item.imagesList = _sortBy(item.imagesList, image => image.id);

  try {
    const [
      user,
      shop,
      geo,
      metro,
      carManufacturer,
      carModel,
      partManufacturer,
    ] = await Promise.all([
      item.userId ? userRepository.get({ id: +item.userId }) : Promise.resolve(null),
      item.shopId ? shopRepository.getShopInfo(+item.shopId) : Promise.resolve(null),
      item.geoId
        ? geoRepository.list({ ids: [String(item.geoId)] }).then(resolved => resolved.items.shift() || null)
        : Promise.resolve(null),
      item.metroId
        ? geoRepository.list({ ids: [String(item.metroId)] }).then(resolved => resolved.items.shift() || null)
        : Promise.resolve(null),
      expertAutosRepository
        .findManufacturersByIds([item.expertCarManufacturerId])
        .then(resolved => resolved.shift() || null),
      expertAutosRepository
        .findModelsByIds([item.expertCarModelId])
        .then(resolved => resolved.shift() || null),
      item.expertPartManufacturerId
        ? expertPartsRepository
            .findManufacturerByIds([item.expertPartManufacturerId])
            .then(resolved => resolved.shift() || null)
        : null,
    ]);

    utils.sendSuccess(res)({
      ...item,
      user,
      shop,
      geo,
      metro,
      carManufacturer,
      carModel,
      partManufacturer,
    });
  } catch (e) {
    utils.throwError(404, 'not found')(e);
    return;
  }
});

export const list = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const criteria = req.query.criteria || {};
  const contractPartsRepository = new NewContractPartsRepository();

  try {
    const result = await contractPartsRepository.list({
      itemStatus: criteria.itemStatus || 'ACTIVE',
      ids: criteria.ids || null,
      excludeIds: criteria.excludeIdsList || null,
      partCondition: (() => {
        switch (Number(criteria.condition)) {
          case GrpcPartCondition.CONDITION_NEW:  return PartCondition.New;
          case GrpcPartCondition.CONDITION_USED: return PartCondition.Used;
          default: return PartCondition.Any;
        }
      })(),
      merchantType: (() => {
        switch (Number(criteria.merchantType)) {
          case GrpcMerchantType.MERCHANT_PERSON: return MerchantType.Person;
          case GrpcMerchantType.MERCHANT_SHOP:   return MerchantType.Shop;
          default: return MerchantType.Any;
        }
      })(),
      shopId: criteria.shop || null,
      cityId: criteria.city || null,
      metroId: criteria.metro || null,
      carManufacturerId: criteria.carManufacturer || null,
      carModelId: criteria.carModel || null,
      priceRange: {
        min: criteria.priceMin || null,
        max: criteria.priceMax || null,
      },
      carYears: criteria.yearsList || null,
      carChassis: criteria.chassisList || null,
      carEngine: criteria.enginesList || null,
      text: criteria.text || null,
      isSet: criteria.isSet && true,
      inStock: criteria.inStock && true,
      hasImage: Number(criteria.imagesPresence) ? true : null,
      partPlacement: (() => {
        if (!criteria.placementList) {
          return [];
        }
        return criteria.placementList.map((placement: string) => {
          switch (Number(placement)) {
            case GrpcPlacement.PLACEMENT_BOTTOM: return Placement.Bottom;
            case GrpcPlacement.PLACEMENT_TOP:    return Placement.Top;
            case GrpcPlacement.PLACEMENT_LEFT:   return Placement.Left;
            case GrpcPlacement.PLACEMENT_RIGHT:  return Placement.Right;
            case GrpcPlacement.PLACEMENT_REAR:   return Placement.Rear;
            case GrpcPlacement.PLACEMENT_FRONT:  return Placement.Front;
            default: return Placement.Any;
          }
        });
      })(),
      skip: req.query.offset || null,
      first: req.query.limit || null,
      sort: (() => {
        const order = req.query.order;
        const defaultOrder = [ContractPartsSort.HasImageDesc, ContractPartsSort.ActualAtDesc];
        if (!order) {
          return defaultOrder;
        }
        switch (true) {
          case (order.field === 'price' && order.direction === 'asc'):  return [ContractPartsSort.CostAsc];
          case (order.field === 'price' && order.direction === 'desc'): return [ContractPartsSort.CostDesc];
          case (order.field === 'date' && order.direction === 'asc'):   return [ContractPartsSort.ActualAtAsc];
          case (order.field === 'date' && order.direction === 'desc'):  return [ContractPartsSort.ActualAtDesc];
          default: return defaultOrder;
        }
      })(),
    });

    utils.sendSuccess(res)({
      data: result.items.map(item => ({
        ...item,
        images: _sortBy(item.images, image => image.id),
      })),
      count: result.count,
    });
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }

});

export const filters = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const contractPartsRepository = new ContractPartsRepository();
  const expertAutosRepository = new ExpertAutosRepository();

  const shopRepository = new ShopRepository();
  const geoRepository = new GeoRepository();

  try {
    const result = await contractPartsRepository.getPartsFacetFilter(req.query);

    // reverse() - год выпуска авто в фильтре выводить начиная с большего
    result.yearsList = _sortBy(result.yearsList.filter(item => item.id), item => item.id).reverse();
    result.chassisList = _sortBy(result.chassisList.filter(item => item.slug), item => item.name);
    result.enginesList = _sortBy(result.enginesList.filter(item => item.slug), item => item.name);

    // const cityIdsList: GetPartsFacetFilterResponse.Option.AsObject[] = _get(result, 'cityIdsList', []);
    // const metroIdsList: GetPartsFacetFilterResponse.Option.AsObject[] = _get(result, 'metroIdsList', []);
    const geoIdsList = result.cityIdsList.concat(result.metroIdsList);

    const [
      shop,
      geo,
      carModel,
      carManufacturer,
    ] = await Promise.all([
      result.shopIdsList.length
        ? shopRepository
            .list({ id: result.shopIdsList.map(item => item.id) })
            .then(resolved => utils.remapIdentifiableCollections(resolved.data, result.shopIdsList))
        : Promise.resolve([]),
      geoIdsList.length
        ? geoRepository
            .list({ ids: geoIdsList.map(item => String(item.id)) })
            .then(resolved => utils.remapIdentifiableCollections(resolved.items, geoIdsList))
        : Promise.resolve([]),
      result.carModelIdsList.length
        ? expertAutosRepository
            .findModelsByIds(result.carModelIdsList.map(item => item.id))
            .then(resolved => sortFilterByPop(utils.remapIdentifiableCollections(resolved, result.carModelIdsList)))
        : Promise.resolve([]),
      expertAutosRepository
        .findManufacturersByIds(result.carManufacturerIdsList.map(item => item.id))
        .then(resolved => sortFilterByPop(utils.remapIdentifiableCollections(resolved, result.carManufacturerIdsList))),
    ]);

    delete result.shopIdsList;
    delete result.cityIdsList;
    delete result.metroIdsList;
    delete result.carManufacturerIdsList;
    delete result.carModelIdsList;

    utils.sendSuccess(res)({
      ...result,
      shop,
      carManufacturer,
      carModel,
      city: geo.filter(item => item.level === GeoLevel.City),
      metro: geo.filter(item => item.level === GeoLevel.Metro),
    });
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
});

export const getCarManufacturers = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const hasHidden = req.query.hasHidden === 'true';
  const expertAutosRepository = new ExpertAutosRepository();
  // Hotfix 1.29.4
  try {
    const result = await expertAutosRepository.findManufacturersByCriteria({ hasHidden });
    utils.sendSuccess(res)(result);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
});

export const getCarModels = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const carManufacturerId = Number(req.query.carManufacturer) || 0;
  const hasHidden = req.query.hasHidden === 'true';
  const expertAutosRepository = new ExpertAutosRepository();
  // Hotfix 1.29.4
  try {
    const result = await expertAutosRepository
      .findModelsByCriteria({ hasHidden, manufacturerIds: [carManufacturerId] });
    utils.sendSuccess(res)(result);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
});

export const countParts = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const count = await redis.asyncGet('contractPartsCount');
  if (count) {
    utils.sendSuccess(res)(count);
    return true;
  }

  const contractPartsRepository = new ContractPartsRepository();
  try {
    const result = await contractPartsRepository.countParts();
    redis.asyncSetex('contractPartsCount', config.get('redis.cacheAges.counters'), result);
    utils.sendSuccess(res)(result);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
});

export const countMerchants = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const count = await redis.asyncGet('contractMerchantsCount');
  if (count) {
    utils.sendSuccess(res)(count);
    return true;
  }

  const contractPartsRepository = new ContractPartsRepository();
  try {
    const result = await contractPartsRepository.countMerchants();
    redis.asyncSetex('contractMerchantsCount', config.get('redis.cacheAges.counters'), result);
    utils.sendSuccess(res)(result);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
});

export const getPartNameSuggestions = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  if (!req.query.text) {
    utils.sendSuccess(res)([]);
    return true;
  }

  const contractPartsRepository = new ContractPartsRepository();
  try {
    const result = await contractPartsRepository.getPartsSearchSuggestions(req.query.text);
    utils.sendSuccess(res)(result);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
});

const sortFilterByPop = <T extends CountableType>(values: T[], inputPopCount = 10): T[] => {
  if (!Array.isArray(values) || !values.length) {
    return [];
  }
  const input: T[] = [...values];
  let output: T[] = [];
  let popCount = inputPopCount;
  while (popCount && input.length) {
    const { total: max } = _maxBy(input, (item: { total: number }) => item.total)!;
    const maxIndex = input.findIndex((item: { total: number }) => item.total === max);
    output.push(input.splice(maxIndex, 1)[0]);
    popCount -= 1;
  }
  output = output.concat(input); // what's (if) left
  return output;
};
