import { Response } from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { Feedback } from '../entities/feedback';
import { FeedbackRepository } from '../repositories/feedback-repository';
const repository: FeedbackRepository = new FeedbackRepository();

export const create = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data: Feedback[] = await repository
      .createFeedback({
        ...req.body.data,
        userId: req.user ? req.user.id : null,
      })
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data);
  }),
];

export const addLike = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data: Feedback[] = await repository
      .addLike({
        ...req.body.data,
        userId: req.user ? req.user.id : null,
      })
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data && data[0]);
  }),
];
