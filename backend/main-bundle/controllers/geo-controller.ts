import {
  Request,
  Response,
} from 'express';
import * as utils from '../../common-bundle/utils';

import { GeoLevel } from '../../api-bundle/graphql/types';

import { GeoRepository } from '../repositories/geo-repository';

export const get = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const geoRepository = new GeoRepository();
  try {
    const result = await geoRepository.list({
      ids: req.query.ids && req.query.ids.length ? req.query.ids.map(String) : null,
      slugs: req.query.slugs && req.query.slugs.length ? req.query.slugs.map(String) : null,
      level: req.query.level ? req.query.level as GeoLevel : null,
      parentId: req.query.parentId || null,
    });
    utils.sendSuccess(res)(result.items);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
  }
});

export const getTree = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const geoRepository = new GeoRepository();

  try {
    const rawResult = await geoRepository.list();
    utils.sendSuccess(res)(rawResult.items
      .filter(country => country.level === GeoLevel.Country)
      .map(country => ({
        ...country,
        items: rawResult.items
          .filter(region => region.level === GeoLevel.Area && region.parentId === country.id)
          .map(region => ({
            ...region,
            items: rawResult.items.filter(city => city.level === GeoLevel.City && city.parentId === region.id),
          })),
      })));
  } catch (e) {
    utils.throwError(500, 'api error')(e);
  }
});

export const list = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const geoRepository = new GeoRepository();
  const level = req.query.level ? req.query.level as GeoLevel : null;

  try {
    const rawResult = await geoRepository.list();
    utils.sendSuccess(res)(rawResult.items
      .map(item => ({
        ...item,
        parent: rawResult.items.find(parent => parent.id === item.parentId) || null,
      }))
      .filter(item => !level || item.level === level));
  } catch (e) {
    utils.throwError(500, 'api error')(e);
  }
});

export const resolveIp = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const geoRepository = new GeoRepository();

  try {
    const ip = utils.getIpFromRequest(req);
    let result = null;
    if (ip && !utils.isBot(req)) {
      result = await geoRepository.resolve({ ip });
    }
    utils.sendSuccess(res)(result);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
  }
});
