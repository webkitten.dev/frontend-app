import { Request, Response } from 'express';
import config from 'config';

import redis from '../../common-bundle/redis';
import * as utils from '../../common-bundle/utils';

import { PartItemRepository } from '../repositories/part-item-repository';
import { PartSellerRepository } from '../repositories/part-seller-repository';
import { TyreWheelItemRepository } from '../repositories/tyre-wheel-item-repository';
import { TyreWheelSellerRepository } from '../repositories/tyre-wheel-seller-repository';
import { VinRequestSellerRepository } from '../repositories/vin-request-seller-repository';
import { ContractPartsRepository } from '../../api-bundle/grpc/repositories/contract-parts-repository';
import { CitySellerRepository } from '../repositories/city-seller-repository';

const partItemRepository = new PartItemRepository();
const partSellerRepository = new PartSellerRepository();
const tyreWheelItemRepository = new TyreWheelItemRepository();
const tyreWheelSellerRepository = new TyreWheelSellerRepository();
const vinRequestSellerRepository = new VinRequestSellerRepository();
const citySellerRepository = new CitySellerRepository();

export const counters = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const count = await redis.asyncGet('infoCounters');
  if (count) {
    utils.sendSuccess(res)(count);
    return true;
  }

  const contractPartsRepository = new ContractPartsRepository();

  try {
    const [
      contractItems,
      newItems,
      tyreWheelItems,
      contractSellers,
      newSellers,
      tyreWheelSellers,
      citiesTotal,
    ] = await Promise.all([
      contractPartsRepository.countParts(),
      partItemRepository.count(),
      tyreWheelItemRepository.count(),
      contractPartsRepository.countMerchants(),
      partSellerRepository.count(),
      tyreWheelSellerRepository.count(),
      citySellerRepository.count(),
    ]);

    const result = {
      citiesTotal,
      itemsTotal: contractItems + newItems + tyreWheelItems,
      sellersTotal: contractSellers + newSellers + tyreWheelSellers,
    };
    redis.asyncSetex('infoCounters', config.get('redis.cacheAges.counters'), result);
    utils.sendSuccess(res)(result);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
});
