import {
  Request,
  Response,
  NextFunction,
} from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { OrganizationRepository } from '../repositories/organization-repository';
import { OrganizationInn } from '../entities/organization-inn';
import { OrganizationInput } from '../entities/organization-input';
import { OrganizationBankInfo } from '../entities/organization-bank-info';
import { OrganizationShopStoList } from '../entities/organization-shop-sto-list';
const repository: OrganizationRepository = new OrganizationRepository();

export const getShopSto = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data: OrganizationShopStoList = await repository
      .getShopSto(Number(req.user!.id))
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data);
  }),
];

export const getByInn = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  let data: OrganizationInn | void;
  try {
    data = await repository.getByInn(
      req.query.requisiteId ? Number(req.query.requisiteId) : null,
      req.query.inn,
    );
  } catch (e) {
    switch (e.message) {
      // TODO: Часть сообщение [в службу поддержки] формируется на FE
      case 'E_ORGANIZATION_GET_BY_INN_ORGANIZATION_ISSET':
        utils.throwError(400, 'bad request')(new Error('Эта организация уже зарегистрирована, '
          + 'обратитесь к своему менеджеру'));
        break;
      case 'E_ORGANIZATION_GET_BY_INN_NOT_FOUND_INN':
        utils.throwError(400, 'bad request')(new Error('Организация с таким ИНН не найдена, '
          + 'обратитесь к своему менеджеру'));
        break;
      default:
        utils.throwError(500, 'api error')(e);
    }
  }
  utils.sendSuccess(res)(data);
});

export const getBankInfo = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: OrganizationBankInfo = await repository
    .getBankInfo(req.query.bik)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});

export const create = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    let data: OrganizationShopStoList | void;
    try {
      data = await repository.create(new OrganizationInput({
        userId: req.user!.id,
        shortOrganizationName: req.body.shortOrganizationName,
        fullOrganizationName: req.body.fullOrganizationName,
        inn: req.body.inn,
        kpp: req.body.kpp,
        ogrn: req.body.ogrn,
        managerName: req.body.managerName,
        managerPost: req.body.managerPost,
        bik: req.body.bik,
        correspondentAccount: req.body.correspondentAccount,
        bankAccount: req.body.bankAccount,
        bankName: req.body.bankName,
        showForCustomer: req.body.showForCustomer,
        legalAddress: req.body.legalAddress,
        factAddress: req.body.factAddress,
        postAddress: req.body.postAddress,
      }));
    } catch (e) {
      if (e.message === 'E_ORGANIZATION_CREATE_ORGANIZATION_ISSET') {
        const error = new Error('Эта организация уже зарегистрирована, '
          + 'обратитесь к своему менеджеру или в службу поддержки');
        utils.throwError(400, 'bad request')(error);
      } else {
        utils.throwError(500, 'api error')(e);
      }
    }
    utils.sendSuccess(res)(data);
  }),
];

export const update = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    let data: OrganizationShopStoList | void;
    try {
      data = await repository.update(new OrganizationInput({
        userId: req.user!.id,
        shortOrganizationName: req.body.shortOrganizationName,
        fullOrganizationName: req.body.fullOrganizationName,
        id: req.body.id,
        inn: req.body.inn,
        kpp: req.body.kpp,
        ogrn: req.body.ogrn,
        managerName: req.body.managerName,
        managerPost: req.body.managerPost,
        bik: req.body.bik,
        correspondentAccount: req.body.correspondentAccount,
        bankAccount: req.body.bankAccount,
        bankName: req.body.bankName,
        showForCustomer: req.body.showForCustomer,
        legalAddress: req.body.legalAddress,
        factAddress: req.body.factAddress,
        postAddress: req.body.postAddress,
      }));
    } catch (e) {
      if (e.message === 'E_ORGANIZATION_UPDATE_ORGANIZATION_ISSET') {
        const error = new Error('Эта организация уже зарегистрирована, '
          + 'обратитесь к своему менеджеру или в службу поддержки');
        utils.throwError(400, 'bad request')(error);
      } else {
        utils.throwError(500, 'api error')(e);
      }
    }
    utils.sendSuccess(res)(data);
  }),
];
