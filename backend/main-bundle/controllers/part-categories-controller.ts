import {
  Request,
  Response,
} from 'express';
import * as utils from '../../common-bundle/utils';

import {
  flatMap as _flatMap,
  uniq as _uniq,
  sortBy as _sortBy,
  get as _get,
} from 'lodash';

import {
  FindPartCriteria,
} from '@gisauto/part-group-service-proto/types_pb';
import {
  PartGroup,
  PartName,
} from '@gisauto/expert-service-proto/parts/types_pb';

import { GeoRepository } from '../repositories/geo-repository';
import { ExpertPartsRepository } from '../../api-bundle/grpc/repositories/expert-parts-repository';
import { ExpertAutosRepository } from '../../api-bundle/grpc/repositories/expert-autos-repository';
import { PartGroupRepository } from '../../api-bundle/grpc/repositories/part-group-repository';
import { SeoRepository } from '../repositories/seo-repository';
const seoRepository = new SeoRepository();

export const getAsyncData = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const locationId = req.query.locationId ? Number(req.query.locationId) : 0;

  const geoRepository = new GeoRepository();

  try {
    const limit = 20;
    const offset = 0;
    const order = {
      field: 'name',
      direction: 'asc',
    };

    const [
      partsCategories,
      urlParamsData,
      seoInfo,
    ] = await Promise.all([
      getTreeFunc(),
      resolveUrlSlugs(req.query.group, req.query.subgroup, req.query.partName, req.query.partManufacturer),
      seoRepository.getContents(req.query.url),
    ]);

    const urlParams = {
      group: _get(urlParamsData, 'group.id', ''),
      subgroup: _get(urlParamsData, 'subgroup.id', ''),
      partName: _get(urlParamsData, 'partName.id', ''),
      partManufacturer: _get(urlParamsData, 'partManufacturer.id', ''),
      city: locationId || 0,
    };
    const partsGridCriteria = getFindPartCriteriaFromReq(urlParams);

    const [
      { list, count, sourceCityId },
      filtersValues,
    ] = await Promise.all([
      getPartGridListFunc(partsGridCriteria, limit, offset, order),
      getPartGridFiltersFunc(partsGridCriteria),
    ]);

    utils.sendSuccess(res)({
      limit,
      offset,
      order,
      list,
      count,
      filtersValues,
      partsCategories,
      urlParamsData,
      seoInfo,
      noResultInUserCity: locationId ? sourceCityId !== locationId : false,
      targetLocation: locationId
        ? await geoRepository.list({ ids: [String(locationId)] }).then(resolved => resolved.items.shift() || null)
        : null,
    });
  } catch (e) {
    utils.throwError(500, 'api error')(e);
  }
});

export const getPartGridFilters = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  try {
    const partsGridCriteria = getFindPartCriteriaFromReq(req.query);
    utils.sendSuccess(res)(await getPartGridFiltersFunc(partsGridCriteria));
  } catch (e) {
    utils.throwError(500, 'api error')(e);
  }
});

export const getPartGridList = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const limit = req.query.limit ? Number(req.query.limit) : 1;
  const offset = req.query.offset ? Number(req.query.offset) : 0;
  const order = req.query.order || { field: null, direction: null };

  try {
    const partsGridCriteria = getFindPartCriteriaFromReq(req.query);
    utils.sendSuccess(res)(await getPartGridListFunc(partsGridCriteria, limit, offset, order));
  } catch (e) {
    utils.throwError(500, 'api error')(e);
  }
});

export const getTree = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  try {
    utils.sendSuccess(res)(await getTreeFunc());
  } catch (e) {
    utils.throwError(500, 'api error')(e);
  }
});

const getTreeFunc = async () => {
  const expertPartsRepository = new ExpertPartsRepository();
  const groups = await expertPartsRepository.findPartGroupsBySlugs();
  // Получаем id всех подгрупп
  const subgroupIds = _flatMap(groups, ({ groupsList }) => groupsList.map(i => i.id));
  // Получаем все partName для своих подгрупп
  const partNames =  await expertPartsRepository.findPartNamesBySubGroupIds(subgroupIds);

  const maxItemsCount = 30;

  // Формируем окончательное структуру с учетом partNames
  return _sortBy(
    groups.map((group) => {
      return {
        ...group,
        groupsList: group.groupsList
          .filter(subgroup => Boolean(subgroup.total))
          .map((subgroup) => {
            return {
              ...subgroup,
              groupsList: partNames.filter(part => part.groupsList[0] === subgroup.id),
            };
          })
          .reduce((acc: any[], subgroup) => {
            const length = acc.reduce((count, current) => (count + 1 + current.groupsList.length), 0);
            if (length < maxItemsCount) {
              if (subgroup.groupsList.length < maxItemsCount - length - 1) {
                acc.push(subgroup);
              } else {
                acc.push({
                  ...subgroup,
                  groupsList: subgroup.groupsList.slice(0, maxItemsCount - length - 1),
                });
              }
            }
            return acc;
          }, []),
      };
    }),
    group => (group.id === 23 ? 1 : (group.id === 24 ? -1 : 0)), // "прочее" в конец, "запчасти для то" в начало
  );
};

const resolveUrlSlugs = async (
  groupSlug?: string,
  subgroupSlug?: string,
  partNameSlug?: string,
  partManufacturerSlug?: string,
) => {
  const expertPartsRepository = new ExpertPartsRepository();
  const partGroupRepository = new PartGroupRepository();

  const [
    group,
    partName,
    partManufacturer,
  ] = await Promise.all([
    groupSlug
      ? expertPartsRepository
          .findPartGroupsBySlugs([groupSlug])
          .then(resolved => resolved.shift() || null)
      : null,
    partNameSlug
      ? expertPartsRepository
          .findPartNamesBySlugs([partNameSlug])
          .then(resolved => resolved.shift() || null)
      : null,
    partManufacturerSlug
      ? expertPartsRepository
          .findManufacturerBySlugs([partManufacturerSlug])
          .then(resolved => resolved.shift() || null)
      : null,
  ]);

  if (groupSlug && !group) {
    // TODO: Нормальные error сообщения
    utils.throwError(404, 'not found')(new Error(`group was not found by slug ${groupSlug}`));
  }

  let partNamePartManufacturers = null;
  if (partNameSlug) {
    if (!partName) {
      utils.throwError(404, 'not found')(new Error(`partName was not found by slug ${partNameSlug}`));
    }
    const partNamePartManufacturerIds = await partGroupRepository
      .findPartManufacturersByPartNameId(partName!.id)
      .then(resolved => resolved.map(item => item.id));
    partNamePartManufacturers = partNamePartManufacturerIds.length
      ? await expertPartsRepository.findManufacturerByIds(partNamePartManufacturerIds)
      : [];
  }

  if (partManufacturerSlug && !partManufacturer) {
    utils.throwError(404, 'not found')(new Error(`partManufacturer was not found by slug ${partManufacturerSlug}`));
  }

  let subgroup: PartGroup.AsObject | null = null;
  let subgroupPartNames: PartName.AsObject[] = [];
  if (group && subgroupSlug) {
    subgroup = group.groupsList.find(subgroup => subgroup.slug === subgroupSlug) || null;
    if (subgroup === null) {
      utils.throwError(404, 'not found')(new Error(`subgroup was not found by slug ${subgroupSlug}`));
    }
    subgroupPartNames = await expertPartsRepository.findPartNamesBySubGroupIds([subgroup!.id]);
  }

  return {
    partManufacturer,
    group: group ? {
      ...group,
      groupsList: group.groupsList.filter(subgroup => subgroup.total),
    } : null,
    subgroup: subgroup ? {
      ...subgroup,
      groupsList: subgroupPartNames,
    } : null,
    partName: partName ? {
      ...partName,
      groupsList: partNamePartManufacturers!.map(manufacturer => ({
        id: manufacturer.id,
        name: manufacturer.brand,
        slug: manufacturer.slug,
      })),
    } : null,
  };
};

const getPartGridFiltersFunc = async (criteria: FindPartCriteria) => {
  if (!criteria.getGroupId()) {
    return null;
  }

  const expertPartsRepository = new ExpertPartsRepository();
  const expertAutosRepository = new ExpertAutosRepository();
  const partGroupRepository = new PartGroupRepository();

  // данные для фильтров
  const partGridFiltersResult = await partGroupRepository.findFilterByCriteria(criteria);

  const partManufacturerIds = _uniq(partGridFiltersResult.partManufacturerIdsList.map(part => part.id));
  const carManufacturerIds = _uniq(partGridFiltersResult!.carManufacturerIdsList.map(item => item.id));
  const partIds = _uniq(partGridFiltersResult!.partIdsList.map(item => item.id));
  const partNameIds = _uniq(partGridFiltersResult!.partNameIdsList.map(item => item.id));

  // ресолв производителей запчастей, производителей автомобилей, запчастей, имён запчастей, городов
  const [
    partManufacturersResolveResult,
    carManufacturersResolveResult,
    partsResolveResult,
    partNamesResolveResult,
  ] = await Promise.all([
    partManufacturerIds.length ? expertPartsRepository.findManufacturerByIds(partManufacturerIds) : [],
    carManufacturerIds.length ? expertAutosRepository.findManufacturersByIds(carManufacturerIds) : [],
    partIds.length ? expertPartsRepository.findPartByIds(partIds) : [],
    partNameIds.length ? expertPartsRepository.findPartNamesByIds(partNameIds) : [],
  ]);

  return {
    carManufacturer: _sortBy(carManufacturersResolveResult, item => item.name)
      .map(item => ({ id: item.id, name: item.name })),
    partManufacturer: _sortBy(partManufacturersResolveResult, item => item.brand)
      .map(item => ({ id: item.id, name: item.brand })),
    partId: _sortBy(partsResolveResult, item => item.vendorCode)
      .map(item => ({ id: item.id, name: item.vendorCode })),
    partName: _sortBy(partNamesResolveResult, item => item.name)
      .map(item => ({ id: item.id, name: item.name })),
  };
};

const getPartGridListFunc = async (
  criteria: FindPartCriteria,
  limit: number,
  offset: number,
  order: {
    field: string | null,
    direction: string | null,
  },
) => {
  const expertPartsRepository = new ExpertPartsRepository();
  const expertAutosRepository = new ExpertAutosRepository();
  const partGroupRepository = new PartGroupRepository();

  // данные для грида: part_id, total, minPrice, maxPrice
  if (!criteria.getGroupId()) {
    return {
      sourceCityId: 0,
      count: 0,
      list: [],
    };
  }

  const partGridListResult = await partGroupRepository.findPartsByCriteria(criteria, limit, offset, order);

  if (!partGridListResult!.total) {
    return {
      sourceCityId: partGridListResult!.sourceCityId,
      count: partGridListResult!.total,
      list: partGridListResult!.data,
    };
  }

  const partIds = _uniq(partGridListResult!.data.map(part => part.id));
  const partsResolveResult = await expertPartsRepository.findPartByIds(partIds);
  const partManufacturerIds = _uniq(partsResolveResult.map(part => part.manufacturerId));

  // производителей запчастей; дополнение грида данными о применяемости
  const [
    partManufacturersResolveResult,
    partApplicabilityResult,
  ] = await Promise.all([
    expertPartsRepository.findManufacturerByIds(partManufacturerIds),
    expertAutosRepository.findTypesApplicability(partIds),
  ]);

  // мёржим результаты: информацию о запчасти по partId,
  // затем производителя по manufacturerId на информацию
  return {
    sourceCityId: partGridListResult!.sourceCityId,
    count: partGridListResult!.total,
    list: utils.remapIdentifiableCollections(partGridListResult!.data, partsResolveResult)
      .map((part) => {
        const manufacturer = partManufacturersResolveResult.find(manuf => manuf.id === part.manufacturerId)!;

        const applicabilityItem = partApplicabilityResult.find(([partId]) => partId === part.id);
        let applicability: string[] = [];
        if (applicabilityItem) {
          const [, { itemsList: applicabilityRaw }] = applicabilityItem;
          applicability = _uniq(applicabilityRaw.map(item => item.manufacturer));
        }

        return {
          ...part,
          manufacturer,
          applicability,
        };
      }),
  };
};

const getFindPartCriteriaFromReq = (req: { [key: string]: any }): FindPartCriteria => {
  if (!req) {
    return new FindPartCriteria();
  }

  const groupId = req.group ? Number(req.group) : null;
  const subgroupId = req.subgroup ? Number(req.subgroup) : null;
  const partNameId = req.partName ? Number(req.partName) : null;
  const partManufacturerId = req.partManufacturer ? Number(req.partManufacturer) : null;

  const criteria = new FindPartCriteria();
  groupId && criteria.setGroupId(groupId);
  subgroupId && criteria.setSubgroupId(subgroupId);
  partNameId && criteria.setPartNameId(partNameId);
  partManufacturerId && criteria.setPartManufacturerId(partManufacturerId);

  req.partId && criteria.setPartId(Number(req.partId));
  req.carManufacturer && criteria.setCarManufacturerId(Number(req.carManufacturer));
  criteria.setCityId(req.city ? Number(req.city) : 0);

  return criteria;
};
