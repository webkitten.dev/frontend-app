import {
  Request,
  Response,
} from 'express';

import * as utils from '../../common-bundle/utils';

import { PartGroupRepository } from '../repositories/part-group-repository';

const repository = new PartGroupRepository();

export const getPopular = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const data = await repository
    .getPopular()
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data.items);
});
