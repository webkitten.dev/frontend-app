import {
  Request,
  Response,
  NextFunction,
} from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { ExpertAutosRepository } from '../../api-bundle/grpc/repositories/expert-autos-repository';
import { PartRepository } from '../repositories/part-info-repository';
const repository = new PartRepository();

export const get = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data: object | void = await repository
      .get({
        ...req.query.criteria,
        geoId: Number(req.query.criteria.geoId) || null,
        userId: req.user ? req.user.id : null,
      })
      .catch((e) => {
        if (e.message === 'E_PART_GET_PART_NOT_FOUND') {
          utils.throwError(404, 'not found')(new Error('Запчасть не найдена'));
        } else {
          utils.throwError(500, 'api error')(new Error(e.message));
        }
      });
    utils.sendSuccess(res)(data);
  }),
];

export const getApplicability = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  if (!req.query.id) {
    utils.throwError(400, 'bad request')(new Error('no part id specified'));
  }
  const id = Number(req.query.id);

  const expertAutosRepository = new ExpertAutosRepository();

  try {
    const rawResult = await expertAutosRepository.findTypesApplicability([id]);
    const [, { itemsList: result }] = rawResult
      .find(([partId]) => partId === id) || [null, { itemsList: [] }];
    utils.sendSuccess(res)(result);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
  }
});
