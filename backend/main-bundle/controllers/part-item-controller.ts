import {
  Request,
  Response,
  NextFunction,
} from 'express';
import {
  get as _get,
} from 'lodash';
import config from 'config';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';
import redis from '../../common-bundle/redis';

import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';
import { PartItem } from '../entities/part-item';
import { FastOrder } from '../entities/fast-order';
import { StatSearchedPart } from '../entities/stat-searched-part';

import { PartItemRepository } from '../repositories/part-item-repository';
import { StatSearchedPartRepository } from '../repositories/stat-searched-part-repository';
import { GeoRepository } from '../repositories/geo-repository';
import { ExpertPartsRepository } from '../../api-bundle/grpc/repositories/expert-parts-repository';
import { NewPartRepository } from '../repositories/new-part-repository';

const repository = new PartItemRepository();
const statRepository = new StatSearchedPartRepository();
const geoRepository = new GeoRepository();
const newPartRepository = new NewPartRepository();

import {
  LoggerContainer,
  LoggerCategories,
} from '../../common-bundle/logger';
const logger = LoggerContainer.get(LoggerCategories.MONOLITH);

export const count = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const count = await redis.asyncGet('partItemCount');
  if (count) {
    utils.sendSuccess(res)(count);
    return true;
  }

  const data: number = await repository
    .count()
    .catch(utils.throwError(500, 'api error'));

  redis.asyncSetex('partItemCount', config.get('redis.cacheAges.counters'), data);
  utils.sendSuccess(res)(data);
});

export const getLastRequested = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const cookies = require('cookie-universal')(req, res, true);
    const data = await newPartRepository
      .getLastRequested({
        geoIds: cookies.get('currentLocation') ? [String(cookies.get('currentLocation'))] : null,
        isOrganization: _get(req, 'session.authUser.isOrganization', false),
      })
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data.items);
  }),
];

export const createFastOrder = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const currentLocation = +require('cookie-universal')(req, res, true).get('currentLocation') || 0;
    const object = new FastOrder({
      itemId: req.body.itemId,
      itemQuantity: req.body.itemQuantity,
      itemIsWholesale: req.body.itemIsWholesale,
      userId: req.user ? req.user.id : 0,
      customerEmail: req.body.customerEmail,
      customerPhone: req.body.customerPhone,
      customerName: req.body.customerName,
      customerCityId: req.body.customerCityId ? req.body.customerCityId : currentLocation || 0,
      customerComment:  req.body.customerComment,
    });
    const data: object | void  = await repository
      .createFastOrder(object)
      .catch((e) => {
        logger.error('createFastOrder error %j', e);
        if (e.message === 'E_PART_ITEM_CREATE_FAST_ORDER_NOT_AUTH_EMAIL_EXISTS') {
          const error: Error = new Error('Пользователь зарегистрирован и не авторизован');
          utils.throwError(401, 'email already exists')(error);
        } else {
          const error: Error = new Error('Временная ошибка, пожалуйста, повторите запрос позднее');
          utils.throwError(500, 'api error')(error);
        }
      });
    utils.sendSuccess(res)(data);
  }),
];

export const list = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    let userTimeZone = null;
    let openedNow = false;

    if (req.query.filterLocation) {
      ({ timezoneName: userTimeZone } = await geoRepository
        .list({ ids: [String(req.query.filterLocation)] })
        .then(resolved => resolved.items.shift() || { timezoneName: null })
        .catch(utils.throwError(500, 'api error'))
      );
    }

    if (req.query.criteria.availability !== 'AVAILABILITY_FOREIGN_ORIGINAL'
      && req.query.criteria.availability !== 'AVAILABILITY_FOREIGN_COUNTERPART') {
      openedNow = Boolean(Number(req.query.criteria.openedNow));
    }

    const data: EntityListResolveType<PartItem> = await repository
      .list(
        {
          userTimeZone,
          ...req.query.criteria,
          openedNow,
          userId: req.user ? req.user.id : null,
        },
        req.query.limit,
        req.query.offset,
        req.query.orderField,
        req.query.orderDirection,
      )
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)({
      ...data,
      type: req.query.criteria.hasOwnProperty('availability') && req.query.criteria.availability.length
        ? req.query.criteria.availability
        : 'AVAILABILITY_RESULT_ORIGINAL',
    });
  }),
];

export const filter = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const userId = req.user ? req.user.id : null;

    const statData: StatSearchedPart = new StatSearchedPart({
      userId,
      locationId: Number(_get(req.query, 'criteria.filterLocation', 0)),
      manufacturer: req.query.manufacturer,
      vendorCode: req.query.vendorCode,
      clientIp: utils.getIpFromRequest(req),
      referer: req.headers['referer'],
      sessionId: utils.getHashedSessionId(req),
      hash: null,
      userAgent: req.headers['user-agent'],
    });

    const data: { [key: string]: any } | void = await repository
      .filter({
        userId,
        manufacturer: req.query.manufacturer,
        vendorCode: req.query.vendorCode,
        ...{
          ...req.query.criteria,
          filterIsWholesale: null,
          filterMinPrice: null,
          filterMaxPrice: null,
          filterOpenedNow: null,
          userTimeZone: null,
        },
      })
      .catch((e) => {
        if (
          e.message === 'E_PART_ITEM_FILTER_PART_INVALID'
          || e.message === 'E_PART_ITEM_FILTER_PART_NOT_FOUND'
        ) {
          statRepository.create(statData).catch(() => {});
        }
        utils.throwError(500, 'api error')(e);
      });

    if (data && data.hash) {
      statData.hash = data.hash;
    }
    statRepository.create(statData).catch(() => {});

    utils.sendSuccess(res)(data);
  }),
];

export const popular = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data = await repository
    .popular()
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});

export const availability = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data = await repository
    .availability()
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});

export const getPartManufacturerBySlug = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const data = await new ExpertPartsRepository()
    .findManufacturerBySlugs([req.query.slug])
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data.shift());
});
