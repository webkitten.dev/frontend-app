import {
  Request,
  Response,
  NextFunction,
} from 'express';
import * as utils from '../../common-bundle/utils';

import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';
import { PartItemName } from '../entities/part-item-name';
import { PartItemNameRepository } from '../repositories/part-item-name-repository';
import { ExpertPartsRepository } from '../../api-bundle/grpc/repositories/expert-parts-repository';
const repository: PartItemNameRepository = new PartItemNameRepository();

export const get = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: EntityListResolveType<PartItemName> = await repository
    .list(req.query)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data.data);
});

export const getPartNameBySlug = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  if (!req.query.partNameSlug) {
    utils.throwError(400, 'bad request')(new Error('no partNameSlug requested'));
  }

  const expertPartRepository = new ExpertPartsRepository();

  try {
    const partName = await expertPartRepository
      .findPartNamesBySlugs([req.query.partNameSlug])
      .then(resolved => resolved.shift() || null);

    if (!partName) {
      utils.throwError(404, 'not found')(new Error(`partNameSlug ${req.query.partNameSlug} not found`));
    }

    utils.sendSuccess(res)(partName);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
});

export const getPartNameById = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  if (!req.query.partNameId) {
    utils.throwError(400, 'bad request')(new Error('no partNameId requested'));
  }

  const expertPartRepository = new ExpertPartsRepository();

  try {
    const partName = await expertPartRepository
      .findPartNamesByIds([req.query.partNameId])
      .then(resolved => resolved.shift() || null);

    if (!partName) {
      utils.throwError(404, 'not found')(new Error(`partNameId ${req.query.partNameId} not found`));
    }

    utils.sendSuccess(res)(partName);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
    return;
  }
});

export const getPartManufacturersByPartName = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  if (!req.query.partName) {
    utils.throwError(400, 'bad request')(new Error('no partName specified'));
  }
  const partName = String(req.query.partName);

  const expertPartRepository = new ExpertPartsRepository();

  try {
    const partNameResolve = await expertPartRepository.findPartNamesBySlugs([partName])
      .then(resolved => resolved.shift() || null);
    if (!partNameResolve) {
      utils.throwError(404, 'not found')(new Error(`partName was not found by slug ${partName}`));
    }

    const result = await expertPartRepository.findManufacturersByPartNameId(partNameResolve!.id);
    utils.sendSuccess(res)(result);
  } catch (e) {
    utils.throwError(500, 'api error')(e);
  }
});
