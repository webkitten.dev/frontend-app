import {
  Request,
  Response,
  NextFunction,
} from 'express';
import config from 'config';
import * as utils from '../../common-bundle/utils';
import redis from '../../common-bundle/redis';

import { PartSellerRepository } from '../repositories/part-seller-repository';
const repository: PartSellerRepository = new PartSellerRepository();

export const count = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const count = await redis.asyncGet('partSellerCount');
  if (count) {
    utils.sendSuccess(res)(count);
    return true;
  }

  const data: number = await repository
    .count()
    .catch(utils.throwError(500, 'api error'));

  redis.asyncSetex('partSellerCount', config.get('redis.cacheAges.counters'), data);
  utils.sendSuccess(res)(data);
});
