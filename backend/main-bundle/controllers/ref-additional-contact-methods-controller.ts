import {
  Request,
  Response,
  NextFunction,
} from 'express';
import * as utils from '../../common-bundle/utils';

import { RefAdditionalContactMethodsRepository } from '../repositories/ref-additional-contact-methods-repository';
import { Ref } from '../entities/ref';
const repository: RefAdditionalContactMethodsRepository = new RefAdditionalContactMethodsRepository();

export const get = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: Ref = await repository
    .get()
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});
