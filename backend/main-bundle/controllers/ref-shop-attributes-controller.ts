import {
  Request,
  Response,
  NextFunction,
} from 'express';
import * as utils from '../../common-bundle/utils';

import { RefShopAttributesRepository } from '../repositories/ref-shop-attributes-repository';
import { Ref } from '../entities/ref';
const repository: RefShopAttributesRepository = new RefShopAttributesRepository();

export const get = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: Ref = await repository
    .get()
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});
