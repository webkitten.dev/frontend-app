import {
  Request,
  Response,
} from 'express';
import * as utils from '../../common-bundle/utils';

import { ServiceGroup } from '../entities/service-group';
import { RefStoServiceGroups } from '../repositories/ref-sto-service-groups';
import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';

const repository: RefStoServiceGroups = new RefStoServiceGroups();

export const get = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const data: EntityListResolveType<ServiceGroup> = await repository
    .list()
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data.data);
});
