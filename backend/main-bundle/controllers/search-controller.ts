import { Response } from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { SearchRepository } from '../repositories/search-repository';

const repository = new SearchRepository();

export const list = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .list({
        query: req.query.query,
        userId: req.user ? Number(req.user.id) : null,
        geoId: req.cookies['currentLocation'] ? Number(req.cookies['currentLocation']) : null,
      }).catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data.data);
  }),
];
