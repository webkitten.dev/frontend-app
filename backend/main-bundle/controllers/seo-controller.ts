import {
  Request,
  Response,
  NextFunction,
} from 'express';
import * as utils from '../../common-bundle/utils';

import { SeoRepository } from '../repositories/seo-repository';
const repository: SeoRepository = new SeoRepository();

export const slogan = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: object = await repository
    .getSlogan()
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});

export const contents = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: object = await repository
    .getContents(req.query.url)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});
