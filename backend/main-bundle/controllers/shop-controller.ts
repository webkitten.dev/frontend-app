import {
  Request,
  Response,
} from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { ShopInput } from '../entities/shop-input';
import { ShopRepository } from '../repositories/shop-repository';
import { ShopRequestPhoneToConfirm } from '../entities/request-phone-send-to-confirm';
import { ShopRequestEmailToConfirm } from '../entities/request-email-send-to-confirm';
import { ShopRequestPhoneConfirm } from '../entities/request-phone-confirm';
import { ShopRequestEmailConfirm } from '../entities/request-email-confirm';

const repository = new ShopRepository();

export const infoGet = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const data = await repository
    .getShopInfo(Number(req.query.id), Number(req.query.organizationId))
    .catch((e) => {
      switch (e.message) {
        case 'E_SHOP_INFO_GET_NO_SHOP_FOUND':
        case 'E_SHOP_INFO_GET_NO_ORGANIZATION_FOUND':
        case 'E_SHOP_INFO_GET_NOT_VALID_ORGANIZATION_FOR_SHOP':
          const error: Error = new Error('Магазин не найден');
          utils.throwError(404, 'page not found')(error);
          break;
        default:
          utils.throwError(500, 'api error')(new Error(e.message));
      }
    });

  utils.sendSuccess(res)(data);
});

export const get = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const data = await repository
    .get({ id: [Number(req.query.id)] })
    .catch(utils.throwError(500, 'api error'));

  utils.sendSuccess(res)(data);
});

export const updateShop = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .updateShop(new ShopInput({
        userId: req.user!.id,
        shopId: Number(req.body.shopId),
        cityId: Number(req.body.city.id),
        legalId: Number(req.body.legalId),
        paymentMethods: req.body.paymentMethods,
        organizationId: Number(req.body.organizationId),
        shopName: req.body.shopName,
        shopAttributes: req.body.shopAttributes,
        email: req.body.email,
        phones: req.body.phones,
        contacts: req.body.contacts,
        street: req.body.street,
        building: req.body.building,
        office: req.body.office,
        metroId: Number(req.body.metroId),
        site: req.body.site,
        schedules: req.body.schedules,
        coordLat: Number(req.body.coordLat),
        coordLon: Number(req.body.coordLon),
        additionalInfo: req.body.additionalInfo,
        tradingMarks: req.body.tradingMarks,
        officialMarks: req.body.officialMarks,
        itemRedirect: Boolean(req.body.itemRedirect),
        orderRedirect: Boolean(req.body.orderRedirect),
        itemUrl: req.body.itemUrl,
      }))
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data);
  }),
];

export const createShop = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .createShop(new ShopInput({
        userId: req.user!.id,
        cityId: Number(req.body.city.id),
        legalId: Number(req.body.legalId),
        paymentMethods: req.body.paymentMethods,
        organizationId: Number(req.body.organizationId),
        shopName: req.body.shopName,
        email: req.body.email,
        phones: req.body.phones,
        contacts: req.body.contacts,
        street: req.body.street,
        building: req.body.building,
        office: req.body.office,
        shopAttributes: req.body.shopAttributes,
        metroId: Number(req.body.metroId),
        site: req.body.site,
        schedules: req.body.schedules,
        coordLat: Number(req.body.coordLat),
        coordLon: Number(req.body.coordLon),
        additionalInfo: req.body.additionalInfo,
        tradingMarks: req.body.tradingMarks,
        officialMarks: req.body.officialMarks,
        itemRedirect: Boolean(req.body.itemRedirect),
        orderRedirect: Boolean(req.body.orderRedirect),
        itemUrl: req.body.itemUrl,
      }))
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data);
  }),
];

export const sendPhoneToConfirm = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .sendPhoneToConfirm(new ShopRequestPhoneToConfirm({
        ...req.body,
        userId: req.user!.id,
      }))
      .catch((e) => {
        switch (e.message) {
          case 'E_SHOP_SEND_PHONE_TO_CONFIRM_NO_USER_FOUND':
            utils.throwError(401, 'NO_USER_FOUND')(new Error('NO_USER_FOUND'));
            break;
          case 'E_SHOP_SEND_PHONE_TO_CONFIRM_USER_IS_BLOCKED':
            utils.throwError(401, 'USER_IS_BLOCKED')(new Error('USER_IS_BLOCKED'));
            break;
          case 'E_SHOP_SEND_PHONE_TO_CONFIRM_SERVICE_CANT_SEND_SMS_TO_THIS_USER':
            utils.throwError(401, 'SEND_SMS_TO_THIS_USER')(new Error('SEND_SMS_TO_THIS_USER'));
            break;
          case 'E_SHOP_SEND_PHONE_TO_CONFIRM_NO_SHOP_FOUND':
            utils.throwError(401, 'NO_SHOP_FOUND')(new Error('NO_SHOP_FOUND'));
            break;
          case 'E_SHOP_SEND_PHONE_TO_CONFIRM_THIS_IS_NOT_USER_SHOP':
            utils.throwError(401, 'NO_SHOP_FOUND')(new Error('NO_SHOP_FOUND'));
            break;
          case 'E_SHOP_SEND_PHONE_TO_CONFIRM_NO_SHOP_PHONE_FOUND':
            utils.throwError(401, 'NO_SHOP_PHONE_FOUND')(new Error('NO_SHOP_PHONE_FOUND'));
            break;
          case 'E_SHOP_SEND_PHONE_TO_CONFIRM_NO_PHONE_IS_ACTUAL_CONFIRMED':
            utils.throwError(401, 'PHONE_IS_ACTUAL_CONFIRMED')(new Error('Номер телефона уже подтвержден'));
            break;
          case 'E_SHOP_SEND_PHONE_TO_CONFIRM_THIS_IS_NOT_PHONE_SHOP':
            utils.throwError(401, 'IS_NOT_PHONE_SHOP')(new Error('IS_NOT_PHONE_SHOP'));
            break;
          case 'E_USER_SEND_PHONE_TO_CONFIRM_SERVICE_CANT_SEND_SMS_TO_THIS_USER':
            utils.throwError(401, 'CANT_SEND_SMS_TO_THIS_USER')(new Error('CANT_SEND_SMS_TO_THIS_USER'));
            break;
          case 'E_USER_CHECK_UNIQUE_PHONE_PHONE_ISSET':
            utils.throwError(500, 'not valid phone')(new Error('Номер телефона занят'));
            break;
          default:
            utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
        }
      });

    utils.sendSuccess(res)(data);
  }),
];

export const phoneConfirm = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .phoneConfirm(new ShopRequestPhoneConfirm({
        ...req.body,
        userId: req.user!.id,
      }))
      .catch((e) => {
        switch (e.message) {
          case 'E_SHOP_CONFIRM_PHONE_NO_USER_FOUND':
            utils.throwError(401, 'NO_USER_FOUND')(new Error('CONFIRM_PHONE_NO_USER_FOUND'));
            break;
          case 'E_SHOP_CONFIRM_PHONE_NO_SHOP_FOUND':
            utils.throwError(401, 'NO_SHOP_FOUND')(new Error('CONFIRM_PHONE_NO_SHOP_FOUND'));
            break;
          case 'E_SHOP_CONFIRM_PHONE_THIS_IS_NOT_USER_SHOP':
            utils.throwError(401, 'IS_NOT_USER_SHOP')(new Error('CONFIRM_PHONE_THIS_IS_NOT_USER_SHOP'));
            break;
          case 'E_SHOP_CONFIRM_PHONE_NO_SHOP_PHONE_FOUND':
            utils.throwError(401, 'NO_SHOP_PHONE_FOUND')(new Error('CONFIRM_PHONE_NO_SHOP_PHONE_FOUND'));
            break;
          case 'E_SHOP_CONFIRM_PHONE_THIS_IS_NOT_PHONE_SHOP':
            utils.throwError(401, 'THIS_IS_NOT_PHONE_SHOP')(new Error('THIS_IS_NOT_PHONE_SHOP'));
            break;
          case 'E_SHOP_CONFIRM_PHONE_NOT_VALID_CONFIRMATION_INFO':
            utils.throwError(401, 'NOT_VALID_CONFIRMATION_INFO')(new Error('Неверный код подтверждения'));
            break;
          default:
            utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
        }
      });

    utils.sendSuccess(res)(data);
  }),
];

export const sendEmailToConfirm = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .sendEmailToConfirm(new ShopRequestEmailToConfirm({
        ...req.body,
        userId: req.user!.id,
      }))
      .catch((e) => {
        switch (e.message) {
          case 'E_SHOP_SEND_EMAIL_TO_CONFIRM_CONFIRM_EMAIL_NO_USER_FOUND':
            utils.throwError(500, 'NO_USER_FOUND')(new Error('NO_USER_FOUND'));
            break;
          case 'E_SHOP_SEND_EMAIL_TO_CONFIRM_NO_SHOP_FOUND':
            utils.throwError(500, 'NO_SHOP_FOUND')(new Error('NO_SHOP_FOUND'));
            break;
          case 'E_SHOP_SEND_EMAIL_TO_CONFIRM_THIS_IS_NOT_USER_SHOP':
            utils.throwError(500, 'THIS_IS_NOT_USER_SHOP')(new Error('THIS_IS_NOT_USER_SHOP'));
            break;
          case 'E_SHOP_SEND_EMAIL_TO_CONFIRM_SHOP_DONT_HAVE_EMAIL':
            utils.throwError(401, 'SHOP_DONT_HAVE_EMAIL')(new Error('SHOP_DONT_HAVE_EMAIL'));
            break;
          default:
            utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
        }
      });

    utils.sendSuccess(res)(data);
  }),
];

export const emailConfirm = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .emailConfirm(new ShopRequestEmailConfirm({
        ...req.body,
        userId: req.user!.id,
      }))
      .catch((e) => {
        switch (e.message) {
          case 'E_SHOP_EMAIL_CONFIRM_NO_USER_FOUND':
            utils.throwError(500, 'NO_USER_FOUND')(new Error('NO_USER_FOUND'));
            break;
          case 'E_SHOP_EMAIL_CONFIRM_NO_SHOP_FOUND':
            utils.throwError(500, 'NO_SHOP_FOUND')(new Error('NO_SHOP_FOUND'));
            break;
          case 'E_SHOP_EMAIL_CONFIRM_THIS_IS_NOT_USER_SHOP':
            utils.throwError(500, 'IS_NOT_USER_SHOP')(new Error('THIS_IS_NOT_USER_SHOP'));
            break;
          case 'E_SHOP_EMAIL_CONFIRM_SHOP_DONT_HAVE_EMAIL':
            utils.throwError(500, 'SHOP_DONT_HAVE_EMAIL')(new Error('SHOP_DONT_HAVE_EMAIL'));
            break;
          case 'E_SHOP_EMAIL_CONFIRM_NOT_VALID_CONFIRMATION_INFO':
            utils.throwError(401, 'NOT_VALID_CONFIRMATION_INFO')(new Error('NOT_VALID_CONFIRMATION_INFO'));
            break;
          default:
            utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
        }
      });

    utils.sendSuccess(res)(data);
  }),
];
