import { Response } from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { ShopOrderRepository } from '../repositories/shop-order-repository';
import { ShopOrder } from '../entities/shop-order';
import { CartItem } from '../entities/cart-item';
const repository: ShopOrderRepository = new ShopOrderRepository();

export const create = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const object = new ShopOrder({
      userId: req.user ? req.user.id : null,
      cartItemsList: req.body.cartItemsList.map((rawItem: CartItem) => new CartItem(rawItem)),
      customerName: req.body.customerName,
      customerCityId: req.body.customerCityId,
      customerEmail: req.body.customerEmail,
      customerPhone: req.body.customerPhone,
    });

    const data: object | void = await repository
      .create(object)
      .catch((e) => {
        if (e.message === 'E_CART_ORDER_NOT_AUTH_EMAIL_EXISTS') {
          const error: Error = new Error('Пользователь зарегистрирован и не авторизован');
          utils.throwError(401, 'email already exists')(error);
        } else {
          const error: Error = new Error('Временная ошибка, пожалуйста, повторите запрос позднее');
          utils.throwError(500, 'api error')(error);
        }
      });
    utils.sendSuccess(res)(data);
  }),
];
