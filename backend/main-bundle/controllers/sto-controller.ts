import { Response } from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { StoInput } from '../entities/sto-input';
import { StoRepository } from '../repositories/sto-repository';
import { StoRequestPhoneToConfirm } from '../entities/request-phone-send-to-confirm';
import { StoRequestEmailToConfirm } from '../entities/request-email-send-to-confirm';
import { StoRequestPhoneConfirm } from '../entities/request-phone-confirm';
import { StoRequestEmailConfirm } from '../entities/request-email-confirm';

const repository = new StoRepository();

export const get = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .get({ stoId: Number(req.query.id), userId: req.user!.id })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data);
  }),
];

export const updateSto = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .updateSto(new StoInput({
        userId: req.user!.id,
        stoId: Number(req.body.stoId),
        cityId: Number(req.body.city.id),
        legalId: Number(req.body.legalId),
        paymentMethods: req.body.paymentMethods,
        organizationId: Number(req.body.organizationId),
        stoName: req.body.stoName,
        email: req.body.email,
        phones: req.body.phones,
        contacts: req.body.contacts,
        street: req.body.street,
        building: req.body.building,
        office: req.body.office,
        metroId: Number(req.body.metroId),
        site: req.body.site,
        spares: req.body.spares,
        nonSpecifiedServiceGroup: req.body.nonSpecifiedServiceGroup,
        sendNotification: req.body.sendNotification,
        hourPriceMin: Number(req.body.hourPriceMin),
        hourPriceMax: Number(req.body.hourPriceMax),
        schedules: req.body.schedules,
        coordLat: Number(req.body.coordLat),
        coordLon: Number(req.body.coordLon),
        additionalInfo: req.body.additionalInfo,
        brands: req.body.brands,
        taskBrands: req.body.taskBrands,
        serviceGroup: req.body.serviceGroup,
        serviceGroupTask: req.body.serviceGroupTask,
        service: req.body.service,
        taskService: req.body.taskService,
      }))
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data);
  }),
];

export const createSto = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .createSto(new StoInput({
        userId: req.user!.id,
        cityId: Number(req.body.city.id),
        legalId: Number(req.body.legalId),
        paymentMethods: req.body.paymentMethods,
        organizationId: Number(req.body.organizationId),
        stoName: req.body.stoName,
        email: req.body.email,
        phones: req.body.phones,
        contacts: req.body.contacts,
        street: req.body.street,
        building: req.body.building,
        office: req.body.office,
        metroId: Number(req.body.metroId),
        site: req.body.site,
        spares: req.body.spares,
        nonSpecifiedServiceGroup: req.body.nonSpecifiedServiceGroup,
        sendNotification: req.body.sendNotification,
        hourPriceMin: Number(req.body.hourPriceMin),
        hourPriceMax: Number(req.body.hourPriceMax),
        schedules: req.body.schedules,
        coordLat: Number(req.body.coordLat),
        coordLon: Number(req.body.coordLon),
        additionalInfo: req.body.additionalInfo,
        brands: req.body.brands,
        taskBrands: req.body.taskBrands,
        serviceGroup: req.body.serviceGroup,
        serviceGroupTask: req.body.serviceGroupTask,
        service: req.body.service,
        taskService: req.body.taskService,
      }))
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data);
  }),
];

export const sendPhoneToConfirm = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .sendPhoneToConfirm(new StoRequestPhoneToConfirm({ ...req.body, userId: req.user!.id }))
      .catch((e) => {
        switch (e.message) {
          case 'E_STO_SEND_PHONE_TO_CONFIRM_NO_USER_FOUND':
            utils.throwError(401, 'not valid phone')(new Error('Не найден пользователь'));
            break;
          case 'E_STO_SEND_PHONE_TO_CONFIRM_USER_IS_BLOCKED':
            utils.throwError(401, 'not valid phone')(new Error('Не найден пользователь'));
            break;
          case 'E_STO_SEND_PHONE_TO_CONFIRM_SERVICE_CANT_SEND_SMS_TO_THIS_USER':
            utils.throwError(401, 'not valid phone')(new Error('Не найден пользователь'));
            break;
          case 'E_STO_SEND_PHONE_TO_CONFIRM_NO_SHOP_FOUND':
            utils.throwError(401, 'not valid phone')(new Error('Не найден пользователь'));
            break;
          case 'E_STO_SEND_PHONE_TO_CONFIRM_THIS_IS_NOT_USER_SHOP':
            utils.throwError(401, 'not valid phone')(new Error('Не найден пользователь'));
            break;
          case 'E_STO_SEND_PHONE_TO_CONFIRM_NO_SHOP_PHONE_FOUND':
            utils.throwError(401, 'not valid phone')(new Error('Не найден пользователь'));
            break;
          case 'E_STO_SEND_PHONE_TO_CONFIRM_NO_PHONE_IS_ACTUAL_CONFIRMED':
            utils.throwError(401, 'PHONE_IS_ACTUAL_CONFIRMED')(new Error('Номер телефона уже подтвержден'));
            break;
          case 'E_STO_SEND_PHONE_TO_CONFIRM_THIS_IS_NOT_PHONE_STO':
            utils.throwError(401, 'not valid phone')(new Error('Не найден пользователь'));
            break;
          case 'E_USER_SEND_PHONE_TO_CONFIRM_SERVICE_CANT_SEND_SMS_TO_THIS_USER':
            utils.throwError(401, 'not valid phone')(new Error('Не найден пользователь'));
            break;
          case 'E_USER_CHECK_UNIQUE_PHONE_PHONE_ISSET':
            utils.throwError(500, 'not valid phone')(new Error('Номер телефона занят'));
            break;
          default:
            utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
        }
      });

    utils.sendSuccess(res)(data);
  }),
];

export const phoneConfirm = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .phoneConfirm(new StoRequestPhoneConfirm({ ...req.body, userId: req.user!.id }))
      .catch((e) => {
        switch (e.message) {
          case 'E_STO_CONFIRM_PHONE_NO_USER_FOUND':
            utils.throwError(401, 'NO_USER_FOUND')(new Error('CONFIRM_PHONE_NO_USER_FOUND'));
            break;
          case 'E_STO_CONFIRM_PHONE_NO_SHOP_FOUND':
            utils.throwError(401, 'NO_SHOP_FOUND')(new Error('CONFIRM_PHONE_NO_SHOP_FOUND'));
            break;
          case 'E_STO_CONFIRM_PHONE_THIS_IS_NOT_USER_SHOP':
            utils.throwError(401, 'IS_NOT_USER_SHOP')(new Error('CONFIRM_PHONE_THIS_IS_NOT_USER_SHOP'));
            break;
          case 'E_STO_CONFIRM_PHONE_NO_SHOP_PHONE_FOUND':
            utils.throwError(401, 'NO_SHOP_PHONE_FOUND')(new Error('CONFIRM_PHONE_NO_SHOP_PHONE_FOUND'));
            break;
          case 'E_STO_CONFIRM_PHONE_THIS_IS_NOT_PHONE_STO':
            utils.throwError(401, 'THIS_IS_NOT_PHONE_STO')(new Error('THIS_IS_NOT_PHONE_STO'));
            break;
          case 'E_STO_CONFIRM_PHONE_NOT_VALID_CONFIRMATION_INFO':
            utils.throwError(401, 'NOT_VALID_CONFIRMATION_INFO')(new Error('Неверный код подтверждения'));
            break;
          default:
            utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
        }
      });

    utils.sendSuccess(res)(data);
  }),
];

export const sendEmailToConfirm = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .sendEmailToConfirm(new StoRequestEmailToConfirm({
        ...req.body,
        userId: req.user!.id,
      }))
      .catch((e) => {
        switch (e.message) {
          case 'E_STO_SEND_EMAIL_TO_CONFIRM_CONFIRM_EMAIL_NO_USER_FOUND':
            utils.throwError(500, 'NO_USER_FOUND')(new Error('NO_USER_FOUND'));
            break;
          case 'E_STO_SEND_EMAIL_TO_CONFIRM_NO_SHOP_FOUND':
            utils.throwError(500, 'NO_SHOP_FOUND')(new Error('NO_SHOP_FOUND'));
            break;
          case 'E_STO_SEND_EMAIL_TO_CONFIRM_THIS_IS_NOT_USER_SHOP':
            utils.throwError(500, 'THIS_IS_NOT_USER_SHOP')(new Error('THIS_IS_NOT_USER_SHOP'));
            break;
          case 'E_STO_SEND_EMAIL_TO_CONFIRM_SHOP_DONT_HAVE_EMAIL':
            utils.throwError(401, 'SHOP_DONT_HAVE_EMAIL')(new Error('SHOP_DONT_HAVE_EMAIL'));
            break;
          default:
            utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
        }
      });

    utils.sendSuccess(res)(data);
  }),
];

export const emailConfirm = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .emailConfirm(new StoRequestEmailConfirm({
        ...req.body,
        userId: req.user!.id,
      }))
      .catch((e) => {
        switch (e.message) {
          case 'E_STO_EMAIL_CONFIRM_NO_USER_FOUND':
            utils.throwError(500, 'NO_USER_FOUND')(new Error('NO_USER_FOUND'));
            break;
          case 'E_STO_EMAIL_CONFIRM_NO_SHOP_FOUND':
            utils.throwError(500, 'NO_SHOP_FOUND')(new Error('NO_SHOP_FOUND'));
            break;
          case 'E_STO_EMAIL_CONFIRM_THIS_IS_NOT_USER_SHOP':
            utils.throwError(500, 'IS_NOT_USER_SHOP')(new Error('THIS_IS_NOT_USER_SHOP'));
            break;
          case 'E_STO_EMAIL_CONFIRM_SHOP_DONT_HAVE_EMAIL':
            utils.throwError(500, 'SHOP_DONT_HAVE_EMAIL')(new Error('SHOP_DONT_HAVE_EMAIL'));
            break;
          case 'E_STO_EMAIL_CONFIRM_NOT_VALID_CONFIRMATION_INFO':
            utils.throwError(401, 'NOT_VALID_CONFIRMATION_INFO')(new Error('Неверный код подтверждения'));
            break;
          default:
            utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
        }
      });

    utils.sendSuccess(res)(data);
  }),
];
