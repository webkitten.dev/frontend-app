import {
  Request,
  Response,
  NextFunction,
} from 'express';
import * as utils from '../../common-bundle/utils';

import { TechSupportRepository } from '../repositories/tech-support-repository';
const repository: TechSupportRepository = new TechSupportRepository();

export const createRequest = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: object = await repository
    .createRequest(
      req.body.receiver,
      req.body.city,
      req.body.name,
      req.body.phone,
      req.body.email,
      req.body.message,
    )
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});
