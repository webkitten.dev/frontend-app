import {
  Request,
  Response,
} from 'express';
import {
  isEmpty as _isEmpty,
} from 'lodash';
import * as utils from '../../common-bundle/utils';

import { TyreItemRepository } from '../repositories/tyre-item-repository';
const repository: TyreItemRepository = new TyreItemRepository();

export const count = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const data = await repository
    .count(req.query.criteria)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});

export const tags = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const data = await repository
    .tags(req.query.criteria)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data.data);
});

export const filters = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const data = await repository
    .filters(req.query.criteria)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});

export const list = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  let criteria = req.query.criteria;
  if (_isEmpty(criteria)) {
    criteria = { isPopular: true };
  }
  const data = await repository
    .list(
      criteria,
      req.query.limit,
      req.query.offset,
      req.query.order && req.query.order.field,
      req.query.order && req.query.order.direction,
    )
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data.data);
});
