import {
  Request,
  Response,
  NextFunction,
} from 'express';
import config from 'config';
import * as utils from '../../common-bundle/utils';

import redis from '../../common-bundle/redis';
import { TyreWheelItemRepository } from '../repositories/tyre-wheel-item-repository';
const repository: TyreWheelItemRepository = new TyreWheelItemRepository();

export const count = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const count = await redis.asyncGet('tyreWheelCount');
  if (count) {
    utils.sendSuccess(res)(count);
    return true;
  }

  const data: number = await repository
    .count()
    .catch(utils.throwError(500, 'api error'));
  redis.asyncSetex('tyreWheelCount', config.get('redis.cacheAges.counters'), data);
  utils.sendSuccess(res)(data);
});
