import {
  Request,
  Response,
  NextFunction,
} from 'express';
import config from 'config';
import * as utils from '../../common-bundle/utils';

import redis from '../../common-bundle/redis';
import { TyreWheelSellerRepository } from '../repositories/tyre-wheel-seller-repository';
const repository: TyreWheelSellerRepository = new TyreWheelSellerRepository();

export const count = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const count = await redis.asyncGet('tyreWheelSellerCount');
  if (count) {
    utils.sendSuccess(res)(count);
    return true;
  }

  const data: number = await repository
    .count()
    .catch(utils.throwError(500, 'api error'));
  redis.asyncSetex('tyreWheelSellerCount', config.get('redis.cacheAges.counters'), data);
  utils.sendSuccess(res)(data);
});
