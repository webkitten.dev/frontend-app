import {
  Request,
  Response,
} from 'express';
import {
  get as _get,
  isEmpty as _isEmpty,
} from 'lodash';
import config from 'config';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import {
  LoggerContainer,
  LoggerCategories,
} from '../../common-bundle/logger';
const logger = LoggerContainer.get(LoggerCategories.APP);

import { ProfileNotification } from '../entities/profile-notification';
import { User } from '../entities/user';

import { UserRepository } from '../repositories/user-repository';
import { RequestUniqueAdditionalContact } from '../entities/request-unique-additional-contact';

const repository = new UserRepository();

export const get = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  if (req.query.hasOwnProperty('id')) {
    utils.throwError(503, 'access denied')(new Error('Security issue'));
    return;
  }

  const data = await repository
    .get(req.query)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});

export const getCounters = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .getCounters({ id: req.user!.id })
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data);
  }),
];

export const getNotificationSetup = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .getNotificationSetup(req.user!.id)
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data);
  }),
];

export const create = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const data = await repository
    .create(new User({
      username: req.body.username,
      password: req.body.password,
      isOrganization: req.body.isOrganization,
    }))
    .catch((e) => {
      let error: Error;
      switch (e.message) {
        case 'E_USER_CREATE_USER_ISSET':
          error = new Error('Указанный e-mail принадлежит другому Пользователю.');
          utils.throwError(401, 'user exists')(error);
          break;
        case 'E_USER_CREATE_NO_PASSWORD_OR_USERNAME':
          error = new Error('Необходимо ввести логин и пароль.');
          utils.throwError(400, 'empty password or username')(error);
          break;
        default:
          error = new Error('Временная ошибка, пожалуйста, повторите запрос позднее.');
          utils.throwError(500, 'api error')(error);
      }
    });
  utils.sendSuccess(res)(data);
});

export const createChatUser = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  try {
    const data = await repository.createChatUser(new User({
      username: req.body.username,
      firstName: req.body.firstName,
      phone: req.body.phone,
    }));

    req.session!.authUser = data;
    const token = jwt.sign(Number(data.id!), data.username!);
    const cookies = require('cookie-universal')(req, res);
    cookies.set(config.get('jwt.cookieName'), token, config.get('jwt.cookieOptions'));

    utils.sendSuccess(res)(data);
  } catch (e) {
    let error: Error;
    switch (e.message) {
      case 'E_USER_CREATE_CHAT_USER_PHONE_ISSET':
        error = new Error('Указанный телефонный номер принадлежит другому Пользователю.');
        utils.throwError(401, 'phone exists')(error);
        break;
      case 'E_USER_CREATE_CHAT_USER_USER_ISSET':
        error = new Error('Указанный e-mail принадлежит другому Пользователю.');
        utils.throwError(401, 'email exists')(error);
        break;
      default:
        error = new Error('Временная ошибка, пожалуйста, повторите запрос позднее.');
        utils.throwError(500, 'api error')(error);
    }
  }
});

export const createGuest = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  try {
    const data = await repository.createGuest(new User({
      firstName: req.body.firstName,
      phone: req.body.phone ? req.body.phone : null,
    }));

    req.session!.authUser = data;
    const token = jwt.sign(Number(data.id!), data.username!);
    const cookies = require('cookie-universal')(req, res);
    cookies.set(config.get('jwt.cookieName'), token, config.get('jwt.cookieOptions'));

    utils.sendSuccess(res)(true);
  } catch (e) {
    let error: Error;
    switch (e.message) {
      case 'E_USER_CREATE_GUEST_PHONE_ISSET':
        error = new Error('Указанный телефон принадлежит другому Пользователю.');
        utils.throwError(401, 'phone exists')(error);
        break;
      default:
        error = new Error('Временная ошибка, пожалуйста, повторите запрос позднее.');
        utils.throwError(500, 'api error')(error);
    }
  }
});

export const recoverPassword = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const data = await repository
    .recoverPassword(req.body.email)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});

export const setupNotification = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const notificationList: ProfileNotification[] = [];

    for (const rawInput of req.body.notificationList) {
      notificationList.push(new ProfileNotification({
        notificationId: +rawInput.notificationId,
        mainSwitch: rawInput.mainSwitch,
        mainMail: rawInput.mainMail,
        additionalMailSwitch: rawInput.additionalMailSwitch,
        additionalMailValue: rawInput.additionalMailValue,
      }));
    }

    const data = await repository
      .setupNotification(req.user!.id, notificationList)
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data);
  }),
];

export const getMyShops = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const userShops = await repository
      .getShops({ id: +req.user!.id })
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(userShops);
  }),
];

export const refresh = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    try {
      const data = await repository.get({ id: req.user!.id });

      if (_isEmpty(data)) {
        throw new Error('no user found');
      }

      req.session!.authUser = data;
      utils.sendSuccess(res)(data);
    } catch (error) {
      utils.throwError(500, 'api error')(error);
    }
  }),
];

export const fetch = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    logger.debug('fetch %j', req.user);
    if (!req.user || !req.user.id) {
      delete req.session!.authUser;
      utils.sendSuccess(res)(null);
      return;
    }

    if (Number(_get(req, 'session.authUser.id', '')) === req.user.id) {
      utils.sendSuccess(res)(req.session!.authUser);
      return;
    }

    try {
      const data = await repository.get({ id: req.user.id });
      req.session!.authUser = data;
      utils.sendSuccess(res)(req.session!.authUser);
      return;
    } catch (e) {
      delete req.session!.authUser;
    }
    utils.sendSuccess(res)(null);
  }),
];

export const update = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .updateUser({
        userId: req.user!.id,
        ...req.body,
      })
      .catch((e) => {
        let error: Error;
        if (e.message) {
          switch (e.message) {
            case 'E_USER_UPDATE_NOT_VALID_PASSWORD':
              utils.throwError(401, 'incorrect password')(new Error('Неверный пароль.'));
              break;
            case 'E_USER_UPDATE_PHONE_ISSET':
              error = new Error('Введенный телефон уже существует.');
              utils.throwError(401, 'duplicate phone number')(error);
              break;
            case 'E_USER_UPDATE_USER_ISSET':
              error = new Error('Введенный email уже существует.');
              utils.throwError(401, 'duplicate email')(error);
              break;
            default:
              utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
          }
        }
      });

    if (data && data.length) {
      const user = data[0];
      req.session!.authUser = user;
      utils.sendSuccess(res)(user);
    }
  }),
];

export const sendPhoneToConfirm = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .sendToConfirm(
        {
          userId: req.user!.id,
          ...req.body,
        },
        'Phone',
      )
      .catch((e) => {
        if (e.message) {
          switch (e.message) {
            case 'E_USER_SEND_PHONE_TO_CONFIRM_SERVICE_CANT_SEND_SMS_TO_THIS_USER':
              utils.throwError(502, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
              break;
            default:
              utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
          }
        }
      });

    utils.sendSuccess(res)(data);
  }),
];

export const sendEmailToConfirm = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .sendToConfirm(
        {
          userId: req.user!.id,
          ...req.body,
        },
        'Email',
      )
      .catch((e) => {

        if (e.message) {
          switch (e.message) {
            default:
              utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
          }
        }
      });

    utils.sendSuccess(res)(data);
  }),
];

export const confirmPhone = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .confirm(
        {
          userId: req.user!.id,
          ...req.body,
        },
        'Phone',
      )
      .catch((e) => {
        if (e.message) {
          switch (e.message) {
            case 'E_USER_CONFIRM_PHONE_NOT_VALID_CONFIRMATION_INFO':
              utils.throwError(401, 'invalid verify code')(new Error('Неверный код подтверждения'));
              break;
            default:
              utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
          }
        }
      });

    utils.sendSuccess(res)(data);
  }),
];

export const confirmEmail = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .confirm(
        {
          userId: req.user!.id,
          ...req.body,
        },
        'Email',
      )
      .catch((e) => {
        if (e.message) {
          switch (e.message) {
            case 'E_USER_CONFIRM_EMAIL_NOT_VALID_CONFIRMATION_INFO':
              utils.throwError(401, 'invalid verify code')(new Error('Невереный код подтверждения'));
              break;
            default:
              utils.throwError(500, 'api error')(new Error('Временная ошибка, пожалуйста, повторите запрос позднее'));
          }
        }
      });

    utils.sendSuccess(res)(data);
  }),
];

export const checkUniquePhone = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .checkUniquePhone(req.query.phone, {
        ...req.query.additionalInformationRequest,
        userId: req.user!.id,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data);
  }),
];

export const checkUniqueEmail = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .checkUniqueEmail({
        ...req.query,
        userId: req.user!.id,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data);
  }),
];

export const checkUniqueAdditionalContact = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .checkUniqueAdditionalContact(new RequestUniqueAdditionalContact({
        contactTypeId:  req.query.contactTypeId && Number(req.query.contactTypeId),
        contact:  req.query.contact && req.query.contact,
        userId: req.user!.id,
      }))
      .catch(utils.throwError(500, 'api error'));
    utils.sendSuccess(res)(data);
  }),
];

export const checkRecoveryToken = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  const data = await repository
    .checkRecoveryToken(req.query.token)
    .then(
      utils.throwIf(r => _isEmpty(r), 404, 'not found', new Error('Токен не найден')),
      utils.throwError(500, 'api error'),
    );
  utils.sendSuccess(res)(data);
});

export const recoverPasswordByToken = utils.wrapAsyncMethod(async (req: Request, res: Response) => {
  await repository
    .recoverPasswordByToken(req.body.token, req.body.password)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)({ success: true });
});
