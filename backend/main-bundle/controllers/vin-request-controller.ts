import {
  Request,
  Response,
  NextFunction,
} from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { VinRequestRepository } from '../repositories/vin-request-repository';
import { VinRequest } from '../entities/vin-request';
import { VinRequestElementPartInput } from '../entities/vin-request-element';

const repository: VinRequestRepository = new VinRequestRepository();

export const create = [
  jwt.middlewareAnonym,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const partList: VinRequestElementPartInput[] = [];

    for (const rawPart of req.body.partList) {
      const part = new VinRequestElementPartInput({
        partNameId: rawPart.partName.id ? Number(rawPart.partName.id) : null,
        partName: rawPart.partName.name,
        partVendorCode: rawPart.partVendorCode ? rawPart.partVendorCode : '',
        partCondition: +rawPart.partCondition.id,
        partQuantity: +rawPart.partQuantity,
      });
      partList.push(part);
    }

    const object = new VinRequest({
      partList,
      vin: req.body.vin,
      carManufacturerId: +req.body.carManufacturerId,
      carModelId: +req.body.carModelId,
      carBody: req.body.carBody,
      carEngine: req.body.carEngine,
      carReleaseYear: +req.body.carReleaseYear,
      userId: req.user ? req.user.id : 0,
      customerName: req.body.customerName,
      customerCityId: req.body.customerCityId ? +req.body.customerCityId.id : 0,
      customerEmail: req.body.customerEmail,
      customerPhone: req.body.customerPhone,
    });

    const data: object | void = await repository
      .create(object)
      .catch((e) => {
        if (e.message === 'E_VINREQUEST_CREATE_NOT_AUTH_EMAIL_EXISTS') {
          const error: Error = new Error('Пользователь зарегистрирован и не авторизован');
          utils.throwError(401, 'email already exists')(error);
        } else {
          const error: Error = new Error('Временная ошибка, пожалуйста, повторите запрос позднее');
          utils.throwError(500, 'api error')(error);
        }
      });
    utils.sendSuccess(res)(data);
  }),
];

export const vinInfo = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  const data: object = await repository
    .vinInfo(req.query)
    .catch(utils.throwError(500, 'api error'));
  utils.sendSuccess(res)(data);
});
