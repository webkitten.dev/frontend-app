import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class Abuse extends BaseEntity<Abuse> {
  userId: number | undefined | null;
  shopId: number | undefined;
  comment: string | undefined;
}
