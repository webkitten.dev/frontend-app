import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class BooleanResult extends BaseEntity<BooleanResult> {
  success: boolean | undefined;
}
