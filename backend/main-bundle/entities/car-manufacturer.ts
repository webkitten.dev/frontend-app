import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class CarManufacturer extends BaseEntity<CarManufacturer> {
  id: number | undefined;

  brand: string | undefined;
}
