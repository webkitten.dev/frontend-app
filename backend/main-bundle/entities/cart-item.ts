import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class CartItem {
  public itemId: number | null = null;
  public shopId: number | null = null;
  public organizationId: number | null = null;
  public userId: number | null = null;
  public isWholesalePrice: boolean | null = null;
  public inStock: boolean | null = null;
  public manufacturerId: number | null = null;
  public brand: string | null = null;
  public vendorCode: string | null = null;
  public name: string | null = null;
  public nameStandard: string | null = null;
  public commentName: string | null = null;
  public quantity: number | null = null;
  public maxQuantity: number | null = null;
  public price: number | null = null;
  public seller: string | null = null;
  public city: string | null = null;
  public address: string | null = null;
  public customerComment: string | null = null;
  public createdAt: string | null = null;
  public delivery: string | null = null;
  public deliveryString: string | null = null;
  public deliveryFrom: number | null = null;
  public deliveryTo: number | null = null;
  public deliveryRetailCondition: string | null = null;
  public deliveryWholesaleCondition: string | null = null;
  public charCode: string | null = null;
  public discountCheck: number | null = null;

  [key: string]: any;
  constructor(properties: CartItem) {
    for (const property in properties) {
      if (this.hasOwnProperty(property)) {
        // TODO: Найти лучший способ чем явное приведение типов.
        switch (true) {
          case (property === 'itemId'):
          case (property === 'shopId'):
          case (property === 'organizationId'):
          case (property === 'userId'):
          case (property === 'manufacturerId'):
          case (property === 'quantity'):
          case (property === 'maxQuantity'):
          case (property === 'price'):
          case (property === 'deliveryFrom'):
          case (property === 'deliveryTo'):
          case (property === 'discountCheck'):
            this[property] = properties[property]
              ? +properties[property]
              : 0;
            break;
          case (property === 'brand'):
          case (property === 'vendorCode'):
          case (property === 'name'):
          case (property === 'nameStandard'):
          case (property === 'commentName'):
          case (property === 'seller'):
          case (property === 'city'):
          case (property === 'address'):
          case (property === 'customerComment'):
          case (property === 'createdAt'):
          case (property === 'delivery'):
          case (property === 'deliveryRetailCondition'):
          case (property === 'deliveryWholesaleCondition'):
          case (property === 'charCode'):
            this[property] = properties[property]
              ? properties[property].toString()
              : '';
            break;
          default:
            this[property] = properties[property];
        }
      }
    }
  }
}
