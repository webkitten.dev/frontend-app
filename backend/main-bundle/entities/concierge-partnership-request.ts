import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class ConciergePartnershipRequest extends BaseEntity<ConciergePartnershipRequest> {
  phone: string | undefined;
  userId: number | undefined | null;
  geoTagId: number | undefined | null;
  userIp: string | undefined;
  organizationCityId: number | undefined;
  email: string | undefined | null;
  contactPerson: string | undefined | null;
}
