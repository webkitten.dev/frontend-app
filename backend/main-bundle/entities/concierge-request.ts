import { BaseEntity } from '../../common-bundle/essence/base-entity';

export enum ConciergeRequestProblem {
  PROBLEM_SEARCH_PARTS,
  PROBLEM_SEARCH_AUTOSERVICE,
  PROBLEM_OTHER,
}

export class ConciergeRequest extends BaseEntity<ConciergeRequest> {
  id: number | undefined;
  phone: string | undefined;
  problem: ConciergeRequestProblem | undefined;
  userId: number | undefined | null;
  geoTagId: number | undefined | null;
  userIp: string | undefined;
}
