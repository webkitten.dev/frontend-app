import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class FastOrder extends BaseEntity<FastOrder> {
  itemId: number | undefined;
  itemQuantity: number | undefined;
  itemIsWholesale: boolean | undefined;
  userId: number | undefined;
  customerEmail: string | undefined;
  customerPhone: string | undefined;
  customerName: string | undefined;
  customerCityId: number | undefined;
  customerComment: string | undefined;
}
