import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class FeedbackAddLike extends BaseEntity<FeedbackAddLike> {
  public userId: number | undefined;
  public feedbackId: number | undefined;
}
