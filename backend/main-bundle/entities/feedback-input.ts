import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class FeedbackInput extends BaseEntity<FeedbackInput> {
  public userId: number |  undefined;
  public shopId: number |  undefined;
  public recipientUserId: number | undefined;
  public rating: number | undefined;
  public comment: string | undefined;
}
