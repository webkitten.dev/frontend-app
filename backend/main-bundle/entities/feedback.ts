import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class Feedback extends BaseEntity<Feedback> {
  public id: number | undefined;
  public rating: number | undefined;
  public senderUserName: string | undefined;
  public senderComment: string | undefined;
  public approved: boolean | undefined;
  public isRead: boolean | undefined;
  public createdAt: string | undefined;
  public senderOrganizationLegalName: string | undefined;
  public answersCount: number | undefined;
  public senderClaim: string | undefined;
  public likersCount: number | undefined;
  public senderClaimStatus: string | undefined;
}
