import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class GeoTag extends BaseEntity<GeoTag> {
  public id!: number;
  public name: string | undefined;
  public parent: GeoTag | undefined;
  public children: GeoTag[] | undefined;
  public level: number | undefined;
  public lft: number | undefined;
  public rgt: number | undefined;
  public coordLat: number | undefined;
  public coordLng: number | undefined;
  public boundSouth: number | undefined;
  public boundNorth: number | undefined;
  public boundWest: number | undefined;
  public boundEast: number | undefined;
  public slug: String | undefined;
  public prepositionalName: String | undefined;
  public timezoneId: String | undefined;
  public timezoneOffset: String | undefined;
}
