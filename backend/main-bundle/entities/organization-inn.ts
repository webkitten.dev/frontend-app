import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class OrganizationInn extends BaseEntity<OrganizationInn> {
  userId: number | undefined;
  shortOrganizationName: string | undefined;
  fullOrganizationName: string | undefined;
  inn: string | undefined;
  kpp: string | undefined;
  ogrn: string | undefined;
  managerName: string | undefined;
  managerPost: string | undefined;
  bik: string | undefined;
  correspondentAccount: string | undefined;
  bankAccount: string | undefined;
  bankName: string | undefined;
  showForCustomer: boolean | undefined;
  legalAddress: string | undefined;
  factAddress: string | undefined;
  postAddress: string | undefined;
}
