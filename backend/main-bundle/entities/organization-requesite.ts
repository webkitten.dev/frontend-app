import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class OrganizationRequesite extends BaseEntity<OrganizationRequesite> {
  id: number | undefined;
  shortOrganizationName: string | undefined;
  fullOrganizationName: string | undefined;
  inn: string | undefined;
  kpp: string | undefined;
  ogrn: string | undefined;
  bik: string | undefined;
  managerName: string | undefined;
  managerPost: string | undefined;
  correspondentAccount: string | undefined;
  bankAccount: string | undefined;
  bankName: string | undefined;
  showForCustomer: string | undefined;
  legalAddress: string | undefined;
  factAddress: string | undefined;
  postAddress: string | undefined;
}
