import { BaseEntity } from '../../common-bundle/essence/base-entity';
import { Shop } from './shop';
import { Sto } from './sto';

export class Organization extends BaseEntity<Organization> {
  public shops: Shop[] | undefined;
  public autoServices: Sto[] | undefined;
}
