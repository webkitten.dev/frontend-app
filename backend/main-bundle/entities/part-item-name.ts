import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class PartItemName extends BaseEntity<PartItemName> {
  public id: number | undefined;
  public name: string | undefined;
}
