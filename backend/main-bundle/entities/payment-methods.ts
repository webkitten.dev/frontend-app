import { BaseEntity } from '../../common-bundle/essence/base-entity';
import { Shop } from './shop';

export class Organization extends BaseEntity<Organization> {
  public shops: Shop[] | undefined;
}
