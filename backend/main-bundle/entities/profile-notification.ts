import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class ProfileNotification extends BaseEntity<ProfileNotification> {
  notificationId: number | undefined;
  mainSwitch: boolean | undefined;
  mainMail: boolean | undefined;
  additionalMailSwitch: boolean | undefined;
  additionalMailValue: string | undefined;
}
