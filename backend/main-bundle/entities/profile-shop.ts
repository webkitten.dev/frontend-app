import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class ProfileShop extends BaseEntity<ProfileShop> {
  id: number | undefined;
  shopName: string | undefined;
  shopEmail: string | undefined;
  shopSite: string | undefined;
  shopAdditionalInfo: string | undefined;
  isMainShop: boolean | undefined;
  legal: string | undefined;
  city: string | undefined;
  metro: string | undefined;
  coordLat: number | undefined;
  coordLng: number | undefined;
  street: string | undefined;
  building: string | undefined;
  office: string | undefined;
  shopScheduleString: string | undefined;
  mainShop: number | undefined;

}
