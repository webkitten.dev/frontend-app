import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class Ref extends BaseEntity<Ref> {
  id: number | undefined;

  name: string | undefined;

  code: string | undefined;
}
