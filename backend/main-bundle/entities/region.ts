import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class Region extends BaseEntity<Region> {
  public id: number | undefined;
  public parent: number | undefined;
  public name: string | undefined;
  public level: number | undefined;
  public coords: { lat: number, lng: number } | undefined;
  public items: Region | undefined;
  public counts: { shop: number, sto: number } | undefined;
}
