export class RequestEmailConfirm {
  public userId: number | undefined;
  public code: string | undefined;

  constructor(model: RequestEmailConfirm) {
    this.userId = model.userId;
    this.code = model.code;
  }
}

export class ShopRequestEmailConfirm extends RequestEmailConfirm {
  public shopId: number | undefined;

  constructor(model: ShopRequestEmailConfirm) {
    super(model);
    this.shopId = model.shopId;
  }
}

export class StoRequestEmailConfirm extends RequestEmailConfirm {
  public stoId: number | undefined;

  constructor(model: StoRequestEmailConfirm) {
    super(model);
    this.stoId = model.stoId;
  }
}
