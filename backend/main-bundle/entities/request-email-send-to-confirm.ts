export class RequestEmailSendToConfirm {
  public userId: number | undefined;

  constructor(model: RequestEmailSendToConfirm) {
    this.userId = model.userId;
  }
}

export class ShopRequestEmailToConfirm extends RequestEmailSendToConfirm {
  public shopId: number | undefined;

  constructor(model: ShopRequestEmailToConfirm) {
    super(model);
    this.shopId = model.shopId;
  }
}

export class StoRequestEmailToConfirm extends RequestEmailSendToConfirm {
  public stoId: number | undefined;

  constructor(model: StoRequestEmailToConfirm) {
    super(model);
    this.stoId = model.stoId;
  }
}
