export class RequestPhoneConfirm {
  public userId: number | undefined;
  public phoneId: number | undefined;
  public code: string | undefined;

  constructor(model: RequestPhoneConfirm) {
    this.phoneId = model.phoneId;
    this.userId = model.userId;
    this.code = model.code;
  }
}

export class ShopRequestPhoneConfirm extends RequestPhoneConfirm {
  public shopId: number | undefined;

  constructor(model: ShopRequestPhoneConfirm) {
    super(model);
    this.shopId = model.shopId;
  }
}

export class StoRequestPhoneConfirm extends RequestPhoneConfirm {
  public stoId: number | undefined;

  constructor(model: StoRequestPhoneConfirm) {
    super(model);
    this.stoId = model.stoId;
  }
}
