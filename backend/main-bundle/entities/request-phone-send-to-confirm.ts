export class RequestPhoneSendToConfirm {
  public userId: number | undefined;
  public phoneId: number | undefined;

  constructor(model: RequestPhoneSendToConfirm) {
    this.phoneId = model.phoneId;
    this.userId = model.userId;
  }
}

export class ShopRequestPhoneToConfirm extends RequestPhoneSendToConfirm {
  public shopId: number | undefined;

  constructor(model: ShopRequestPhoneToConfirm) {
    super(model);
    this.shopId = model.shopId;
  }
}

export class StoRequestPhoneToConfirm extends RequestPhoneSendToConfirm {
  public stoId: number | undefined;

  constructor(model: StoRequestPhoneToConfirm) {
    super(model);
    this.stoId = model.stoId;
  }
}
