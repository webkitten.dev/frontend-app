import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class RequestUniqueAdditionalContact extends BaseEntity<RequestUniqueAdditionalContact> {
  public contact: string | undefined;
  public contactTypeId: number | undefined;
  public userId: number | undefined;
  public shopId?: number | undefined;
  public stoId?: number | undefined;
}
