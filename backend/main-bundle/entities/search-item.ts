import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class SearchItem extends BaseEntity<SearchItem> {
  partId?: number;
  partName?: string;
  brand?: string;
  vendorCode?: string;
  carManufacturers?: string[];
  userLocationCount?: number;
  allOffersCount?: number;
  partImages?: string[];
}
