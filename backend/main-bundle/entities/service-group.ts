import { BaseEntity } from '../../common-bundle/essence/base-entity';
import { Ref } from '../entities/ref';

export class ServiceGroup extends BaseEntity<ServiceGroup> {
  id: number | undefined;

  name: string | undefined;

  services: Ref[] | undefined;
}
