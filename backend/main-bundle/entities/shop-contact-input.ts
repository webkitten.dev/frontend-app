import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class ShopContactInput extends BaseEntity<ShopContactInput> {
  contactTypeId: number | undefined;
  contactValue: string | undefined;
}
