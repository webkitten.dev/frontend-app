import { BaseEntity } from '../../common-bundle/essence/base-entity';
import { ShopPhoneInput } from './shop-phone-input';
import { ShopContactInput } from './shop-contact-input';
import { ShopScheduleInput } from './shop-schedule-input';

export class ShopInput extends BaseEntity<ShopInput> {
  userId: number | undefined;
  shopId: number | undefined;
  cityId: number | undefined;
  legalId: number | undefined;
  paymentMethods: number[] | undefined;
  organizationId: number | undefined;
  shopName: string | undefined;
  email: string | undefined;
  phones: ShopPhoneInput[] | undefined;
  contacts: ShopContactInput[] | undefined;
  shopAttributes: number[] | undefined;
  street: string | undefined;
  building: string | undefined;
  office: string | undefined;
  metroId: number | undefined;
  site: string | undefined;
  schedules: ShopScheduleInput[] | undefined;
  coordLat: number | undefined;
  coordLon: number | undefined;
  additionalInfo: string | undefined;
  tradingMarks: number[] | undefined;
  officialMarks: number[] | undefined;
  itemRedirect?: boolean;
  orderRedirect?: boolean;
  itemUrl?: string;
}
