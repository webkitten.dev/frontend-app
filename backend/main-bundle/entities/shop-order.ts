import { BaseEntity } from '../../common-bundle/essence/base-entity';
import { CartItem } from './cart-item';

export class ShopOrder extends BaseEntity<ShopOrder> {
  public userId: number | undefined | null;
  public cartItemsList: CartItem[] | undefined;
  public customerName: string | undefined;
  public customerCityId: number | undefined;
  public customerEmail: string | undefined;
  public customerPhone: string | undefined;
}
