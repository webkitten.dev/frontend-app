import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class ShopPhoneInput extends BaseEntity<ShopPhoneInput> {
  phoneValue: string | undefined;
  phonePosition: number | undefined;
  phoneComment: string | undefined;
}
