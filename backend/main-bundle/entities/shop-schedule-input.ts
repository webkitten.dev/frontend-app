import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class ShopScheduleInput extends BaseEntity<ShopScheduleInput> {
  scheduleDayNumber: number | undefined;
  scheduleOpeningTime: string | undefined;
  scheduleClosingTime: string | undefined;
  scheduleDinnerBegin: string | undefined;
  scheduleDinnerEnd: string | undefined;
}
