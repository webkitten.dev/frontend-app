import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class Shop extends BaseEntity<Shop> {
  public id: number | undefined;
  public shopName: string | undefined;
}
