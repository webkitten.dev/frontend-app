import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class StatSearchedPart extends BaseEntity<StatSearchedPart> {
  public userId: number | undefined | null;
  public locationId: number | undefined | null;
  public vendorCode!: string;
  public manufacturer!: string;
  public clientIp: string | undefined;
  public referer: string | undefined;
  public sessionId: string | undefined;
  public hash?: string | null;
  public userAgent: string | undefined;
}
