import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class StoContactInput extends BaseEntity<StoContactInput> {
  contactTypeId: number | undefined;
  contactValue: string | undefined;
  contactPosition: number | undefined;
}
