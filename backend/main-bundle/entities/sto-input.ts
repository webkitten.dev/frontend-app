import { BaseEntity } from '../../common-bundle/essence/base-entity';
import { ShopPhoneInput } from './shop-phone-input';
import { StoContactInput } from './sto-contact-input';
import { ShopScheduleInput } from './shop-schedule-input';

export class StoInput extends BaseEntity<StoInput> {
  userId: number | undefined;
  stoId: number | undefined;
  cityId: number | undefined;
  legalId: number | undefined;
  paymentMethods: number[] | undefined;
  organizationId: number | undefined;
  stoName: string | undefined;
  email: string | undefined;
  phones: ShopPhoneInput[] | undefined;
  contacts: StoContactInput[] | undefined;
  street: string | undefined;
  building: string | undefined;
  office: string | undefined;
  metroId: number | undefined;
  site: string | undefined;
  spares: boolean | undefined;
  nonSpecifiedServiceGroup: boolean | undefined;
  sendNotification: boolean | undefined;
  hourPriceMin: number | undefined;
  hourPriceMax: number | undefined;
  schedules: ShopScheduleInput[] | undefined;
  coordLat: number | undefined;
  coordLon: number | undefined;
  additionalInfo: string | undefined;
  brands: number[] | undefined;
  taskBrands: number[] | undefined;
  serviceGroup: number[] | undefined;
  serviceGroupTask: number[] | undefined;
  service: number[] | undefined;
  taskService: number[] | undefined;
}
