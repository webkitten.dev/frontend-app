import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class Sto extends BaseEntity<Sto> {
  public id: number | undefined;
  public name: string | undefined;
}
