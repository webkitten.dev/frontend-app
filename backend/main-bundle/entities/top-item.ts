/* tslint:disable */
import { Item } from './item';

export class TopItem extends Item {
  part_name?: string;
  vendor_code?: string;
  brand?: string;
  offers_count?: string;
}
