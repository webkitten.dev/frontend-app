import { BaseEntity } from '../../common-bundle/essence/base-entity';
import { GeoTag } from './geo-tag';

export class UserProfile extends BaseEntity<UserProfile> {
  public confirmedPhone: boolean | undefined;
  public confirmedEmail: boolean | undefined;
  public city: GeoTag | undefined;
  public phone: string | undefined;
  public lastName: string | undefined;
  public firstName: string | undefined;
  public middleName: string | undefined;
}
