import { BaseEntity } from '../../common-bundle/essence/base-entity';
import { UserProfile } from './user-profile';
import { Organization } from './organization';
import { ProfileNotification } from './profile-notification';

export class User extends BaseEntity<User> {
  public password: string | undefined;
  public id: number | undefined;
  public username: string | undefined;
  public phone: string | undefined;
  public firstName: string | undefined;
  public displayedName: string | undefined;
  public active: boolean | undefined;
  public profileSended: boolean | undefined;
  public organizationSended: boolean | undefined;
  public createdAt: string | undefined;
  public socialNetworkShortName: string | undefined;
  public socialNetworkAccountId: string | undefined;
  public recoveryToken: string | undefined;
  public recoveryExpiration: string | undefined;
  public userProfile: UserProfile | undefined;
  public notificationSettings: [ProfileNotification] | undefined;
  public lastLoginDate: string | undefined;
  public lastLoginIp: string | undefined;
  public sellerLicenseAcceptedAt: string | undefined;
  public vwLicenseAcceptedAt: string | undefined;
  public lastBlockedSendSmsAt: string | undefined;
  public forTesting: boolean | undefined;
  public isOrganization: boolean | undefined;
  public isCompleteProfile: boolean | undefined;
  public isOrganizationWithShop: boolean | undefined;
  public countNewIncomingRequests: number | undefined;
  public countNewOutgoingRequests: number | undefined;
  public countNewVWRequests: number | undefined;
  public countUnreadReviews: number | undefined;
  public organization: Organization | undefined;
  public chatRole: string | undefined;
}
