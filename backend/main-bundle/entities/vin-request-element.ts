import { BaseEntity } from '../../common-bundle/essence/base-entity';

export class VinRequestElementPartInput extends BaseEntity<VinRequestElementPartInput> {
  partCondition: number | undefined;
  partNameId?: number | null;
  partName: string | undefined;
  partPhoto: string | undefined;
  partQuantity: number | undefined;
  partVendorCode: string | undefined;
}
