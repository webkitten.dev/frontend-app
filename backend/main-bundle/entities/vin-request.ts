import { BaseEntity } from '../../common-bundle/essence/base-entity';
import { VinRequestElementPartInput } from './vin-request-element';

export class VinRequest extends BaseEntity<VinRequest> {
  vin: string | undefined;
  carManufacturerId: number | undefined;
  carModelId: number | undefined;
  carBody: string | undefined;
  carEngine: string | undefined;
  carReleaseYear: number | undefined;
  userId: number | undefined;
  customerName: string | undefined;
  customerCityId: number | undefined;
  customerEmail: string | undefined;
  customerPhone: string | undefined;
  customerAdditionalInfo: string | undefined;
  partList: VinRequestElementPartInput[] | undefined;
}
