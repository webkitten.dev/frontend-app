export const GET_LIST = `query(
  $itemStatus: PartAdStatus = ACTIVE
  $ids: [ID!]
  $excludeIds: [ID!]
  $partCondition: PartCondition = ANY
  $merchantType: MerchantType = ANY
  $shopId: ID
  $cityId: ID
  $metroId: ID
  $carManufacturerId: ID
  $carModelId: ID
  $priceRange: Range
  $carYears: [Int!]
  $carChassis: String
  $carEngine: String
  $text: String
  $isSet: Boolean
  $inStock: Boolean
  $hasImage: Boolean
  $partPlacement: [Placement!]
  $skip: Int = 0
  $first: Int = 100
  $sort: [ContractPartsSort!] = [hasImage_DESC, activeUntilAt_DESC]
) {
  data:ContractParts_list(
    itemStatus: $itemStatus
    ids: $ids
    excludeIds: $excludeIds
    partCondition: $partCondition
    merchantType: $merchantType
    shopId: $shopId
    cityId: $cityId
    metroId: $metroId
    carManufacturerId: $carManufacturerId
    carModelId: $carModelId
    priceRange: $priceRange
    carYears: $carYears
    carChassis: $carChassis
    carEngine: $carEngine
    text: $text
    isSet: $isSet
    inStock: $inStock
    hasImage: $hasImage
    partPlacement: $partPlacement
    skip: $skip
    first: $first
    sort: $sort
  ) {
    pageInfo {
      count
    }
    edges {
      node {
        id
        actualAt
        name
        statisticsData {
          adId
          adName
          userId
          shopId
          geoId
          brandId
          brandName
          modelId
          modelName
          chassis
          engines
          years
        }
        city {
          id
          name
        }
        seller {
          __typename
          ... on Shop {
            id
            name
            isNetshop
            company {
              id
            }
            schedule(cityId: $cityId) {
              legacy {
                current_date_time_with_tz
                schedule_day_isset
                around_the_clock
                opening_date_time
                close_date_time
                dinner_begin_date_time
                dinner_end_date_time
              }
              isConfirmed
            }
            rating {
              votes
              rate
            }
            address {
              street
              building
              office
              isConfirmed
            }
          }
          ... on User {
            id
            name
            profile {
              fullname {
                first
                middle
                last
              }
            }
          }
          websites {
            isConfirmed
          }
          emails {
            isConfirmed
          }
          phones {
            isConfirmed
          }
          messengers {
            kind
            value
            isConfirmed
          }
        }
        car {
          id
          name
          manufacturer {
            id
            name
          }
        }
        cost
        color
        partCondition
        images {
          id
          small { url }
          medium { url }
          big { url }
          extra { url }
        }
        carChassis
        carEngines
        carYears {
          min
          max
        }
        inStock
        quantity
        partPlacement
        part {
          vendorCode
          manufacturer {
            name
          }
        }
      }
    }
  }
}`;
