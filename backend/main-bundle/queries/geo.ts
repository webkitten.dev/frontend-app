export const GET = `query(
  $ids: [ID!]
  $slugs: [String!]
  $names: [String!]
  $parentId: ID
  $level: GeoLevel = ALL
) {
  data:Geo_get(
    ids: $ids
    slugs: $slugs
    names: $names
    parentId: $parentId
    level: $level
  ) {
    pageInfo {
      count
    }
    edges {
      node {
        id
        parentId
        name
        slug
        prepositionalName
        level
        timezoneName
      }
    }
  }
}`;

export const RESOLVE = `query(
  $ip: String!
) {
  data:Geo_resolve(
    ip: $ip
  ) {
    id
    parentId
    name
    slug
    prepositionalName
    level
    timezoneName
  }
}`;
