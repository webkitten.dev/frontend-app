export const GET_LAST_REQUESTED = `query(
  $geoIds: [ID!]
  $isOrganization: Boolean!
) {
  data:SearchByNumber_getLastRequested(
    geoIds: $geoIds
    isOrganization: $isOrganization
  ) {
    pageInfo {
      count
    }
    edges {
      node {
        partId
        createdAt
        vendorCode
        brand
        partName
        priceMin
        priceMax
        userLocationCount
        allOffersCount
      }
    }
  }
}`;
