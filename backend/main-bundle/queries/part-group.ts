export const GET_POPULAR = `query {
  data:PartGroup_getPopular {
    pageInfo {
      count
    }
    edges {
      node {
        id
        name
        slug
        subgroups {
          id
          name
          slug
          partnames {
            id
            name
            slug
          }
        }
      }
    }
  }
}`;
