import GraphQLQuery from 'graphql-query-builder';
import { AbstractRepository } from './abstract-repository';
import { IEntityRead } from '../../common-bundle/io/i-entity-repository-read';
import { IEntityWrite } from '../../common-bundle/io/i-entity-repository-write';
import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';

export abstract class AbstractEntityRepository<T>
  extends AbstractRepository
  implements IEntityRead<T>, IEntityWrite<T> {
  public count(criteria?: object): Promise<number> {
    throw new Error('Method not implemented.');
  }

  public list(
    criteria?: object,
    limit?: number,
    offset?: number,
    orderField?: string,
    orderDirection?: string,
  ): Promise<EntityListResolveType<T>> {
    throw new Error('Method not implemented.');
  }

  public get(criteria: object): Promise<T> {
    throw new Error('Method not implemented.');
  }

  public create(item: T): Promise<T> {
    throw new Error('Method not implemented.');
  }

  public update(item: T): Promise<T> {
    throw new Error('Method not implemented.');
  }

  public delete(item: T): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  protected _count(query: GraphQLQuery): Promise<number> {
    return new Promise((resolve, reject) => {
      this.request(this.getRequestParams(query, 'get'))
        .then((data) => {
          resolve(data.count);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  protected _list(query: GraphQLQuery): Promise<EntityListResolveType<T>> {
    return new Promise((resolve, reject) => {
      this.request(this.getRequestParams(query, 'get'), true)
        .then((data: { data: any[], count: number }) => {
          const result: T[] = [];
          data.data.forEach((item: object) => {
            const obj: T = this.getObject(item);
            result.push(obj);
          });
          resolve({
            data: result,
            count: data.count,
          });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  protected _get(query: GraphQLQuery): Promise<T> {
    return new Promise((resolve, reject) => {
      this.request(this.getRequestParams(query, 'get'), true)
        .then((data: { data: any[], count: number }) => {
          const result: T = this.getObject(data.data.shift()!);
          resolve(result);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  protected _create(query: GraphQLQuery): Promise<T> {
    return new Promise((resolve, reject) => {
      this.request(this.getRequestParams(query, 'post'))
        .then((data: object[]) => {
          const result: T = this.getObject(data[0]);
          resolve(result);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  protected _update(query: GraphQLQuery): Promise<T> {
    return new Promise((resolve, reject) => {
      this.request(this.getRequestParams(query, 'post'))
        .then((data: object[]) => {
          const result: T = this.getObject(data);
          resolve(result);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  protected _delete(query: GraphQLQuery): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  protected wrapFields(fields: any[]): (GraphQLQuery | string)[] {
    const query = new GraphQLQuery('list');
    query.find(fields);
    return [query, 'count'];
  }

  protected abstract getObject(data: object): T;
}
