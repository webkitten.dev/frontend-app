import config from 'config';

import {
  LoggerContainer,
  LoggerCategories,
} from '../../common-bundle/logger';
const logger = LoggerContainer.get(LoggerCategories.MONOLITH);

import {
  AbstractRepository as GraphqlAbstractRepository,
} from '../../api-bundle/graphql/repositories/abstract-repository';
import { Request } from '../../api-bundle/graphql/entities/request';

const tokenHeaderName = config.get<string>('api.main.tokenHeader');
const tokenHeaderValue = process.env.SERVICE_MONOLITH_TOKEN as string;

export abstract class AbstractRepository extends GraphqlAbstractRepository {
  protected readonly queryNamespace?: string;
  protected readonly endpoint = process.env.SERVICE_MONOLITH_URI as string;
  protected readonly headers = {
    [tokenHeaderName]: tokenHeaderValue,
  };
  protected readonly additionalFields: { [key: string]: string[] } = {
    meta: ['success', 'errors'],
  };

  protected request(requestConfig: Request, reformatRequest = false): Promise<any> {
    return new Promise((resolve, reject) => {
      super.request(requestConfig)
        .then((data) => {
          if (!data.meta.success) {
            const payload = `${requestConfig.requestType} {${requestConfig.requestPayload.toString()}}`;
            if (data.meta.errors && data.meta.errors.length) {
              const e = data.meta.errors.shift();
              logger.warn('Backend error <%s> in %s', e, payload);
              reject(new Error(e));
              return;
            }
            if (data.meta.queryErrors && data.meta.queryErrors.length) {
              const e = data.meta.queryErrors.shift();
              logger.error('Query error <%s> in %s', e, payload);
              reject(new Error(e));
              return;
            }
            logger.error('Unknown error in %s', payload);
            reject(new Error('unknown error'));
          }
          if (reformatRequest) {
            delete data.meta;
            resolve(data);
          } else {
            resolve(data.data);
          }
        })
        .catch(error => reject(error));
    });
  }
}
