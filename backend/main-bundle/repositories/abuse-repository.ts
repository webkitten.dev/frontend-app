import { AbstractEntityRepository } from './abstract-entity-repository';
import { Abuse } from '../entities/abuse';
import { BooleanResult } from '../entities/boolean-result';

export class AbuseRepository extends AbstractEntityRepository<Abuse> {
  protected readonly queryNamespace: string = 'Abuse';

  public createAbuse(abuse: Abuse): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'create',
      { userId: abuse.userId, shopId: abuse.shopId, comment: abuse.comment },
      ['success'],
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  protected getObject(data: any): Abuse {
    return new Abuse(data);
  }
}
