import { AbstractEntityRepository } from './abstract-entity-repository';
import { CarManufacturer } from '../entities/car-manufacturer';

export class CarManufacturerRepository extends AbstractEntityRepository<CarManufacturer>  {
  protected readonly queryNamespace: string = 'CarManufacturer';

  getWithRegion(): Promise<CarManufacturer> {
    const query: GraphQLQuery = this.getQuery(
      'getWithRegion',
      {},
      this.wrapFields([
        this.getCountryFields('china'),
        this.getCountryFields('europe'),
        this.getCountryFields('japan'),
        this.getCountryFields('russia'),
        this.getCountryFields('korea'),
        this.getCountryFields('truck'),
        this.getCountryFields('usa'),
        this.getCountryFields('other'),
      ]),
    );
    return this._get(query);
  }

  protected getObject(data: object): CarManufacturer {
    return new CarManufacturer(data);
  }

  protected getCountryFields(name: string): GraphQLQuery {
    return this.getQuery(name, {}, ['id', 'name'], true);
  }
}
