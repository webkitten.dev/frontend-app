import { User } from '../entities/user';
import { AbstractEntityRepository } from './abstract-entity-repository';
import { CartItem } from '../entities/cart-item';
import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';

export class CartRepository extends AbstractEntityRepository<CartItem> {
  protected readonly queryNamespace: string = 'Cart';

  public async count(user: User): Promise<number> {
    const query: GraphQLQuery = this.getQuery(
      'count',
      { userId: +user.id! },
      ['count'],
    );
    return this._count(query);
  }

  public list(criteria: { userId: string; }): Promise<EntityListResolveType<CartItem>> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      { userId: +criteria.userId },
      this.wrapFields([
        'itemId',
        'inStock',
        'shopId',
        'organizationId',
        'userId',
        'isWholesalePrice',
        'manufacturerId',
        'brand',
        'vendorCode',
        'name',
        'nameStandard',
        'commentName',
        'quantity',
        'maxQuantity',
        'price',
        'seller',
        'city',
        'address',
        'customerComment',
        'deliveryString',
        'delivery',
        'deliveryFrom',
        'deliveryTo',
        'deliveryRetailCondition',
        'deliveryWholesaleCondition',
        'charCode',
        'discountCheck',
      ]),
    );
    return this._list(query);
  }

  public updateCart(
    userId: number,
    cartItemsList: CartItem[],
  ): Promise<object> {
    const query: GraphQLQuery = this.getQuery(
      'update',
      {
        userId,
        cartItemsList,
      },
      this.wrapFields([
        'itemId',
        'shopId',
        'organizationId',
        'userId',
        'isWholesalePrice',
        'manufacturerId',
        'brand',
        'vendorCode',
        'name',
        'nameStandard',
        'commentName',
        'quantity',
        'maxQuantity',
        'price',
        'seller',
        'city',
        'address',
        'customerComment',
        'delivery',
        'deliveryFrom',
        'deliveryTo',
        'deliveryRetailCondition',
        'deliveryWholesaleCondition',
        'charCode',
        'discountCheck',
      ]),
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  protected getObject(data: any): CartItem {
    return new CartItem(data);
  }
}
