import { AbstractRepository } from './abstract-repository';
import config from 'config';

export class CheckautoRepository extends AbstractRepository {
  protected readonly queryNamespace: string = 'Checkauto';

  public getInfo(): Promise<object> {
    const query: GraphQLQuery = this.getQuery('getInfo', {}, [
      'servicePrice',
      this.getPaymentSystemQuery(),
    ]);
    return this.request(this.getRequestParams(query, 'get'));
  }

  public getShortReportInfo(id: string): Promise<object> {
    const query: GraphQLQuery = this.getQuery('getShortReportInfo', { id }, [
      'info',
      'servicePrice',
      this.getPaymentSystemQuery(),
    ]);
    return this.request(this.getRequestParams(query, 'get'));
  }

  public getFullReportInfo(id: string): Promise<object> {
    const query: GraphQLQuery = this.getQuery('getFullReportInfo', { id }, [
      this.getQuery(
        'vinRequest',
        {},
        [
          'id',
          this.getQuery(
            'vinRequestData',
            {},
            [
              'id',
              'vin',
            ],
            true,
          ),
          'vin',
          'value',
          'createdAt',
        ],
        true,
      ),
      'info',
      'fullReportReady',
      'paymentSuccess',
      this.getPaymentSystemQuery(),
    ]);
    return this.request(this.getRequestParams(query, 'get'));
  }

  public init(vin: string): Promise<object> {
    const query: GraphQLQuery = this.getQuery('initVinRequest', { vin }, [
      'id',
      'captcha',
      'session',
      'success',
    ]);
    return this.request(this.getRequestParams(query, 'get'));
  }

  public create(vin: string, captcha?: string, session?: string): Promise<object> {
    const query: GraphQLQuery = this.getQuery(
      'createVinRequest',
      {
        captcha,
        session,
        vin,
      },
      [
        'id',
        'success',
      ],
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  public updateRestrictions(id: string, captcha?: string, session?: string): Promise<object> {
    const query: GraphQLQuery = this.getQuery(
      'updateVinRequestRestrictions',
      {
        id,
        captcha,
        session,
      },
      [
        'id',
        'captcha',
        'session',
        'success',
      ],
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  public createPayment(
    vinRequestDataId: string,
    email: string,
    phone: string,
    register: boolean,
    userId: number | null,
    promocode?: string,
  ) {
    const query: GraphQLQuery = this.getQuery(
      'createPayment',
      {
        vinRequestDataId,
        email,
        phone,
        promocode,
        register: !!register,
        userId: userId && +userId,
        shortReportUrl: `https://${config.get('client.domains.main')}/check-auto/report/`,
        fullReportUrl: `https://${config.get('client.domains.main')}/check-auto/full-report/`,
      },
      [
        'payment',
        'report',
      ],
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  public promoCodeInfo(code: string, userId: number | null): Promise<object> {
    const query: GraphQLQuery = this.getQuery(
      'getPromoCodeInfo',
      {
        code,
        userId: userId && +userId,
      },
      ['servicePrice'],
    );
    return this.request(this.getRequestParams(query, 'get'));
  }

  private getPaymentSystemQuery(): GraphQLQuery {
    return this.getQuery(
      'paymentSystem',
      {},
      [
        'id',
        'name',
        'code',
        'enabled',
        'selected',
        'url',
      ],
      true,
    );
  }
}
