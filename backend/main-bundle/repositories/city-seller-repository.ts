import { AbstractEntityRepository } from './abstract-entity-repository';
import { CitySeller } from '../entities/city-seller';

export class CitySellerRepository extends AbstractEntityRepository<CitySeller> {
  protected readonly queryNamespace: string = 'CitySeller';

  public count(): Promise<number> {
    const query: GraphQLQuery = this.getQuery(
      'count',
      {},
      ['count'],
    );
    return this._count(query);
  }

  protected getObject(data: object): CitySeller {
    return new CitySeller(data);
  }
}
