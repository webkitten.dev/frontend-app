import { AbstractEntityRepository } from './abstract-entity-repository';
import { ConciergePartnershipRequest } from '../entities/concierge-partnership-request';

export class ConciergePartnershipRequestRepository extends AbstractEntityRepository<ConciergePartnershipRequest> {
  protected readonly queryNamespace: string = 'ConciergePartnershipRequest';

  public create(item: ConciergePartnershipRequest): Promise<ConciergePartnershipRequest> {
    const query: GraphQLQuery = this.getQuery(
      'create',
      item,
      this.wrapFields(['id']),
    );
    return this._create(query);
  }

  protected getObject(data: object): ConciergePartnershipRequest {
    return new ConciergePartnershipRequest(data);
  }
}
