import { AbstractEntityRepository } from './abstract-entity-repository';
import { ConciergeRequest } from '../entities/concierge-request';

export class ConciergeRequestRepository extends AbstractEntityRepository<ConciergeRequest> {
  protected readonly queryNamespace: string = 'ConciergeRequest';

  public create(item: ConciergeRequest): Promise<ConciergeRequest> {
    const query: GraphQLQuery = this.getQuery(
      'create',
      item,
      this.wrapFields(['id']),
    );
    return this._create(query);
  }

  protected getObject(data: object): ConciergeRequest {
    return new ConciergeRequest(data);
  }
}
