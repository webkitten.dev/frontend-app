import { AbstractRepository } from '../../api-bundle/graphql/repositories/new-abstract-repository';

import { GET_LIST } from '../queries/contract-part';
import {
  QueryContractParts_ListArgs,
  ContractPart,
} from '../../api-bundle/graphql/types';

import { ListResponse } from '../../api-bundle/graphql/entities/list-response';

export class ContractPartsRepository extends AbstractRepository {
  public list(variables: QueryContractParts_ListArgs): Promise<ListResponse<ContractPart>> {
    return this._list<ContractPart>(GET_LIST, variables);
  }
}
