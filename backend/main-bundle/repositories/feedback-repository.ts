import { AbstractEntityRepository } from './abstract-entity-repository';
import { FeedbackInput } from '../entities/feedback-input';
import { FeedbackAddLike } from '../entities/feedback-add-like';
import { Feedback } from '../entities/feedback';

export class FeedbackRepository extends AbstractEntityRepository<Feedback> {
  protected readonly queryNamespace: string = 'Feedback';

  public createFeedback(item: FeedbackInput): Promise<Feedback[]> {
    const query: GraphQLQuery = this.getQuery(
      'create',
      item,
      this.wrapFields(['id']),
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  public addLike(item: FeedbackAddLike): Promise<Feedback[]> {
    const query: GraphQLQuery = this.getQuery(
      'addLike',
      item,
      this.wrapFields(['id', 'likersCount']),
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  protected getObject(data: object): Feedback {
    return new Feedback(data);
  }
}
