import { AbstractRepository } from '../../api-bundle/graphql/repositories/new-abstract-repository';

import { GET, RESOLVE } from '../queries/geo';
import {
  QueryGeo_GetArgs,
  QueryGeo_ResolveArgs,
  Geo,
} from '../../api-bundle/graphql/types';

import { ListResponse } from '../../api-bundle/graphql/entities/list-response';

export class GeoRepository extends AbstractRepository {
  public list(variables?: QueryGeo_GetArgs): Promise<ListResponse<Geo>> {
    return this._list<Geo>(GET, variables || {});
  }

  public resolve(variables: QueryGeo_ResolveArgs): Promise<Geo> {
    return this._get<Geo>(RESOLVE, variables);
  }
}
