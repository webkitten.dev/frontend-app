import { AbstractRepository } from '../../api-bundle/graphql/repositories/new-abstract-repository';

import { GET_LAST_REQUESTED } from '../queries/new-part';
import {
  QuerySearchByNumber_GetLastRequestedArgs,
  SearchByNumberLastRequested,
} from '../../api-bundle/graphql/types';

import { ListResponse } from '../../api-bundle/graphql/entities/list-response';

export class NewPartRepository extends AbstractRepository {
  public getLastRequested(
    variables: QuerySearchByNumber_GetLastRequestedArgs,
  ): Promise<ListResponse<SearchByNumberLastRequested>> {
    return this._list<SearchByNumberLastRequested>(GET_LAST_REQUESTED, variables);
  }
}
