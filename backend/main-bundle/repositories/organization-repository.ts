import { AbstractEntityRepository } from './abstract-entity-repository';
import { OrganizationMain } from '../entities/organization-main';
import { OrganizationInn } from '../entities/organization-inn';
import { OrganizationInput } from '../entities/organization-input';
import { OrganizationRequesite } from '../entities/organization-requesite';
import { OrganizationBankInfo } from '../entities/organization-bank-info';
import { OrganizationShopStoList } from '../entities/organization-shop-sto-list';

export class OrganizationRepository extends AbstractEntityRepository<OrganizationMain> {
  protected readonly queryNamespace: string = 'Organization';

  public getShopSto(userId: number): Promise<OrganizationShopStoList> {
    const query = this.getQuery(
      'getShopSto',
      { userId },
      this.wrapFields([
        'id',
        'type',
        'name',
        this.getQuery(
          'city',
          {},
          [
            'id',
            'name',
            'prepositionalName',
            'region',
            this.getQuery(
              'coord',
              {},
              [
                'lat',
                'lng',
              ],
              true,
            ),
          ],
          true,
        ),
        'address',
        'email',
        'site',
        this.getPhonesFields('phones'),
      ]));
    return this.request(this.getRequestParams(query, 'get'));
  }

  protected getPhonesFields(name: string): GraphQLQuery {
    return this.getQuery(name, {}, ['id', 'phone', 'position', 'comment'], true);
  }

  public getByInn(requisiteId: number | null, inn: string): Promise<OrganizationInn> {
    const query: GraphQLQuery = this.getQuery(
      'getByInn',
      { inn, requisiteId },
      [
        'shortOrganizationName',
        'fullOrganizationName',
        'inn',
        'kpp',
        'ogrn',
        'managerName',
        'managerPost',
        'stateStatus',
        'liquidationDate',
      ],
    );
    return this.request(this.getRequestParams(query, 'get'));
  }

  public getBankInfo(bik: string): Promise<OrganizationBankInfo> {
    const query: GraphQLQuery = this.getQuery(
      'getBankInfo',
      { bik },
      [
        'correspondentAccount',
        'bankName',
      ],
    );
    return this.request(this.getRequestParams(query, 'get'));
  }

  public create(item: OrganizationInput): Promise<OrganizationRequesite> {
    const query: GraphQLQuery = this.getQuery(
      'create',
      item,
      this.wrapFields([
        'id',
        'shortOrganizationName',
        'fullOrganizationName',
        'inn',
        'kpp',
        'ogrn',
        'bik',
        'managerName',
        'managerPost',
        'correspondentAccount',
        'bankAccount',
        'bankName',
        'showForCustomer',
        'legalAddress',
        'factAddress',
        'postAddress',
      ]),
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  public update(item: OrganizationInput): Promise<OrganizationRequesite> {
    const query: GraphQLQuery = this.getQuery(
      'update',
      item,
      this.wrapFields([
        'id',
        'shortOrganizationName',
        'fullOrganizationName',
        'inn',
        'kpp',
        'ogrn',
        'bik',
        'managerName',
        'managerPost',
        'correspondentAccount',
        'bankAccount',
        'bankName',
        'showForCustomer',
        'legalAddress',
        'factAddress',
        'postAddress',
      ]),
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  protected getObject(data: object): OrganizationMain {
    return new OrganizationMain(data);
  }
}
