import { AbstractRepository } from '../../api-bundle/graphql/repositories/new-abstract-repository';

import { GET_POPULAR } from '../queries/part-group';
import {
  PartGroup,
} from '../../api-bundle/graphql/types';

import { ListResponse } from '../../api-bundle/graphql/entities/list-response';

export class PartGroupRepository extends AbstractRepository {
  public getPopular(): Promise<ListResponse<PartGroup>> {
    return this._list<PartGroup>(GET_POPULAR);
  }
}
