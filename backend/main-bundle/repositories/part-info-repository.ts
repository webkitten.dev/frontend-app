import { AbstractEntityRepository } from './abstract-entity-repository';
import { Part } from '../entities/part-info';

export class PartRepository extends AbstractEntityRepository<Part> {
  protected readonly queryNamespace: string = 'Part';

  public get(criteria: object): Promise<object> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      Object.assign(criteria || {}, {}),
      this.wrapFields([
        'partName',
        'partVendorCode',
        'additionalVendorCode',
        'additionalPartManufacturers',
        'partManufacturer',
        'partManufacturerId',
        'partCarModels',
        'description',
        'partNameAliases',
        'partImages',
        'partType',
        'minPrice',
        'maxPrice',
        this.getQuery(
          'tecdoc',
          {},
          [
            this.getQuery(
              'characteristics',
              {},
              [
                'vendorCode',
                'name',
                'value',
              ],
              true,
            ),
          ],
          true,
        ),
        this.getQuery(
          'counterparts',
          {},
          [
            'vendor_code',
            'brand',
            'part_name',
            'min_price',
            'max_price',
            'is_tested',
            'user_location_count',
            'all_offers_count',
          ],
          true,
        ),
        this.getQuery(
          'carTypes',
          {},
          [
            'brand',
            'model_name',
            'type_name',
          ],
          true,
        ),
      ]),
    );
    return this._get(query);
  }

  protected getObject(data: object): Part {
    return new Part(data);
  }
}
