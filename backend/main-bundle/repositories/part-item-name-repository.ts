import { AbstractEntityRepository } from './abstract-entity-repository';
import { PartItemName } from '../entities/part-item-name';
import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';

export class PartItemNameRepository extends AbstractEntityRepository<PartItemName> {
  protected readonly queryNamespace: string = 'PartItemName';

  public list(
    criteria?: object,
    limit?: number,
    offset?: number,
    orderField?: string,
    orderDirection?: string,
  ): Promise<EntityListResolveType<PartItemName>> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      Object.assign(criteria || {}, {
        listLimit: limit && +limit,
        listOffset: offset && +offset,
        listOrderField: orderField,
        listOrderDirection: orderDirection,
      }),
      this.wrapFields([
        'id',
        'name',
      ]),
    );
    return this._list(query);
  }

  protected getObject(data: object): PartItemName {
    return new PartItemName(data);
  }
}
