import { AbstractEntityRepository } from './abstract-entity-repository';
import { PartItem } from '../entities/part-item';
import { User } from '../entities/user';
import { FastOrder } from '../entities/fast-order';
import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';
import { TopItem } from '../entities/top-item';

export class PartItemRepository extends AbstractEntityRepository<PartItem> {
  protected readonly queryNamespace: string = 'PartItem';

  public countCart(user: User): Promise<number> {
    const query: GraphQLQuery = this.getQuery(
      'countCart',
      { userId: +user.id! },
      ['count'],
    );
    return this._count(query);
  }

  public createFastOrder(item: FastOrder): Promise<object> {
    const query: GraphQLQuery = this.getQuery(
      'createFastOrder',
      item,
      this.wrapFields([
        'id',
        'serialNumber',
        'quantity',
        'customerFio',
        'customerEmail',
        'customerPhone',
        'discountCheck',
      ]),
    );
    return this._create(query);
  }

  public count(): Promise<number> {
    const query: GraphQLQuery = this.getQuery(
      'count',
      {},
      ['count'],
    );
    return this._count(query);
  }

  public getLastRequested(locationId: number, userId?: number | null): Promise<EntityListResolveType<object>> {
    const query: GraphQLQuery = this.getQuery(
      'getLastRequested',
      { locationId, userId },
      this.wrapFields([
        'partName',
        'priceMin',
        'priceMax',
        'createdAt',
        'brand',
        'brandSlug',
        'vendorCode',
        'sellerCounter',
      ]),
    );
    return this._list(query);
  }

  public list(
    criteria: object,
    limit: number,
    offset: number,
    orderField: string,
    orderDirection: string,
  ): Promise<EntityListResolveType<PartItem>> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      Object.assign(criteria || {}, {
        listLimit: limit && +limit,
        listOffset: offset && +offset,
        listOrderField: orderField,
        listOrderDirection: orderDirection,
      }),
      this.wrapFields([
        'part_id',
        'item_id',
        'shop_id',
        'user_id',
        'old_price',
        'price',
        'vendor_code',
        'brand_slug',
        'part_name',
        'comment',
        'brand',
        'quantity',
        'delivery',
        'created_at',
        'item_name',
        'discount_check',
        'in_stock',
        'delivery_from',
        'delivery_to',
        'delivery_string',
        'is_wholesale',
        'is_wholesale_only',
        'shop_name',
        'office',
        'quantity_string',
        'organization_id',
        'street',
        'building',
        'shop_additional_info',
        'city_name',
        'is_netshop',
        'prepositional_name',
        'feedback_count',
        'avg_rating',
        'part_manufacturer_id',
        'contact_is_confirmed',
        this.getQuery('additional_contacts', {}, [
          'type_id',
          'value',
        ], true),
        'opening_date_time',
        'close_date_time',
        'dinner_begin_date_time',
        'around_the_clock',
        'coord_lng',
        'coord_lat',
        'dinner_end_date_time',
        'schedule_day_isset',
        'current_date_time_with_tz',
        'item_redirect_url',
        'order_redirect_url',
        'is_outdated',
      ]),
    );
    return this._list(query);
  }

  public filter(criteria: object): Promise<object> {
    const query: GraphQLQuery = this.getQuery(
      'filter',
      criteria,
      [
        'hash',
        this.getQuery(
          'filters',
          {},
          [
            this.getFilterFields('filterLocation'),
            this.getFilterFields('filterManufacturer'),
            this.getFilterFields('filterShop'),
            this.getFilterFields('filterIsWholesale'),
            this.getFilterFields('filterDelivery'),
            this.getFilterFields('filterRating'),
            this.getFilterFields('filterMetro'),
          ],
          true,
        ),
      ],
    );
    return this.request(this.getRequestParams(query, 'get'));
  }

  public popular(): Promise<TopItem[]> {
    const query: GraphQLQuery = this.getQuery(
      'getTopPopular',
      {},
      this.wrapFields([
        'part_name',
        'vendor_code',
        'brand',
        'offers_count',
      ]),
    );
    return this.request(this.getRequestParams(query, 'get'));
  }

  public availability(): Promise<TopItem[]> {
    const query: GraphQLQuery = this.getQuery(
      'getTopAvailability',
      {},
      this.wrapFields([
        'part_name',
        'vendor_code',
        'brand',
        'offers_count',
      ]),
    );
    return this.request(this.getRequestParams(query, 'get'));
  }

  protected getFilterFields(name: string): GraphQLQuery {
    return this.getQuery(name, {}, ['id', 'name'], true);
  }

  protected getObject(data: object): PartItem {
    return new PartItem(data);
  }
}
