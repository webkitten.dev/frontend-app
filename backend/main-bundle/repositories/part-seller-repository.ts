import { AbstractEntityRepository } from './abstract-entity-repository';
import { PartSeller } from '../entities/part-seller';

export class PartSellerRepository extends AbstractEntityRepository<PartSeller> {
  protected readonly queryNamespace: string = 'PartSeller';

  public count(): Promise<number> {
    const query: GraphQLQuery = this.getQuery(
      'count',
      {},
      ['count'],
    );
    return this._count(query);
  }

  protected getObject(data: object): PartSeller {
    return new PartSeller(data);
  }
}
