import { AbstractEntityRepository } from './abstract-entity-repository';
import { ProfileShop } from '../entities/profile-shop';

export class ProfileShopRepository extends AbstractEntityRepository<ProfileShop> {
  protected readonly queryNamespace: string = 'Shop';

  public get(criteria: { [key: string]: any }): Promise<ProfileShop> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      criteria,
      this.wrapFields(this.getFields()),
    );
    return this._get(query);
  }

  protected getObject(data: object): ProfileShop {
    return new ProfileShop(data);
  }

  protected getFields(): any[] {
    return [
      'id',
      'shopName',
      'shopEmail',
      'shopSite',
      'shopAdditionalInfo',
      'isMainShop',
      this.getQuery('shopAttributes', {}, [
        'id',
        'name',
        'code',
      ],            true),
      this.getQuery('shopPhones', {}, [
        'shopPhone',
        'comment',
        'position',
      ],            true),
      this.getQuery('paymentMethods', {}, [
        'id',
        'name',
        'code',
      ],            true),
      this.getQuery('shopSchedule', {}, [
        'dayNumber',
        'openingTime',
        'closingTime',
        'dinnerBeginTime',
        'dinnerEndTime',
      ],            true),
      this.getQuery('shopAdditionalContacts', {}, [
        'contact',
        this.getQuery('contactType', {}, [
          'name',
          'code',
        ],            true),
      ],            true),
      // tradingManufacturers: [CarManufacturer]
      // officialDealers: [CarManufacturer]
      this.getQuery(
        'organization',
        {},
        [
          this.getQuery(
            'legals',
            {},
            [
              'id',
              'name',
            ],
            true,
          ),
        ],
        true,
      ),
      'legal',
      // 'city',
      // 'metro',
      'coordLat',
      'coordLng',
      'street',
      'building',
      'office',
      'shopScheduleString',
      'mainShop',
    ];
  }
}
