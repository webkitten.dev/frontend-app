import { AbstractEntityRepository } from './abstract-entity-repository';
import { Ref } from '../entities/ref';

export class RefShopAttributesRepository extends AbstractEntityRepository<Ref> {
  protected readonly queryNamespace: string = 'RefShopAttributes';

  public get(): Promise<Ref> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      {},
      this.wrapFields([
        'id',
        'name',
        'code',
      ]),
    );
    return this.request(this.getRequestParams(query, 'get'));
  }

  protected getObject(data: object): Ref {
    return new Ref(data);
  }
}
