import { AbstractEntityRepository } from './abstract-entity-repository';
import { ServiceGroup } from '../entities/service-group';
import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';

export class RefStoServiceGroups extends AbstractEntityRepository<ServiceGroup> {
  protected readonly queryNamespace: string = 'RefStoServiceGroups';

  public list(): Promise<EntityListResolveType<ServiceGroup>> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      {},
      this.wrapFields([
        'id',
        'name',
        this.getQuery('services', {}, [
          'id',
          'name',
        ], true),
      ]),
    );

    return this._list(query);
  }

  protected getObject(data: object): ServiceGroup {
    return new ServiceGroup(data);
  }
}
