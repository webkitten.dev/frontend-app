import { AbstractEntityRepository } from './abstract-entity-repository';

import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';
import { SearchItem } from '../entities/search-item';

export class SearchRepository extends AbstractEntityRepository<SearchItem> {
  protected readonly queryNamespace: string = 'SearchHelper';

  public list(criteria: {
    query: string,
    userId: number | null,
    geoId: number | null,
  }): Promise<EntityListResolveType<SearchItem>> {
    const query = this.getQuery(
      'get',
      criteria || {},
      this.wrapFields([
        'partId',
        'partName',
        'brand',
        'vendorCode',
        'carManufacturers',
        'userLocationCount',
        'allOffersCount',
        'partImages',
      ]),
    );
    return this._list(query);
  }

  protected getObject(data: object): SearchItem {
    return new SearchItem(data);
  }
}
