import { AbstractRepository } from './abstract-repository';

export class SeoRepository extends AbstractRepository {
  protected readonly queryNamespace: string = 'Seo';

  public getSlogan(): Promise<object> {
    const query: GraphQLQuery = this.getQuery('getSlogan', {}, [
      'href',
      'slogan',
      'declines',
    ]);
    return this.request(this.getRequestParams(query, 'get'));
  }

  public getContents(url: string): Promise<object> {
    const query: GraphQLQuery = this.getQuery('getContents', { url }, [
      'title',
      'description',
      'pageHeader',
      'content',
    ]);
    return this.request(this.getRequestParams(query, 'get'));
  }
}
