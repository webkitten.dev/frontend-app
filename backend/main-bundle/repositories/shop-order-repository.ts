import { AbstractEntityRepository } from './abstract-entity-repository';
import { ShopOrder } from '../entities/shop-order';

export class ShopOrderRepository extends AbstractEntityRepository<ShopOrder> {
  protected readonly queryNamespace: string = 'ShopOrder';

  public create(item: ShopOrder): Promise<ShopOrder> {
    const query: GraphQLQuery = this.getQuery(
      'create',
      item,
      this.wrapFields([
        'id',
        'serialNumber',
      ]),
    );
    return this._create(query);
  }

  protected getObject(data: object): ShopOrder {
    return new ShopOrder(data);
  }
}
