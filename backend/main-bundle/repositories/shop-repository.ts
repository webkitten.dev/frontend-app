import { AbstractEntityRepository } from './abstract-entity-repository';
import { Shop } from '../entities/shop';
import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';
import { ShopInput } from '../entities/shop-input';
import { BooleanResult } from '../entities/boolean-result';
import { ShopRequestPhoneToConfirm } from '../entities/request-phone-send-to-confirm';

import {
  ShopRequestPhoneConfirm,
} from '../entities/request-phone-confirm';
import { ShopRequestEmailConfirm } from '../entities/request-email-confirm';
import { ShopRequestEmailToConfirm } from '../entities/request-email-send-to-confirm';

export class ShopRepository extends AbstractEntityRepository<Shop> {
  protected readonly queryNamespace: string = 'Shop';

  public async list(
    criteria: object,
    limit?: number,
    offset?: number,
    orderField?: string,
    orderDirection?: string,
  ): Promise<EntityListResolveType<Shop>> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      Object.assign(criteria || {}, {
        listLimit: limit && +limit,
        listOffset: offset && +offset,
        listOrderField: orderField,
        listOrderDirection: orderDirection,
      }),
      this.wrapFields([
        'id',
        'shopName',
      ]),
    );
    return this._list(query);
  }

  public async getUserId(id: number): Promise<number> {
    const query: GraphQLQuery = this.getQuery('infoGet', { id }, this.wrapFields([
      this.getQuery(
        'organization',
        {},
        [this.getQuery('user', {}, ['id'], true)],
        true,
      ),
    ]));
    const data: { [key: string]: any } = await this.request(this.getRequestParams(query, 'get'));
    return data.shift().organization.user.id;
  }

  public get(criteria: { [key: string]: any }): Promise<Shop> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      criteria,
      this.wrapFields([
        'id',
        'shopName',
        'shopEmail',
        'shopSite',
        'shopAdditionalInfo',
        'isMainShop',
        this.getQuery('metro', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('city', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('shopAttributes', {}, [
          'id',
          'name',
          'code',
        ], true),
        this.getQuery('shopPhones', {}, [
          'id',
          'shopPhone',
          'comment',
          'position',
          'phoneIsConfirmed',
        ], true),
        this.getQuery('paymentMethods', {}, [
          'id',
          'name',
          'code',
        ], true),
        this.getQuery('shopSchedule', {}, [
          'dayNumber',
          'openingTime',
          'closingTime',
          'dinnerBeginTime',
          'dinnerEndTime',
        ], true),
        this.getQuery('shopAdditionalContacts', {}, [
          'id',
          'contact',
          'additionalContactIsConfirmed',
          this.getQuery('contactType', {}, [
            'name',
            'code',
          ], true),
        ], true),
        this.getQuery(
          'officialDealers',
          {},
          [
            'id',
            'brand',
          ],
          true,
        ),
        this.getQuery(
          'tradingManufacturers',
          {},
          [
            'id',
            'brand',
          ],
          true,
        ),
        this.getQuery(
          'organization',
          {},
          [
            this.getQuery(
              'legals',
              {},
              [
                'id',
                'name',
              ],
              true,
            ),
          ],
          true,
        ),
        this.getQuery('legal', {}, [
          'id',
          'name',
        ], true),
        'coordLat',
        'coordLng',
        'street',
        'building',
        'office',
        'shopScheduleString',
        'emailIsConfirmed',
        'siteIsConfirmed',
        'scheduleIsConfirmed',
        'addressIsConfirmed',
        this.getQuery('shopUrl', {}, [
          'itemUrl',
        ], true),
        'itemRedirect',
        'orderRedirect',
      ]),
    );
    return this._get(query);
  }

  public getShopInfo(id: number, organizationId?: number): Promise<Shop> {
    const query: GraphQLQuery = this.getQuery('infoGet', { id, organizationId }, this.wrapFields([
      this.getQuery(
        'feedbacks',
        {},
        [
          'id',
          'rating',
          'senderUserName',
          'senderComment',
          'isRead',
          'approved',
          'createdAt',
          'senderOrganizationLegalName',
          'answersCount',
          'likersCount',
          'senderClaim',
          'senderClaimStatus',
        ],
        true,
      ),
      this.getQuery(
        'organization',
        {},
        [
          'id',
          'inn',
          'legalName',
          'ftpDir',
          this.getQuery('user', {}, ['id'], true),
          this.getQuery(
            'legals',
            {},
            [
              'id',
              'name',
              'fullName',
              'inn',
              'kpp',
              'ogrn',
              'bankCity',
              'bankAccount',
              'correspondentAccount',
              'bik',
              'accountantName',
              'directorName',
              'directorPost',
              'legalAddress',
              'factAddress',
              'postAddress',
              'showToCustomers',
            ],
            true,
          ),
        ],
        true,
      ),
      this.getQuery(
        'shopData',
        {},
        [
          'id',
          'shop_name',
          'city_name',
          'email_exists',
          'phone_exists',
          'phones',
          'shop_site',
          'street',
          'lat',
          'lng',
          'building',
          'office',
          'shop_additional_info',
          'shop_payment_methods',
          'shop_attributes',
          'trading_manufacturers',
          'official_dealer',
          'count_feedbacks',
          'avg_rating',
          'legal_id',
          'contact_is_confirmed',
          'email_is_confirmed',
          'site_is_confirmed',
          'schedule_is_confirmed',
          'additional_contact_is_confirmed',
          'phone_is_confirmed',
          'address_is_confirmed',
          this.getQuery(
            'shop_schedule',
            {},
            [
              'day_number',
              'opening_time',
              'closing_time',
              'dinner_begin_time',
              'dinner_end_time',
            ],
            true,
          ),
          this.getQuery('additional_contacts', {}, [
            'type_id',
            'value',
          ], true),
        ],
        true,
      ),
      this.getQuery(
        'filials',
        {},
        [
          'id',
          'shop_name',
          'city_name',
          'lat',
          'lng',
          'street',
          'building',
          'office',
          'email_exists',
          'shop_site',
          'shop_additional_info',
          'phones',
          this.getQuery('additional_contacts', {}, [
            'type_id',
            'value',
          ], true),
        ],
        true,
      ),
    ]));
    return this._get(query);
  }

  public updateShop(item: ShopInput): Promise<Shop> {
    const query: GraphQLQuery = this.getQuery(
      'update',
      item,
      this.wrapFields([
        'id',
        'shopName',
        'shopEmail',
        'shopSite',
        'shopAdditionalInfo',
        'isMainShop',
        'coordLat',
        'coordLng',
        'street',
        'building',
        'office',
        'shopScheduleString',
        this.getQuery('paymentMethods', {}, [
          'id',
          'name',
          'code',
        ], true),
        this.getQuery('shopAttributes', {}, [
          'id',
          'name',
          'code',
        ], true),
        this.getQuery('tradingManufacturers', {}, [
          'id',
          'brand',
        ], true),
        this.getQuery('officialDealers', {}, [
          'id',
          'brand',
        ], true),
        this.getQuery('organization', {}, [
          'id',
          'inn',
        ], true),
        this.getQuery('legal', {}, [
          'id',
          'inn',
          'name',
        ], true),
        this.getQuery('city', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('metro', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('shopPhones', {}, [
          'id',
          'shopPhone',
          'comment',
          'position',
        ], true),
        this.getQuery('shopAdditionalContacts', {}, [
          'id',
          'contact',
          // 'position',
          this.getQuery('contactType', {}, [
            'id',
            'name',
            'code',
          ], true),
        ], true),
        this.getQuery('shopSchedule', {}, [
          'dayNumber',
          'openingTime',
          'closingTime',
          'dinnerBeginTime',
          'dinnerEndTime',
        ], true),
        this.getQuery('shopUrl', {}, [
          'itemUrl',
        ], true),
        'itemRedirect',
        'orderRedirect',
      ]),
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  public createShop(item: ShopInput): Promise<Shop> {
    const query: GraphQLQuery = this.getQuery(
      'create',
      item,
      this.wrapFields([
        'id',
        'shopName',
        'shopEmail',
        'shopSite',
        'shopAdditionalInfo',
        'isMainShop',
        'coordLat',
        'coordLng',
        'street',
        'building',
        'office',
        'shopScheduleString',
        this.getQuery('paymentMethods', {}, [
          'id',
          'name',
          'code',
        ], true),
        this.getQuery('shopAttributes', {}, [
          'id',
          'name',
          'code',
        ], true),
        this.getQuery('tradingManufacturers', {}, [
          'id',
          'brand',
        ], true),
        this.getQuery('officialDealers', {}, [
          'id',
          'brand',
        ], true),
        this.getQuery('organization', {}, [
          'id',
          'inn',
        ], true),
        this.getQuery('legal', {}, [
          'id',
          'inn',
          'name',
        ], true),
        this.getQuery('city', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('metro', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('shopPhones', {}, [
          'id',
          'shopPhone',
          'comment',
          'position',
        ], true),
        this.getQuery('shopAdditionalContacts', {}, [
          'id',
          'contact',
          this.getQuery('contactType', {}, [
            'id',
            'name',
            'code',
          ], true),
        ], true),
        this.getQuery('shopSchedule', {}, [
          'dayNumber',
          'openingTime',
          'closingTime',
          'dinnerBeginTime',
          'dinnerEndTime',
        ], true),
      ]),
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  public sendPhoneToConfirm(request: ShopRequestPhoneToConfirm): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'sendPhoneToConfirm',
      { ...request },
      ['success'],
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  public phoneConfirm(request: ShopRequestPhoneConfirm): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'phoneConfirm',
      { ...request },
      ['success'],
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  public sendEmailToConfirm(request: ShopRequestEmailToConfirm): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'sendEmailToConfirm',
      { ...request },
      ['success'],
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  public emailConfirm(request: ShopRequestEmailConfirm): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'emailConfirm',
      { ...request },
      ['success'],
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  protected getObject(data: object): Shop {
    return new Shop(data);
  }
}
