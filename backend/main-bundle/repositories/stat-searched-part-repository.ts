import { AbstractEntityRepository } from './abstract-entity-repository';
import { StatSearchedPart } from '../entities/stat-searched-part';

export class StatSearchedPartRepository extends AbstractEntityRepository<StatSearchedPart> {
  protected readonly queryNamespace: string = 'StatSearchPart';

  public create(item: StatSearchedPart): Promise<StatSearchedPart> {
    const query: GraphQLQuery = this.getQuery(
      'create',
      item,
      ['success'],
    );
    return this._create(query);
  }

  protected getObject(data: object): StatSearchedPart {
    return new StatSearchedPart(data);
  }
}
