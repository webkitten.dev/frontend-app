import { AbstractEntityRepository } from './abstract-entity-repository';
import { Sto } from '../entities/sto';
import { StoInput } from '../entities/sto-input';
import { StoRequestPhoneToConfirm } from '../entities/request-phone-send-to-confirm';
import { BooleanResult } from '../entities/boolean-result';
import { StoRequestPhoneConfirm } from '../entities/request-phone-confirm';
import { StoRequestEmailToConfirm } from '../entities/request-email-send-to-confirm';
import { StoRequestEmailConfirm } from '../entities/request-email-confirm';

export class StoRepository extends AbstractEntityRepository<Sto> {
  protected readonly queryNamespace: string = 'Sto';

  public updateSto(item: StoInput): Promise<Sto> {
    const query: GraphQLQuery = this.getQuery(
      'update',
      item,
      this.wrapFields([
        'id',
        'name',
        'address',
        'street',
        'building',
        'office',
        'email',
        'site',
        'additionalInfo',
        'coordLat',
        'coordLng',
        'hourPriceMin',
        'hourPriceMax',
        'spares',
        'nonSpecifiedServiceGroup',
        'sendNotifications',
        this.getQuery('city', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('metro', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('user', {}, [
          'id',
          'username',
        ], true),
        this.getQuery('organization', {}, [
          'id',
          'inn',
        ], true),
        this.getQuery('legal', {}, [
          'id',
          'inn',
          'name',
        ], true),
        this.getQuery('paymentMethods', {}, [
          'id',
          'name',
          'code',
        ], true),
        this.getQuery('schedules', {}, [
          'dayNumber',
          'openingTime',
          'closingTime',
          'dinnerBeginTime',
          'dinnerEndTime',
        ], true),
        this.getQuery('phones', {}, [
          'id',
          'phone',
          'comment',
          'position',
        ], true),
        this.getQuery('additionalContacts', {}, [
          'id',
          'contact',
          'position',
          this.getQuery('contactType', {}, [
            'id',
            'name',
            'code',
          ], true),
        ], true),
        this.getQuery('brands', {}, [
          'id',
          'brand',
        ], true),
        this.getQuery('serviceGroups', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('services', {}, [
          'id',
          'name',
          'name',
        ], true),
        this.getQuery('taskBrands', {}, [
          'id',
          'brand',
        ], true),
        this.getQuery('taskServiceGroups', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('taskServices', {}, [
          'id',
          'name',
        ], true),
      ]),
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  public createSto(item: StoInput): Promise<Sto> {
    const query: GraphQLQuery = this.getQuery(
      'create',
      item,
      this.wrapFields([
        'id',
        'name',
        'address',
        'street',
        'building',
        'office',
        'email',
        'site',
        'additionalInfo',
        'coordLat',
        'coordLng',
        'hourPriceMin',
        'hourPriceMax',
        'spares',
        'nonSpecifiedServiceGroup',
        'sendNotifications',
        this.getQuery('city', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('metro', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('user', {}, [
          'id',
          'username',
        ], true),
        this.getQuery('organization', {}, [
          'id',
          'inn',
        ], true),
        this.getQuery('legal', {}, [
          'id',
          'inn',
          'name',
        ], true),
        this.getQuery('paymentMethods', {}, [
          'id',
          'name',
          'code',
        ], true),
        this.getQuery('schedules', {}, [
          'dayNumber',
          'openingTime',
          'closingTime',
          'dinnerBeginTime',
          'dinnerEndTime',
        ], true),
        this.getQuery('phones', {}, [
          'id',
          'phone',
          'comment',
          'position',
        ], true),
        this.getQuery('additionalContacts', {}, [
          'id',
          'contact',
          'position',
          this.getQuery('contactType', {}, [
            'id',
            'name',
            'code',
          ], true),
        ], true),
        this.getQuery('brands', {}, [
          'id',
          'brand',
        ], true),
        this.getQuery('serviceGroups', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('services', {}, [
          'id',
          'name',
          'name',
        ], true),
        this.getQuery('taskBrands', {}, [
          'id',
          'brand',
        ], true),
        this.getQuery('taskServiceGroups', {}, [
          'id',
          'name',
        ], true),
        this.getQuery('taskServices', {}, [
          'id',
          'name',
        ], true),
      ]),
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  public get(criteria: { [key: string]: any }): Promise<Sto> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      criteria,
      this.wrapFields(this.getFields()),
    );
    return this._get(query);
  }

  public sendPhoneToConfirm(request: StoRequestPhoneToConfirm): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'sendPhoneToConfirm',
      { ...request },
      ['success'],
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  public phoneConfirm(request: StoRequestPhoneConfirm): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'phoneConfirm',
      { ...request },
      ['success'],
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  public sendEmailToConfirm(request: StoRequestEmailToConfirm): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'sendEmailToConfirm',
      { ...request },
      ['success'],
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  public emailConfirm(request: StoRequestEmailConfirm): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'emailConfirm',
      { ...request },
      ['success'],
    );

    return this.request(this.getRequestParams(query, 'post'));
  }

  protected getFields(): any[] {
    return [
      'id',
      'name',
      'email',
      'site',
      'additionalInfo',
      'spares',
      'nonSpecifiedServiceGroup',
      'sendNotifications',
      'hourPriceMin',
      'hourPriceMax',
      this.getQuery('city', {}, [
        'id',
        'name',
      ], true),
      this.getQuery('metro', {}, [
        'id',
        'name',
      ], true),
      this.getQuery('phones', {}, [
        'id',
        'phone',
        'comment',
        'position',
        'phoneIsConfirmed',
      ], true),
      this.getQuery('paymentMethods', {}, [
        'id',
        'name',
        'code',
      ], true),
      this.getQuery('schedules', {}, [
        'dayNumber',
        'openingTime',
        'closingTime',
        'dinnerBeginTime',
        'dinnerEndTime',
      ], true),
      this.getQuery('additionalContacts', {}, [
        'id',
        'contact',
        'additionalContactIsConfirmed',
        this.getQuery('contactType', {}, [
          'name',
          'code',
        ], true),
      ], true),
      this.getQuery(
        'brands',
        {},
        [
          'id',
          'brand',
        ],
        true,
      ),
      this.getQuery(
        'taskBrands',
        {},
        [
          'id',
          'brand',
        ],
        true,
      ),
      this.getQuery(
        'taskServiceGroups',
        {},
        [
          'id',
          'name',
        ],
        true,
      ),
      this.getQuery(
        'taskServices',
        {},
        [
          'id',
          'name',
        ],
        true,
      ),
      this.getQuery(
        'services',
        {},
        [
          'id',
          'name',
        ],
        true,
      ),
      this.getQuery(
        'organization',
        {},
        [
          this.getQuery(
            'legals',
            {},
            [
              'id',
              'name',
            ],
            true,
          ),
        ],
        true,
      ),
      this.getQuery('legal', {}, [
        'id',
        'name',
      ], true),
      'coordLat',
      'coordLng',
      'street',
      'building',
      'office',
      'emailIsConfirmed',
      'siteIsConfirmed',
      'scheduleIsConfirmed',
      'addressIsConfirmed',
    ];
  }

  protected getObject(data: object): Sto {
    return new Sto(data);
  }
}
