import { AbstractRepository } from './abstract-repository';

export class TechSupportRepository extends AbstractRepository {
  protected readonly queryNamespace: string = 'TechSupport';

  public createRequest(
    receiver: string,
    city: number,
    name: string,
    phone: string,
    email: string,
    message: string,
  ): Promise<object> {
    const query: GraphQLQuery = this.getQuery(
      'createRequest',
      {
        receiver,
        name,
        phone,
        email,
        message,
        city: +city,
      },
      ['success'],
    );
    return this.request(this.getRequestParams(query, 'post'));
  }
}
