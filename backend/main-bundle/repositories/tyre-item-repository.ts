import { AbstractEntityRepository } from './abstract-entity-repository';
import { TyreItem } from '../entities/tyre-item';
import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';

export class TyreItemRepository extends AbstractEntityRepository<TyreItem> {
  protected readonly queryNamespace: string = 'TyreItem';

  public tags(criteria: object): Promise<EntityListResolveType<object>> {
    const query: GraphQLQuery = this.getQuery(
      'tags',
      criteria,
      this.wrapFields([
        'label',
        'slug',
        this.getQuery(
          'filters',
          {},
          [
            'season',
            'brand',
            'model',
            'width',
            'profile',
            'diameter',
            'carManufacturer',
            'carModel',
            'carType',
            'carYear',
          ],
          true,
        ),
      ]),
    );
    return this._list(query);
  }

  public filters(criteria: object): Promise<object> {
    const query: GraphQLQuery = this.getQuery(
      'filters',
      criteria,
      this.wrapFields([
        this.getFilterFields('season'),
        this.getFilterFields('brand'),
        this.getFilterFields('model'),
        this.getFilterFields('width'),
        this.getFilterFields('profile'),
        this.getFilterFields('diameter'),
        this.getFilterFields('carManufacturer'),
        this.getFilterFields('carModel'),
        this.getFilterFields('carType'),
        this.getFilterFields('carYear'),
      ]),
    );
    return this._get(query);
  }

  public list(
    criteria?: object,
    limit?: number,
    offset?: number,
    orderField?: string,
    orderDirection?: string,
  ): Promise<EntityListResolveType<TyreItem>> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      Object.assign(criteria || {}, {
        listLimit: limit && +limit,
        listOffset: offset && +offset,
        listOrderField: orderField,
        listOrderDirection: orderDirection,
      }),
      this.wrapFields([
        'id',
        'modelName',
        'width',
        'profile',
        'diameter',
        'size',
        'season',
        'hasStud',
        'loadIndex',
        'loadIndexAlt',
        'speedIndex',
        'minPrice',
        'isInch',
        this.getQuery(
          'part',
          {},
          [
            'fullTyreName',
            'vendorCode',
            this.getQuery('manufacturer', {}, ['id', 'brand', 'laximoBrand'], true),
            this.getQuery('image', {}, ['id', this.getQuery('mediumFile', {}, ['id', 'name'], true)], true),
          ],
          true,
        ),
        this.getQuery('tyreName', {}, ['name', 'slug'], true),
      ]),
    );
    return this._list(query);
  }

  public count(criteria: object): Promise<number> {
    const query: GraphQLQuery = this.getQuery(
      'count',
      criteria,
      ['count'],
    );
    return this._count(query);
  }

  protected getObject(data: object): TyreItem {
    return new TyreItem(data);
  }

  protected getFilterFields(name: string): GraphQLQuery {
    return this.getQuery(name, {}, ['id', 'name'], true);
  }
}
