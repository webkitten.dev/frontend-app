import { AbstractEntityRepository } from './abstract-entity-repository';
import { TyreWheelItem } from '../entities/tyre-wheel-item';

export class TyreWheelItemRepository extends AbstractEntityRepository<TyreWheelItem> {
  protected readonly queryNamespace: string = 'TyreWheelItem';

  public count(): Promise<number> {
    const query: GraphQLQuery = this.getQuery(
      'count',
      {},
      ['count'],
    );
    return this._count(query);
  }

  protected getObject(data: object): TyreWheelItem {
    return new TyreWheelItem(data);
  }
}
