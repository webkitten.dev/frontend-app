import { AbstractEntityRepository } from './abstract-entity-repository';
import { TyreWheelSeller } from '../entities/tyre-wheel-seller';

export class TyreWheelSellerRepository extends AbstractEntityRepository<TyreWheelSeller> {
  protected readonly queryNamespace: string = 'TyreWheelSeller';

  public count(): Promise<number> {
    const query: GraphQLQuery = this.getQuery(
      'count',
      {},
      ['count'],
    );
    return this._count(query);
  }

  protected getObject(data: object): TyreWheelSeller {
    return new TyreWheelSeller(data);
  }
}
