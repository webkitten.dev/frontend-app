import { AbstractEntityRepository } from './abstract-entity-repository';
import { User } from '../entities/user';
import { Shop } from '../entities/shop';
import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';
import { BooleanResult } from '../entities/boolean-result';

import config from 'config';
import { ProfileNotification } from '../entities/profile-notification';
import { RequestUniqueAdditionalContact } from '../entities/request-unique-additional-contact';

export class UserRepository extends AbstractEntityRepository<User> {
  protected readonly queryNamespace: string = 'User';

  public list(
    criteria?: object,
    limit?: number,
    offset?: number,
    orderField?: string,
    orderDirection?: string,
  ): Promise<EntityListResolveType<User>> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      Object.assign(criteria || {}, {
        listLimit: limit && +limit,
        listOffset: offset && +offset,
        listOrderField: orderField,
        listOrderDirection: orderDirection,
      }),
      this.wrapFields(this.getFields()),
    );
    return this._list(query);
  }

  public setupNotification(userId: number, notificationList: ProfileNotification[]): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'setupNotification',
      { userId, notificationList },
      ['success'],
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  public getNotificationSetup(id: number): Promise<ProfileNotification[]> {
    const query: GraphQLQuery = this.getQuery(
      'getNotification',
      { userId: id },
      this.wrapFields([
        'notificationId',
        'mainSwitch',
        'mainMail',
        'additionalMailSwitch',
        'additionalMailValue',
        this.getQuery('notificationType', {}, ['id', 'name', 'code'], true),
      ]),
    );
    return this.request(this.getRequestParams(query, 'get'));
  }

  public get(criteria: object): Promise<User> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      criteria,
      this.wrapFields(this.getFields()),
    );
    return this._get(query);
  }

  public create(item: User): Promise<User> {
    const query: GraphQLQuery = this.getQuery(
      'create',
      item,
      this.wrapFields(['id']),
    );
    return this._create(query);
  }

  public createChatUser(item: User): Promise<User> {
    const query: GraphQLQuery = this.getQuery(
      'createChatUser',
      item,
      this.wrapFields(this.getFields()),
    );
    return this._create(query);
  }

  public createGuest(item: User): Promise<User> {
    const query: GraphQLQuery = this.getQuery(
      'createGuest',
      item,
      this.wrapFields(this.getFields()),
    );
    return this._create(query);
  }

  public recoverPassword(email: string): Promise<object> {
    const query: GraphQLQuery = this.getQuery(
      'recoverPassword',
      {
        email,
        baseUrl: `https://${config.get('domains.main')}/users/recover-password?token=`,
      },
      ['success', 'message'],
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  public getCounters(criteria: object): Promise<object> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      criteria,
      this.wrapFields([
        'countIncomingRequests',
        'countNewIncomingRequests',
        'countOutgoingRequests',
        'countNewOutgoingRequests',
        'countUnreadReviews',
        'hasMyReviews',
        'hasAboutMeReviews',
      ]),
    );
    return this._get(query);
  }

  public getShops(criteria: object): Promise<Shop[]> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      criteria,
      this.wrapFields([
        this.getQuery('organization', {}, [this.getQuery('shops', {}, ['id', 'shopName'], true)], true),
      ]),
    );
    return new Promise((resolve, reject) => {
      this._get(query)
      .then((data: User) => {
        resolve(data.organization!.shops);
      })
      .catch((error) => {
        reject(error);
      });
    });
  }

  public updateUser(item: User): Promise<User[]> {
    const query: GraphQLQuery = this.getQuery(
      'update',
      item,
      this.wrapFields(this.getFields()),
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  public sendToConfirm(criteria: object, whatToConfirm: string): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      `send${whatToConfirm}ToConfirm`,
      criteria,
      [
        'success',
      ],
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  public confirm(criteria: object, whatToConfirm: string): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      `confirm${whatToConfirm}`,
      criteria,
      [
        'success',
      ],
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  // TODO: type for additionalInformationRequest
  // Но очень нравится конструкция с получением данных
  public checkUniquePhone(
    phone: string,
    additionalInformationRequest: { shopId?: number, stoId?: number, userId?: number },
  ): Promise<BooleanResult> {
    const shopId = (additionalInformationRequest.shopId && Number(additionalInformationRequest.shopId)) || null;
    const stoId = (additionalInformationRequest.stoId && Number(additionalInformationRequest.stoId)) || null;
    const userId = additionalInformationRequest.userId || null;

    const query: GraphQLQuery = this.getQuery(
      'checkUniquePhone',
      { phone, shopId, stoId, userId },
      ['success'],
    );

    return this.request(this.getRequestParams(query, 'get'))
      .then(response => response.success)
      .catch(error => false);
  }

  public checkUniqueAdditionalContact(request: RequestUniqueAdditionalContact): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'checkUniqueAdditionalContact',
      { ...request },
      ['success'],
    );

    return this.request(this.getRequestParams(query, 'get'))
      .then(response => response.success)
      .catch(error => false);
  }

  public checkUniqueEmail(request: { email: string, userId: number }): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'checkUniqueEmail',
      { ...request },
      ['success'],
    );

    return this.request(this.getRequestParams(query, 'get'))
      .then(response => response.success)
      .catch(error => false);
  }

  public checkRecoveryToken(token: string): Promise<BooleanResult> {
    const query: GraphQLQuery = this.getQuery(
      'checkRecoveryToken',
      { token },
      ['success'],
    );
    return this.request(this.getRequestParams(query, 'get'));
  }

  public recoverPasswordByToken(token: string, password: string): Promise<User> {
    const query: GraphQLQuery = this.getQuery(
      'recoverPasswordByToken',
      { token, password },
      this.wrapFields(this.getFields()),
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  protected getObject(data: object): User {
    return new User(data);
  }

  protected getFields(): any[] {
    return [
      'id',
      'username',
      'displayedName',
      'organizationSended',
      'isOrganization',
      'isOrganizationWithShop',
      'isCompleteProfile',
      this.getQuery('userProfile', {}, [
        'lastName',
        'firstName',
        'middleName',
        'phone',
        this.getQuery('city', {}, [
          'id',
          'name',
          this.getQuery('parent', {}, [
            'id',
            'name',
          ], true),
        ], true),
        'confirmedPhone',
        'confirmedEmail',
      ], true),
      this.getQuery('organization', {}, [
        'id',
        'legalName',
        this.getQuery('legals', {}, [
          'id',
          'name',
          'fullName',
          'inn',
          'kpp',
          'ogrn',
          'bankCity',
          'bankAccount',
          'correspondentAccount',
          'bik',
          'accountantName',
          'directorName',
          'directorPost',
          'legalAddress',
          'factAddress',
          'postAddress',
          'showToCustomers',
          this.getQuery('shops', {}, [
            'id',
            'shopName',
          ], true),
          this.getQuery('autoServices', {}, [
            'id',
            'name',
          ], true),
        ], true),
      ], true),
      'chatRole',
    ];
  }
}
