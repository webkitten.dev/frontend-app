import { AbstractEntityRepository } from './abstract-entity-repository';
import { VinRequest } from '../entities/vin-request';

export class VinRequestRepository extends AbstractEntityRepository<VinRequest> {
  protected readonly queryNamespace: string = 'VinRequest';

  public create(item: VinRequest): Promise<VinRequest> {
    const query: GraphQLQuery = this.getQuery(
      'create',
      item,
      [
        'id',
        'sellerCounter',
        'customerName',
        'customerEmail',
        'customerPhone',
        'carManufacturer',
        'carModel',
        'carBody',
        'carReleaseYear',
        'carEngine',
        this.getQuery(
          'partList',
          {},
          [
            'id',
            'partName',
            'partVendorCode',
            'quantity',
            this.getQuery('partCondition', {}, ['id', 'name', 'code'], true),
          ],
          true,
        ),
      ],
    );
    return this.request(this.getRequestParams(query, 'post'));
  }

  public vinInfo(criteria: object): Promise<object> {
    const query: GraphQLQuery = this.getQuery('vinInfo', criteria, [
      'manufacturer',
      'model',
      'body',
      'engine',
      'year',
      'manufacturer_name',
      'model_name',
      'udakovService',
    ]);
    return this.request(this.getRequestParams(query, 'get'));
  }

  protected getObject(data: object): VinRequest {
    return new VinRequest(data);
  }
}
