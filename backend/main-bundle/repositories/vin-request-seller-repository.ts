import { AbstractEntityRepository } from './abstract-entity-repository';
import { VinRequestSeller } from '../entities/vin-request-seller';

export class VinRequestSellerRepository extends AbstractEntityRepository<VinRequestSeller> {
  protected readonly queryNamespace: string = 'VinRequestSeller';

  public count(): Promise<number> {
    const query: GraphQLQuery = this.getQuery(
      'count',
      {},
      ['count'],
    );
    return this._count(query);
  }

  protected getObject(data: object): VinRequestSeller {
    return new VinRequestSeller(data);
  }
}
