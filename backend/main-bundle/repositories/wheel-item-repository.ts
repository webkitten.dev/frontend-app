import { AbstractEntityRepository } from './abstract-entity-repository';
import { WheelItem } from '../entities/wheel-item';
import { EntityListResolveType } from '../../common-bundle/essence/entity-list-resolve-type';

export class WheelItemRepository extends AbstractEntityRepository<WheelItem> {
  protected readonly queryNamespace: string = 'WheelItem';

  public filters(criteria: object): Promise<object> {
    const query: GraphQLQuery = this.getQuery(
      'filters',
      criteria,
      this.wrapFields([
        this.getFilterFields('type'),
        this.getFilterFields('brand'),
        this.getFilterFields('width'),
        this.getFilterFields('diameter'),
        this.getFilterFields('drill'),
        this.getFilterFields('offset'),
        this.getFilterFields('centerDiameter'),
        this.getFilterFields('carManufacturer'),
        this.getFilterFields('carModel'),
        this.getFilterFields('carType'),
        this.getFilterFields('carYear'),
      ]),
    );
    return this._get(query);
  }

  public list(
    criteria?: object,
    limit?: number,
    offset?: number,
    orderField?: string,
    orderDirection?: string,
  ): Promise<EntityListResolveType<WheelItem>> {
    const query: GraphQLQuery = this.getQuery(
      'get',
      Object.assign(criteria || {}, {
        listLimit: limit && +limit,
        listOffset: offset && +offset,
        listOrderField: orderField,
        listOrderDirection: orderDirection,
      }),
      this.wrapFields([
        'part_image_id',
        'part_id',
        'part_manufacturer_brand',
        'part_manufacturer_slug',
        'vendor_code',
        'model_name',
        'width',
        'diameter',
        'pcd',
        'drill',
        'center_diameter',
        'offset',
        'type',
        'min_price',
      ]),
    );
    return this._list(query);
  }

  public count(criteria: object): Promise<number> {
    const query: GraphQLQuery = this.getQuery(
      'count',
      criteria,
      ['count'],
    );
    return this._count(query);
  }

  protected getObject(data: object): WheelItem {
    return new WheelItem(data);
  }

  protected getFilterFields(name: string): GraphQLQuery {
    return this.getQuery(name, {}, ['id', 'name'], true);
  }
}
