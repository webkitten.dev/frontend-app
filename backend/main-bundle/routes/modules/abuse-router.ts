import { Router } from 'express';
import * as controller from '../../controllers/abuse-controller';

const router = Router();
router.post('/create', controller.create);

export default {
  router,
  prefix: '/abuse',
};
