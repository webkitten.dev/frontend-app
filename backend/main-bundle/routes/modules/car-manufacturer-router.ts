import { Router } from 'express';
import * as controller from '../../controllers/car-manufacturer-controller';

const router = Router();
router.get('/list', controller.list);
router.get('/get', controller.get);
router.get('/getWithRegion', controller.getWithRegion);

export default {
  router,
  prefix: '/carManufacturer',
};
