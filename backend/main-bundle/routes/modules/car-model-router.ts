import { Router } from 'express';
import * as controller from '../../controllers/car-model-controller';

const router = Router();
router.get('/getByManufacturerId', controller.getByManufacturerId);

export default {
  router,
  prefix: '/carModel',
};
