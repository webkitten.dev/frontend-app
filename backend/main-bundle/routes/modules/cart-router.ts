import { Router } from 'express';
import * as controller from '../../controllers/cart-controller';

const router = Router();
router.get('/count', controller.count);
router.get('/get', controller.get);
router.post('/update', controller.update);
router.post('/add', controller.add);

export default {
  router,
  prefix: '/cart',
};
