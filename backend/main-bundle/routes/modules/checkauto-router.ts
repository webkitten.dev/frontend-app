import { Router } from 'express';
import * as controller from '../../controllers/checkauto-controller';

const router = Router();
router.get('/info', controller.info);
router.get('/shortReportInfo', controller.shortReportInfo);
router.get('/fullReportInfo', controller.fullReportInfo);
router.get('/init', controller.init);
router.post('/create', controller.create);
router.post('/updateRestrictions', controller.updateRestrictions);
router.post('/createPayment', controller.createPayment);
router.get('/promoCodeInfo', controller.promoCodeInfo);

export default {
  router,
  prefix: '/checkauto',
};
