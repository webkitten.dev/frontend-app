import { Router } from 'express';
import * as controller from '../../controllers/city-seller-controller';

const router = Router();
router.get('/count', controller.count);

export default {
  router,
  prefix: '/citySeller',
};
