import { Router } from 'express';
import * as controller from '../../controllers/concierge-partnership-request-controller';

const router = Router();
router.post('/create', controller.create);

export default {
  router,
  prefix: '/conciergePartnershipRequest',
};
