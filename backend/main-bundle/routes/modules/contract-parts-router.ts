import { Router } from 'express';
import * as controller from '../../controllers/contract-parts-controller';

const router = Router();
router.get('/get', controller.get);
router.get('/list', controller.list);
router.get('/filters', controller.filters);
router.get('/getCarManufacturers', controller.getCarManufacturers);
router.get('/getCarModels', controller.getCarModels);
router.get('/countParts', controller.countParts);
router.get('/countMerchants', controller.countMerchants);
router.get('/getPartNameSuggestions', controller.getPartNameSuggestions);

export default {
  router,
  prefix: '/contractParts',
};
