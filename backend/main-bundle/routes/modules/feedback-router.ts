import { Router } from 'express';
import * as controller from '../../controllers/feedback-controller';

const router = Router();
router.post('/create', controller.create);
router.post('/addLike', controller.addLike);

export default {
  router,
  prefix: '/feedback',
};
