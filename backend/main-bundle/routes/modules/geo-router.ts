import { Router } from 'express';
import * as controller from '../../controllers/geo-controller';

const router = Router();
router.get('/get', controller.get);
router.get('/getTree', controller.getTree);
router.get('/list', controller.list);
router.get('/resolveIp', controller.resolveIp);

export default {
  router,
  prefix: '/geo',
};
