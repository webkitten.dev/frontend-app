import { Router } from 'express';
import * as controller from '../../controllers/info-controller';

const router = Router();
router.get('/counters', controller.counters);

export default {
  router,
  prefix: '/info',
};
