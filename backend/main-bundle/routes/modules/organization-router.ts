import { Router } from 'express';
import * as controller from '../../controllers/organization-controller';

const router = Router();
router.get('/getShopSto', controller.getShopSto);
router.get('/getByInn', controller.getByInn);
router.get('/getBankInfo', controller.getBankInfo);
router.post('/create', controller.create);
router.post('/update', controller.update);

export default {
  router,
  prefix: '/organization',
};
