import { Router } from 'express';
import * as controller from '../../controllers/part-categories-controller';

const router = Router();
router.get('/getAsyncData', controller.getAsyncData);
router.get('/getPartGridFilters', controller.getPartGridFilters);
router.get('/getPartGridList', controller.getPartGridList);
router.get('/getTree', controller.getTree);

export default {
  router,
  prefix: '/partCategories',
};
