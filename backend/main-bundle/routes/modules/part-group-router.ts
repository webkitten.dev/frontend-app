import { Router } from 'express';
import * as controller from '../../controllers/part-group-controller';

const router = Router();
router.get('/getPopular', controller.getPopular);

export default {
  router,
  prefix: '/partGroup',
};
