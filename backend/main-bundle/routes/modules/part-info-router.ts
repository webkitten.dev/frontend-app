import { Router } from 'express';
import * as controller from '../../controllers/part-info-controller';

const router = Router();
router.get('/get', controller.get);
router.get('/getApplicability', controller.getApplicability);

export default {
  router,
  prefix: '/partInfo',
};
