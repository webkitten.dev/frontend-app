import { Router } from 'express';
import * as controller from '../../controllers/part-item-name-controller';

const router = Router();
router.get('/get', controller.get);
router.get('/getPartNameBySlug', controller.getPartNameBySlug);
router.get('/getPartNameById', controller.getPartNameById);
router.get('/getPartManufacturersByPartName', controller.getPartManufacturersByPartName);

export default {
  router,
  prefix: '/partItemName',
};
