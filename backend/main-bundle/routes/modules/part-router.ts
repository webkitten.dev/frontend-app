import { Router } from 'express';
import * as itemController from '../../controllers/part-item-controller';
import * as sellerController from '../../controllers/part-seller-controller';

const router = Router();
router.get('/filter', itemController.filter);
router.get('/list', itemController.list);
router.get('/countItem', itemController.count);
router.get('/getLastRequested', itemController.getLastRequested);
router.get('/countSeller', sellerController.count);
router.post('/createFastOrder', itemController.createFastOrder);
router.get('/popular', itemController.popular);
router.get('/availability', itemController.availability);
router.get('/getPartManufacturerBySlug', itemController.getPartManufacturerBySlug);

export default {
  router,
  prefix: '/part',
};
