import { Router } from 'express';
import * as controller from '../../controllers/ref-additional-contact-methods-controller';

const router = Router();
router.get('/get', controller.get);

export default {
  router,
  prefix: '/refAdditionalContactMethods',
};
