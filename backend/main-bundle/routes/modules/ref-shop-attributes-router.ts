import { Router } from 'express';
import * as controller from '../../controllers/ref-shop-attributes-controller';

const router = Router();
router.get('/get', controller.get);

export default {
  router,
  prefix: '/refShopAttributes',
};
