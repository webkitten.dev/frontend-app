import { Router } from 'express';
import * as controller from '../../controllers/ref-sto-service-groups';

const router = Router();
router.get('/get', controller.get);

export default {
  router,
  prefix: '/refStoServiceGroups',
};
