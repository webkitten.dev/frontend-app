import { Router } from 'express';
import * as controller from '../../controllers/search-controller';

const router = Router();
router.get('/list', controller.list);

export default {
  router,
  prefix: '/search',
};
