import { Router } from 'express';
import * as controller from '../../controllers/seo-controller';

const router = Router();
router.get('/slogan', controller.slogan);
router.get('/contents', controller.contents);

export default {
  router,
  prefix: '/seo',
};
