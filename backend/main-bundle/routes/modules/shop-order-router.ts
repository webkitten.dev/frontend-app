import { Router } from 'express';
import * as controller from '../../controllers/shop-order-controller';

const router = Router();
router.post('/create', controller.create);

export default {
  router,
  prefix: '/shopOrder',
};
