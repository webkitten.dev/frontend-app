import { Router } from 'express';
import * as controller from '../../controllers/shop-controller';

const router = Router();
router.get('/infoGet', controller.infoGet);
router.get('/get', controller.get);
router.post('/update', controller.updateShop);
router.post('/create', controller.createShop);
router.post('/sendPhoneToConfirm', controller.sendPhoneToConfirm);
router.post('/phoneConfirm', controller.phoneConfirm);
router.post('/sendEmailToConfirm', controller.sendEmailToConfirm);
router.post('/emailConfirm', controller.emailConfirm);

export default {
  router,
  prefix: '/shop',
};
