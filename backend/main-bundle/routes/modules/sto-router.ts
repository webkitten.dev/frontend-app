import { Router } from 'express';
import * as controller from '../../controllers/sto-controller';

const router = Router();
router.get('/get', controller.get);
router.post('/update', controller.updateSto);
router.post('/create', controller.createSto);
router.post('/sendPhoneToConfirm', controller.sendPhoneToConfirm);
router.post('/phoneConfirm', controller.phoneConfirm);
router.post('/sendEmailToConfirm', controller.sendEmailToConfirm);
router.post('/emailConfirm', controller.emailConfirm);

export default {
  router,
  prefix: '/sto',
};
