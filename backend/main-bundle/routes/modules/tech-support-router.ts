import { Router } from 'express';
import * as controller from '../../controllers/tech-support-controller';

const router = Router();
router.post('/createRequest', controller.createRequest);

export default {
  router,
  prefix: '/techSupport',
};
