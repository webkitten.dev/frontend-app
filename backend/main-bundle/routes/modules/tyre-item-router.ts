import { Router } from 'express';
import * as controller from '../../controllers/tyre-item-controller';

const router = Router();
router.get('/list', controller.list);
router.get('/filters', controller.filters);
router.get('/tags', controller.tags);
router.get('/count', controller.count);

export default {
  router,
  prefix: '/tyreItem',
};
