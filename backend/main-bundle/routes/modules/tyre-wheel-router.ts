import { Router } from 'express';
import * as itemController from '../../controllers/tyre-wheel-item-controller';
import * as sellerController from '../../controllers/tyre-wheel-seller-controller';

const router = Router();
router.get('/countItem', itemController.count);
router.get('/countSeller', sellerController.count);

export default {
  router,
  prefix: '/tyreWheel',
};
