import { Router } from 'express';
import * as controller from '../../controllers/user-controller';

const router = Router();
router.get('/get', controller.get);
router.get('/getCounters', controller.getCounters);
router.get('/getMyShops', controller.getMyShops);
router.get('/refresh', controller.refresh);
router.get('/fetch', controller.fetch);
router.get('/checkRecoveryToken', controller.checkRecoveryToken);
router.get('/getNotificationSetup', controller.getNotificationSetup);
router.get('/checkUniquePhone', controller.checkUniquePhone);
router.get('/checkUniqueEmail', controller.checkUniqueEmail);
router.get('/checkUniqueAdditionalContact', controller.checkUniqueAdditionalContact);
router.post('/recoverPassword', controller.recoverPassword);
router.post('/create', controller.create);
router.post('/createChatUser', controller.createChatUser);
router.post('/createGuest', controller.createGuest);
router.post('/update', controller.update);
router.post('/sendPhoneToConfirm', controller.sendPhoneToConfirm);
router.post('/confirmPhone', controller.confirmPhone);
router.post('/sendEmailToConfirm', controller.sendEmailToConfirm);
router.post('/confirmEmail', controller.confirmEmail);
router.post('/setupNotification', controller.setupNotification);
router.post('/recoverPasswordByToken', controller.recoverPasswordByToken);

export default {
  router,
  prefix: '/user',
};
