import { Router } from 'express';
import * as sellerController from '../../controllers/vin-request-seller-controller';
import * as requestController from '../../controllers/vin-request-controller';

const router = Router();
router.get('/countSeller', sellerController.count);
router.post('/createVinRequest', requestController.create);
router.get('/vinInfo', requestController.vinInfo);

export default {
  router,
  prefix: '/vinRequest',
};
