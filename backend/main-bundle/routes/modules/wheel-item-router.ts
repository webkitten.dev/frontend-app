import { Router } from 'express';
import * as controller from '../../controllers/wheel-item-controller';

const router = Router();
router.get('/list', controller.list);
router.get('/filters', controller.filters);
router.get('/count', controller.count);

export default {
  router,
  prefix: '/wheelItem',
};
