import config from 'config';
import fs from 'fs';
import path from 'path';
import vhost from 'vhost';
import { Application as ExpressApplication } from 'express';
import { Nuxt } from 'nuxt-start';

import {
  LoggerContainer,
  LoggerCategories,
} from './common-bundle/logger';
const logger = LoggerContainer.get(LoggerCategories.APP);

const getNuxtApps = () => {
  const nuxtConfigFolder = path.resolve(__dirname, '../config/');
  const nuxtConfigFiles = fs
    .readdirSync(nuxtConfigFolder)
    .filter(file => file.charAt(0) !== '_')
    .filter(file => config.has(`domains.${file.slice(0, -3)}`));

  return nuxtConfigFiles
    .map(file => ({
      host: String(config.get(`domains.${file.slice(0, -3)}`)),
      config: Object(require(`${nuxtConfigFolder}/${file}`)),
    }));
};

export default async (app: ExpressApplication | null = null, builder: any = null) => {
  let nuxtApps = getNuxtApps();
  if (process.env.MAIN_DOMAIN_MODE) {
    nuxtApps = nuxtApps.filter(app => app.host === config.get('domains.main'));
  }
  for (const nuxtApp of nuxtApps) {
    const nuxt = new Nuxt(nuxtApp.config);
    await nuxt.ready();
    if (app) {
      if (process.env.MAIN_DOMAIN_MODE) {
        app.use(nuxt.render);
      } else {
        app.use(vhost(nuxtApp.host, nuxt.render));
      }
      logger.info(`Renderer ${nuxtApp.host}`);
    }
    if (builder) {
      logger.info(`Building ${nuxtApp.host} start`);
      await new builder(nuxt)
        .build()
        .catch((e: any) => {
          logger.error(e);
          process.exit(1);
        });
      logger.info(`Building ${nuxtApp.host} end`);
    }
  }
};
