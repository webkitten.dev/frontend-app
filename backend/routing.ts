import {
  Router,
  Request,
  Response,
  NextFunction,
} from 'express';
import * as utils from './common-bundle/utils';

import mainRoutes from './main-bundle/routes';
import searchRoutes from './search-bundle/routes';
import statRoutes from './stats-bundle/routes';
import chatRoutes from './chat-bundle/routes';
import clickRoutes from './click-bundle/routes';

const router = Router();
router.use('/_main', mainRoutes);
router.use('/_search', searchRoutes);
router.use('/_stats', statRoutes);
router.use('/_chat', chatRoutes);
router.use('/_click', clickRoutes);

router.use('/ping', (req: Request, res: Response, next: NextFunction) => {
  utils.sendSuccess(res)(true);
});

export default router;
