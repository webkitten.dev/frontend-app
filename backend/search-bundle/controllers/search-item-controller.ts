import {
  Request,
  Response,
  NextFunction,
} from 'express';
import {
  isEmpty as _isEmpty,
} from 'lodash';
import * as utils from '../../common-bundle/utils';

import { SearchItemRepository } from '../repositories/search-item-repository';
const repository: SearchItemRepository = new SearchItemRepository();

export const find = utils.wrapAsyncMethod(async (req: Request, res: Response, next: NextFunction) => {
  if (!req.query.query) {
    utils.throwError(400, 'bad request')(new Error('Не передана поисковая строка'));
  }
  const data: object = await repository
    .find(req.query.query)
    .then(
      utils.throwIf(r => _isEmpty(r), 404, 'not found', new Error('Ничего не найдено')),
      utils.throwError(500, 'api error'),
    );
  utils.sendSuccess(res)(data);
});
