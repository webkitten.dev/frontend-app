import { AxiosRequestConfig } from 'axios';

import { AbstractRepository as CommonAbstractRepository } from '../../common-bundle/io/abstract-repository';
import { Request as CommonRequest } from '../../common-bundle/essence/request';
import { SearchItem } from '../entities/search-item';

export class SearchItemRepository extends CommonAbstractRepository {
  public find(query: string): Promise<SearchItem[]> {
    return this.request(this.getRequestParams({ string: query }));
  }

  protected getRequestParams(query: { string: string }): CommonRequest {
    const requestConfig: AxiosRequestConfig = {
      url: process.env.SERVICE_SEARCH_URI,
      method: 'get',
    };
    requestConfig.url += `?q=${encodeURIComponent(query.string)}`;
    return {
      prepared: requestConfig,
      requestType: 'q',
      requestPayload: query,
    };
  }
}
