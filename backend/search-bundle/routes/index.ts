import { Router } from 'express';

import SearchItem from './modules/search-item-router';

const router = Router();
router.use(SearchItem.prefix, SearchItem.router);

export default router;
