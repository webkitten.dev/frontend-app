import { Router } from 'express';
import * as controller from '../../controllers/search-item-controller';

const router = Router();
router.get('/find', controller.find);

export default {
  router,
  prefix: '/searchItem',
};
