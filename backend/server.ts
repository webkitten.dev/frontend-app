import config from 'config';
import express from 'express';
import cookieParser from 'cookie-parser';
import expressSession from 'express-session';
import connectRedis from 'connect-redis';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import cors from 'cors';

import {
  LoggerContainer,
  LoggerCategories,
} from './common-bundle/logger';
const logger = LoggerContainer.get(LoggerCategories.APP);

import routing from './routing';
import * as utils from './common-bundle/utils';

const domains: { [key: string]: string } = config.get('domains');

export default (withBackend = true): express.Application => {
  const app = express();

  // response parsers
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json({ type: 'application/json' }));
  app.use(cookieParser());

  // security
  app.use(helmet({
    frameguard: {
      action: 'allow-from',
      domain: 'http://webvisor.com/',
    },
    hsts: false,
  }));
  app.use('/frontapp', (req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.removeHeader('X-Frame-Options');
    next();
  });

  // CORS
  const whitelist = Object.keys(domains).map(key => domains[key]);
  logger.info('CORS whitelist: %j', whitelist);
  app.use(cors({
    origin: (origin: string | null | undefined, callback: (error: Error | null, success?: boolean) => void) => {
      if (origin !== null && origin !== undefined) {
        const originHost = origin.split('://')[1];
        if (whitelist.indexOf(originHost) !== -1) {
          callback(null, true);
        } else {
          callback(new Error('Not allowed by CORS'));
        }
      } else {
        callback(null, true);
      }
    },
    methods: 'GET,POST',
    credentials: true,
    preflightContinue: true,
  }));

  app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug(req.url);
    next();
  });

  // sessions
  if (!process.env.SESSION_SECRET) {
    logger.error('session secret cannot be empty');
    process.exit(1);
  }
  const redisParams = new URL(process.env.REDIS_SESSIONS_URI as string);
  const redisStore = connectRedis(expressSession);
  app.use(expressSession({
    store: new redisStore({
      port: Number(redisParams.port),
      host: redisParams.hostname,
      db: Number(redisParams.pathname.replace('/', '')),
    }),
    secret: process.env.SESSION_SECRET as string,
    proxy: true,
    cookie: {
      secure: true,
      httpOnly: true,
      domain: `.${domains.base}`,
    },
    resave: false,
    saveUninitialized: true,
  }));

  if (withBackend) {
    // backend routing
    app.use(config.get('api.baseUrl'), routing);
  }

  // main error handler
  app.use((error: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (error.status) {
      error.errorCode = error.status;
    }
    utils.sendError(res)(error);
  });

  return app;
};
