import { Response, NextFunction } from 'express';
import config from 'config';

import * as utils from '../common-bundle/utils';
import { Request } from '../common-bundle/jwt';

export default (req: Request, res: Response, next: NextFunction) => {
  const ids = config.get<number[]>('allStatsAllowedUserIds');
  if (!req.user || !ids.includes(req.user.id)) {
    utils.throwError(403, 'forbidden')(new Error('Нет доступа'));
  }
  return next();
};
