import { Response } from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';
import allMiddleware from '../all-middleware';

import { ContractPartsStatisticsFilters } from '../../api-bundle/graphql/types';
import { ContractPartRepository } from '../repositories/contract-part-repository';

const repository = new ContractPartRepository();

export const stat = [
  jwt.middlewareAuth,
  allMiddleware,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const offset = Number(req.body.offset) || 0;

    const data = await repository
      .list({
        userIds: req.body.userIds && req.body.userIds.length ? req.body.userIds.map(String) : null,
        citiesIds: req.body.cityIds && req.body.cityIds.length ? req.body.cityIds.map(String) : null,
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
        shopIds: req.body.shopIds || null,
        manufacturerIds: req.body.brandIds || null,
        modelIds: req.body.modelIds || null,
        itemIds: req.body.titleIds || null,
        chassis: req.body.chassisIds || null,
        engines: req.body.engineIds || null,
        years: req.body.yearIds || null,
        group: ContractPartRepository.getGroup(req.body.groupBy, req.body.periodGroup),
        sort: ContractPartRepository.getSort(req.body.order, req.body.dir),
        skip: offset,
        first: Number(req.body.limit) || 0,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)({
      items: data.items,
      itemsMeta: {
        offset,
        count: data.items.length,
        total: data.count,
      },
    });
  }),
];

export const brands = [
  jwt.middlewareAuth,
  allMiddleware,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .reference({
        field: ContractPartsStatisticsFilters.CarManufacturer,
        userIds: req.body.userIds && req.body.userIds.length ? req.body.userIds.map(String) : null,
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const models = [
  jwt.middlewareAuth,
  allMiddleware,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .reference({
        field: ContractPartsStatisticsFilters.CarModel,
        userIds: req.body.userIds && req.body.userIds.length ? req.body.userIds.map(String) : null,
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const modelYears = [
  jwt.middlewareAuth,
  allMiddleware,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .reference({
        field: ContractPartsStatisticsFilters.CarYear,
        userIds: req.body.userIds && req.body.userIds.length ? req.body.userIds.map(String) : null,
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const users = [
  jwt.middlewareAuth,
  allMiddleware,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .reference({
        field: ContractPartsStatisticsFilters.User,
        userIds: req.body.userIds && req.body.userIds.length ? req.body.userIds.map(String) : null,
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const filials = [
  jwt.middlewareAuth,
  allMiddleware,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .reference({
        field: ContractPartsStatisticsFilters.Shop,
        userIds: req.body.userIds && req.body.userIds.length ? req.body.userIds.map(String) : null,
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const cities = [
  jwt.middlewareAuth,
  allMiddleware,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .reference({
        field: ContractPartsStatisticsFilters.City,
        userIds: req.body.userIds && req.body.userIds.length ? req.body.userIds.map(String) : null,
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const chassis = [
  jwt.middlewareAuth,
  allMiddleware,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .reference({
        field: ContractPartsStatisticsFilters.CarChassis,
        userIds: req.body.userIds && req.body.userIds.length ? req.body.userIds.map(String) : null,
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const engines = [
  jwt.middlewareAuth,
  allMiddleware,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .reference({
        field: ContractPartsStatisticsFilters.CarEngine,
        userIds: req.body.userIds && req.body.userIds.length ? req.body.userIds.map(String) : null,
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const titles = [
  jwt.middlewareAuth,
  allMiddleware,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .reference({
        field: ContractPartsStatisticsFilters.Title,
        userIds: req.body.userIds && req.body.userIds.length ? req.body.userIds.map(String) : null,
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];
