import { Response } from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { ContractPartsStatisticsFilters } from '../../api-bundle/graphql/types';
import { ContractPartRepository } from '../repositories/contract-part-repository';
import { UserRepository } from '../../main-bundle/repositories/user-repository';

const statRepository = new ContractPartRepository();
const userRepository = new UserRepository();

export const stat = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const offset = Number(req.body.offset) || 0;

    const data = await statRepository.list({
      userIds: [String(req.user!.id)],
      fromDate: req.body.dateFrom || null,
      toDate: req.body.dateTo || null,
      shopIds: req.body.shopIds || null,
      manufacturerIds: req.body.brandIds || null,
      modelIds: req.body.modelIds || null,
      itemIds: req.body.titleIds || null,
      chassis: req.body.chassisIds || null,
      engines: req.body.engineIds || null,
      years: req.body.yearIds || null,
      group: ContractPartRepository.getGroup(req.body.groupBy, req.body.periodGroup),
      sort: ContractPartRepository.getSort(req.body.order, req.body.dir),
      skip: offset,
      first: Number(req.body.limit) || 0,
    })
    .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)({
      items: data.items,
      itemsMeta: {
        offset,
        count: data.items.length,
        total: data.count,
      },
    });
  }),
];

export const brands = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await statRepository
      .reference({
        field: ContractPartsStatisticsFilters.CarManufacturer,
        userIds: [String(req.user!.id)],
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const models = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await statRepository
      .reference({
        field: ContractPartsStatisticsFilters.CarModel,
        userIds: [String(req.user!.id)],
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const modelYears = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await statRepository
      .reference({
        field: ContractPartsStatisticsFilters.CarYear,
        userIds: [String(req.user!.id)],
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const shops = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await statRepository
      .reference({
        field: ContractPartsStatisticsFilters.Shop,
        userIds: [String(req.user!.id)],
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const chassis = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await statRepository
      .reference({
        field: ContractPartsStatisticsFilters.CarChassis,
        userIds: [String(req.user!.id)],
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const engines = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await statRepository
      .reference({
        field: ContractPartsStatisticsFilters.CarEngine,
        userIds: [String(req.user!.id)],
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const titles = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await statRepository
      .reference({
        field: ContractPartsStatisticsFilters.Title,
        userIds: [String(req.user!.id)],
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const filials = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const shops = await userRepository
      .getShops({ id: Number(req.user!.id) })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(shops.map(shop => ({ id: shop.id, name: shop.shopName })));
  }),
];
