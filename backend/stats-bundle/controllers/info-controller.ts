import { Response } from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { NewPartRepository } from '../repositories/new-part-repository';
import { ContractPartRepository } from '../repositories/contract-part-repository';

const newPartRepository = new NewPartRepository();
const contractPartsStatRepository = new ContractPartRepository();

export const hasStat = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const [
      resultPpn,
      resultKz,
    ] = await Promise.all([
      newPartRepository.list({
        userIds: [String(req.user!.id)],
        group: NewPartRepository.getGroup('', ''),
      }).then((data) => {
        if (!data || !data.items || !data.items.length) {
          return false;
        }
        return [
          data.items[0].viewShopCard,
          data.items[0].sendOrderCart,
          data.items[0].sendOrderQuick,
          data.items[0].viewPhone,
          data.items[0].viewEmail,
          data.items[0].viewMap,
          data.items[0].viewWebsite,
          data.items[0].clickOrder,
          data.items[0].clickAddToCart,
          data.items[0].viewMessenger,
          data.items[0].clickExternalURLName,
          data.items[0].clickExternalURLOrder,
          data.items[0].clickChat,
        ].some(Boolean);
      }),
      contractPartsStatRepository.list({
        userIds: [String(req.user!.id)],
        group: ContractPartRepository.getGroup('', ''),
      }).then((data) => {
        if (!data || !data.items || !data.items.length) {
          return false;
        }
        return [
          data.items[0].viewAd,
          data.items[0].viewShopCard,
          data.items[0].viewPhone,
          data.items[0].viewEmail,
          data.items[0].viewMap,
          data.items[0].viewMessenger,
          data.items[0].clickChat,
        ].some(Boolean);
      }),
    ]).catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)({
      ppn: resultPpn,
      kz: resultKz,
    });
  }),
];
