import { Response } from 'express';

import * as utils from '../../common-bundle/utils';
import * as jwt from '../../common-bundle/jwt';

import { SearchByNumberStatisticsFilters } from '../../api-bundle/graphql/types';
import { NewPartRepository } from '../repositories/new-part-repository';

const repository = new NewPartRepository();

export const stat = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const offset = Number(req.body.offset) || 0;

    const data = await repository
      .list({
        userIds: [String(req.user!.id)],
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
        shopIds: req.body.shopIds || null,
        brandIds: req.body.brandIds || null,
        partIds: req.body.partIds || null,
        group: NewPartRepository.getGroup(req.body.groupBy, req.body.periodGroup),
        sort: NewPartRepository.getSort(req.body.order, req.body.dir),
        skip: offset,
        first: Number(req.body.limit) || 0,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)({
      items: data.items.map(item => ({
        ...item,
        sendOrder: item.sendOrderCart + item.sendOrderQuick,
        clickOrder: item.clickOrderQuick + item.clickAddToCart,
      })),
      itemsMeta: {
        offset,
        count: data.items.length,
        total: data.count,
      },
    });
  }),
];

export const brands = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .reference({
        field: SearchByNumberStatisticsFilters.PartManufacturer,
        userIds: [String(req.user!.id)],
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const parts = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .reference({
        field: SearchByNumberStatisticsFilters.Item,
        userIds: [String(req.user!.id)],
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];

export const shops = [
  jwt.middlewareAuth,
  utils.wrapAsyncMethod(async (req: jwt.Request, res: Response) => {
    const data = await repository
      .reference({
        field: SearchByNumberStatisticsFilters.Shop,
        userIds: [String(req.user!.id)],
        fromDate: req.body.dateFrom || null,
        toDate: req.body.dateTo || null,
      })
      .catch(utils.throwError(500, 'api error'));

    utils.sendSuccess(res)(data.items);
  }),
];
