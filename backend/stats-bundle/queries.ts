export const GET_CONTRACT_PART = `query(
  $userIds: [ID!]
  $citiesIds: [ID!]
  $shopIds: [ID!]
  $manufacturerIds: [ID!]
  $modelIds: [ID!]
  $itemIds: [ID!]
  $chassis: [String!]
  $engines: [String!]
  $years: [Int!]
  $fromDate: Datetime
  $toDate: Datetime
  $skip: Int
  $first: Int
  $group: ContractPartsStatisticsGroup!
  $sort: ContractPartsStatisticsSort
) {
  data:Statistics_contractParts(
    userIds: $userIds
    citiesIds: $citiesIds
    shopIds: $shopIds
    manufacturerIds: $manufacturerIds
    modelIds: $modelIds
    itemIds: $itemIds
    chassis: $chassis
    engines: $engines
    years: $years
    fromDate: $fromDate
    toDate: $toDate
    skip: $skip
    first: $first
    group: $group
    sort: $sort
  ) {
    pageInfo {
      count
    }
    edges {
      node {
        id
        group {
          name
        }
        viewAd
        viewShopCard
        viewPhone
        viewEmail
        viewMessenger
        viewMap
        viewWebsite
        clickChat
      }
    }
  }
}`;

export const GET_CONTRACT_PART_FILTER = `query(
  $userIds: [ID!]
  $field: ContractPartsStatisticsFilters!
  $fromDate: Datetime
  $toDate: Datetime
) {
  data:Statistics_contractPartsFilters(
    userIds: $userIds
    field: $field
    fromDate: $fromDate
    toDate: $toDate
  ) {
    pageInfo {
      count
    }
    edges {
      node {
        id
        name
      }
    }
  }
}`;

export const GET_NEW_PART = `query(
  $userIds: [ID!]
  $citiesIds: [ID!]
  $shopIds: [ID!]
  $brandIds: [ID!]
  $partIds: [ID!]
  $fromDate: Datetime
  $toDate: Datetime
  $skip: Int
  $first: Int
  $group: SearchByNumberStatisticsGroup!
  $sort: SearchByNumberStatisticsSort
) {
  data:Statistics_searchByNumber(
    userIds: $userIds
    citiesIds: $citiesIds
    shopIds: $shopIds
    brandIds: $brandIds
    partIds: $partIds
    fromDate: $fromDate
    toDate: $toDate
    skip: $skip
    first: $first
    group: $group
    sort: $sort
  ) {
    pageInfo {
      count
    }
    edges {
      node {
        id
        group {
          name
        }
        viewShopCard
        sendOrderCart
        sendOrderQuick
        viewPhone
        viewEmail
        viewMessenger
        viewMap
        viewWebsite
        clickOrderQuick
        clickAddToCart
        clickExternalURLName
        clickExternalURLOrder
        clickChat
      }
    }
  }
}`;

export const GET_NEW_PART_FILTER = `query(
  $userIds: [ID!]
  $field: SearchByNumberStatisticsFilters!
  $fromDate: Datetime
  $toDate: Datetime
) {
  data:Statistics_searchByNumberFilters(
    userIds: $userIds
    field: $field
    fromDate: $fromDate
    toDate: $toDate
  ) {
    pageInfo {
      count
    }
    edges {
      node {
        id
        name
      }
    }
  }
}`;
