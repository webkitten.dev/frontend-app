import { AbstractRepository } from './abstract-repository';

import { GET_CONTRACT_PART, GET_CONTRACT_PART_FILTER } from '../queries';
import {
  QueryStatistics_ContractPartsArgs,
  QueryStatistics_ContractPartsFiltersArgs,
  ContractPartsStat,
  ContractPartsStatisticsGroup,
  ContractPartsStatisticsSort,
  Nameable,
} from '../../api-bundle/graphql/types';

import { ListResponse } from '../../api-bundle/graphql/entities/list-response';

export class ContractPartRepository extends AbstractRepository {
  public list(variables: QueryStatistics_ContractPartsArgs): Promise<ListResponse<ContractPartsStat>> {
    return this._list<ContractPartsStat>(GET_CONTRACT_PART, variables);
  }

  public reference(variables: QueryStatistics_ContractPartsFiltersArgs): Promise<ListResponse<Nameable[]>> {
    return this._list<Nameable[]>(GET_CONTRACT_PART_FILTER, variables);
  }

  public static getGroup(group: string, period: string): ContractPartsStatisticsGroup {
    if (group === 'PERIOD') {
      switch (period) {
        case 'DAY':   return ContractPartsStatisticsGroup.DateDay ;
        case 'WEEK':  return ContractPartsStatisticsGroup.DateWeek ;
        case 'MONTH': return ContractPartsStatisticsGroup.DateMonth ;
        case 'YEAR':  return ContractPartsStatisticsGroup.DateYear ;
        default:
      }
    } else {
      switch (group) {
        case 'SHOP':    return ContractPartsStatisticsGroup.Shop;
        case 'BRAND':   return ContractPartsStatisticsGroup.CarManufacturer;
        case 'MODEL':   return ContractPartsStatisticsGroup.CarModel;
        case 'YEAR':    return ContractPartsStatisticsGroup.Year;
        case 'CHASSIS': return ContractPartsStatisticsGroup.Chassis;
        case 'ENGINE':  return ContractPartsStatisticsGroup.Engine;
        case 'TITLE':   return ContractPartsStatisticsGroup.Item;
        case 'USER':    return ContractPartsStatisticsGroup.User;
        case 'CITY':    return ContractPartsStatisticsGroup.City;
        default:
      }
    }
    return ContractPartsStatisticsGroup.None;
  }

  public static getSort(order: string, dir: string): ContractPartsStatisticsSort {
    if (dir === 'ASC') {
      switch (order) {
        case 'group':         return ContractPartsStatisticsSort.GroupAsc;
        case 'viewAd':        return ContractPartsStatisticsSort.ViewAdAsc;
        case 'viewPhone':     return ContractPartsStatisticsSort.ViewPhoneAsc;
        case 'viewEmail':     return ContractPartsStatisticsSort.ViewEmailAsc;
        case 'viewMap':       return ContractPartsStatisticsSort.ViewMapAsc;
        case 'viewMessenger': return ContractPartsStatisticsSort.ViewMessengerAsc;
        case 'viewShopCard':  return ContractPartsStatisticsSort.ViewShopCardAsc;
        case 'clickChat':     return ContractPartsStatisticsSort.ClickChatAsc;
        default:
      }
    } else {
      switch (order) {
        case 'group':         return ContractPartsStatisticsSort.GroupDesc;
        case 'viewAd':        return ContractPartsStatisticsSort.ViewAdDesc;
        case 'viewPhone':     return ContractPartsStatisticsSort.ViewPhoneDesc;
        case 'viewEmail':     return ContractPartsStatisticsSort.ViewEmailDesc;
        case 'viewMap':       return ContractPartsStatisticsSort.ViewMapDesc;
        case 'viewMessenger': return ContractPartsStatisticsSort.ViewMessengerDesc;
        case 'viewShopCard':  return ContractPartsStatisticsSort.ViewShopCardDesc;
        case 'clickChat':     return ContractPartsStatisticsSort.ClickChatDesc;
        default:
      }
    }
    return ContractPartsStatisticsSort.GroupDesc;
  }
}
