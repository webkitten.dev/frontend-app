import { AbstractRepository } from './abstract-repository';

import { GET_NEW_PART, GET_NEW_PART_FILTER } from '../queries';
import {
  QueryStatistics_SearchByNumberArgs,
  QueryStatistics_SearchByNumberFiltersArgs,
  SearchByNumberStat,
  SearchByNumberStatisticsGroup,
  SearchByNumberStatisticsSort,
  Nameable,
} from '../../api-bundle/graphql/types';

import { ListResponse } from '../../api-bundle/graphql/entities/list-response';

export class NewPartRepository extends AbstractRepository {
  public list(variables: QueryStatistics_SearchByNumberArgs): Promise<ListResponse<SearchByNumberStat>> {
    return this._list<SearchByNumberStat>(GET_NEW_PART, variables);
  }

  public reference(variables: QueryStatistics_SearchByNumberFiltersArgs): Promise<ListResponse<Nameable[]>> {
    return this._list<Nameable[]>(GET_NEW_PART_FILTER, variables);
  }

  public static getGroup(group: string, period: string): SearchByNumberStatisticsGroup {
    if (group === 'PERIOD') {
      switch (period) {
        case 'DAY':   return SearchByNumberStatisticsGroup.DateDay ;
        case 'WEEK':  return SearchByNumberStatisticsGroup.DateWeek ;
        case 'MONTH': return SearchByNumberStatisticsGroup.DateMonth ;
        case 'YEAR':  return SearchByNumberStatisticsGroup.DateYear ;
        default:
      }
    } else {
      switch (group) {
        case 'SHOP':  return SearchByNumberStatisticsGroup.Shop;
        case 'BRAND': return SearchByNumberStatisticsGroup.Brand;
        case 'PART':  return SearchByNumberStatisticsGroup.Part;
        case 'USER':  return SearchByNumberStatisticsGroup.User;
        case 'CITY':  return SearchByNumberStatisticsGroup.City;
        default:
      }
    }
    return SearchByNumberStatisticsGroup.None;
  }

  public static getSort(order: string, dir: string): SearchByNumberStatisticsSort {
    if (dir === 'ASC') {
      switch (order) {
        case 'group':                 return SearchByNumberStatisticsSort.GroupAsc;
        case 'viewShopCard':          return SearchByNumberStatisticsSort.ViewShopCardAsc;
        case 'sendOrder':             return SearchByNumberStatisticsSort.SendOrderAsc;
        case 'viewPhone':             return SearchByNumberStatisticsSort.ViewPhoneAsc;
        case 'viewEmail':             return SearchByNumberStatisticsSort.ViewEmailAsc;
        case 'viewMap':               return SearchByNumberStatisticsSort.ViewMapAsc;
        case 'viewWebsite':           return SearchByNumberStatisticsSort.ViewWebsiteAsc;
        case 'viewMessenger':         return SearchByNumberStatisticsSort.ViewMessengerAsc;
        case 'clickOrder':            return SearchByNumberStatisticsSort.ClickOrderAsc;
        case 'clickExternalURLName':  return SearchByNumberStatisticsSort.ClickExternalUrlNameAsc;
        case 'clickExternalURLOrder': return SearchByNumberStatisticsSort.ClickExternalUrlOrderAsc;
        case 'clickChat':             return SearchByNumberStatisticsSort.ClickChatAsc;
        default:
      }
    } else {
      switch (order) {
        case 'group':                 return SearchByNumberStatisticsSort.GroupDesc;
        case 'viewShopCard':          return SearchByNumberStatisticsSort.ViewShopCardDesc;
        case 'sendOrder':             return SearchByNumberStatisticsSort.SendOrderDesc;
        case 'viewPhone':             return SearchByNumberStatisticsSort.ViewPhoneDesc;
        case 'viewEmail':             return SearchByNumberStatisticsSort.ViewEmailDesc;
        case 'viewMap':               return SearchByNumberStatisticsSort.ViewMapDesc;
        case 'viewWebsite':           return SearchByNumberStatisticsSort.ViewWebsiteDesc;
        case 'viewMessenger':         return SearchByNumberStatisticsSort.ViewMessengerDesc;
        case 'clickOrder':            return SearchByNumberStatisticsSort.ClickOrderDesc;
        case 'clickExternalURLName':  return SearchByNumberStatisticsSort.ClickExternalUrlNameDesc;
        case 'clickExternalURLOrder': return SearchByNumberStatisticsSort.ClickExternalUrlOrderDesc;
        case 'clickChat':             return SearchByNumberStatisticsSort.ClickChatDesc;
        default:
      }
    }
    return SearchByNumberStatisticsSort.GroupDesc;
  }
}
