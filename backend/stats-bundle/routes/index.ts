import { Router } from 'express';

import ContractPart from './modules/contract-part-router';
import NewPart from './modules/new-part-router';
import Info from './modules/info-router';

const router = Router();
router.use(ContractPart.prefix, ContractPart.router);
router.use(NewPart.prefix, NewPart.router);
router.use(Info.prefix, Info.router);

export default router;
