import { Router } from 'express';
import * as statController from '../../controllers/contract-part-controller';
import * as statAllController from '../../controllers/contract-part-all-controller';

const routerRef = Router();
routerRef.post('/brands', statController.brands);
routerRef.post('/models', statController.models);
routerRef.post('/shops', statController.shops);
routerRef.post('/filials', statController.filials);
routerRef.post('/modelYears', statController.modelYears);
routerRef.post('/chassis', statController.chassis);
routerRef.post('/engines', statController.engines);
routerRef.post('/titles', statController.titles);

const routerRefAll = Router();
routerRefAll.post('/brands', statAllController.brands);
routerRefAll.post('/models', statAllController.models);
routerRefAll.post('/users', statAllController.users);
routerRefAll.post('/filials', statAllController.filials);
routerRefAll.post('/cities', statAllController.cities);
routerRefAll.post('/modelYears', statAllController.modelYears);
routerRefAll.post('/chassis', statAllController.chassis);
routerRefAll.post('/engines', statAllController.engines);
routerRefAll.post('/titles', statAllController.titles);

const router = Router();
router.use('/ref', routerRef);
router.use('/ref-all', routerRefAll);
router.post('/stat', statController.stat);
router.post('/stat-all', statAllController.stat);

export default {
  router,
  prefix: '/contractPart',
};
