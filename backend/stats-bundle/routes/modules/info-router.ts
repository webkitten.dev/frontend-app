import { Router } from 'express';
import * as controller from '../../controllers/info-controller';

const router = Router();
router.get('/hasStat', controller.hasStat);

export default {
  router,
  prefix: '/info',
};
