import { Router } from 'express';
import * as statController from '../../controllers/new-part-controller';
import * as statAllController from '../../controllers/new-part-all-controller';

const routerRef = Router();
routerRef.post('/brands', statController.brands);
routerRef.post('/parts', statController.parts);
routerRef.post('/shops', statController.shops);

const routerRefAll = Router();
routerRefAll.post('/brands', statAllController.brands);
routerRefAll.post('/parts', statAllController.parts);
routerRefAll.post('/shops', statAllController.shops);
routerRefAll.post('/cities', statAllController.cities);
routerRefAll.post('/users', statAllController.users);

const router = Router();
router.use('/ref', routerRef);
router.use('/ref-all', routerRefAll);
router.post('/stat', statController.stat);
router.post('/stat-all', statAllController.stat);

export default {
  router,
  prefix: '/newPart',
};
