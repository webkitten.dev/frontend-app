/* eslint-disable */
// TODO: не работает после редизайна - нужна адаптация под новые компоненты
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import factory from '~/assets/js/helpers/factory-vue';
import MOCK from '~/assets/js/mocks/index';
import FastOrder from '~/components/redesign/app-modals/app-modal/fast-order';

let fastOrder;
let $post;
const mockAxios = new MockAdapter(axios);

const order = {
  customerCityId: MOCK.getLocation().id,
  customerComment: 'Тестовый комментарий',
  customerEmail: 'ya@test.com',
  customerName: 'Алекс',
  customerPhone: '79139097191',
  itemId: 1026055062,
  itemIsWholesale: false,
  itemQuantity: 1,
};

const fastOrderURL = '/_main/part/createFastOrder';

const sendBtn = '.ui.button.fluid.yellow';

const options = {
  type: 'mount',
  component: FastOrder,
  state: {
    regions: [],
    location: MOCK.getLocation(),
  },
  actions: {
    loadRegions: () => {},
  },
  propsData: {
    order: {},
    visible: false,
    user: null,
  },
  mocks: {
    $axios: axios,
  },
};

describe('Быстрый заказ', () => {
  beforeEach(() => {
    fastOrder = factory(options);
    axios.$post = jest.fn(() => new Promise((resolve, reject) => { resolve, reject }));
    mockAxios.onPost(fastOrderURL).reply(200, {
      data: [
        {
          customerEmail: '',
          customerFio: '',
          customerPhone: '',
          discountCheck: 0,
          id: '',
          quantity: 0,
          serialNumber: 0,
        },
      ],
      type: 'success',
    });

    $post = jest.spyOn(axios, '$post');
  });

  afterEach(() => {
    fastOrder.destroy();
  });

  it('является экземпляром Vue', () => {
    expect(fastOrder.isVueInstance()).toBeTruthy();
  });

  it('Проверка входного параметра перед открытием модального окна', () => {
    expect(fastOrder.props()).toEqual({
      user: null,
      visible: false,
      order: {},
    });
  });

  it('Проверка входного параметра visible после открытием модального окна', () => {
    fastOrder.setProps({
      order: MOCK.getFastOrder(),
      visible: true,
      user: null,
    });

    expect(fastOrder.props()).toEqual({
      user: null,
      visible: true,
      order: MOCK.getFastOrder(),
    });
  });

  describe('Неавторизированный пользователь', () => {
    describe('Валидация', () => {
      it('Город взят из vuex location', () => {
        expect(fastOrder.vm.user).toBeFalsy();
        expect(fastOrder.vm.city.id).toBe(MOCK.getLocation().id);
        expect(fastOrder.vm.validation.hasError('city')).toBeFalsy();
      });

      it('Без указания необходимых полей', () => {
        fastOrder.setData({
          city: null,
        });

        const sendOrder = fastOrder.find(sendBtn);

        sendOrder.trigger('click');

        expect(fastOrder.vm.validation.hasError('phone')).toBeTruthy();
        expect(fastOrder.vm.validation.hasError('email')).toBeTruthy();
        expect(fastOrder.vm.validation.hasError('city')).toBeTruthy();

        expect(fastOrder.vm.validation.firstError('city')).toBe('Поле обязательно к заполнению.');
        expect(fastOrder.vm.validation.firstError('email')).toBe('Укажите хотя бы один способ связи с вами.');
        expect(fastOrder.vm.validation.firstError('phone')).toBe('Укажите хотя бы один способ связи с вами.');
      });

      it('Невалидный email', () => {
        fastOrder.find('[placeholder="E-mail"]').setValue('invalid email');

        expect(fastOrder.vm.validation.hasError('email')).toBeTruthy();
        expect(fastOrder.vm.validation.firstError('email')).toBe('Введите корректный E-mail.');
      });

      it('Валидный email', () => {
        fastOrder.find('[placeholder="E-mail"]').setValue('ya@test.com');

        expect(fastOrder.vm.validation.hasError('phone')).toBeFalsy();
        expect(fastOrder.vm.validation.hasError('email')).toBeFalsy();
      });

      it('Невалидный номер телефона', () => {
        fastOrder.find('[placeholder="Номер телефона"]').setValue('invalid phone number');

        expect(fastOrder.vm.validation.hasError('phone')).toBeTruthy();
        expect(fastOrder.vm.validation.firstError('phone')).toBe('Введите корректный номер телефона');
      });

      it('Валидный номер телефона', () => {
        fastOrder.find('[placeholder="Номер телефона"]').setValue('913909');

        expect(fastOrder.vm.validation.hasError('phone')).toBeFalsy();
        expect(fastOrder.vm.validation.hasError('email')).toBeFalsy();
      });
    });

    describe('Отправка заказа', () => {
      beforeEach(() => {
        fastOrder.setProps({
          order: MOCK.getFastOrder(),
          visible: true,
          user: null,
        });
      });

      it('С указанием всех полей', async () => {
        fastOrder.find('[placeholder="Ваше имя"]').setValue(order.customerName);
        fastOrder.find('[placeholder="Номер телефона"]').setValue(order.customerPhone);
        fastOrder.find('[placeholder="E-mail"]').setValue(order.customerEmail);
        fastOrder.find('[placeholder="Комментарий для продавца"]').setValue(order.customerComment);

        await fastOrder.vm.send();

        expect($post).toHaveBeenCalledWith(fastOrderURL, order);
      });

      it('С указанием только номера телефона', async () => {
        expect(fastOrder.isVueInstance()).toBeTruthy();

        fastOrder.find('[placeholder="Номер телефона"]').setValue(order.customerPhone);

        await fastOrder.vm.send();

        expect($post).toHaveBeenCalledWith(fastOrderURL, {
          customerCityId: order.customerCityId,
          customerComment: '',
          customerEmail: '',
          customerName: 'Не указано',
          customerPhone: order.customerPhone,
          itemId: order.itemId,
          itemIsWholesale: order.itemIsWholesale,
          itemQuantity: order.itemQuantity,
        });
      });

      // TODO: Вынести тест ошибок в отдельный describe.
      // it('Error 500', async () => {
      //   // TODO: Нужен class для ошибок чтобы каждый раз не писать руками.
      //   mockAxios.onPost(fastOrderURL).reply(500, {
      //     error: {
      //       code: 500,
      //     },
      //   });
      //
      //   await sendOrder();
      //
      //   expect(await $post().catch(error => error.response.data.error.errorCode)).toBe(500);
      //   expect(await $post).toHaveBeenCalled();
      // });
    });
  });

  describe('Авторизированный пользователь', () => {
    beforeEach(() => {
      fastOrder = factory(options);

      fastOrder.setProps({
        order: MOCK.getFastOrder(),
        visible: true,
        user: MOCK.getAuthUser(),
      });
    });

    it('Город не указан', async () => {
      fastOrder.setData({
        city: null,
      });

      await fastOrder.vm.$validate();

      expect(fastOrder.vm.validation.hasError('city')).toBeTruthy();
      expect(fastOrder.vm.validation.firstError('city')).toBe('Поле обязательно к заполнению.');
    });

    it('Город взят из userProfile', () => {
      expect(fastOrder.vm.user).toBeTruthy();
      expect(fastOrder.vm.city.id).toBe(MOCK.getAuthUser().userProfile.city.id);
      expect(fastOrder.vm.validation.hasError('city')).toBeFalsy();
    });

    it('Номер телефона взят из userProfile', () => {
      expect(fastOrder.vm.user).toBeTruthy();
      expect(fastOrder.vm.phone).toBe(MOCK.getAuthUser().userProfile.phone);
    });

    it('Поле почта отсутствует', () => {
      expect(fastOrder.find('input[placeholder="E-mail"]').exists()).toBeFalsy();
    });

    describe('Валидация', () => {
      it('Невалидный номер телефона', () => {
        fastOrder.find('[placeholder="Номер телефона"]').setValue('alice');

        expect(fastOrder.vm.validation.hasError('phone')).toBeTruthy();
        expect(fastOrder.vm.validation.firstError('phone')).toBe('Введите корректный номер телефона');
      });

      it('Валидный номер телефона', () => {
        fastOrder.find('[placeholder="Номер телефона"]').setValue('913909');

        expect(fastOrder.vm.validation.hasError('phone')).toBeFalsy();
      });

      it('Город', () => {
        expect(fastOrder.vm.validation.hasError('city')).toBeFalsy();
      });
    });

    describe('Отправка заказа', () => {
      it('С указанием номера телефона пользователя', async () => {
        fastOrder.vm.comment = '';
        await fastOrder.vm.send();

        expect($post).toHaveBeenCalledWith(fastOrderURL, {
          customerCityId: Number(MOCK.getAuthUser().userProfile.city.id),
          customerComment: '',
          customerEmail: MOCK.getAuthUser().username,
          customerName: MOCK.getAuthUser().userProfile.firstName,
          customerPhone: MOCK.getAuthUser().userProfile.phone,
          itemId: order.itemId,
          itemIsWholesale: order.itemIsWholesale,
          itemQuantity: order.itemQuantity,
        });
      });
    });
  });

  it('Закрытие модального окна', () => {
    fastOrder.find('.icon.close').trigger('click');

    expect(fastOrder.vm.orderSent).toBeFalsy();
    expect(fastOrder.emitted()['fast-order-close'][0]).toEqual([false]);
  });
});
