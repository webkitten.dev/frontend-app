const path = require('path');

const babelPlugins = ['transform-class-properties'];
if (process.env.NODE_ENV !== 'development') {
  babelPlugins.push('transform-remove-console');
}

module.exports = {
  dev: process.env.NODE_ENV === 'development',
  modules: [
    ['@nuxtjs/axios', {
      prefix: '/_backend',
      browserBaseURL: '/_backend',
    }],
    '@nuxtjs/style-resources',
    'nuxt-config/module',
    ['nuxt-env', {
      keys: ['NODE_ENV'],
    }],
    ['cookie-universal-nuxt', { parseJSON: false }],
    ['vue-yandex-maps/nuxt', {
      apiKey: '9c87bd57-4f70-4214-aba9-003d28948c37',
      lang: 'ru_RU',
      coordorder: 'latlong',
      version: '2.1',
    }],
    ['~/modules/google-maps', {
      key: 'AIzaSyCyXrHqAhl6Ah2-jfNVPWM0LVo0d1hX_9s',
    }],
  ],
  plugins: [
    '~/plugins/axios',
    '~/plugins/vue-global',
    '~/plugins/vue-filters',
    '~/plugins/vue-directives',
    '~/plugins/navigation',
  ],
  router: {
    prefetchLinks: false,
    middleware: ['modals-reset', 'navigation-reset', 'refetch-user'],
  },
  buildModules: ['@nuxt/typescript-build'],
  build: {
    transpile: [
      'lodash-es',
      'vue-yandex-maps',
      /^vue2-google-maps($|\/)/,
    ],
    profile: process.env.NODE_ENV === 'development',
    devtools: process.env.NODE_ENV === 'development',
    parallel: process.env.NODE_ENV === 'development',
    cache: process.env.NODE_ENV === 'development',
    hardSource: process.env.NODE_ENV === 'development',
    optimization: {
      minimize: process.env.NODE_ENV !== 'development',
      // splitChunks: {
      //   cacheGroups: {
      //     styles: {
      //       name: 'styles',
      //       test: /\.(css|vue)$/,
      //       chunks: 'all',
      //       enforce: true,
      //     },
      //   },
      // },
    },
    postcss: {
      parser: 'postcss-scss',
      plugins: {
        'postcss-import': {},
        'postcss-url': true,
        'postcss-nested': {},
        'postcss-preset-env': { stage: 1 },
        cssnano: { preset: 'default' },
      },
      preset: {
        autoprefixer: {
          grid: true,
        },
      },
    },
    extend: (config, ctx) => {
      config.module.rules.push({
        test: /\.vue$/,
        loader: 'vue-svg-inline-loader',
      });

      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
    babel: {
      plugins: babelPlugins,
      presets({ isServer }) {
        return [
          [
            require.resolve('@nuxt/babel-preset-app'), {
              targets: isServer ? { node: '10' } : { ie: '11' },
              corejs: { version: 2 },
            },
          ],
        ];
      },
    },
    extractCSS: process.env.NODE_ENV !== 'development',
  },
  styleResources: {
    scss: [
      path.resolve(__dirname, '../assets/scss/globals/variables/*.scss'),
      path.resolve(__dirname, '../assets/scss/globals/mixins/*.scss'),
    ],
  },
  loading: {
    color: '#ffdb64',
    height: '4px',
    duration: 7000,
    continuous: true,
  },
  head: {
    title: 'Поиск автозапчастей по всей России',
    meta: [
      { hid: 'charset', charset: 'UTF-8' },
      { hid: 'cache-control', 'http-equiv': 'Cache-Control', content: 'max-age=259200, must-revalidate' },

      {
        hid: 'viewport',
        name: 'viewport',
        content: 'width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, maximum-scale=1.0',
      },
      { hid: 'HandheldFriendly', name: 'HandheldFriendly', content: 'True' },
      { hid: 'SKYPE_TOOLBAR', name: 'SKYPE_TOOLBAR', content: 'SKYPE_TOOLBAR_PARSER_COMPATIBLE' },
      { hid: 'format-detection-phone', name: 'format-detection', content: 'telephone=no' },
      { hid: 'format-detection-address', name: 'format-detection', content: 'address=no' },
      { hid: 'robots', name: 'robots', content: 'index, follow' },
    ],
    link: [
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&subset=cyrillic' },
      { rel: 'apple-touch-icon', href: '/apple-touch-icon.png' },
      { rel: 'apple-touch-icon', sizes: '76x76', href: '/apple-touch-icon-76x76.png' },
      { rel: 'apple-touch-icon', sizes: '120x120', href: '/apple-touch-icon-120x120.png' },
      { rel: 'apple-touch-icon', sizes: '152x152', href: '/apple-touch-icon-152x152.png' },
      {
        hid: 'icon',
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico?v2',
      },
    ],
    script: [
      {
        hid: 'ie-detect',
        innerHTML: 'if (location.pathname!="/old-browsers" && (window.navigator.userAgent.indexOf("MSIE") !== -1 '
        + '|| (!!window.MSInputMethodContext && !!document.documentMode))) location.href="/old-browsers";',
      },
      // {
      //   hid: 'sendpulse',
      //   src: '//cdn.sendpulse.com/js/push/53bb519d6ab92d9ffd352561c5ef1ad8_1.js',
      //   async: true,
      //   charset: 'UTF-8',
      // },
    ],
    bodyAttrs: {
      // TODO: Это для нового layout не понадобиться.
      class: 'body layout-adaptive ios-scroll-fix',
    },
    htmlAttrs: {
      lang: 'ru',
    },
    __dangerouslyDisableSanitizers: ['script'],
  },
};
