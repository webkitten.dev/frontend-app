module.exports = {
  main: {
    googleAnalytics: {
      id: 'UA-79913342-2',
    },
    googleTagManager: {
      id: 'GTM-W68DRCV',
    },
    yandexMetrika: {
      id: '45455697',
      clickmap: true,
      trackLinks: true,
      accurateTrackBounce: true,
      webvisor: true,
      trackHash: true,
    },
    vkApi: {
      id: 'VK-RTRG-257036-e7zRe',
    },
  },
  helper: {
    googleAnalytics: {
      id: 'UA-79913342-4',
    },
    yandexMetrika: {
      id: '51615149',
      clickmap: true,
      trackLinks: true,
      accurateTrackBounce: true,
      webvisor: true,
      trackHash: true,
    },
  },
};
