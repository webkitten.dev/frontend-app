const toLowerCase = { from: /(?<=^[^?]*)[A-Z]/g, to: () => upper => upper.toLowerCase(), statusCode: 301 };

module.exports = {
  main: [
    toLowerCase,
    { from: /(?<=^\/do\/part\/)c_rossijskaya-federaciya/, to: 'c_rossija', statusCode: 301 },
    { from: /(?<=^\/do\/part\/)c_kitajskaya-narodnaya-respublika/, to: 'c_kitaj', statusCode: 301 },
    { from: /(?<=^\/do\/part\/)c_respublika-belarus/, to: 'c_belorussia', statusCode: 301 },
    { from: /(?<=^\/do\/part\/)c_respublika-kazahstan/, to: 'c_kazahstan', statusCode: 301 },
    { from: /(?<=^\/do\/part\/)c_federativnaya-respublika-germaniya/, to: 'c_germaniya', statusCode: 301 },
    { from: /(?<=^\/do\/part\/)c_astana/, to: 'c_nursultan', statusCode: 301 },
    { from: /(?<=^\/do\/part\/)c_astana_kz/, to: 'c_nursultan_kz', statusCode: 301 },
  ],
  helper: [
    toLowerCase,
    { from: '^/faq$', to: '/faq/help-finding-parts', statusCode: 302 },
  ],
};
