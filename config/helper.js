const merge = require('lodash/merge');
const config = require('config');

const baseConfig = require('./_base');
const metricsConfig = require('./_metrics');
const redirectsConfig = require('./_redirects');

module.exports = merge({}, baseConfig, {
  dir: {
    pages: 'pages/helper',
  },
  buildDir: '.nuxt/helper',
  build: {
    publicPath: process.env.STATIC_PUBLIC_PATH
      ? `${process.env.STATIC_PUBLIC_PATH}helper/`
      : '/_nuxt/',
  },
  modules: baseConfig.modules.concat([
    ['~/modules/metrics/ga', metricsConfig.helper.googleAnalytics],
    ['~/modules/metrics/ym', metricsConfig.helper.yandexMetrika],
    ['@nuxtjs/redirect-module', redirectsConfig.helper],
  ]),
  head: {
    title: 'Автоконсьерж GisAuto сделает всё за вас!',
    meta: baseConfig.head.meta.concat([
      { hid: 'yandex-verification', name: 'yandex-verification', content: 'd5dda7a256b99cca' },
      { hid: 'og:title', name: 'og:title', content: 'Автоконсьерж GisAuto сделает всё за вас!' },
      { hid: 'og:type', name: 'og:type', content: 'website' },
      { hid: 'og:url', name: 'og:url', content: `https://${config.get('domains.helper')}/` },
      { hid: 'og:image', name: 'og:image', content: 'https://gisauto.ru/bundles/gisautoweb/images/gisauto_social.png' },
      { hid: 'og:site_name', name: 'og:site_name', content: 'Автоконсьерж GisAuto сделает всё за вас!' },
      {
        hid: 'og:description',
        name: 'og:description',
        content: 'Заказать услуги автоконсьержа: подбор запчастей, подсчет стоимости ремонта, запись в ближайшее '
          + 'СТО, покупка подходящего автомобиля и другие вопросы, связанные с автомобилем. Оставьте свой номер '
          + 'телефона - специалист свяжется с вами через 5 минут и поможет решить проблему.',
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Заказать услуги автоконсьержа: подбор запчастей, подсчет стоимости ремонта, запись в ближайшее'
          + 'СТО, покупка подходящего автомобиля и другие вопросы, связанные с автомобилем. Оставьте свой номер '
          + 'телефона - специалист свяжется с вами через 5 минут и поможет решить проблему.',
      },
    ]),
  },
});
