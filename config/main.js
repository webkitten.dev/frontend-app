const merge = require('lodash/merge');
const config = require('config');

const baseConfig = require('./_base');
const metricsConfig = require('./_metrics');
const redirectsConfig = require('./_redirects');

module.exports = merge({}, baseConfig, {
  dir: {
    pages: 'pages/main',
  },
  buildDir: '.nuxt/main',
  build: {
    publicPath: process.env.STATIC_PUBLIC_PATH
      ? `${process.env.STATIC_PUBLIC_PATH}main/`
      : '/_nuxt/',
  },
  modules: baseConfig.modules.concat([
    ['~/modules/metrics/vk-api', metricsConfig.main.vkApi],
    ['~/modules/metrics/gtm', metricsConfig.main.googleTagManager],
    ['~/modules/metrics/ga', metricsConfig.main.googleAnalytics],
    ['~/modules/metrics/ym', metricsConfig.main.yandexMetrika],
    ['@nuxtjs/redirect-module', redirectsConfig.main],
    '~/modules/metrics/stat',
  ]),
  router: {
    extendRoutes(routes) {
      return routes.map((route) => {
        switch (route.name) {
          case 'tyres':
            return {
              ...route,
              path: '/tyres'
                + '/:season(s_summer|s_winter|s_all-seasons)?'
                + '/:brand(b_[\\w_-]+)?'
                + '/:model(m_[\\w_-]+)?'
                + '/:width(w_[\\d\\.]+)?'
                + '/:profile(p_[\\d\\.]+)?'
                + '/:diameter(d_[\\d\\.]+)?'
                + '/:carManufacturer(cman_[\\w_-]+)?'
                + '/:carModel(cmod_[\\w_-]+)?'
                + '/:carType(ct_[^\\/]+)?'
                + '/:carYear(cy_\\d+)?',
            };

          case 'wheels':
            return {
              ...route,
              path: '/wheels'
                + '/:type(t_[\\w_-]+)?'
                + '/:brand(b_[\\w_-]+)?'
                + '/:width(w_[\\d\\.-]+)?'
                + '/:diameter(di_[\\d-]+)?'
                + '/:drill(dr_[\\w_-]+)?'
                + '/:offset(o_[\\w_-]+)?'
                + '/:centerDiameter(dia_[\\w_-]+)?'
                + '/:carManufacturer(cman_[\\w_-]+)?'
                + '/:carModel(cmod_[\\w_-]+)?'
                + '/:carType(ct_[^\\/]+)?'
                + '/:carYear(cy_\\d+)?',
            };

          case 'do-part':
            return {
              ...route,
              path: '/do/part'
                + '/:city(c_[\\w_-]+)?'
                + '/:carManufacturer(mf_[\\w_-]+)?'
                + '/:carModel(m_[\\w_-]+)?',
            };

          case 'categories':
            return {
              ...route,
              path: '/categories'
                + '/:group([\\w-]+)?'
                + '/:subgroup([\\w-]+)?'
                + '/:partName([\\w-]+)?'
                + '/:partManufacturer([\\w-]+)?',
            };
          default:
            return route;
        }
      });
    },
  },
  head: {
    title: 'Поиск автозапчастей по всей России',
    meta: baseConfig.head.meta.concat([
      { hid: 'yandex-verification', name: 'yandex-verification', content: 'd5dda7a256b99cca' },
      { hid: 'yandex-verification-new', name: 'yandex-verification', content: 'f97a49f80ed27a95' },
      { hid: 'og:title', name: 'og:title', content: 'Поиск автозапчастей по всей России' },
      { hid: 'og:type', name: 'og:type', content: 'website' },
      { hid: 'og:url', name: 'og:url', content: `https://${config.get('domains.main')}/` },
      { hid: 'og:image', name: 'og:image', content: 'https://gisauto.ru/bundles/gisautoweb/images/gisauto_social.png' },
      {
        hid: 'og:site_name',
        name: 'og:site_name',
        content: 'GisAuto.ru - огромная база автозапчастей и быстрый поиск',
      },
      {
        hid: 'og:description',
        name: 'og:description',
        content: 'Поиск запчастей по VIN коду автомобиля, каталогам, наименованию или номеру запчасти. '
          + 'Прайс-агрегатор по всей России',
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Поиск запчастей по VIN коду автомобиля, каталогам, наименованию или номеру запчасти. '
          + 'Прайс-агрегатор по всей России',
      },
    ]),
    link: baseConfig.head.link.concat([
      {
        rel: 'search',
        type: 'application/opensearchdescription+xml',
        href: 'https://gisauto.ru/opensearchdescription.xml',
        title: 'GisAuto.ru найдет запчасти в твоем городе',
      },
    ]),
    script: baseConfig.head.script.concat([
      {
        hid: 'org-schemaorg',
        type: 'application/ld+json',
        innerHTML: JSON.stringify({
          '@context': 'http://schema.org',
          '@type': 'Organization',
          name: 'GisAuto.ru',
          url: 'https://gisauto.ru/',
          contactPoint: [{
            '@type': 'ContactPoint',
            telephone: '+7-800-700-4191',
            contactType: 'customer service',
            contactOption: 'TollFree',
            areaServed: 'RU',
            availableLanguage: 'Russian',
          }],
          sameAs: [
            'https://vk.com/gisauto',
            'https://ok.ru/gisautoru',
            'https://www.instagram.com/gisauto.ru/',
          ],
          logo: 'https://gisauto.ru/logo-gisauto_ru.png',
        }),
      },
    ]),
  },
});
