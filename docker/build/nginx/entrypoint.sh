#!/bin/bash

set -e

rm /etc/nginx/conf.d/default.conf
cp -r /opt/nginx/ssl /etc/nginx/conf.d/

mustache "/opt/nginx/vars.yml" "/opt/nginx/default.conf" > "/etc/nginx/conf.d/default.conf"

while ! curl -s node:3000 > /dev/null; do
  >&2 echo "Node is unavailable - sleeping"
  sleep 1
done

>&2 echo "Node is up - executing command"

exec "$@"
