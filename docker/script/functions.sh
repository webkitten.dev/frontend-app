#!/usr/bin/env bash

DIR=$(pwd)
ROOT_DIR=$(dirname "${DIR}")
SUDO_CMD=$(test -z $(id -Gn | xargs -n1 | grep '^docker$') && echo sudo)
PROJECT_DIR="/app"
NPM_HOME="${DIR}/volumes/npm"

node_container() {
    if [[ ! -d ${NPM_HOME} ]]; then
        mkdir -p ${NPM_HOME}
    fi

    ${SUDO_CMD} docker run \
        -it \
        --rm \
        -v "${NPM_HOME}:/.npm" \
        -v "${NPM_HOME}:/home/node/.npm" \
        -v ${ROOT_DIR}:/app \
        -w /app \
        -u $(id -u) \
        -p 3000:3000 \
        --dns 172.16.1.1 \
        --network gisauto \
        --link frontendapp_nginx:gisauto.test \
        --name frontendapp_node_npm \
        gisauto-front/node \
        $@
}
