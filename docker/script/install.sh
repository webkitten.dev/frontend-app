#!/usr/bin/env bash

NET_NAME=gisauto

. $(dirname $0)/functions.sh

if [[ -z "$(docker network inspect ${NET_NAME} > /dev/null 2>&1 && echo 1)" ]]; then
    docker network create --subnet 172.18.0.0/24 ${NET_NAME}
fi

if [[ ! -f "${DIR}/backend/config/development.js" ]]; then
    cp "${DIR}/backend/config/development.js.dist" "${DIR}/backend/config/development.js"
fi

if [[ ! -f "${DIR}/docker/volumes/nginx/vars.yml" ]]; then
    cp "${DIR}/docker/volumes/nginx/vars.yml.dist" "${DIR}/docker/volumes/nginx/vars.yml"
fi

if [[ ! -f "${DIR}/docker/docker-compose.yml" ]]; then
    cp "${DIR}/docker/docker-compose.yml.dist" "${DIR}/docker/docker-compose.yml"
fi

bash -c "cd ${DIR}/docker/ && docker-compose build"

bash -c "cd ${DIR}/docker/ && ./npm install"
