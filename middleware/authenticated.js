export default function ({
  store,
  error,
  route,
  redirect,
}) {
  if (!store.state.user.auth) {
    error({ statusCode: 401, message: 'Пожалуйста, авторизуйтесь' });
    return;
  }
  if (!store.state.user.auth.isOrganization && route.path.includes('organization')) {
    redirect('/cabinet/wrench/profile');
  }
}
