export default function ({
  app: { router },
  store,
  params,
  query,
  redirect,
  from,
  route: { name: routeName },
}) {
  if (!params.city) {
    const slug = store.state.location.slug;
    params.city = slug ? `c_${slug}` : 'c_all_city';
    const status = routeName === 'do' ? 302 : 301;
    const to = { name: 'do-part', params, query };
    // @todo vue-router неадекватно ведёт себя при редиректе на клиенте без изменения маршрута
    if (process.client && from.name === 'do-part') {
      window.location.href = router.resolve(to).href;
      return true;
    }
    return redirect(status, to);
  }
  return true;
}
