import { ACTION_MANAGER } from '~/assets/js/store/type-modules/type-modules';
import { SET_HIDE } from '~/assets/js/store/infoModal/mutation-types';
import { RESET_ALL } from '~/assets/js/store/actionManager/mutation-types';

export default function ({ store }) {
  if (process.client) {
    store.commit(`${ACTION_MANAGER}/${RESET_ALL}`);
    store.commit(`infoModal/${SET_HIDE}`);
  }
}
