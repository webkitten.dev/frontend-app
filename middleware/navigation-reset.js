import { ACTION_MANAGER } from '~/assets/js/store/type-modules/type-modules';
import { SET_SIDEBAR_SECTION } from '~/assets/js/store/actionManager/mutation-types';

export default function (context) {
  if (context.app.$setNavigation) {
    context.app.$setNavigation(context);
    context.store.commit(`${ACTION_MANAGER}/${SET_SIDEBAR_SECTION}`, context.store.state.navigation);
  }
}
