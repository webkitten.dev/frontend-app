import { vendorCodeSlug } from '~/assets/js/filters';

/**
 * проверяем валидность урла
 * у производителей запчастей есть алиасы, однако если была открыта страница с таким алиасом -
 * должен произойти 301 редирект на страницу с основным наименованием производителя
 * аналогично для артикулов с неразрешенными символами (см фильтр)
 * @param app
 * @param route
 * @param redirect
 * @param error
 * @returns {Promise<boolean|error|*>}
 */
export default async function ({
  app,
  route,
  redirect,
  error,
}) {
  if (
    !(
      route.params.manufacturer
      && route.params.manufacturer.length
      && route.params.vendorCode
      && route.params.vendorCode.length
    )
  ) {
    error({ statusCode: 404 });
    return false;
  }

  if (/[а-яё]+/g.test(route.params.manufacturer.toLowerCase())) {
    return true;
  }

  const redirectParams = {};

  try {
    const { data } = await app.$axios.$get('/_main/part/getPartManufacturerBySlug', {
      params: { slug: route.params.manufacturer },
    });
    if (data && data.slug && data.slug !== route.params.manufacturer) {
      redirectParams.manufacturer = data.slug;
    }
  } catch (e) {
    console.error(e);
  }

  if (route.params.vendorCode !== vendorCodeSlug(route.params.vendorCode)) {
    redirectParams.vendorCode = vendorCodeSlug(route.params.vendorCode);
  }

  if (redirectParams.manufacturer || redirectParams.vendorCode) {
    return redirect(301, {
      name: route.name,
      params: {
        manufacturer: route.params.manufacturer,
        vendorCode: route.params.vendorCode,
        ...redirectParams,
      },
      query: route.query,
    });
  }

  return true;
}
