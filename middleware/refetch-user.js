import { REFETCH_USER } from '~/assets/js/store/user/action-types';

export default async function ({ store, from, route }) {
  if (process.client && from && !from.path.match(/^\/cabinet/) && !route.path.match(/^\/cabinet/)) {
    await store.dispatch(`user/${REFETCH_USER}`);
  }
}
