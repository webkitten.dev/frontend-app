import { REFRESH_USER } from '~/assets/js/store/user/action-types';

const deniedRoutes = [
  'users-recover-password',
];

export default async function ({ store, from }) {
  if (store.state.user.auth && (!process.client || !deniedRoutes.includes(from.name))) {
    await store.dispatch(`user/${REFRESH_USER}`);
  }
}
