const path = require('path');

module.exports = function nuxtVueGoogleMaps(options) {
  if (!options.key) {
    // return;
  }
  this.addPlugin({
    src: path.resolve(__dirname, '../plugins/google-maps.js'),
    fileName: 'google-maps.js',
    options,
    ssr: true,
  });
};
