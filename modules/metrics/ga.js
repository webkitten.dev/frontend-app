const path = require('path');

module.exports = function googleAnalytics(_options) {
  const options = {
    ...{
      id: null,
      layer: 'dataLayer',
    },
    ..._options,
  };

  if (!options.id) {
    return;
  }

  this.addPlugin({
    src: path.resolve(__dirname, '../../plugins/metrics/ga.js'),
    fileName: 'ga.js',
    ssr: false,
    options,
  });
};
