const path = require('path');

module.exports = function googleTagManager(_options) {
  const options = {
    ...{
      id: null,
      layer: 'dataLayer',
    },
    ..._options,
  };

  if (!options.id) {
    return;
  }

  this.addPlugin({
    src: path.resolve(__dirname, '../../plugins/metrics/gtm.js'),
    fileName: 'gtm.js',
    ssr: false,
    options,
  });
};
