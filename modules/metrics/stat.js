const path = require('path');

module.exports = function stat() {
  this.addPlugin({
    src: path.resolve(__dirname, '../../plugins/metrics/stat-ppn.js'),
    fileName: 'stat.js',
    ssr: false,
  });
  this.addPlugin({
    src: path.resolve(__dirname, '../../plugins/metrics/stat-do.js'),
    fileName: 'stat-do.js',
    ssr: false,
  });
};
