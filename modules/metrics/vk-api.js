const path = require('path');

module.exports = function vkApi(_options) {
  const options = {
    id: null,
    ..._options,
  };

  if (!options.id) {
    return;
  }

  this.addPlugin({
    src: path.resolve(__dirname, '../../plugins/metrics/vk-api.js'),
    fileName: 'vk-api.js',
    ssr: false,
    options,
  });
};
