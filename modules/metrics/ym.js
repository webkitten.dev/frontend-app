const path = require('path');

module.exports = function yandexMetrika(_options) {
  const options = {
    ...{
      id: null,
      clickmap: false,
      trackLinks: false,
      accurateTrackBounce: false,
      webvisor: false,
      trackHash: false,
    },
    ..._options,
  };

  if (!options.id) {
    return;
  }

  this.addPlugin({
    src: path.resolve(__dirname, '../../plugins/metrics/ym.js'),
    fileName: 'ym.js',
    ssr: false,
    options,
  });
};
