export default function({ app, nuxtState }, inject) {
  const options = <%= JSON.stringify(options) %>;

  inject('ga', (category, action) => {
    console.log(`metrics:ga ${category} ${action}`);
    if (window.gtag) {
      window.gtag('event', action, { event_category: category });
    }
  });

  if (nuxtState.env.NODE_ENV !== 'production') {
    return;
  }

  if (window.gtag) {
    return;
  }

  const script = window.document.createElement('script');
  script.type = 'text/javascript';
  script.async = true;
  script.src = `//www.googletagmanager.com/gtag/js?id=${options.id}`;
  window.document.head.appendChild(script);

  window[options.layer] = window[options.layer] || [];
  window.gtag = function () {
    window[options.layer].push(arguments);
  };

  window.gtag('js', new Date());

  app.router.afterEach(to => {
    gtag('config', options.id, { page_path: to.fullPath });
  });
}
