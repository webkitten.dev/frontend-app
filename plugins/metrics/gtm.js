export default function ({ nuxtState }, inject) {
  const options = <%= JSON.stringify(options) %>;

  inject('gtm', (payload = {}) => {
    console.log('metrics:gtm ', payload);
    window[options.layer] = window[options.layer] || [];
    window[options.layer].push(payload);
  });

  if (nuxtState.env.NODE_ENV !== 'production') {
    return;
  }

  const script = window.document.createElement('script');
  script.type = 'text/javascript';
  script.async = true;
  script.src = `//www.googletagmanager.com/gtm.js?id=${options.id}`;
  window.document.head.appendChild(script);

  window[options.layer] = window[options.layer] || [];
  window[options.layer].push({
    event: 'gtm.js',
    'gtm.start': new Date().getTime(),
  });
}
