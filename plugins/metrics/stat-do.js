export default ({ app }, inject) => {
  inject('statDo', (type, params) => {
    if (!process.client) {
      return;
    }
    const data = Object.assign({}, params === undefined ? {} : params, { type });
    app.$axios.$post('/_click/contractPart', data, { progress: false });
  });
};
