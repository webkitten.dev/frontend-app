export default ({ app }, inject) => {
  inject('statPpn', (type, params) => {
    if (!process.client) {
      return;
    }
    const data = Object.assign({}, params === undefined ? {} : params, { type });
    app.$axios.$post('/_click/newPart', data, { progress: false });
  });
};
