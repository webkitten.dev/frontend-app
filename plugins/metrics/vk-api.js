export default function ({ nuxtState }) {
  const options = <%= JSON.stringify(options) %>;

  if (nuxtState.env.NODE_ENV !== 'production') {
    return;
  }

  const script = window.document.createElement('script');
  script.type = 'text/javascript';
  script.async = true;
  script.innerHTML = `!function(){var t=document.createElement("script");t.type="text/javascript",
    t.async=!0,t.src="https://vk.com/js/api/openapi.js?156",t.onload=function(){
    VK.Retargeting.Init('${options.id}'),VK.Retargeting.Hit()},document.head.appendChild(t)}();`;
  window.document.head.appendChild(script);
}
