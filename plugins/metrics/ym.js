export default ({ app, nuxtState }, inject) => {
  const options = <%= JSON.stringify(options) %>;

  inject('ym', (name, payload = {}) => {
    console.log(`metrics:ym ${name}`, payload);
    const func = () => {
      window[`yaCounter${options.id}`].reachGoal(name, payload);
    };
    if (window[`yaCounter${options.id}`]) {
      func();
    } else if (window.yandex_metrika_callbacks && window.yandex_metrika_callbacks.length) {
      window.yandex_metrika_callbacks.push(func);
    }
  });

  if (nuxtState.env.NODE_ENV !== 'production') {
    return;
  }

  if (window[`yaCounter${options.id}`]) {
    return;
  }

  const script = window.document.createElement('script');
  script.type = 'text/javascript';
  script.async = true;
  script.src = '//mc.yandex.ru/metrika/watch.js';
  window.document.head.appendChild(script);

  let ready = false;
  app.router.onReady(() => {
    ready = true;
  });

  function create() {
    window[`yaCounter${options.id}`] = new Ya.Metrika(options);
    app.router.afterEach((to, from) => {
      if (!ready) {
        return;
      }
      window[`yaCounter${options.id}`].hit(to.fullPath, {
        referer: from.fullPath,
      });
    });
  }

  if (window.Ya && window.Ya.Metrika) {
    create();
  } else {
    window.yandex_metrika_callbacks = window.yandex_metrika_callbacks || [];
    window.yandex_metrika_callbacks.push(create);
  }
}
