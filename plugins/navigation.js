import { findKey, pick, values } from 'lodash-es';

export default (context) => {
  context.app.$setNavigation = function setNavigation({ store, route, app }, navigation) {
    const subdomains = values(pick(app.$config.domains, ['sto', 'helper']));

    if (navigation) {
      store.commit('SET_NAVIGATION', navigation);
      return;
    }

    const subdomain = findKey(
      app.$config.domains,
      domain => subdomains.includes(domain) && domain === store.state.domain,
    );

    if (subdomain) {
      store.commit('SET_NAVIGATION', subdomain);
      return;
    }

    if (/check-auto/.test(route.fullPath)) {
      store.commit('SET_NAVIGATION', 'check-auto');
    } else {
      store.commit('SET_NAVIGATION', 'parts');
    }
  };

  context.app.$setNavigation(context);
};
