import Vue from 'vue';

import ClickOutside from '~/assets/js/directives/click-outside';
import DomMove from '~/assets/js/directives/dom-move';
import EraseControl from '~/assets/js/directives/erase-control';
import ImageFallback from '~/assets/js/directives/image-fallback';
import ShowPasswordControl from '~/assets/js/directives/show-password-control';
import ScrollLock from '~/assets/js/directives/scroll-lock';

Vue.directive('erase-control', process.client ? EraseControl : {});
Vue.directive('click-outside', process.client ? ClickOutside : {});
Vue.directive('dom-move', process.client ? DomMove : {});
Vue.directive('image-fallback', process.client ? ImageFallback : {});
Vue.directive('password-control', process.client ? ShowPasswordControl : {});
Vue.directive('scroll-lock', process.client ? ScrollLock : {});
