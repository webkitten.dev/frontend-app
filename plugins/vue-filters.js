import Vue from 'vue';
import * as filters from '~/assets/js/filters';

Vue.filter('sellerPartName', filters.sellerPartName);
Vue.filter('specifyingSearchRequest', filters.specifyingSearchRequest);
Vue.filter('brandSlug', filters.brandSlug);
Vue.filter('vendorCodeSlug', filters.vendorCodeSlug);
Vue.filter('dateFormat', filters.dateFormat);
Vue.filter('round', filters.round);
Vue.filter('spacifyNumber', filters.spacifyNumber);
Vue.filter('capitalize', filters.capitalize);
Vue.filter('uppercase', filters.uppercase);
Vue.filter('lowercase', filters.lowercase);
Vue.filter('join', filters.join);
Vue.filter('transChoose', filters.transChoose);
Vue.filter('trimPhoneNumber', filters.trimPhoneNumber);
