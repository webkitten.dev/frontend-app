import Vue from 'vue';
import AsyncComputed from 'vue-async-computed';
import VTooltip from 'v-tooltip';
import VModal from 'vue-js-modal/dist/ssr.index';
import SuiModal from 'semantic-ui-vue/dist/commonjs/modules/Modal/Modal';

import FLink from '~/components/blocks/link';

const Lightbox = process.client ? require('vue-easy-lightbox') : null;

if (Lightbox !== null) {
  Vue.use(Lightbox);
}

Vue.component('sui-modal', SuiModal);
Vue.use(VModal);
Vue.use(AsyncComputed);

Vue.use(VTooltip);
VTooltip.options.disposeTimeout = 0;
VTooltip.options.defaultClass = 'kit tooltip dark';
VTooltip.options.popover.defaultBaseClass = 'ui popup';

Vue.component('flink', FLink);
