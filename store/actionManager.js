import * as MUTATIONS from '~/assets/js/store/actionManager/mutation-types';

const state = () => ({
  sidebarShow: false,
  profileSidebarShow: false,
  sidebarSection: 'parts',
  authModalShow: false,
  callbackModalShow: false,
  locationModalShow: false,
  showResetPasswordModal: false,
  beginChatModal: false,
  fastOrderModalShow: false,
  headerSubMenuShow: false,
});

const mutations = {
  [MUTATIONS.SHOW_RESET_PASSWORD_MODAL](state) {
    state.showResetPasswordModal = true;
  },
  [MUTATIONS.HIDE_RESET_PASSWORD_MODAL](state) {
    state.showResetPasswordModal = false;
  },
  [MUTATIONS.SHOW_PROFILE_SIDEBAR](state) {
    state.profileSidebarShow = true;
  },
  [MUTATIONS.HIDE_PROFILE_SIDEBAR](state) {
    state.profileSidebarShow = false;
  },
  [MUTATIONS.RESET_PROFILE_SIDEBAR](state) {
    state.profileSidebarShow = false;
  },
  [MUTATIONS.TOGGLE_PROFILE_SIDEBAR](state) {
    state.profileSidebarShow = !state.profileSidebarShow;
  },
  [MUTATIONS.TOGGLE_SIDEBAR](state) {
    state.sidebarShow = !state.sidebarShow;
  },
  [MUTATIONS.SHOW_SIDEBAR](state) {
    state.sidebarShow = true;
  },
  [MUTATIONS.HIDE_SIDEBAR](state) {
    state.sidebarShow = false;
  },
  [MUTATIONS.RESET_SIDEBAR](state) {
    state.sidebarShow = false;
  },
  [MUTATIONS.SET_SIDEBAR_SECTION](state, section) {
    state.sidebarSection = section;
  },
  [MUTATIONS.SHOW_AUTH_MODAL](state) {
    state.authModalShow = true;
  },
  [MUTATIONS.HIDE_AUTH_MODAL](state) {
    state.authModalShow = false;
  },
  [MUTATIONS.SHOW_CALLBACK_MODAL](state) {
    state.callbackModalShow = true;
  },
  [MUTATIONS.HIDE_CALLBACK_MODAL](state) {
    state.callbackModalShow = false;
  },
  [MUTATIONS.SHOW_LOCATION_MODAL](state) {
    state.locationModalShow = true;
  },
  [MUTATIONS.HIDE_LOCATION_MODAL](state) {
    state.locationModalShow = false;
  },
  [MUTATIONS.SHOW_BEGIN_CHAT_MODAL](state) {
    state.beginChatModal = true;
  },
  [MUTATIONS.HIDE_BEGIN_CHAT_MODAL](state) {
    state.beginChatModal = false;
  },
  [MUTATIONS.SHOW_FAST_ORDER_MODAL](state) {
    state.fastOrderModalShow = true;
  },
  [MUTATIONS.HIDE_FAST_ORDER_MODAL](state) {
    state.fastOrderModalShow = false;
  },
  [MUTATIONS.SHOW_HEADER_SUBMENU](state) {
    state.headerSubMenuShow = true;
  },
  [MUTATIONS.HIDE_HEADER_SUBMENU](state) {
    state.headerSubMenuShow = false;
  },
  [MUTATIONS.RESET_ALL](state) {
    Object.keys(state).forEach((key) => {
      if (state[key] === true) {
        state[key] = false;
      }
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
};
