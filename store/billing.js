import * as MUTATIONS from '~/assets/js/store/billing/mutation-types';
import * as ACTIONS from '~/assets/js/store/billing/action-types';

const state = () => ({
  checkauto: {
    promocode: null,
  },
});

const mutations = {
  [MUTATIONS.SET_CHECKAUTO_PROMOCODE](state, { code, price }) {
    state.checkauto.promocode = {
      code,
      price,
    };
  },
};

const actions = {
  async [ACTIONS.CHECK_CHECKAUTO_PROMOCODE]({ commit }, code) {
    try {
      const { type, data } = await this.$axios.$get('/_main/checkauto/promoCodeInfo', { params: { code } });
      if (type === 'success') {
        commit(MUTATIONS.SET_CHECKAUTO_PROMOCODE, {
          code,
          price: data.servicePrice,
        });
        return true;
      }
    } catch (e) {
      console.error(e);
    }
    return false;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
