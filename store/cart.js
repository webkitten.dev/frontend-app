import { has } from 'lodash-es';

import Item from '~/assets/js/model/item/item';
import * as MUTATIONS from '~/assets/js/store/cart/mutation-types';
import * as ACTIONS from '~/assets/js/store/cart/action-types';

const state = () => ({
  items: [],
  isAdded: false,
  isPreorder: false,
});

const getters = {
  counter(state) {
    return state.items.length;
  },
  selectedItems(state) {
    return state.items.length
      ? state.items.filter(item => item.checked)
      : [];
  },
  items(state) {
    return state.items.length
      ? state.items
      : [];
  },
  isPreorder(state) {
    if (has(state, 'isPreorder')) {
      return state.isPreorder;
    }
    return false;
  },
  isInCart(state) {
    return searchedItem => state.items.filter(item => item.id === searchedItem.id).length;
  },
  itemQuantity(state) {
    return (searchedItem) => {
      const item = state.items.find(fitem => fitem.id === searchedItem.id);

      return item && item.contractModal ? item.contractModal.quantity : 0;
    };
  },
};

const mutations = {
  [MUTATIONS.SET_IS_ADDED](state, value) {
    state.isAdded = value;
  },
  [MUTATIONS.SET_ITEMS](state, items) {
    state.items = items;
  },
  [MUTATIONS.SET_SELECT_ITEM_BY_ID](state, id) {
    if (state.items.length && id) {
      const item = state.items.find(fitem => fitem.id === id);

      item.checked = item ? !item.checked : item.checked;
    }
  },
  [MUTATIONS.DELETE_ITEM_BY_ID](state, id) {
    if (state.items.length && id) {
      state.items = state.items.filter(item => item.id !== id);
    }
  },
  [MUTATIONS.SET_IS_PREORDER](state, value) {
    if (has(state, 'isPreorder')) {
      state.isPreorder = value;
    }
  },
  [MUTATIONS.SET_QUANTITY_ITEM](state, payload) {
    if (payload && state.items.length) {
      const item = state.items.find(fitem => fitem.id === payload.id);

      if (item && has(item.contractModal, 'quantity')) {
        item.contractModal.quantity = payload.quantity;
      }
    }
  },
  [MUTATIONS.SET_COMMENTS](state, payload) {
    if (payload && state.items.length) {
      if (payload.forAllItems) {
        state.items.forEach((item) => { item.contractModal.customerComment = payload.comment; });
      } else {
        const item = state.items.find(fitem => fitem.id === payload.itemId);

        if (item && has(item.contractModal, 'customerComment')) {
          item.contractModal.customerComment = payload.comment;
        }
      }
    }
  },
};

const actions = {
  async [ACTIONS.ADD_ITEMS]({ state, commit }, parts) {
    commit(MUTATIONS.SET_IS_ADDED, true);

    // TODO: Раскурить этот код. Есть подозрение что можно сделать лучше.
    const add = async (part) => {
      const itemInCart = state.items.find(cartItem => cartItem.id === part.id);

      part.contractModal.quantity = itemInCart
        ? itemInCart.contractModal.quantity + 1
        : 1;

      try {
        const { data } = await this.$axios.$post('/_main/cart/add', {
          cartItem: part.contractModal,
        });

        commit(MUTATIONS.SET_ITEMS, data.map(item => new Item(item)));
      } catch (e) {
        console.error(e);
      }
    };

    await parts.forEach(async (part) => { await add(part); });

    commit(MUTATIONS.SET_IS_ADDED, false);
  },

  async [ACTIONS.GET]({ commit }, auth) {
    try {
      const { data = [] } = await this.$axios.$get('/_main/cart/get');

      commit(MUTATIONS.SET_ITEMS, data.map((item) => {
        const newItem = new Item(item);
        newItem.checked = !Item.isOwnedByUser(auth && auth.id, newItem)
          && newItem.inStock && !Item.isOutdated(newItem, this.$config.itemOutdatedTreshold);

        return newItem;
      }));
    } catch (e) {
      console.error(e);
    }
  },

  async [ACTIONS.UPDATE]({ state, commit }) {
    const selectedItems = state.items.filter(item => item.checked);

    try {
      const { data } = await this.$axios.$post('/_main/cart/update', {
        cartItemsList: state.items.map(item => item.contractModal),
      });

      commit(MUTATIONS.SET_ITEMS, data.map((item) => {
        const newItem = new Item(item);
        newItem.checked = selectedItems.some(_item => _item.id === newItem.id);

        return newItem;
      }));
    } catch (e) {
      console.error(e);
    }
  },

  async [ACTIONS.CLEAR]({ commit }) {
    try {
      const { data } = await this.$axios.$post('/_main/cart/update', {
        cartItemsList: [],
      });

      commit(MUTATIONS.SET_ITEMS, data);
    } catch (e) {
      console.error(e);
    }
  },

  [ACTIONS.SELECT_ALL_ITEMS]({ state, commit }, { value, auth }) {
    if (state.items.length) {
      commit(MUTATIONS.SET_ITEMS, state.items
        .map(item => ({
          ...item,
          checked: value && !Item.isOwnedByUser(auth && auth.id, item),
        })));
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
