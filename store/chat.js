import {
  map,
  uniq,
  union,
  clone,
  forEach,
  difference,
} from 'lodash-es';

import getDialogRequest from '~/assets/js/helpers/chat-get-dialog-request';

import * as MUTATIONS from '~/assets/js/store/chat/mutation-types';

import * as ACTIONS from '~/assets/js/store/chat/action-types';

const state = () => ({
  scriptLoaded: false,
  opened: false,
  newMessages: 0,
  dialogOnload: null,
});

const mutations = {
  [MUTATIONS.SET_LOAD](state) {
    state.scriptLoaded = true;
  },
  [MUTATIONS.SET_OPEN](state) {
    state.opened = true;
  },
  [MUTATIONS.SET_CLOSE](state) {
    state.opened = false;
  },
  [MUTATIONS.SET_NEW_MESSAGES](state, cnt) {
    state.newMessages = cnt;
  },
  [MUTATIONS.SET_OPEN_DIALOG_ONLOAD](state, dialogParams) {
    state.dialogOnload = dialogParams;
  },
};

const shopOnlineState = {
  listReady: [],
  listOnline: [],
  queue: [],
  interval: null,
};

const actions = {
  [ACTIONS.GET_SHOP_ONLINE_STATUS](ctx, { id }) {
    return new Promise((resolve) => {
      if (shopOnlineState.listOnline.indexOf(Number(id)) !== -1) {
        resolve(true);
      } else if (shopOnlineState.listReady.indexOf(Number(id)) !== -1) {
        resolve(false);
      } else {
        shopOnlineState.queue.push({ id, resolve });
        if (shopOnlineState.interval !== null) {
          clearTimeout(shopOnlineState.interval);
        }
        shopOnlineState.interval = setTimeout(() => {
          shopOnlineState.interval = null;
          const queue = clone(shopOnlineState.queue);
          shopOnlineState.queue = [];
          const shopIds = uniq(map(queue, it => it.id));
          this.$axios.$get('/_chat/status/onlineShops', {
            params: { id: shopIds },
            progress: false,
          }).then(({ data }) => {
            shopOnlineState.listReady = union(shopOnlineState.listReady, shopIds);
            const listOffline = difference(shopIds, data); // если OFFLINE то в ответе нет запрошенных ID
            shopOnlineState.listOnline = union(shopOnlineState.listOnline, data);
            // исключим тех, кто ранее был ONLINE, а теперь OFFLINE
            shopOnlineState.listOnline = difference(shopOnlineState.listOnline, listOffline);
          }).finally(() => {
            forEach(queue, (it) => {
              it.resolve(shopOnlineState.listOnline.indexOf(Number(it.id)) !== -1);
            });
          });
        }, 300);
      }
    });
  },
  [ACTIONS.SHOW_ROOM]({ state }, { window, roomId, type }) {
    if (state.opened && state.scriptLoaded) {
      window.imbaChat.openChat();
      if (type === 'create') {
        window.imbaChat.openRoomForce(roomId);
      } else {
        window.imbaChat.openRoom(roomId);
      }
    }
  },
  async [ACTIONS.OPEN]({ commit, state }, window) {
    try {
      if (!state.opened && state.scriptLoaded) {
        const { data: tokenData } = await this.$axios.$get('/_chat/comet/token', { progress: false });
        commit(MUTATIONS.SET_OPEN);

        const loadOptions = {
          user_id: tokenData.user_id,
          key: tokenData.token,
          language: undefined,
          holder: 'body',
        };

        if (state.dialogOnload) {
          const promise = getDialogRequest.call(
            this,
            state.dialogOnload.type,
            state.dialogOnload.id,
            state.dialogOnload.params,
          );
          commit(MUTATIONS.SET_OPEN_DIALOG_ONLOAD, null);

          const dialogResponse = await promise;
          if (dialogResponse && dialogResponse.data && dialogResponse.data.roomId) {
            loadOptions.openRoomWhenLoaded = dialogResponse.data.roomId;
          }
        }

        window.ImbaChat.load(loadOptions);
      }
    } catch (e) {
      console.error(e);
    }
  },
  [ACTIONS.CLOSE]({ commit, state }, window) {
    if (state.opened) {
      commit(MUTATIONS.SET_CLOSE);
      if (state.scriptLoaded) {
        window.ImbaChat.destroy();
      }
    }
  },
  [ACTIONS.LOADED]({ commit, state, rootState }, window) {
    return new Promise((resolve, reject) => {
      if (!state.scriptLoaded) {
        const script = window.document.createElement('script');
        script.src = `//${this.$config.domains.chat}/chat.js?include=jquery`;
        script.onload = () => {
          window.tabSignal.connect('imbaChat.newMessagesUpdate', (event) => {
            commit(MUTATIONS.SET_NEW_MESSAGES, Number(event.newMessagesSum));
          });
          // @todo убрать эти костыли, когда в чате будут нормальные промисы
          if (!rootState.user.auth) {
            window.ImbaChat.preload({ api_url: `//${this.$config.domains.chat}/imbachat` });
          }
          commit(MUTATIONS.SET_LOAD);
          resolve();
        };
        script.onerror = (e) => {
          reject(e);
        };
        window.document.body.appendChild(script);
      } else {
        resolve();
      }
    });
  },
  [ACTIONS.OPEN_TECHSUPPORT_ROOM]({ state }, window) {
    if (state.scriptLoaded && state.opened) {
      window.imbaChat.openChat();
      window.imbaChat.openRoomWithSupport();
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
