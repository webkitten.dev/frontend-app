import { has, isNil } from 'lodash-es';
import md5 from 'md5';

import FastOrder from '~/assets/js/model/fast-order';

import * as ROOT_MUTATIONS from '~/assets/js/store/root/mutation-types';
import * as ROOT_ACTIONS from '~/assets/js/store/root/action-types';
import * as USER_ACTIONS from '~/assets/js/store/user/action-types';
import * as ACTION_MANAGER_MUTATIONS from '~/assets/js/store/actionManager/mutation-types';
// import * as CART_ACTIONS from '~/assets/js/store/cart/action-types';
import * as TYPE_MODULES from '~/assets/js/store/type-modules/type-modules';
import storeMutationsMediator from '~/assets/js/store/plugins/store-mutations-mediator';

const state = () => ({
  domain: '',
  sessionId: '',
  isBot: false,
  location: {
    id: null,
    name: null,
    prepositionalName: null,
    level: null,
    slug: null,
    autoDetected: false,
  },
  navigation: 'parts',
  fastOrder: null,
  popularPartGroups: [],
});

const mutations = {
  [ROOT_MUTATIONS.SET_DOMAIN](state, domain) {
    state.domain = domain;
  },
  [ROOT_MUTATIONS.SET_SESSION_ID](state, sessionId) {
    state.sessionId = sessionId;
  },
  [ROOT_MUTATIONS.SET_NAVIGATION](state, navigation) {
    state.navigation = navigation;
  },
  [ROOT_MUTATIONS.SET_LOCATION](state, {
    id,
    name,
    prepositionalName,
    level,
    slug,
    autoDetected,
  }) {
    !isNil(id) && (state.location.id = String(id));
    name && (state.location.name = name);
    prepositionalName && (state.location.prepositionalName = prepositionalName);
    !isNil(level) && (state.location.level = String(level));
    slug && (state.location.slug = slug);
    state.location.autoDetected = Boolean(autoDetected);
  },
  [ROOT_MUTATIONS.SET_LOCATION_AUTO_DETECT_DISABLE](state) {
    state.location.autoDetected = false;
  },
  [ROOT_MUTATIONS.SET_FAST_ORDER](state, payload) {
    state.fastOrder = new FastOrder(payload);
  },
  [ROOT_MUTATIONS.SET_IS_BOT](state, isBot) {
    state.isBot = Boolean(isBot);
  },
  [ROOT_MUTATIONS.SET_POPULAR_PART_GROUPS](state, data) {
    state.popularPartGroups = Array.isArray(data) ? data : [];
  },
};

const actions = {
  async nuxtServerInit({ state, commit, dispatch }, { req, query }) {
    try {
      commit(ROOT_MUTATIONS.SET_DOMAIN, req.hostname);
      commit(ROOT_MUTATIONS.SET_SESSION_ID, md5(req.sessionID));
      const userAgent = req.headers['user-agent'].toString();
      const botRegExp = new RegExp(this.$config.botUserAgents.join('|'), 'gi');
      commit(ROOT_MUTATIONS.SET_IS_BOT, botRegExp.test(userAgent));

      await dispatch(`${TYPE_MODULES.USER}/${USER_ACTIONS.REFETCH_USER}`);

      if (has(query, 'authModal') && !state.user.auth) {
        commit(`${TYPE_MODULES.ACTION_MANAGER}/${ACTION_MANAGER_MUTATIONS.SHOW_AUTH_MODAL}`);
      }

      await Promise.all([
        dispatch(`${TYPE_MODULES.USER}/${USER_ACTIONS.GET_COUNTERS}`),
        dispatch(`${TYPE_MODULES.USER}/${USER_ACTIONS.LOAD_STAT}`),
        dispatch(ROOT_ACTIONS.LOAD_POPULAR_PART_GROUPS),
        // dispatch(`${TYPE_MODULES.CART}/${CART_ACTIONS.GET}`),
      ]);

      const alreadyVisited = Boolean(this.$cookies.get('alreadyVisited'));
      const autoDetectLocationDisabled = Boolean(this.$cookies.get('autoDetectLocationDisabled'));
      const locationId = Number(this.$cookies.get('currentLocation'));

      if (locationId) {
        const { data: [data] } = await this.$axios.$get('/_main/geo/get', { params: { ids: [locationId] } });
        dispatch(ROOT_ACTIONS.SET_LOCATION, data);
      }

      if (!autoDetectLocationDisabled) {
        await dispatch(ROOT_ACTIONS.AUTO_DETECT_LOCATION);
      }

      if (!alreadyVisited) {
        this.$cookies.set('alreadyVisited', '1', {
          domain: `.${this.$config.domains.base}`,
          path: '/',
          maxAge: 60 * 60 * 24 * 365 * 10,
        });
      }
    } catch (e) {
      console.error(e);
    }
  },
  async [ROOT_ACTIONS.AUTO_DETECT_LOCATION]({ commit }) {
    try {
      const { data } = await this.$axios.$get('/_main/geo/resolveIp');

      if (data && data.id) {
        this.$cookies.set('currentLocation', data.id, {
          domain: `.${this.$config.domains.base}`,
          path: '/',
          maxAge: 60 * 60 * 24 * 365 * 10,
        });
      }

      commit(ROOT_MUTATIONS.SET_LOCATION, { ...data, autoDetected: true });
    } catch (e) {
      console.error(e);
    }
  },
  [ROOT_ACTIONS.DISABLE_AUTO_DETECT_LOCATION]({ commit }) {
    commit(ROOT_MUTATIONS.SET_LOCATION_AUTO_DETECT_DISABLE);

    this.$cookies.set('autoDetectLocationDisabled', '1', {
      domain: `.${this.$config.domains.base}`,
      path: '/',
      maxAge: 60 * 60 * 24 * 365 * 10,
    });
  },
  [ROOT_ACTIONS.SET_LOCATION]({ commit }, data) {
    this.$cookies.set('currentLocation', data.id, {
      domain: `.${this.$config.domains.base}`,
      path: '/',
      maxAge: 60 * 60 * 24 * 365 * 10,
    });

    commit(ROOT_MUTATIONS.SET_LOCATION, { ...data, autoDetected: false });
  },
  async [ROOT_ACTIONS.LOAD_POPULAR_PART_GROUPS]({ commit }) {
    try {
      const { data } = await this.$axios.$get('/_main/partGroup/getPopular');
      commit(ROOT_MUTATIONS.SET_POPULAR_PART_GROUPS, data || []);
    } catch (e) {
      console.error(e);
    }
  },
};

const plugins = [storeMutationsMediator];

export default {
  state,
  mutations,
  actions,
  plugins,
};
