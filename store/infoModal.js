import * as MUTATIONS from '~/assets/js/store/infoModal/mutation-types';
import * as ACTIONS from '~/assets/js/store/infoModal/action-types';

const state = () => ({
  text: '',
  button: {
    text: '',
    url: '',
    domain: '',
    buttonVisible: true,
  },
  show: false,
});

const mutations = {
  [MUTATIONS.SET_SHOW](state) {
    state.show = true;
  },
  [MUTATIONS.SET_HIDE](state) {
    state.show = false;
  },
  [MUTATIONS.SET_TOGGLE](state) {
    state.show = !state.show;
  },
  [MUTATIONS.SET_TEXT](state, text) {
    state.text = text;
  },
  [MUTATIONS.SET_BUTTON_TEXT](state, text) {
    state.button.text = text;
  },
  [MUTATIONS.SET_BUTTON_URL](state, url) {
    state.button.url = url;
  },
  [MUTATIONS.SET_BUTTON_DOMAIN](state, domain) {
    state.button.domain = domain;
  },
  [MUTATIONS.SET_BUTTON_VISIBILITY](state, visible) {
    state.button.buttonVisible = visible;
  },
};

const actions = {
  [ACTIONS.SHOW]({ commit }, {
    text,
    buttonText,
    buttonUrl,
    buttonDomain,
    buttonVisibility,
  }) {
    commit(MUTATIONS.SET_TEXT, text);
    commit(MUTATIONS.SET_BUTTON_TEXT, buttonText);
    commit(MUTATIONS.SET_BUTTON_URL, buttonUrl);
    commit(MUTATIONS.SET_BUTTON_DOMAIN, buttonDomain);
    commit(MUTATIONS.SET_BUTTON_VISIBILITY, buttonVisibility);
    commit(MUTATIONS.SET_SHOW);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
