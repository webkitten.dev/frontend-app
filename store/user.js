import {
  get,
  isNil,
} from 'lodash-es';
import * as MUTATIONS from '~/assets/js/store/user/mutation-types';
import * as ACTIONS from '~/assets/js/store/user/action-types';

const initialUserCounters = Object.freeze({
  incomingRequests: 0,
  incomingRequestsNew: 0,
  outgoingRequests: 0,
  outgoingRequestsNew: 0,
  unreadReviews: 0,
  hasMyReviews: false,
  hasAboutMeReviews: false,
});

const initialStat = Object.freeze({
  ppn: false,
  kz: false,
});

const state = () => ({
  auth: null,
  counters: initialUserCounters,
  stat: initialStat,
});

const getters = {
  userProfileDone(state) {
    return state.auth !== null
      && state.auth.userProfile !== null
      && !isNil(state.auth.userProfile.firstName)
      && !isNil(state.auth.userProfile.city)
      && !isNil(state.auth.userProfile.city.id)
      && !isNil(state.auth.userProfile.phone);
  },
  organizationsEditAvailable(state, getters) {
    return getters.userProfileDone
      && state.auth.isOrganization;
  },
  shopsEditAvailable(state, getters) {
    return getters.organizationsEditAvailable
      && get(state, 'auth.organization.legals', false)
      && Array.isArray(state.auth.organization.legals)
      && Boolean(state.auth.organization.legals.length);
  },
  hasStat(state) {
    return state.stat.ppn || state.stat.kz;
  },
};

const mutations = {
  [MUTATIONS.SET_USER](state, user) {
    state.auth = user;
  },
  [MUTATIONS.SET_USER_COUNTERS](state, counters) {
    state.counters = counters;
  },
  [MUTATIONS.SET_STAT](state, { ppn, kz }) {
    state.stat = {
      ppn,
      kz,
    };
  },
};

const actions = {
  async [ACTIONS.LOGIN]({ commit, dispatch }, { username, password }) {
    const { data } = await this.$axios.$post('/_main/auth/login', { username, password });
    commit(MUTATIONS.SET_USER, data);
    dispatch(ACTIONS.GET_COUNTERS);
    dispatch(ACTIONS.LOAD_STAT);
  },

  async [ACTIONS.LOGOUT]({ state, commit }) {
    if (state.auth) {
      try {
        await this.$axios.$post('/_main/auth/logout');
        commit(MUTATIONS.SET_USER, null);
        commit(MUTATIONS.SET_USER_COUNTERS, initialUserCounters);
        commit(MUTATIONS.SET_STAT, initialStat);
      } catch (e) {
        console.error(e);
      }
    }
  },

  async [ACTIONS.GET_COUNTERS]({ state, commit }) {
    if (state.auth) {
      try {
        let userCounters = await this.$axios.$get('/_main/user/getCounters');
        userCounters = userCounters || { data: 0 };
        commit('SET_USER_COUNTERS', userCounters.data);
      } catch (e) {
        console.error(e);
      }
    }
  },

  async [ACTIONS.LOAD_STAT]({ state, commit }) {
    if (state.auth && state.auth.isOrganization) {
      try {
        const {
          data: {
            ppn,
            kz,
          },
        } = await this.$axios.$get('/_stats/info/hasStat');
        commit(MUTATIONS.SET_STAT, { ppn, kz });
      } catch (e) {
        console.error(e);
      }
    }
  },

  async [ACTIONS.REFRESH_USER]({ state, commit }) {
    if (state.auth) {
      try {
        const { data } = await this.$axios.$get('/_main/user/refresh');
        commit(MUTATIONS.SET_USER, data || null);
        if (!data) {
          commit(MUTATIONS.SET_USER_COUNTERS, initialUserCounters);
          commit(MUTATIONS.SET_STAT, initialStat);
        }
      } catch (e) {
        console.error(e);
      }
    }
  },

  async [ACTIONS.REFETCH_USER]({ commit }) {
    try {
      const { data } = await this.$axios.$get('/_main/user/fetch');
      commit(MUTATIONS.SET_USER, data || null);
      if (!data) {
        commit(MUTATIONS.SET_USER_COUNTERS, initialUserCounters);
        commit(MUTATIONS.SET_STAT, initialStat);
      }
    } catch (e) {
      console.error(e);
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
